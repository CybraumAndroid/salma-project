package com.salmabutchershop.salma.meridian.app.customer.pro_select;

/**
 * Created by libin on 9/29/2016.
 */
public class Product_Select_Model {
    public String getOld_price() {
        return old_price;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getButcher_id() {
        return butcher_id;
    }

    public void setButcher_id(String butcher_id) {
        this.butcher_id = butcher_id;
    }

    public void setOld_price(String old_price) {
        this.old_price = old_price;
    }

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getShop_addrs() {
        return shop_addrs;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getCat_id() {
        return cat_id;
    }

    public String getTyp_id() {
        return typ_id;
    }

    public void setTyp_id(String typ_id) {
        this.typ_id = typ_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public void setShop_addrs(String shop_addrs) {
        this.shop_addrs = shop_addrs;
    }

    String pro_id,pro_name,sub_id,description,main_pro_name,old_price,type_old_price,cat_id,typ_id,
            brand,stock_in,stock_balance,stock_unit,price,
            currency,price_quantity,discount,discount_unit,percent,pro_image,added_by,user_type,delivery_info,min_order_amount,butcher_discount,delivery_charge,butcher_id,shop_addrs,shop_name,phone,email,latitude,longitude;

    public String getPro_id() {
        return pro_id;
    }

    public String getType_old_price() {
        return type_old_price;
    }

    public void setType_old_price(String type_old_price) {
        this.type_old_price = type_old_price;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getStock_in() {
        return stock_in;
    }

    public void setStock_in(String stock_in) {
        this.stock_in = stock_in;
    }

    public String getStock_balance() {
        return stock_balance;
    }

    public void setStock_balance(String stock_balance) {
        this.stock_balance = stock_balance;
    }

    public String getStock_unit() {
        return stock_unit;
    }

    public void setStock_unit(String stock_unit) {
        this.stock_unit = stock_unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice_quantity() {
        return price_quantity;
    }

    public void setPrice_quantity(String price_quantity) {
        this.price_quantity = price_quantity;
    }

    public String getDiscount_unit() {
        return discount_unit;
    }

    public void setDiscount_unit(String discount_unit) {
        this.discount_unit = discount_unit;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getPro_image() {
        return pro_image;
    }

    public void setPro_image(String pro_image) {
        this.pro_image = pro_image;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getDelivery_info() {
        return delivery_info;
    }

    public void setDelivery_info(String delivery_info) {
        this.delivery_info = delivery_info;
    }

    public String getButcher_discount() {
        return butcher_discount;
    }

    public void setButcher_discount(String butcher_discount) {
        this.butcher_discount = butcher_discount;
    }

    public String getMain_pro_name() {
        return main_pro_name;
    }

    public void setMain_pro_name(String main_pro_name) {
        this.main_pro_name = main_pro_name;
    }
}
