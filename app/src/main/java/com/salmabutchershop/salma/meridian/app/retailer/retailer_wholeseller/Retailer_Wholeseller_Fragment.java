package com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.retailer.productdet.RetailerProductFragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Retailer_Wholeseller_Fragment extends AppCompatActivity {
private RecyclerView recyclerView1;
    private ProgressDialog pdlog;
    ArrayList<WholeSellerModel> data;
    String url="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/wholeseller.php";
    Retailer_Wholeseller_DataAdapter adapter;
    ProgressBar progress;
    String cat_id,sub_id;
    String Wurl;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retailer_wholeseller_fragment);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        recyclerView1=(RecyclerView)findViewById(R.id.recyclerView1);
        RecyclerView.LayoutManager mLayoutmanager=new LinearLayoutManager(getApplicationContext());
        recyclerView1.setLayoutManager(mLayoutmanager);
        Intent intent = getIntent();
        cat_id=intent.getStringExtra("ct_id");
      sub_id = intent.getStringExtra("sub_id");
        data=new ArrayList<WholeSellerModel>();
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Retailer_Wholeseller_Fragment.this.goBack();
            }
        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new FetchDetails().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Retailer_Wholeseller_Fragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Retailer_Wholeseller_Fragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(Retailer_Wholeseller_Fragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/

        }



    }

    private void goBack() {
        super.onBackPressed();
    }

    class FetchDetails extends AsyncTask<String,String,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress_loader.setVisibility(View.VISIBLE);
            String l=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>"+l);
            if (l.equalsIgnoreCase("English")) {
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/wholeseller-products.php?cat_id="+cat_id+"&subcat_id="+sub_id;
            }
            else{
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/ar/wholeseller-products.php?cat_id="+cat_id+"&subcat_id="+sub_id;
            }
        }

        @Override
        protected String doInBackground(String... strings) {


            HttpHandler h=new HttpHandler();
            String jsonString=h.makeServiceCall(Wurl);
          //  if(jsonString!=null){
                if(jsonString != null && !jsonString.isEmpty() && !jsonString.equals("null")) {
                try{
                    JSONArray imageArray=new JSONArray(jsonString);
                    for(int i=0;i<imageArray.length();i++){
                        JSONObject jsonData=imageArray.getJSONObject(i);
                        WholeSellerModel wm=new WholeSellerModel();

                        wm.id=jsonData.getString("id");
                        wm.subcat_id=jsonData.getString("subcat_id");
                        wm.cat_id=jsonData.getString("cat_id");
                        wm.shope_name=jsonData.getString("shop_name");
                        wm.name=jsonData.getString("name");
                        wm.address=jsonData.getString("address");
                        wm.address2=jsonData.getString("address2");
                        wm.location=jsonData.getString("location");
                        wm.logo=jsonData.getString("wholeseller_logo");
                        wm.wholeseller_star=jsonData.getString("whleseller_star");

                        data.add(wm);

                        System.out.println("***************************************************");
                        System.out.println("(Retailer_Wholeseller_Fragment)  name: "+jsonData.getString("name")+"\n");
                        System.out.println("(Retailer_Wholeseller_Fragment)wholeseller_logo url : "+jsonData.getString("wholeseller_logo"));
                        System.out.println("***************************************************");

                        adapter=new Retailer_Wholeseller_DataAdapter(getApplicationContext(),data);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
else {

                    runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            new SweetAlertDialog(Retailer_Wholeseller_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(getResources().getString(R.string.Sorry))
                                    .setContentText(getResources().getString(R.string.NoProducts))
                                    .setConfirmText(getResources().getString(R.string.ok))
                                    .setCustomImage(R.drawable.logo)

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                            sDialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                    });

                }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress_loader.setVisibility(View.GONE);

            recyclerView1.setAdapter(adapter);
            recyclerView1.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            String ids=data.get(position).getId();
                            String cat_ids=data.get(position).getCat_id();
                            String sub_ids=data.get(position).getSubcat_id();
                            Intent i=new Intent(getApplicationContext(),RetailerProductFragment.class);
                            i.putExtra("id",ids);
                            i.putExtra("cat_id",cat_ids);
                            i.putExtra("value",sub_id);
                            i.putExtra("shopname",data.get(position).getShope_name());
                            startActivity(i);

                        }
                    })
            );

        }

    }
}
