package com.salmabutchershop.salma.meridian.app.customer.address;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.salmabutchershop.salma.meridian.app.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity_N extends AppCompatActivity {
    private RecyclerView rv1;
    ArrayList imageNames;
    private DataAdapter adapter;
    private ProgressDialog prgDlg;
    private Button add,view;
DatabaseHandler_N dh;


    private Button viewFav;
    private String[] values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_n);

    rv1=(RecyclerView)findViewById(R.id.recylerView1);
    viewFav=(Button)findViewById(R.id.viewFav);
    rv1.setHasFixedSize(true);

        RecyclerView.LayoutManager lm=new GridLayoutManager(getApplicationContext(),1);
        rv1.setLayoutManager(lm);
        imageNames=new ArrayList<>();




        dh=new DatabaseHandler_N();
        dh.CreateDatas(getApplicationContext());
values=new String[12];
        values[0]="anvin";
        values[1]="raj";
        values[2]="calicut";
        values[3]="quilandy";
        values[4]="residen";
        values[5]="kollam";
        values[6]="ggg";
        values[7]="67";
        values[8]="4567";
        values[9]="7878787878";
        values[10]="45674";
        values[11]="tttyyy";

add=(Button)findViewById(R.id.Add);
        view=(Button)findViewById(R.id.View);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


dh.InsertDatas(getApplicationContext(),values);

            }
        });
view.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
Intent i=new Intent(getApplicationContext(),ViewDatas_N.class);
        startActivity(i);
    }
});





      //  new FetchDetails().execute();


       /* viewFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent i=new Intent(MainActivity_N.this,Favorites.class);
                startActivity(i);


            }
        });
*/

    }

    class FetchDetails extends AsyncTask<String,String,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prgDlg=new ProgressDialog(MainActivity_N.this);
            prgDlg.setMessage("Loading Image...");
            prgDlg.setCancelable(false);
            prgDlg.show();
        }

        @Override
        protected String doInBackground(String... strings) {

           HttpHandler h=new HttpHandler();
            String jsonString=h.makeServiceCall("http://app.zayedevents.com.php56-9.dfw3-2.websitetestlink.com/services/eventgallery.php?event_id=6");
            if(jsonString!=null){
                try{
                    JSONArray imageArray=new JSONArray(jsonString);
                    for(int i=0;i<imageArray.length();i++)
                    {
                        JSONObject jsonData=imageArray.getJSONObject(i);
                        imageModel im=new imageModel();
                        im.event_id=Integer.parseInt(jsonData.getString("event_id"));
                        im.image=jsonData.getString("image");
                        im.extra_data="extra _data";
                        imageNames.add(im);
                        System.out.println("***************************************************");
                        System.out.println("EVENT ID : "+jsonData.getString("event_id")+"\n");
                        System.out.println("IMAGE NAME : "+jsonData.getString("image"));
                        System.out.println("***************************************************");
                        adapter=new DataAdapter(getApplicationContext(),imageNames);


                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prgDlg.isShowing())
                prgDlg.dismiss();

            rv1.setAdapter(adapter);

        }
    }

}
