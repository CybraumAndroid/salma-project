package com.salmabutchershop.salma.meridian.app.retailer.productdet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.retailer.enquiry.RetailerEnquiryFragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 12/6/2016.
 */

public class RetailerProductFragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;
    public static MKLoader progress_loader;
    RetailerProductAdapter adapter1;
    ProgressBar progress;
    String result, galry_image,Dat,Dat1;
    static ArrayList<Retailer_product_Model> zwm;
    Retailer_product_Model wm;
    String idS,catidS,subcat_idS;
    String ids,ids1;
    String pro_id,pro_name,pro_cat_id,pro_subcat_id,stock_unit,price,currency,price_quantity,discount,discount_unit,pro_image;
    String Wurl,minimum_order,wholeseller_id;
    String shopname;
    TextView header_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retailer_product_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        Intent intent = getIntent();
        header_text=(TextView)findViewById(R.id.header_text);
        idS=intent.getStringExtra("id");
        catidS=intent.getStringExtra("cat_id");
        subcat_idS = intent.getStringExtra("value");
        shopname=intent.getStringExtra("shopname");
        header_text.setText(shopname);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               RetailerProductFragment.this.goBack();
            }
        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(RetailerProductFragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(RetailerProductFragment.this);
         /*   com.chootdev.csnackbar.Snackbar.with(RetailerProductFragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/



        }


    }

    private void goBack() {
        super.onBackPressed();
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);
            String l=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>"+l);
            if (l.equalsIgnoreCase("English")) {
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/wholeseller-products-id.php?"+"id="+idS+"&cat_id="+catidS+"&subcat_id="+subcat_idS;
            }
            else{
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/ar/wholeseller-products-id.php?"+"id="+idS+"&cat_id="+catidS+"&subcat_id="+subcat_idS;
            }


        }

        @Override
        protected Void doInBackground(Void... params) {

            try {



                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
            String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
            System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                new SweetAlertDialog(RetailerProductFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                sDialog.dismiss();
                            }
                        })
                        .show();
         *//*       com.nispok.snackbar.Snackbar.with(RetailerProductFragment.this) // context
                        .text("No products!") // text to display
                        .show(RetailerProductFragment.this);*//*
              *//*  com.chootdev.csnackbar.Snackbar.with(RetailerProductFragment.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("No products!")
                        .duration(Duration.SHORT)
                        .show();*//*
               // Toast.makeText(getApplicationContext(), "No products", Toast.LENGTH_SHORT).show();
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                zwm = new ArrayList<Retailer_product_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        wm = new Retailer_product_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        pro_id = mJsonObject.getString("pro_id");
                        pro_name = mJsonObject.getString("pro_name");
                        pro_cat_id = mJsonObject.getString("pro_cat_id");
                        pro_subcat_id = mJsonObject.getString("pro_subcat_id");
                        stock_unit = mJsonObject.getString("stock_unit");

                        price = mJsonObject.getString("price");
                        currency = mJsonObject.getString("currency");
                        price_quantity = mJsonObject.getString("price_quantity");
                        discount = mJsonObject.getString("discount");
                        discount_unit = mJsonObject.getString("discount_unit");
                        pro_image = mJsonObject.getString("pro_image");
                       minimum_order = mJsonObject.getString("minimum_order");
                        //  String o = mJsonObject.getString("specialty_other");
                        wholeseller_id=mJsonObject.getString("wholeseller_id");
                        wm.setPro_id(pro_id);
                        wm.setPro_cat_id(pro_cat_id);
                        wm.setPro_name(pro_name);
                        wm.setPro_subcat_id(pro_subcat_id);
                        wm.setStock_unit(stock_unit);
                        wm.setPrice(price);
                        wm.setCurrency(currency);
                        wm.setPrice_quantity(price_quantity);
                        wm.setDiscount(discount);
                        wm.setDiscount_unit(discount_unit);
                        wm.setPro_image(pro_image);
                        wm.setMinimum_order(minimum_order);
                        wm.setWholeseller_id(wholeseller_id);
                        //  wm.s

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        zwm.add(wm);
                        System.out.println("<<<oo>>>>" + wm);

                        adapter1 = new RetailerProductAdapter(zwm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                new SweetAlertDialog(RetailerProductFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })
                        .show();
            /*    com.nispok.snackbar.Snackbar.with(RetailerProductFragment.this) // context
                        .text("No products!") // text to display
                        .show(RetailerProductFragment.this);
*/
            }

            recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i = new Intent(getApplicationContext(),RetailerEnquiryFragment.class);
                                i.putExtra("id",zwm.get(position).getPro_id());
                                i.putExtra("img",zwm.get(position).getPro_image());
                                i.putExtra("nam",zwm.get(position).getPro_name());
                                i.putExtra("wh_id",zwm.get(position).getWholeseller_id());
                                startActivity(i);

                            }
                        })
                );
            }


        }
    }
