package com.salmabutchershop.salma.meridian.app.butcher.wholseller.productdetail;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by libin on 9/29/2016.
 */
public class Butcher_product_adapter extends RecyclerView.Adapter<Butcher_product_adapter.ViewHolder> {


    List<Butcher_product_Model> zwm;
    Context context;



    public Butcher_product_adapter(ArrayList<Butcher_product_Model> zwm, Context context) {
        this.zwm = zwm;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3,delivey_time;
        // TextView personAge;
ProgressBar gallery_progressbar;
        ImageView personPhoto;
        public static MKLoader progress_loader;
        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

          tv=   (TextView) itemView.findViewById(R.id.min_order);
            tv1=   (TextView) itemView.findViewById(R.id.textView8);
            tv2=   (TextView) itemView.findViewById(R.id.textView11);
            tv3=   (TextView) itemView.findViewById(R.id.textView10);
            gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
            v=   (ImageView) itemView.findViewById(R.id.imageView8);
            delivey_time=(TextView)itemView.findViewById(R.id.delivey_time);
        }
    }


    @Override
    public int getItemCount() {
        return zwm.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.butcher_product_item_newlayout, viewGroup, false);
        ViewHolder pvh = new ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder personViewHolder, final int i) {


        String s = zwm.get(i).getPro_image();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
      personViewHolder.tv1.setTypeface(myFont2);
             personViewHolder.tv1.setText(zwm.get(i).getPro_name());
        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont3);
        personViewHolder.tv2.setText("Price: "+zwm.get(i).getPrice()+" "+zwm.get(i).getCurrency());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
      personViewHolder.tv3.setTypeface(myFont4);
        personViewHolder.tv3.setText("Qty: "+zwm.get(i).getPrice_quantity());


        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont5);
        personViewHolder.tv.setText("Minimum Order:"+zwm.get(i).getMinimum_order()+" Kg");
        //  String img=enm.get(i).getNews_img();
        personViewHolder.delivey_time.setText(zwm.get(i).getDelivery_time());
        System.out.println("ooo" + s);
        try{
        Picasso.with(context).load(s).noFade().into(personViewHolder.v, new Callback() {
            @Override
            public void onSuccess() {
                personViewHolder.progress_loader.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }catch (Exception e){
        e.printStackTrace();
    }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}