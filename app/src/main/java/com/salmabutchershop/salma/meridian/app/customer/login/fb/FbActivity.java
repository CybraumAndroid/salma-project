package com.salmabutchershop.salma.meridian.app.customer.login.fb;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.FrameLayout;

import com.salmabutchershop.salma.meridian.app.R;

public class FbActivity extends FragmentActivity {
    FragmentManager fragmentManager;
    FrameLayout container;
    Button log;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fb);
        container = (FrameLayout) findViewById(R.id.frame_container);
        execute();
    }
    private void execute() {
        String tag = "lib";                                                     //to move to new fragment listfragment
        Fragment fragment = new FaceBukFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment, tag);                 //replace frame container with new fragment
        Bundle args = new Bundle();                                               //new bundle created
        fragment.setArguments(args);
        transaction.addToBackStack(tag).commit();


    }
}
