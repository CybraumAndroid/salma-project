package com.salmabutchershop.salma.meridian.app.customer.newintro;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.GPSTracker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.myaccount.Changepassword;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

public class New_WelcomeActivity extends AppCompatActivity {
    private ViewPager viewPager;
    /*private MyViewPagerAdapter myViewPagerAdapter;*/
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;
    private PrefManager prefManager;
    private ProgressDialog pdlog;
    JSONArray array1;
    private New_ViewPagerAdapter va;
    private ArrayList imageURL;
    ProgressBar progress;
    GPSTracker1 gps;
    double latitude;
    double longitude;
String flag_id;

    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.new_activity_welcome);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen2();
            finish();
        }

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);

        flag_id = myPrefs.getString("flag_id", null);
        if (flag_id != null && !flag_id.isEmpty()) {
            Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
            startActivity(i1);
            finish();
        }
      /*  gps = new GPSTracker(this, New_WelcomeActivity.this);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(New_WelcomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    // \n is for new line
                    System.out.println("----------------------------------------");
                    System.out.println("inside cangetlocation");
                    System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                    System.out.println("----------------------------------------");
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                }
      *//*  else {
            int a = gps.showSettingsAlert(New_WelcomeActivity.this);
        }*//*
                else {
                    // Can't get location.
                    // GPS or network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    int a = gps.showSettingsAlert(New_WelcomeActivity.this);

                }

            } else {

                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    // \n is for new line
                    System.out.println("----------------------------------------");
                    System.out.println("inside cangetlocation");
                    System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                    System.out.println("----------------------------------------");
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                }
      *//*  else {
            int a = gps.showSettingsAlert(New_WelcomeActivity.this);
        }*//*
                else {
                    // Can't get location.
                    // GPS or network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    int a = gps.showSettingsAlert(New_WelcomeActivity.this);

                }
            }
        } else {
            com.nispok.snackbar.Snackbar.with(New_WelcomeActivity.this) // context
                    .text("No internet connection") // text to display
                    .show(New_WelcomeActivity.this);
          *//*  Snackbar.with(this,null)
                    .type(Type.SUCCESS)
                    .message("No internet connection!")
                    .duration(Duration.LONG)
                    .show();*//*


        }*/
        imageURL = new ArrayList();
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });


        // layouts of all welcome sliders
        // add few more layouts if you want


        // adding bottom dots
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            if (ContextCompat.checkSelfPermission(New_WelcomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(New_WelcomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(New_WelcomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
            else {
                gps = new GPSTracker1(New_WelcomeActivity.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                    // \n is for new line
                 //   Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }
            new FetchImage().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(New_WelcomeActivity.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(New_WelcomeActivity.this);
        /*    Snackbar.with(this,null)
                    .type(Type.SUCCESS)
                    .message("No internet connection!")
                    .duration(Duration.LONG)
                    .show();*/
          /*  Toast toast= Toast.makeText(getApplicationContext(),
                    "Oops No Internet Connection", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();*/
            btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    launchHomeScreen();
                }
            });

        }


    }


    private void addBottomDots(int currentPage) {
        if (array1 != null && array1.length() > 0&!array1.equals(null)) {
            dots = new TextView[array1.length()];


        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();

        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
        } else {
            System.out.println(">>>>");
            //dots = new TextView[array1.length()];
        }

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(New_WelcomeActivity.this, Location_Services_Fragment.class));
        finish();
    }
    private void launchHomeScreen2() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(New_WelcomeActivity.this, SelectionPage.class));
        finish();
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            if (position > 0) {
                btnSkip.setTextColor(Color.BLACK);
                btnNext.setTextColor(Color.BLACK);
                if (ContextCompat.checkSelfPermission(New_WelcomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(New_WelcomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(New_WelcomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
                else {
                    gps = new GPSTracker1(New_WelcomeActivity.this);

                    // check if GPS enabled
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();

                        editor.putString("longitude", String.valueOf(longitude));
                        editor.putString("latitude", String.valueOf(latitude));
                        editor.commit();
                        // \n is for new line
                      //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                    }

                }
            } else {
                if (ContextCompat.checkSelfPermission(New_WelcomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(New_WelcomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(New_WelcomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
                else {
                    gps = new GPSTracker1(New_WelcomeActivity.this);

                    // check if GPS enabled
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                       /* SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();

                        editor.putString("longitude", String.valueOf(longitude));
                        editor.putString("latitude", String.valueOf(latitude));
                        editor.commit();*/
                        // \n is for new line
                       // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                    }

                }
                btnSkip.setTextColor(Color.GRAY);
                btnNext.setTextColor(Color.GRAY);
            }
            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == array1.length() - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    class FetchImage extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {

            New_HttpHandler h = new New_HttpHandler();
            String jsonString = h.makeServiceCall(SERVER_CUSTOMER+"intro.php");
            if (jsonString != null && !jsonString.isEmpty() && !jsonString.equals("null")) {
                try {
                    array1 = new JSONArray(jsonString);

                    for (int i = 0; i < array1.length(); i++) {
                        JSONObject obj = array1.getJSONObject(i);
                        ZayedModel zm = new ZayedModel();
                        zm.id = Integer.parseInt(obj.getString("id"));
                        zm.url = obj.getString("image_name");

                        imageURL.add(zm);


                    }
                    va = new New_ViewPagerAdapter(getApplicationContext(), imageURL);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                System.out.println("nodots");
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress_loader.setVisibility(ProgressBar.GONE);

            addBottomDots(0);//calling dot adding function

            viewPager.setAdapter(va);
            addBottomDots(0);

            // making notification bar transparent
            changeStatusBarColor();

      /*  myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);*/
            viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

            btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    launchHomeScreen();
                }
            });

            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // checking for last page
                    // if last page home screen will be launched
                    try {


                    int current = getItem(+1);
                    if (current < array1.length()) {
                        // move to next screen
                        viewPager.setCurrentItem(current);
                    } else {
                        launchHomeScreen();
                    }
                    }catch (NullPointerException e){

                    }
                }
            });
        }
    }


        }