package com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;

import java.util.ArrayList;

public class SpinAdapter extends ArrayAdapter<Product_Services_Model> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private ArrayList<Product_Services_Model> values;

    public SpinAdapter(Context context, int textViewResourceId,
                       ArrayList<Product_Services_Model> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
       return values.size();
    }

    public Product_Services_Model getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        view.setPadding(0,0, 0,0);
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        /*final TextView label = new TextView(context);
        label.setTextColor(Color.WHITE);
        label.setBackgroundResource(android.R.color.darker_gray);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(values.get(position).getCategory_name());

        label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                label.setBackgroundResource(android.R.color.holo_red_dark);
            }
        });*/

        TextView item_name;
        final LinearLayout background;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.simplespinner, parent, false);

            item_name=(TextView)v.findViewById(R.id.txt);
        item_name.setText(values.get(position).getCategory_name());
        background=(LinearLayout)v.findViewById(R.id.spinner_item_background);

        background.setBackgroundResource(R.drawable.spinner_bg);

//background.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View view) {
//        background.setBackgroundResource(R.drawable.spinner_bg);
//
//    }
//});

        // And finally return your dynamic (or custom) view for each spinner item
        return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
            ViewGroup parent) {
        TextView item_name;
     final    LinearLayout background;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.simplespinner, parent, false);

        item_name=(TextView)v.findViewById(R.id.txt);
        background=(LinearLayout)v.findViewById(R.id.spinner_item_background);
        background.setBackgroundResource(R.drawable.spinner_bg);
//item_name.setTextColor(getContext().getResources().getColor(R.color.spy));

        item_name.setText(values.get(position).getCategory_name());
//        background.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                background.setBackgroundResource(R.drawable.spinner_bg);
//
//            }
//        });
        if(position == 0) {
            // Set the item background color
           // item_name.setBackgroundResource(R.drawable.catogory_shadow);
           // item_name.setVisibility(View.GONE);

          //  background.setBackgroundResource(R.drawable.spinner_bg);
          //  background.setVisibility(View.GONE);
            item_name.setTextColor(Color.parseColor("#d62041"));
           /* Typeface myFont2 = Typeface.createFromAsset(context.getAssets(),"Kokila.ttf");
          item_name.setTypeface(myFont2);*/
          //  item_name.setBackgroundColor(Color.parseColor("#d4d446"));
        }
        // And finally return your dynamic (or custom) view for each spinner item
        return v;
    }
}
