/*
package com.salmabutchershop.salma.meridian.app.customer.location;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.meridian.dateout.R;
import com.meridian.dateout.explore.ExploreFragment;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.CustomVolleyRequest;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

*/
/**
 * Created by libin on 1/3/2017.
 *//*


public class Googlelogin extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    //Signin button
    private SignInButton signInButton;

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    private GoogleApiClient mGoogleApiClient;
    private static int TIME = 3000;
    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;
    public static String personName, email, str_personphoto;
    //TextViews


    private TextView textViewName;
    private TextView textViewEmail;
    private NetworkImageView profilePhoto;

    //Image Loader
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_layout);

        //Initializing Views
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);

        profilePhoto = (NetworkImageView) findViewById(R.id.profileImage);

        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("816167652294-rtnak331cn8jeklehpt4o1nnotqhr5vt.apps.googleusercontent.com")
                .build();

        //Initializing signinbutton
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this */
/* FragmentActivity *//*
, this */
/* OnConnectionFailedListener *//*
)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        //Setting onclick listener to signing button
        signInButton.setOnClickListener(this);
    }


    //This function will option signing intent
    private void signIn() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }
    }


    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            //Displaying name and email
            textViewName.setText(acct.getDisplayName());
            textViewEmail.setText(acct.getEmail());

            //Initializing image loader
            imageLoader = CustomVolleyRequest.getInstance(this.getApplicationContext())
                    .getImageLoader();


            personName = acct.getGivenName();
            final Uri personPhoto = acct.getPhotoUrl();
            final String profileid = acct.getId();
            email = acct.getEmail();
            if (email != null || personPhoto != null || profileid != null || personName != null) {

                System.out.println("acnt_sign pic.... ....  " + personPhoto);
                System.out.println("acnt_sign name .... ....  " + personName);
                System.out.println("acnt_sign email .... ....  " + email);

                //  recycler_inflate(personName,email,profileid);
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("value_google_user", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("name", personName);
                editor.putString("email", email);

                if (personPhoto != null) {
                    str_personphoto = personPhoto.toString();
                    editor.putString("pic", str_personphoto);

                } else {
                    str_personphoto = null;
                    editor.putString("pic", str_personphoto);
                }

                editor.commit();
//                        new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                }
//                        },TIME);
                Intent inn= new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                startActivity(inn);
                finish();
          */
/*      Intent inn = new Intent(getApplicationContext(), FrameLayoutActivity.class);
                inn.putExtra("name", personName);
                inn.putExtra("email", email);
                inn.putExtra("profileid", profileid);
                inn.putExtra("personPhoto",personPhoto);
                inn.putExtra("go","go");
                startActivity(inn);*//*

            }

            // imageLoader.get(acct.getPhotoUrl().toString(),
            // ImageLoader.getImageListener(profilePhoto,
            // R.mipmap.ic_launcher,
            //   R.mipmap.ic_launcher));

            //Loading image
            //   profilePhoto.setImageUrl(acct.getPhotoUrl().toString(), imageLoader);

        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == signInButton) {
            //Calling signin

            signIn();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

//    public void recycler_inflate(final String name, final String profile_id, final String profilepic) {
//        //   facebookModelArrayList=new ArrayList<>();
//        String REGISTER_URL = "http://www.wildlyyours.com.php56-11.dfw3-2.websitetestlink.com/services/registration.php?";
//        NetworkCheckingClass networkCheckingClass = new NetworkCheckingClass(getApplicationContext());
//        boolean i = networkCheckingClass.ckeckinternet();
//        if (i == true) {
//            StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // Display the first 500 characters of the response string.
//                            //  tv.setText("Response is: "+ response);
//
//                            System.out.println("++++++++++++++RESPONSE+++++++++++++++   dealdetail_google==== :" + response);
//
//
//                            try {
//
//                                JSONObject jsonobject = new JSONObject(response);
//
//
//                                // FacebookModel facebookModel=new FacebookModel();
//
//
//                                String user_id = jsonobject.getString("user_id");
//                                String fullname = jsonobject.getString("fullname");
//                                String username = jsonobject.getString("username");
//                                String photo = jsonobject.getString("photo");
//                                String email = jsonobject.getString("email");
//                                String phone = jsonobject.getString("phone");
//                                String log_status = jsonobject.getString("log_status");
//                                String google_id = jsonobject.getString("google_id");
////                                    facebookModel.setEmail(email);
////                                    facebookModel.setFullname(fullname);
////                            facebookModel.setGoogle_id(google_id);
////                                    facebookModel.setLog_status(log_status);
////                                    facebookModel.setUser_id(user_id);
////                                    facebookModel.setUsername(username);
////                                    facebookModel.setPhoto(photo);
////                                    facebookModel.setPhone(phone);
//
//                                SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences("value_google_id", MODE_PRIVATE).edit();
//                                editor.putString("user_id", user_id);
//                                System.out.println("user_id..." + user_id);
//
//                                editor.commit();
//
//
//////
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
////                            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(CourseRegistrationActivity.this).create();
////                            alertDialog.setTitle("Alert");
////                            alertDialog.setMessage("Please Login to Register For this Course");
////                            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
////                                    new DialogInterface.OnClickListener() {
////                                        public void onClick(DialogInterface dialog, int which) {
////                                            dialog.dismiss();
//////                                        but_regcrc1.setBackgroundResource(R.color.butnbakcolr);
//////                                        but_regcrc1.setTextColor(getResources().getColor(R.color.White));
////
////                                        }
////                                    });
////                            alertDialog.show();
//
//
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> params = new HashMap<String, String>();
//                    http:
////meridian.net.in/demo/etsdc/response.php?fid=1&email=" + email + "&phone=" + phon + "&name=" + fulnam + "&occupation=" + occ + "&location=" + loc + "&password=" + pass
//
//                    // System.out.println("name"+name+"profil"+profile_id+"firstname"+firstName+"lastname"+lastName+profilepic);
//
//                    params.put("name", "nil");
//                    params.put("username", name);
//                    params.put("email", email);
//                    params.put("password", "nil");
//                    params.put("profile_pic",profilepic);
//                    params.put("g_id", profile_id);
//
//                    return params;
//                }
//
//            };
//
//            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
//            int socketTimeout = 30000;//30 seconds - change to what you want
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            stringRequest.setRetryPolicy(policy);
//            requestQueue.add(stringRequest);
//        } else {
//            final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(getApplicationContext()).create();
//            alertDialog.setTitle("Alert");
//            alertDialog.setMessage("Oops Your Connection Seems Off..");
//
//            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "yes", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                    dialog.dismiss();
//
//
//                }
//            });
//
//            alertDialog.show();
//
//
//        }
//
//    }
}
*/
