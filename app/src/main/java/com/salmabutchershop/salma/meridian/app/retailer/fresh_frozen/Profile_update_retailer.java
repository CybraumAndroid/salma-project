package com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Vis_FilePath;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.facebook.login.widget.ProfilePictureView.TAG;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

public class Profile_update_retailer extends Activity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final int PICK_FILE_REQUEST = 1;
    private static final int REQUEST_CODE = 1;
    public static MKLoader progress_loader, progress_loader1;

    static String filename = "null", filenamepath = "null",Reg_id,result,logo,selectedFilePath;
    String fullname,shop_name,address,phone,email,userid,location;
    ImageView  Pfile,profile_upload_icon_retailer,profile_upload_icon_retailer1;
Button lay_edit;
    Button butcher_profile;
    EditText edt_name,edt_shopname,edt_address,edt_phone,edt_email,edt_location;
    String new_name,new_shopname,new_phone,new_address,new_location,new_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retailer_profile_update);
        Pfile = (ImageView) findViewById(R.id.pfile_retailer);
        profile_upload_icon_retailer= (ImageView) findViewById(R.id.profile_upload_icon_retailer);
        edt_name= (EditText) findViewById(R.id.edt_name_update_retailer);
        edt_shopname= (EditText) findViewById(R.id.edt_shopname_update_retailer);
        edt_address= (EditText) findViewById(R.id.edt_address_update_retailer);
        edt_phone= (EditText) findViewById(R.id.edt_phone_update_retailer);
        edt_email= (EditText) findViewById(R.id.edt_email_update_retailer);
        edt_location= (EditText) findViewById(R.id.edt_location_update_retailer);
        lay_edit= (Button) findViewById(R.id.lay_edit_retailer);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        butcher_profile= (Button) findViewById(R.id.update_retailer);

        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
        progress_loader1 = (MKLoader) findViewById(R.id.progress_loader1);
        profile_upload_icon_retailer1 = (ImageView) findViewById(R.id.profile_upload_icon_retailer1);
        edt_name.setEnabled(false);
        edt_shopname.setEnabled(false);
        edt_address.setEnabled(false);
        edt_phone.setEnabled(false);
        edt_email.setEnabled(false);
        edt_location.setEnabled(false);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Profile_update_retailer.this.goBack();
            }
        });


        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        fullname = myPrefs.getString("fullnameR", null);
        shop_name= myPrefs.getString("shp_nm", null);
        address = myPrefs.getString("ads", null);
        phone= myPrefs.getString("phne", null);
         email= myPrefs.getString("eml", null);
        location= myPrefs.getString("locationR", null);
        logo= myPrefs.getString("logoR", null);
        Reg_id= myPrefs.getString("user_idL", null);

        location= myPrefs.getString("locationR", null);

        edt_name.setText(fullname);
        edt_shopname.setText(shop_name);
        edt_address.setText(address);
        edt_phone.setText(phone);
        edt_email.setText(email);
        edt_location.setText(location);


        butcher_profile.setVisibility(View.GONE);
        butcher_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             new  SendPostRequest().execute();
            }
        });



        profile_upload_icon_retailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED) {

                    requestPermissions();
                }
                showFileChooser();
            }
        });

     lay_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                butcher_profile.setVisibility(View.VISIBLE);

                edt_name.setEnabled(true);
                edt_shopname.setEnabled(true);
                edt_address.setEnabled(true);
                edt_phone.setEnabled(true);
                edt_email.setEnabled(true);
                edt_location.setEnabled(true);
            }
        });


        if (DetectConnection
                .checkInternetConnection(getApplicationContext()))
        {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData().execute();
        }
        else
            {
            com.nispok.snackbar.Snackbar.with(Profile_update_retailer.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Profile_update_retailer.this);

        }



    }



    private class SendPostRequest extends AsyncTask<String, Void, String> {

        public void onPreExecute() {


          new_name=  edt_name.getText().toString();
           new_shopname= edt_shopname.getText().toString() ;
            new_phone=edt_phone.getText().toString();
          new_email=  edt_email.getText().toString();
          new_address=  edt_address.getText().toString();
            new_location=edt_location.getText().toString();

            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        public String doInBackground(String... arg0) {
            String js=filename.replaceAll(" ","");
            try {

                URL url = new URL(SERVER+"json/retailer/update_profile.php?"); // here is your URL path


                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>butcher update profileeee.."+fullname+shop_name+address+phone+"useriddd"+Reg_id+location);

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("user_id",Reg_id);
                postDataParams.put("name",new_name);
                postDataParams.put("shop_name",new_shopname);
                postDataParams.put("email",new_email);
                postDataParams.put("phone",new_phone);
                postDataParams.put("address",new_address);
                postDataParams.put("location",new_location);



                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("user_idL", Reg_id);
                editor.putString("fullnameR",new_name);
                editor.putString("shp_nm",new_shopname);
                editor.putString("nam",new_name);
                editor.putString("ads",new_address);
                editor.putString("phne",new_phone);
                editor.putString("eml",new_email);

                editor.putString("locationR",new_location);
                editor.commit();

                // postDataParams.put("profilepic",js);


//                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref_butcher", MODE_PRIVATE);
//                SharedPreferences.Editor editor = preferences.edit();
//                editor.putString("fullname_butcher",fullname);
//                editor.putString("email_butcher",shopname_s);
//                editor.putString("phone_butcher",phone);
//                editor.putString("address_butcher",address1_s+" "+address2_s);
//                editor.commit();


                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>responseeee"+sb.toString());
                    return sb.toString();




                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {

           /* Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();*/
            if (result.contentEquals("\"success\"")) {
                if(!isFinishing()) {
                    progress_loader.setVisibility(ProgressBar.GONE);
                    new SweetAlertDialog(Profile_update_retailer.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Profile ")
                            .setContentText("Upload Sucecssfully")
                            .setConfirmText(getResources().getString(R.string.ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    Intent intent = getIntent();
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    finish();

                                    startActivity(intent);
                                    sweetAlertDialog.dismiss();

                                }
                            })
                            .show();
                }

            }
            else if (result.contentEquals("\"failed\"")) {
                if(!isFinishing())
                {
                    new SweetAlertDialog(Profile_update_retailer.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getResources().getString(R.string.Sorry))
                            .setContentText(getResources().getString(R.string.AlreadyRegistred))
                            .setConfirmText(getResources().getString(R.string.ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })

                            .show();
                }



            }
        }
    }


    private void requestPermissions() {
        ActivityCompat.requestPermissions(Profile_update_retailer.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE);
        // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }
    private void showFileChooser() {
        try {
            Intent intent = new Intent();
            //sets the select file to all types of files
            intent.setType("image/*");
            //allows to select data and return it
            intent.setAction(Intent.ACTION_GET_CONTENT);
            //starts new activity to select file and return data
            startActivityForResult(Intent.createChooser(intent,"Choose File to Vis_Upload.."),PICK_FILE_REQUEST);
        } catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                if(data == null){
                    //no data present
                    return;
                }


                Uri selectedFileUri = data.getData();
                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(),selectedFileUri);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                Log.i(TAG,"Selected File Path:" + selectedFilePath);

                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    // textViewResponse.setText(selectedFilePath);

                    CropImage.activity(selectedFileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                    // logo_img.setImageBitmap(bmp);
                }else{
                    com.nispok.snackbar.Snackbar.with(Profile_update_retailer.this) // context
                            .text("Cannot upload file to server!") // text to display
                            .show(Profile_update_retailer.this);

                }

            }


        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                System.out.println("selectedFileUri : "+resultUri);

                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), resultUri);
                filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                Pfile.setImageBitmap(bmp);
                if (selectedFilePath != null && !selectedFilePath.isEmpty())
                {
                    if (filenamepath != null) {
                        filename = filenamepath;


                    } else {
                        filename = "null";

                    }
                    uploadVideo1();


                    new SendPostRequest1().execute();
                    // tt="";
                }
                else
                {
                    new SweetAlertDialog(Profile_update_retailer.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(getResources().getString(R.string.MyMenu))
                            .setContentText(getResources().getString(R.string.Areyousurewanttosetimageasdefault))
                            .setConfirmText(getResources().getString(R.string.Yes))
                            .setCancelText(getResources().getString(R.string.No))
                            .setCustomImage(R.drawable.logo)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {


                                    // new PostRegistrationRetailer.SendPostRequest().execute();
                                    sDialog.dismiss();
                                }
                            })
                            .show();

                }
            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
            {
                Exception error = result.getError();
            }
        }
    }

    private void uploadVideo1() {
        class UploadVideo extends AsyncTask<Void, Void, String> {

            ProgressDialog uploading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress_loader.setVisibility(View.VISIBLE);
                //   uploading = ProgressDialog.show(PostRegistrationRetailer.this, "Salma", "Please wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //    progress_loader.setVisibility(View.GONE);
                //   uploading.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {
                Vis_Upload4 u = new Vis_Upload4();
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<md>>>>>>>>>>>>>>>>>>>>>>>>>" + selectedFilePath);
                String msg = u.uploadVideo(selectedFilePath);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<mdeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>>>>>>>>>>" + msg);

                return msg;
            }
        }
        UploadVideo uv = new UploadVideo();
        uv.execute();
    }
    @Override
    protected void onStart() {
        super.onStart();

        checkAndRequestPermissions();


    }
    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    private void goBack() {
        Intent i = new Intent(getApplicationContext(), Fresh_Frozen_Retailer.class);

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        // finish();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i = new Intent(getApplicationContext(),Fresh_Frozen_Retailer.class);


        startActivity(i);
        // finish();
        finish();
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    public class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        //    progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER+"json/retailer/profile_picture.php?user_id="+Reg_id
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
            }  else*/
            if (result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                //  mCountryModel1 = new ArrayList<>();
               // eem = new ArrayList<MyAccount_Profile_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                       // ee = new MyAccount_Profile_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);

                        logo = mJsonObject.getString("logo");
                      //  ee.setLogo(logo);
                      //  eem.add(ee);
                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("logoR",logo);
                        editor.commit();

                      //  System.out.println("" + ee);



                       // eem.add(ee);

                       /// System.out.println("" + ee);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    new Picasso.Builder(Profile_update_retailer.this)
                            .downloader(new OkHttpDownloader(Profile_update_retailer.this, Integer.MAX_VALUE))
                            .build()
                            .load(logo)
                            .noFade()
                            .into(Pfile, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progress_loader1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
            /*    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoContent))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .show();*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
        }
    }

    private class SendPostRequest1 extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
            progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        public String doInBackground(String... arg0) {
           /* if (filenamepath != null) {
                filename = filenamepath;


            } else {
                filename = "null";

            }*/
            // filename=filenamepath;
            String js=filename.replaceAll(" ","");

            try {

                URL url = new URL(SERVER+"json/retailer/profile_picture.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("user_id",Reg_id);
           /*     postDataParams.put("profilepic",fullname);
                postDataParams.put("phone",phone);
                postDataParams.put("email",email);
                postDataParams.put("address1",address1_s);
                postDataParams.put("address2",address2_s);
                postDataParams.put("shopname",shopname_s);
                postDataParams.put("country",username);
                postDataParams.put("city",password_s);
                postDataParams.put("token",refreshedToken);
                postDataParams.put("retailer_type",k);
                postDataParams.put("longitude",ltg);
                postDataParams.put("latitude",lts);*/
                postDataParams.put("profilepic",js);
                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {

           /*  Toast.makeText(getApplicationContext(), result,
              Toast.LENGTH_LONG).show();*/
            if (result.contentEquals("\"success\"")){

                new SweetAlertDialog(Profile_update_retailer.this,SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Profile Picture")
                        .setContentText("Upload Sucecssfully")
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                progress_loader.setVisibility(ProgressBar.GONE);
                                if (DetectConnection
                                        .checkInternetConnection(getApplicationContext())) {
                                    //Toast.makeText(getActivity(),
                                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                                    new DownloadData().execute();
                                } else {
                                    com.nispok.snackbar.Snackbar.with(Profile_update_retailer.this) // context
                                            .text("Oops Your Connection Seems Off..!") // text to display
                                            .show(Profile_update_retailer.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


                                    //	Toast.makeText(getActivity(),
                                    //	getResources().getString(R.string.Sorry) +
                                    //	getResources().getString(R.string.cic),
                                    //	Toast.LENGTH_SHORT).show();
                                }
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();



            }


            else if(result.contentEquals("\"already registerd\"")){
                new SweetAlertDialog(Profile_update_retailer.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.AlreadyRegistred))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            }
        }
    }



}
