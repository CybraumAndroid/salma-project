package com.salmabutchershop.salma.meridian.app.customer.address;

/**
 * Created by Rashid on 11/29/2016.
 */

public class DataValues_N {

    public String fname;
    public String lname;
    public String city;



    public String deliveryarea;
String inst;

    public int getId() {
        return id;
    }

    public String getInst() {
        return inst;
    }

    public void setInst(String inst) {
        this.inst = inst;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String addresstype;
    public String street;
    public String building;
    public String floor;
    public String apartno;
    public String mobile;
    public String phone;
    public String addtional_direc;
public int id;
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDeliveryarea() {
        return deliveryarea;
    }

    public void setDeliveryarea(String deliveryarea) {
        this.deliveryarea = deliveryarea;
    }

    public String getAddresstype() {
        return addresstype;
    }

    public void setAddresstype(String addresstype) {
        this.addresstype = addresstype;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartno() {
        return apartno;
    }

    public void setApartno(String apartno) {
        this.apartno = apartno;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddtional_direc() {
        return addtional_direc;
    }

    public void setAddtional_direc(String addtional_direc) {
        this.addtional_direc = addtional_direc;
    }
}
