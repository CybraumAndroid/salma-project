package com.salmabutchershop.salma.meridian.app.customer.myaccount.history;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 1/23/2017.
 */
public class Coustomer_My_order_Details_adapter extends RecyclerView.Adapter<Coustomer_My_order_Details_adapter.ViewHolder> {


    List<Coustomer_MyOrder_List_Model> cmo;
    Context context;



    public Coustomer_My_order_Details_adapter(ArrayList<Coustomer_MyOrder_List_Model> cmo, Context context) {
        this.cmo = cmo;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;

        ImageView v;
        TextView tv,Name,Place,Estmtd_dlvry,Date,Name2;
        Button Nw;

        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);

            Name=   (TextView) itemView.findViewById(R.id.textView48);
            Name2=   (TextView) itemView.findViewById(R.id.textView49);
            Place=   (TextView) itemView.findViewById(R.id.textView50);
            Estmtd_dlvry=   (TextView) itemView.findViewById(R.id.textView51);
           /* Date=   (TextView) itemView.findViewById(R.id.date);
            Nw= (Button) itemView.findViewById(R.id.imageView16);*/




        }
    }


    @Override
    public int getItemCount() {
        return cmo.size();
    }


    @Override
    public Coustomer_My_order_Details_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.my_coustomer_order_item_layout, viewGroup, false);
        Coustomer_My_order_Details_adapter.ViewHolder pvh = new Coustomer_My_order_Details_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Coustomer_My_order_Details_adapter.ViewHolder personViewHolder, final int i) {

        Typeface myFont7 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Name2.setTypeface(myFont7);
        personViewHolder.Name2.setText(cmo.get(i).getPro_type_name());
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Name.setTypeface(myFont2);
        personViewHolder.Name.setText(cmo.get(i).getPro_name());
        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Place.setTypeface(myFont3);
        personViewHolder.Place.setText(cmo.get(i).getPurchase_quantity());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Estmtd_dlvry.setTypeface(myFont4);
        personViewHolder.Estmtd_dlvry.setText(cmo.get(i).getPro_price()+"/"+cmo.get(i).getPro_qty());
       /* Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.Date.setTypeface(myFont5);
        personViewHolder.Date.setText(bmo.get(i).getOrder_date());
        if(bmo.get(i).getOrder_status().equalsIgnoreCase("New")){
            personViewHolder.Nw.setBackgroundResource(R.drawable.neworder);
            personViewHolder.Nw.setText("New Order");

        }
        else {
            personViewHolder.Nw.setBackgroundResource(R.drawable.accepted);
            personViewHolder.Nw.setText("Accepted");
        }*/
        //  String img=enm.get(i).getNews_img();


    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}