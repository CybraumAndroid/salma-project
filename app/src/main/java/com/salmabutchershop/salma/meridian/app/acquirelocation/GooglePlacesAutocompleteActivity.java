package com.salmabutchershop.salma.meridian.app.acquirelocation;

/**
 * Created by libin on 1/17/2017.
 */

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.nispok.snackbar.Snackbar;
import com.salmabutchershop.salma.meridian.app.GPSTracker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Changepassword;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Fragment;
import com.squareup.okhttp.OkHttpClient;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class GooglePlacesAutocompleteActivity extends Activity implements OnItemClickListener {

    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    ProgressBar progress;

    private static final String API_KEY = "AIzaSyAo27AsUvSaSFEQ5OZWGc4l2SczpZhwnL4";
    TextView tc;
    LinearLayout Lx;
    private static String c_code = "ae";

    int locationMode;
    Context mContext;
    GPSTracker gps;
    double latitude;
    double longitude;
String flag="1";
  String flag_id;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.near_by_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
            c_code = myPrefs.getString("c_code", null);
            flag_id=myPrefs.getString("flag_id",null);
            System.out.println("<<<<<<<<<c_code>>>>>" + flag_id);
            if (flag_id != null && !flag_id.isEmpty()){
                Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
                startActivity(i1);
                finish();
            }else {

            }
            Lx = (LinearLayout) findViewById(R.id.lv);
            AutoCompleteTextView autoCompView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
            tc = (TextView) findViewById(R.id.tc);
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(GooglePlacesAutocompleteActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


            } else {

               // Toast.makeText(this, "You already granted permission", Toast.LENGTH_SHORT).show();
                gps = new GPSTracker(this, GooglePlacesAutocompleteActivity.this);

                // Check if GPS enabled
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    // \n is for new line
                    System.out.println("----------------------------------------");
                    System.out.println("inside cangetlocation");
                    System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                    System.out.println("----------------------------------------");
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                } else {
                    // Can't get location.
                    // GPS or network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    int a = gps.showSettingsAlert(GooglePlacesAutocompleteActivity.this);

                }
            }
            autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
            autoCompView.setOnItemClickListener(this);
      /*  startCountAnimation();*/


        } else {
            com.nispok.snackbar.Snackbar.with(GooglePlacesAutocompleteActivity.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(GooglePlacesAutocompleteActivity.this);
          /*  Snackbar.with(this,null)
                    .type(Type.SUCCESS)
                    .message("No internet connection!")
                    .duration(Duration.LONG)
                    .show();*/


        }

    }

    private void startCountAnimation() {
        progress.setVisibility(View.VISIBLE);
        ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues(0, 100);
        animator.setDuration(10000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                tc.setText("" + (int) animation.getAnimatedValue() + "%");
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<time>>>>>>>>>>>>" + tc.getText().toString());


                if (tc.getText().toString().equalsIgnoreCase("100%")) {
                    progress.setVisibility(View.GONE);
                    Lx.setVisibility(View.GONE);

                    if (!((Activity) GooglePlacesAutocompleteActivity.this).isFinishing()) {
                        try {

                            new SweetAlertDialog(GooglePlacesAutocompleteActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(getResources().getString(R.string.choose_your_language))
                                    // .setContentText("Won't be able to recover this file!")

                                    .setCustomImage(R.drawable.logo)
                                    .setCancelText("Arabic")
                                    .setConfirmText("English")
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                    /*sDialog*/
                                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = preferences.edit();
                                            editor.putString("flag_id", flag);
                                            editor.commit();
                                            Locale locale2 = new Locale("ar");
                                            Locale.setDefault(locale2);
                                            Configuration config2 = new Configuration();
                                            config2.locale = locale2;
                                            getBaseContext().getResources().updateConfiguration(config2, null);
                                            Intent i2 = new Intent(getApplicationContext(), SelectionPage.class);
                                            startActivity(i2);
                                            finish();
                                            sDialog.dismiss();
                                        }
                                    })
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = preferences.edit();
                                            editor.putString("flag_id", flag);
                                            editor.commit();


                                            Locale locale2 = new Locale("en");
                                            Locale.setDefault(locale2);
                                            Configuration config2 = new Configuration();
                                            config2.locale = locale2;
                                            getBaseContext().getResources().updateConfiguration(config2, null);
                                            Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
                                            startActivity(i1);
                                            finish();
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }


            }


        });
        animator.start();

    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        if (str.equalsIgnoreCase("")) {
            Toast.makeText(this,R.string.please_enter_correct_place, Toast.LENGTH_SHORT).show();
        } else {


            Lx.setVisibility(View.VISIBLE);
            startCountAnimation();
        }
    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        trustEveryone();
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=" + "country:" + c_code);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

       /*     OkHttpClient client = new OkHttpClient();
            SSLContext sslContext = SslUtils.getSslContextForCertificateFile("BPClass2RootCA-sha2.cer");
            client.setSslSocketFactory(sslContext.getSocketFactory());*/
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    private static void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

    public class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

            switch (requestCode) {
                case 1: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        // permission was granted, yay! Do the

                        // contacts-related task you need to do.

                        gps = new GPSTracker(this, GooglePlacesAutocompleteActivity.this);

                        // Check if GPS enabled
                        if (gps.canGetLocation()) {

                            latitude = gps.getLatitude();
                            longitude = gps.getLongitude();

                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();

                            editor.putString("longitude", String.valueOf(longitude));
                            editor.putString("latitude", String.valueOf(latitude));
                            editor.commit();
                            // \n is for new line
                            System.out.println("----------------------------------------");
                            System.out.println("inside cangetlocation");
                            System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                            System.out.println("----------------------------------------");




                        /*Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/
                        } else {
                            // Can't get location.
                            // GPS or network is not enabled.
                            // Ask user to enable GPS/network in settings.
                            int a = gps.showSettingsAlert(GooglePlacesAutocompleteActivity.this);


                        }

                    } else {
                        com.nispok.snackbar.Snackbar.with(GooglePlacesAutocompleteActivity.this) // context
                                .text(R.string.you_need_to_grant_permission) // text to display
                                .show(GooglePlacesAutocompleteActivity.this);

                    }


                    return;
                }
            }
        }
    }
