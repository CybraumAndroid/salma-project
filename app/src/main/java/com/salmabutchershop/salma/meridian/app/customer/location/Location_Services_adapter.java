package com.salmabutchershop.salma.meridian.app.customer.location;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 10/17/2016.
 */
public class Location_Services_adapter extends RecyclerView.Adapter<Location_Services_adapter.ViewHolder> {


    List<Location_Services_Model> eem;
    Context context;
public int row_index=0;


    public Location_Services_adapter(ArrayList<Location_Services_Model> eem, Context context) {
        this.eem = eem;
        this.context = context;

    }

    public void changeindex(int position) {
        this.row_index=position;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;

        ImageView personPhoto;
LinearLayout Lx;
        ViewHolder(View itemView) {
            super(itemView);
      Lx = ( LinearLayout) itemView.findViewById(R.id.row_linrLayout);

            tv=   (TextView) itemView.findViewById(R.id.evt_typ);
           // cv1=(LinearLayout) itemView.findViewById(R.id.lt1);
           /* tv1=   (TextView) itemView.findViewById(R.id.evnt_frm);
            tv2=   (TextView) itemView.findViewById(R.id.evnt_to);
            tv3=   (TextView) itemView.findViewById(R.id.evnt_vnue);
         ;*/
           // v=   (ImageView) itemView.findViewById(R.id.imageView13);
        }
    }


    @Override
    public int getItemCount() {
        return eem.size();
    }


    @Override
    public Location_Services_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.location_services_item_layout, viewGroup, false);
        Location_Services_adapter.ViewHolder pvh = new Location_Services_adapter.ViewHolder(v);
        return pvh;
    }
    private final ArrayList<Integer> selected = new ArrayList<>();

    @Override
    public void onBindViewHolder(final Location_Services_adapter.ViewHolder personViewHolder, final int i) {

        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.tv.setTypeface(myFont2);
        personViewHolder.tv.setText(eem.get(i).getCity());

     /*  personViewHolder.Lx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=i;
                notifyDataSetChanged();
            }
        });*/
        if(row_index==i){
      personViewHolder.Lx.setBackgroundResource(R.drawable.selectionred);
          personViewHolder.tv.setTextColor(Color.parseColor("#ffffff"));
        }
        else
        {
           personViewHolder.Lx.setBackgroundResource(R.drawable.cntry);
         personViewHolder.tv.setTextColor(Color.parseColor("#d62041"));
        }


    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}