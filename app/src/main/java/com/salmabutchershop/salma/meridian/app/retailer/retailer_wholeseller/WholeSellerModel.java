package com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller;

/**
 * Created by Rashid on 12/6/2016.
 */

public class WholeSellerModel {
    public String getSubcat_id() {
        return subcat_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    String id,wholeseller_star;


    String shope_name,name,address,address2,location,logo,subcat_id,cat_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWholeseller_star() {
        return wholeseller_star;
    }

    public void setWholeseller_star(String wholeseller_star) {
        this.wholeseller_star = wholeseller_star;
    }

    public String getShope_name() {
        return shope_name;
    }

    public void setShope_name(String shope_name) {
        this.shope_name = shope_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
