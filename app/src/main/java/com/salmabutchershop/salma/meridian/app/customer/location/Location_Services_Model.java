package com.salmabutchershop.salma.meridian.app.customer.location;

/**
 * Created by libin on 10/17/2016.
 */
public class Location_Services_Model {
    String id,city,country_id,country_code;

    public String getId() {
        return id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
