
package com.salmabutchershop.salma.meridian.app.customer.address;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.salmabutchershop.salma.meridian.app.customer.myaccount.NewDataAdapter_NS;


/**
 * Created by Rashid on 11/14/2016.
 */


public class DatabaseHandler_N {
    SQLiteDatabase db=null;

   public void DeleteView1(Context context1, NewDataAdapter_N.ViewHolder holder){
        db = context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,img_url VARCHAR,event_id INTEGER,extra_data VARCHAR);");
        db.execSQL("DELETE FROM favourite where id="+holder.primary_id.getText());
      //  System.out.println("deleted button id :"+holder.rmv_btn+"\n image view id :"+holder.imgView2);
    }

    public void InsertIntoSqlite(Context context1,String imgUrl,int event_id,String extra_data)
    {
        db = context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
        db.execSQL("INSERT INTO favourite(img_url,event_id,extra_data) VALUES('" + imgUrl + "','"+event_id+"','"+extra_data+"')");
    }

    public Cursor checkIfAdded(Context context1,String imgUrl){
        db = context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,img_url VARCHAR,event_id INTEGER,extra_data VARCHAR);");
        Cursor rs = db.rawQuery("SELECT img_url FROM favourite where img_url='" + imgUrl + "'", null);
        return rs;
    }


    public Cursor selectAllFAV(Context context1)
    {
        db =context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,img_url VARCHAR,event_id INTEGER,extra_data VARCHAR);");
        Cursor rs = db.rawQuery("SELECT * FROM favourite", null);
        return rs;
    }
///////////////////////////// NEW
    public void CreateDatas(Context context1){
        db = context1.openOrCreateDatabase("DATAS.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS Data1(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,fname VARCHAR,lname VARCHAR,city VARCHAR,deliveryarea VARCHAR,addresstype VARCHAR,street VARCHAR,building VARCHAR,floor VARCHAR,apartno VARCHAR,mobile VARCHAR,phone VARCHAR,additional_direc VARCHAR);");
        System.out.println("INSIDE create db DATAS ");
    }

    public void InsertDatas(Context context1,String[] values)
    {
        db = context1.openOrCreateDatabase("DATAS.db", Context.MODE_PRIVATE, null);
        db.execSQL("INSERT INTO Data1(fname,lname,city,deliveryarea,addresstype,street,building,floor,apartno,mobile,phone,additional_direc) VALUES('" + values[0] + "','"+values[1]+"','"+values[2]+"','"+values[3]+"','"+values[4]+"','"+values[5]+"','"+values[6]+"','"+values[7]+"','"+values[8]+"','"+values[9]+"','"+values[10]+"','"+values[11]+"')");
        System.out.println("INSIDE INSERT function ");
    }

    public Cursor selectAllDATAS(Context context1)
    {
        db =context1.openOrCreateDatabase("DATAS.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS Data1(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,fname VARCHAR,lname VARCHAR,city VARCHAR,deliveryarea VARCHAR,addresstype VARCHAR,street VARCHAR,building VARCHAR,floor VARCHAR,apartno VARCHAR,mobile VARCHAR,phone VARCHAR,additional_direc VARCHAR);");
        Cursor rs = db.rawQuery("SELECT * FROM Data1", null);
        return rs;
    }
    public void DeleteViews(Context context1, NewDataAdapter_N.ViewHolder holder){
        db = context1.openOrCreateDatabase("DATAS.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS Data1(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,fname VARCHAR,lname VARCHAR,city VARCHAR,deliveryarea VARCHAR,addresstype VARCHAR,street VARCHAR,building VARCHAR,floor VARCHAR,apartno VARCHAR,mobile VARCHAR,phone VARCHAR,additional_direc VARCHAR);");
        db.execSQL("DELETE FROM Data1 where id="+holder.primary_id.getText());
        //  System.out.println("deleted button id :"+holder.rmv_btn+"\n image view id :"+holder.imgView2);
    }
    public void DeleteViewN(Context context1, NewDataAdapter_NS.ViewHolder holder){
        db = context1.openOrCreateDatabase("DATAS.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS Data1(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,fname VARCHAR,lname VARCHAR,city VARCHAR,deliveryarea VARCHAR,addresstype VARCHAR,street VARCHAR,building VARCHAR,floor VARCHAR,apartno VARCHAR,mobile VARCHAR,phone VARCHAR,additional_direc VARCHAR);");
        db.execSQL("DELETE FROM Data1 where id="+holder.primary_id.getText());
        //  System.out.println("deleted button id :"+holder.rmv_btn+"\n image view id :"+holder.imgView2);
    }
}
