package com.salmabutchershop.salma.meridian.app.customer.myaccount;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.hbb20.CountryCodePicker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.login.RoundedImageView;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Vis_FilePath;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

public class EditProfile extends AppCompatActivity {

    String Reg_id;
    EditText edtfullnam,edtphonenumber,edtemail,edtcity,edtcountry;
    String str_fullname,str_phone,str_email,str_country;
    Button butsignup;
    boolean edittexterror = false;
    String result, Dat,logo;
    static ArrayList<MyAccount_Profile_Model> eem;
    MyAccount_Profile_Model ee;
    ImageView profile_upload_icon;
    RoundedImageView rounded_imageview;
    private static final int PICK_FILE_REQUEST = 1;
    private static final int REQUEST_CODE = 1;
    String selectedFilePath,cvc;
    public static MKLoader progress_loader, progress_loader1;
    static String filename = "null", filenamepath = "null";
    CountryCodePicker ccp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);


        profile_upload_icon=(ImageView)findViewById(R.id.profile_upload_icon);
        rounded_imageview=(RoundedImageView)findViewById(R.id.rounded_imageview);
        progress_loader1 = (MKLoader) findViewById(R.id.progress_loader1);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);
try {
    SharedPreferences prefrnc = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
    str_fullname = prefrnc.getString("fullname", null);
    str_phone = prefrnc.getString("c_phone", null);
    str_email = prefrnc.getString("c_email", null);
    str_country = prefrnc.getString("c_location", null);
    cvc= myPrefs.getString("cvc", null);
}catch (Exception e){

}


        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.setCountryForNameCode(cvc);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                //  Toast.makeText(PostRegistration.this, "Updated " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
            }
        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData().execute();
        }

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditProfile.this.goBack();
            }
        });

        butsignup=(Button)findViewById(R.id.butsignup);

        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");

        edtfullnam = (EditText) findViewById(R.id.edt_fullname);
        edtfullnam.setTypeface(myFont1);

        edtphonenumber=(EditText)findViewById(R.id.edt_phone);
        edtphonenumber.setTypeface(myFont1);

        edtemail=(EditText)findViewById(R.id.edt_email);
        edtemail.setTypeface(myFont1);

        edtcountry=(EditText)findViewById(R.id.edt_country);
        edtcountry.setTypeface(myFont1);

        edtfullnam.setText(str_fullname);
        edtphonenumber.setText(str_phone);
        edtemail.setText(str_email);
        edtcountry.setText(str_country);




        edtcountry.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                String l = getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>" + l);

                if (l.equalsIgnoreCase("English")) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (edtcountry.getRight() - edtcountry.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            edtcountry.setText("");
                            return true;
                        }
                    }
                } else {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (edtcountry.getLeft() - edtcountry.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                            // your action here
                            edtcountry.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });

butsignup.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        try {

            str_fullname = edtfullnam.getText().toString();
            str_phone = edtphonenumber.getText().toString();
            str_email = edtemail.getText().toString();
            str_country = edtcountry.getText().toString();
            edittexterror = false;
            if (str_fullname.isEmpty() || str_phone.isEmpty() || str_email.isEmpty() || str_country.isEmpty()) {
                com.nispok.snackbar.Snackbar.with(EditProfile.this) // context
                        .text(getResources().getString(R.string.empty_fields)) // text to display
                        .show(EditProfile.this);
                    /*com.chootdev.csnackbar.Snackbar.with(PostRegistration.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Field Empty!")
                            .duration(Duration.SHORT)
                            .show();*/
                edittexterror = true;
                //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

            }  else if (edtemail.getText().toString().isEmpty()) {
                edtemail.setError(getResources().getString(R.string.Enter_Email_Id));
                edittexterror = true;
            } else if (edtphonenumber.getText().toString().isEmpty()) {
                edtphonenumber.setError(getResources().getString(R.string.Enter_Phone));
                edittexterror = true;
            } else if (edtfullnam.getText().toString().isEmpty()) {
                edtfullnam.setError(getResources().getString(R.string.Enter_Full_name));
                edittexterror = true;
            } else if (edtcountry.getText().toString().isEmpty()) {
                edtcountry.setError(getResources().getString(R.string.Enter_country));
                edittexterror = true;
            }  else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                edtemail.setError(getResources().getString(R.string.Invalid_Email));
                edittexterror = true;
            } else if (!android.util.Patterns.PHONE.matcher(edtphonenumber.getText().toString().trim()).matches()) {
                edtphonenumber.setError(getResources().getString(R.string.Invalid_Phone));
                edittexterror = true;
            }
            else {
                edittexterror = false;
            }
                   /* else if (!isValidPassword(edtphon.getText().toString().trim())) {
                        edtphon.setError("Invalid Phone");
                        edittexterror = true;
                    }*/


            if (edittexterror == false) {
                if (DetectConnection
                        .checkInternetConnection(EditProfile.this)) {
                    new SendUpdateDetails().execute();
                }
                else {
                    com.nispok.snackbar.Snackbar.with(EditProfile.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(EditProfile.this);
                                     }
            }


        }catch (Exception e){}
    }
});

        profile_upload_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED) {

                    requestPermissions();
                }
                    showFileChooser();
            }
        });

    }

    class SendUpdateDetails extends AsyncTask<String,String,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
            progress_loader.setVisibility(View.VISIBLE);
            str_fullname = edtfullnam.getText().toString();
            str_phone = edtphonenumber.getText().toString();
            str_email = edtemail.getText().toString();
            str_country = edtcountry.getText().toString();
        }

        @Override
        protected String doInBackground(String... strings) {


            try{
                URL url = new URL(SERVER_CUSTOMER+"update_profile.php"); //  URL path
                JSONObject params=new JSONObject();
                params.put("name",str_fullname);

                params.put("email",str_email);
                params.put("phone",str_phone);
                params.put("address","");
                params.put("location",str_country);
                params.put("user_id",Reg_id);
                Log.e("params",params.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(params));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return  null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress_loader.setVisibility(View.GONE);
            System.out.println("result : "+s);
            if(s!=null) {
                if (s.equalsIgnoreCase("\"success\"")) {


                    new SweetAlertDialog(EditProfile.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                            .setContentText("Profile Updated")
                            .setConfirmText(getResources().getString(R.string.ok))

                            .setCustomImage(R.drawable.logo)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {


                                    // new PostRegistrationRetailer.SendPostRequest().execute();
                                    sDialog.dismiss();
                                    finish();
                                }
                            })
                            .show();


                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("fullname", str_fullname);
                    editor.putString("c_name", str_fullname);
                    editor.putString("c_phone", str_phone);
                    editor.putString("c_email", str_email);
                    editor.putString("c_location", str_country);
                    editor.commit();


                } else {
                    new SweetAlertDialog(EditProfile.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                            .setContentText("Failed")
                            .setConfirmText(getResources().getString(R.string.ok))

                            .setCustomImage(R.drawable.logo)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {


                                    // new PostRegistrationRetailer.SendPostRequest().execute();
                                    sDialog.dismiss();
                                }
                            })
                            .show();
                }
            }
            else {
                new SweetAlertDialog(EditProfile.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                        .setContentText(getResources().getString(R.string.something_went_wrong))
                        .setConfirmText(getResources().getString(R.string.ok))

                        .setCustomImage(R.drawable.logo)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {


                                // new PostRegistrationRetailer.SendPostRequest().execute();
                                sDialog.dismiss();
                            }
                        })
                        .show();
            }



        }
    }
    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        System.out.println("result.toString()"+result.toString());
        return result.toString();
    }
    private void goBack() {
        super.onBackPressed();
    }
    private void showFileChooser() {
        try {


                Intent intent = new Intent();
                //sets the select file to all types of files
                intent.setType("image/*");
                //allows to select data and return it
                intent.setAction(Intent.ACTION_GET_CONTENT);
                //starts new activity to select file and return data
                startActivityForResult(Intent.createChooser(intent, "Choose File to Vis_Upload.."), PICK_FILE_REQUEST);
            }
         catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(EditProfile.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE);
        // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                if(data == null){
                    //no data present
                    return;
                }


                Uri selectedFileUri = data.getData();
                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(),selectedFileUri);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                Log.i("","Selected File Path:" + selectedFilePath);

                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    // textViewResponse.setText(selectedFilePath);

                    CropImage.activity(selectedFileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                    // logo_img.setImageBitmap(bmp);
                }else{
                    com.nispok.snackbar.Snackbar.with(EditProfile.this) // context
                            .text("Cannot upload file to server!") // text to display
                            .show(EditProfile.this);
                   /* Snackbar.with(Butcher_Product_Submit.this,null)
                            .type(Type.SUCCESS)
                            .message("Cannot upload file to server!")
                            .duration(Duration.LONG)
                            .show();*/
                    //  Toast.makeText(getApplicationContext(),"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                }

            }
           /* else if(requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
                if(resultCode==RESULT_OK){
                    previewCapturedImage();
                }*/

            //  }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                System.out.println("selectedFileUri : "+resultUri);

                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), resultUri);
                filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                rounded_imageview.setImageBitmap(bmp);
                if (selectedFilePath != null && !selectedFilePath.isEmpty())

                {
                    if (filenamepath != null) {
                        filename = filenamepath;


                    } else {
                        filename = "null";

                    }
                    uploadVideo1();


                    new SendPostRequest1().execute();
                    // tt="";
                } else {
                    new SweetAlertDialog(EditProfile.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(getResources().getString(R.string.MyMenu))
                            .setContentText(getResources().getString(R.string.Areyousurewanttosetimageasdefault))
                            .setConfirmText(getResources().getString(R.string.Yes))
                            .setCancelText(getResources().getString(R.string.No))
                            .setCustomImage(R.drawable.logo)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {


                                    // new PostRegistrationRetailer.SendPostRequest().execute();
                                    sDialog.dismiss();
                                }
                            })
                            .show();

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
    private void uploadVideo1() {
        class UploadVideo extends AsyncTask<Void, Void, String> {

            ProgressDialog uploading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
               // progress_loader.setVisibility(View.VISIBLE);
                //   uploading = ProgressDialog.show(PostRegistrationRetailer.this, "Salma", "Please wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //    progress_loader.setVisibility(View.GONE);
                //   uploading.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {
                Vis_Upload4 u = new Vis_Upload4();
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<md>>>>>>>>>>>>>>>>>>>>>>>>>" + selectedFilePath);
                String msg = u.uploadVideo(selectedFilePath);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<mdeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>>>>>>>>>>" + msg);

                return msg;
            }
        }
        UploadVideo uv = new UploadVideo();
        uv.execute();
    }
    private class SendPostRequest1 extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
           // progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        public String doInBackground(String... arg0) {
           /* if (filenamepath != null) {
                filename = filenamepath;


            } else {
                filename = "null";

            }*/
            // filename=filenamepath;
            String js=filename.replaceAll(" ","");

            try {

                URL url = new URL(SERVER_CUSTOMER+"profile_picture.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("user_id",Reg_id);
           /*     postDataParams.put("profilepic",fullname);
                postDataParams.put("phone",phone);
                postDataParams.put("email",email);
                postDataParams.put("address1",address1_s);
                postDataParams.put("address2",address2_s);
                postDataParams.put("shopname",shopname_s);
                postDataParams.put("country",username);
                postDataParams.put("city",password_s);
                postDataParams.put("token",refreshedToken);
                postDataParams.put("retailer_type",k);
                postDataParams.put("longitude",ltg);
                postDataParams.put("latitude",lts);*/
                postDataParams.put("profilepic",js);
                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {

           /*  Toast.makeText(getApplicationContext(), result,
              Toast.LENGTH_LONG).show();*/
            if (result.contentEquals("\"success\"")){

                new SweetAlertDialog(EditProfile.this,SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Profile Picture")
                        .setContentText(getResources().getString(R.string.UploadSuccess))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                progress_loader.setVisibility(ProgressBar.GONE);
                                if (DetectConnection
                                        .checkInternetConnection(getApplicationContext())) {
                                    //Toast.makeText(getActivity(),
                                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                                    new DownloadData().execute();
                                } else {
                                    com.nispok.snackbar.Snackbar.with(EditProfile.this) // context
                                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                            .show(EditProfile.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


                                    //	Toast.makeText(getActivity(),
                                    //	getResources().getString(R.string.Sorry) +
                                    //	getResources().getString(R.string.cic),
                                    //	Toast.LENGTH_SHORT).show();
                                }
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();



            }


            else if(result.contentEquals("\"already registerd\"")){
                new SweetAlertDialog(EditProfile.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.AlreadyRegistred))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            }
        }
    }

    private class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          //  progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER_CUSTOMER+"profile_picture.php?user_id="+Reg_id
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            // String sam = result.trim();
           // progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
            }  else*/
            if (result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                //  mCountryModel1 = new ArrayList<>();
                eem = new ArrayList<MyAccount_Profile_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                        ee = new MyAccount_Profile_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //Log.d("OutPut", mJsonObject.getString("doctor_publish"));
//String user_id,subtotal,order_time,email,instruction;
                        logo = mJsonObject.getString("logo");
                        ee.setLogo(logo);
                        eem.add(ee);

                        System.out.println("" + ee);

                      /*
                        user_id = mJsonObject.getString("user_id");
                        subtotal = mJsonObject.getString("subtotal");
                        order_time = mJsonObject.getString("order_time");
                        order_date = mJsonObject.getString("order_date");
                        //  email=mJsonObject.getString("email");
                        // instruction=mJsonObject.getString("email");
                        order_status = mJsonObject.getString("order_status");
                     city= mJsonObject.getString("city");
                        //    deliveryarea= mJsonObject.getString("deliveryarea");
                        //   addresstype= mJsonObject.getString("addresstype");
                        //    street= mJsonObject.getString("street");
                        //     building= mJsonObject.getString("building");
                        //     floor= mJsonObject.getString("floor");
                        //    house_no=mJsonObject.getString("house_no");
                    *//*    Newsdb_model city = new Newsdb_model();
                        city.setName(news_id);
                        city.setState(news_title);
                        city.setDescription(news_content);
                        city.setImg(news_img);
                        handler.addCity(city);*//*

                        ee.setOrder_id(order_id);
                        // ee.setPro_name(pro_name);
                        //  ee.setPro_qty(pro_qty);
                        ee.setSubtotal(subtotal);
                        ee.setOrder_date(order_date);
                        ee.setOrder_status(order_status);
                        ee.setOrder_time(order_time);
                        ee.setCity(order_time);*/
                        // ee.setDeliveryarea(deliveryarea);
                        //  ee.setAddresstype(addresstype);
                        //  ee.setStreet(street);
                        //  ee.setBuilding(building);
                        //  ee.setFloor(floor);
//
                      /*  System.out.println("<<news_id>>>>" + news_id);

                        System.out.println("<< news_title>>>>" +news_title);
                        System.out.println("<< news_content>>>" +news_content);*/

                        eem.add(ee);

                        System.out.println("" + ee);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    rounded_imageview.setBackground(null);
                    new Picasso.Builder(EditProfile.this)
                            .downloader(new OkHttpDownloader(EditProfile.this, Integer.MAX_VALUE))
                            .build()
                            .load(logo)
                            .noFade()
                            .into(rounded_imageview, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progress_loader1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
            /*    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoContent))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .show();*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
        }
    }
}
