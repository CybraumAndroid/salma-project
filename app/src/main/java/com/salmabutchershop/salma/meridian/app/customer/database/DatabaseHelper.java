package com.salmabutchershop.salma.meridian.app.customer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by user 1 on 15-12-2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper
{
    // TABLE INFORMATTION

//    public static final String MEMBER_ITEM = "item";
//    public static final String MEMBER_QUANTITY = "quantity";
//    public static final String MEMBER_PRICE = "price";
//    public static final String MEMBER_TOTAL_PRICE = "total";

    // DATABASE INFORMATION
    static final String DB_NAME = "MEMBER";
    static final int DB_VERSION = 5;
    public static final String TABLE_OUTLET = "tableone";
    // TABLE CREATION STATEMENT

    private static final String CREATE_TABLE = "CREATE TABLE  "+ TABLE_OUTLET + "( outlet_id INTEGER PRIMARY KEY AUTOINCREMENT,outlet_name TEXT  , outlet_qty FLOAT ,outlet_price FLOAT,outlet_tot FLOAT,total FLOAT ,pro_id TEXT,butcher_id TEXT,fds FLOAT,shop_name TEXT,price_quantity TEXT,kg_or_g TEXT,value_befr_kg_or_g TEXT,price_for_1kg FLOAT,price_for_1_gram FLOAT,type_id TEXT)";
    public static final String DELETE_TABLE_OUTLET="DROP TABLE IF EXISTS " + TABLE_OUTLET;


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        System.out.println("table created");

    }
    //Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(DELETE_TABLE_OUTLET);
        //Create tables again
       // db.execSQL("DROP TABLE IF EXISTS tableone");
        onCreate(db);
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void UpdateAmount(int outlet_id,Float qty,Float total)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String UPDATE_TABLE_OUTLET="update "+TABLE_OUTLET+" set outlet_qty='"+qty+"',outlet_tot='"+total+"' where outlet_id='"+outlet_id+"'";
        System.out.println("////////////////////////////////////////\n"+UPDATE_TABLE_OUTLET+"\n////////////////////////////////////////////");
        db.execSQL(UPDATE_TABLE_OUTLET);

    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void insertData(String name, float qty, float price, float tot, float ttal ,String pro_id,String butcher_id,float fds,String shop_name,String price_quantity,String kg_or_g,String value_befr_kg_or_g,float price_for_1kg,float price_for_1_gram,String type_id){


        // Open the database for writing
        SQLiteDatabase db = this.getWritableDatabase();
        // Start the transaction.
        db.beginTransaction();
        ContentValues cv;
        try {
            System.out.println(""+name);
            cv = new ContentValues();
            cv.put("outlet_name", name);


            cv.put("outlet_qty", qty);
            cv.put("outlet_price", price);
            cv.put("outlet_tot", tot);
            cv.put("total", ttal);
            cv.put("pro_id", pro_id);
            cv.put("butcher_id",butcher_id);
            cv.put("fds",fds);
            cv.put("shop_name",shop_name);
            cv.put("price_quantity",price_quantity);
            cv.put("kg_or_g",kg_or_g);
            cv.put("value_befr_kg_or_g",value_befr_kg_or_g);
            cv.put("price_for_1kg",price_for_1kg);
            cv.put("price_for_1_gram",price_for_1_gram);
            cv.put("type_id",type_id);


            System.out.println("price_quantity inside DatabaseHelper is : "+price_quantity);

            long i = db.insert(TABLE_OUTLET, null, cv);
            Log.i("Insert", i + "");

            db.setTransactionSuccessful();

        }
        catch (SQLiteException e)
        {
            e.printStackTrace();

        }
        finally
        {
            db.endTransaction();
            // End the transaction.
            db.close();
            // Close database
        }

    }
    public void delete(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_OUTLET, "outlet_id" + " = ?",
                new String[] { String.valueOf(id) });
    }
    public boolean isExist(String but_id) {
        SQLiteDatabase db= this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_OUTLET + " WHERE butcher_id  = '" + but_id + "'", null);
        boolean exist = (cur.getCount() > 0);
        cur.close();
        db.close();
        return exist;

    }

    public String getdet(String outlet){



        String selectQuery = "SELECT  * FROM " + TABLE_OUTLET+ " WHERE butcher_id = "+outlet;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
   String  outlets ="";
        if (cursor.moveToFirst())
        {
            do {
                outlets = cursor.getString(cursor.getColumnIndex("butcher_id"));
            }
            while (cursor.moveToNext());
        }
        db.close();
        return  selectQuery;


    }
    public String getBut_id( String o){
        String selectQuery = "SELECT  * FROM " + TABLE_OUTLET+ " WHERE outlet_id = "+o;
     /*   SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int  outlets = 0;
        if (cursor.moveToFirst())
        {
            do {
                outlets = cursor.getInt(cursor.getColumnIndex("outlet_tot"));
            }
            while (cursor.moveToNext());
        }
        db.close();
        return  outlets;*/
     return null;
    }
    public long getProfilesCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long cnt  = DatabaseUtils.queryNumEntries(db, TABLE_OUTLET);
        db.close();
        return cnt;
    }
   /* public Cursor checkIfAdded(Context context1,String but_id){
        String selectQuery = "SELECT  * FROM " + TABLE_OUTLET+ " WHERE outlet_id = "+but_id;
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(butcher_id);");
        Cursor rs = db.rawQuery("SELECT img_url FROM favourite where added_by='" + added_by + "'", null);
        //  Cursor rs= db.rawQuery("SELECT img_url FROM favourite where img_url='" + imgUrl + "'", null);
        return selectQuery;
    }*/
    public void removeAll()
    {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        //db.delete(DatabaseHelper.DB_NAME, null, null);
        db.delete(DatabaseHelper.TABLE_OUTLET, null, null);
    }
//    public void selectal() {
//        SQLiteDatabase db=this.getReadableDatabase();
//        db.beginTransaction();
//
//        String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
//        System.out.println("" + selectQuery);
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        if (cursor.getCount() > 0) {
//            while (cursor.moveToNext()) {
//                // Read columns data
//                final int outlet_id = cursor.getInt(cursor.getColumnIndex("outlet_id"));
//                String outlet_name = cursor.getString(cursor.getColumnIndex("outlet_name"));
//                int outlet_qty = cursor.getInt(cursor.getColumnIndex("outlet_qty"));
//                int outlet_price = cursor.getInt(cursor.getColumnIndex("outlet_price"));
//                int  outlet_tot = cursor.getInt(cursor.getColumnIndex("outlet_tot"));
//            }
//        }
//
//    }





}