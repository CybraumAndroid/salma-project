package com.salmabutchershop.salma.meridian.app.retailer;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct.Butcher_main_Product;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_my_order;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.Butcher_Wholseller_Fragment;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.enquiry.EnquiryFragment;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.productdetail.Butcher_product_Model;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.NetworkCheckingClass;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.ContactUs;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.FeedBack;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.NewsUpdates;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;
import com.salmabutchershop.salma.meridian.app.retailer.international.InternationalFragment;
import com.salmabutchershop.salma.meridian.app.retailer.login.LoginActivityRetailer;
import com.salmabutchershop.salma.meridian.app.retailer.login.PostRegistrationRetailer;
import com.salmabutchershop.salma.meridian.app.retailer.productdet.RetailerProductAdapter;
import com.salmabutchershop.salma.meridian.app.retailer.productdet.RetailerProductFragment;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.salmabutchershop.salma.meridian.app.R.drawable.discount;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/6/2016.
 */

public class RetailerMainPage extends AppCompatActivity {

    LinearLayout myOrder, purchase, myMenu, extras;
    NavigationView navigationView;
    TextView t2, t3;
    String Reg_id, fullname, typ, userid;
    RecyclerView recyclerview;

    RetailerMainAdapter adapter1;
    ProgressBar progress;
    static ArrayList<Retailer_main_Model> rmm;
    Retailer_main_Model rm;
    String id, retail_cat, retail_img, result;
    String Wurl;
    public static MKLoader progress_loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_retailer_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //  getSupportActionBar().setDisplayShowTitleEnabled(false);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);
       /* ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RetailerMainPage.this.goBack();
            }
        });*/
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(RetailerMainPage.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(RetailerMainPage.this);
           /* com.chootdev.csnackbar.Snackbar.with(RetailerMainPage.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }


    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);

            String l = getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>" + l);
            if (l.equalsIgnoreCase("English")) {
                Wurl = SERVER+"json/retailer/retailerhome.php";
            } else {
                Wurl = SERVER+"json/retailer/ar/retailerhome.php";
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
            //   String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            // String value = result;
           /* if
                    (result.equalsIgnoreCase("[]")) {
                Toast.makeText(getApplicationContext(), "No items", Toast.LENGTH_SHORT).show();
            }  else*/
            if (result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                rmm = new ArrayList<Retailer_main_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        rm = new Retailer_main_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));

                        //String id,retail_cat,retail_img,result;
                        id = mJsonObject.getString("id");
                        retail_cat = mJsonObject.getString("retail_cat");
                        retail_img = mJsonObject.getString("retail_img");

                        rm.setId(id);
                        rm.setRetail_cat(retail_cat);
                        rm.setRetail_img(retail_img);
                        //  wm.s

                        System.out.println("<<<galry_img>>>>" + retail_cat);

                        rmm.add(rm);
                        System.out.println("<<<oo>>>>" + rm);

                        adapter1 = new RetailerMainAdapter(rmm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
            recyclerview.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent i = new Intent(RetailerMainPage.this, PostRegistrationRetailer.class);
                            i.putExtra("Retail_cat", rmm.get(position).getRetail_cat());
                            startActivity(i);
                            finish();

                        }
                    })
            );
        }


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Intent inn = new Intent(getApplicationContext(), SelectionPage.class);
            startActivity(inn);
            finish();
        } else {

            Intent inn = new Intent(getApplicationContext(), SelectionPage.class);
            startActivity(inn);
            finish();
        }
    }


}