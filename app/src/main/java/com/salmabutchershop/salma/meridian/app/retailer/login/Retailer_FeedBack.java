package com.salmabutchershop.salma.meridian.app.retailer.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

public class Retailer_FeedBack extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    Button submit;
    EditText feedback;
    int selectedRadiobuttonId;
    private String USER_ID_KEY="user_id";
    private String MESSAGE_KEY="subject";
    private String SUBJECT_KEY="message";
    boolean edittexterror = false;
    private String feedbackMessage,subject;
    String  name,shop_name,location,phone,email,message,user_id,userid,countryR,fullname,address;
    ProgressBar progress;
    EditText edtyourname,  edtshopname,  edtcountry , edtphone , edtemail,edtmsg,edtads1,edtads2;
    public static MKLoader progress_loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retailer_feedback_layout);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        edtyourname = (EditText) findViewById(R.id.edt_yourname);
        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtyourname.setTypeface(myFont1);

        edtshopname = (EditText) findViewById(R.id.edt_shopnamee);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtshopname.setTypeface(myFont2);

        edtcountry = (EditText) findViewById(R.id.edt_countrylocation);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtcountry.setTypeface(myFont3);

        edtphone = (EditText) findViewById(R.id.edt_phone);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtphone .setTypeface(myFont4);

        edtemail= (EditText) findViewById(R.id.edt_email);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtemail.setTypeface(myFont5);

        edtmsg = (EditText) findViewById(R.id.feedback);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtmsg.setTypeface(myFont6);


        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idL",null);

        fullname = myPrefs.getString("fullnameR", null);
        shop_name= myPrefs.getString("shp_nm", null);
        address = myPrefs.getString("ads", null);
        phone= myPrefs.getString("phne", null);
        email= myPrefs.getString("eml", null);
   countryR= myPrefs.getString("countryR", null);





        location= myPrefs.getString("locationR", null);



        edtyourname.setText(fullname);
        edtshopname.setText(shop_name);
        edtcountry.setText(countryR);
        edtphone.setText(phone);
        edtemail.setText(email);
        // fullname = myPrefs.getString("fullnameb", null);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaHHHHHHHHHHHHHHHHHHaaaaaaaaaaaaaaaaaaaa>>>>>>>>>"+userid);
        //   System.out.println(">>>>>>fullnameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee>>>>>>>>>"+fullname);

        edtyourname.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                String l = getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>" + l);

                if (l.equalsIgnoreCase("English")) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= ( edtyourname.getRight() -  edtyourname.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            edtyourname.setText("");
                            return true;
                        }
                    }
                } else {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= ( edtyourname.getLeft() - edtyourname.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                            // your action here
                            edtyourname.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });
        edtshopname .setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                String l = getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>" + l);

                if (l.equalsIgnoreCase("English")) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= ( edtshopname .getRight() -  edtshopname .getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            edtshopname.setText("");
                            return true;
                        }
                    }
                } else {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (      edtshopname .getLeft() -     edtshopname .getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                            // your action here
                            edtshopname .setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        edtphone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                String l = getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>" + l);

                if (l.equalsIgnoreCase("English")) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= ( edtphone.getRight() - edtphone.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            edtphone.setText("");
                            return true;
                        }
                    }
                } else {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= ( edtphone.getLeft() - edtphone.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                            // your action here
                            edtphone.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        edtemail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                String l = getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>" + l);

                if (l.equalsIgnoreCase("English")) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (    edtemail.getRight() -     edtemail.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            edtemail.setText("");
                            return true;
                        }
                    }
                } else {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (    edtemail.getLeft() -     edtemail.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                            // your action here
                            edtemail.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });
        // fullname = myPrefs.getString("fullnameb", null);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaHHHHHHHHHHHHHHHHHHaaaaaaaaaaaaaaaaaaaa>>>>>>>>>"+userid);

        submit=(Button)findViewById(R.id.submit);

        progress = (ProgressBar) findViewById(R.id.progress_bar);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Retailer_FeedBack.this.goBack();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    if (edtyourname.getText().toString().isEmpty() || edtshopname.getText().toString().isEmpty() || edtcountry.getText().toString().trim().isEmpty() || edtphone.getText().toString().isEmpty()  || edtemail.getText().toString().isEmpty()) {
                        com.nispok.snackbar.Snackbar.with(Retailer_FeedBack.this) // context
                                .text(getString(R.string.empty_fields))// text to display
                                .show(Retailer_FeedBack.this);

                        edittexterror = true;


                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (edtemail.getText().toString().isEmpty()) {
                        edtemail.setError(getResources().getString(R.string.Enter_Email_Id));
                        edittexterror = true;
                    } else if (edtyourname.getText().toString().isEmpty()) {
                        edtyourname.setError(getResources().getString(R.string.Enter_Full_name));
                        edittexterror = true;
                    } else if (edtphone.getText().toString().isEmpty()) {
                        edtphone.setError(getResources().getString(R.string.Enter_Phone));
                        edittexterror = true;
                    } else if (edtshopname.getText().toString().isEmpty()) {
                        edtshopname.setError(getResources().getString(R.string.Enter_shop_name));
                        edittexterror = true;
                    }  else if (edtcountry.getText().toString().isEmpty()) {
                        edtcountry.setError(getResources().getString(R.string.Enter_country));
                        edittexterror = true;
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (!android.util.Patterns.PHONE.matcher(edtphone.getText().toString().trim()).matches()) {
                        edtphone.setError(getResources().getString(R.string.Invalid_Phone));
                        edittexterror = true;
                    }

                    else if (edtshopname.getText().toString().isEmpty()) {
                        edtshopname.setError(getResources().getString(R.string.Enter_shop_name));
                        edittexterror = true;
                    }
                    if (edittexterror == false) {
                        name=edtyourname.getText().toString();
                        shop_name=edtshopname.getText().toString();
                        location=edtcountry.getText().toString();
                        phone=edtphone.getText().toString();
                        email=edtemail.getText().toString();
                        message=edtmsg.getText().toString();
                        new PostFeedback().execute();
                    }

                } else {
                    com.nispok.snackbar.Snackbar.with(Retailer_FeedBack.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(Retailer_FeedBack.this);


                }





            }
        });

    }

    private void goBack() {
        super.onBackPressed();
    }


    class PostFeedback extends AsyncTask<String,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try{
                URL url = new URL(SERVER+"json/retailer/feedback.php"); // here is your URL path


                JSONObject postDataParams = new JSONObject();

                postDataParams.put("name",name);
                postDataParams.put("shop_name",shop_name);
                postDataParams.put("location",location);
                postDataParams.put("phone",phone);
                postDataParams.put("email",email);
                postDataParams.put("message",message);
                postDataParams.put("user_id",userid);
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e)

            {
                return new String("Exception: " + e.getMessage());
            }


        }

        @Override
        protected void onPostExecute(String result) {


          //  Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            super.onPostExecute(result);
            progress_loader.setVisibility(View.GONE);


            if (result.equals("\"failed\"")) {

                new SweetAlertDialog(Retailer_FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .show();
                //  String named = jsonObj.getString("name");

            }else if(result.equals("\"success\"")){
                new SweetAlertDialog(Retailer_FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.ThankYou))
                        .setContentText(getResources().getString(R.string.Feedbacksubmitted))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)
                       // .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent i=new Intent(getApplicationContext(),Fresh_Frozen_Retailer.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);

                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();



            }
            else {
                new SweetAlertDialog(Retailer_FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }


        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}
