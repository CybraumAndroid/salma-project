package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;


import java.net.UnknownHostException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by libin on 9/29/2016.
 */
public class Customer_Butcher_List_adapter extends RecyclerView.Adapter<Customer_Butcher_List_adapter.ViewHolder> {


    private ArrayList<Customer_Butcher_List_Model> zwm;
    Context context;
    String added_id,cat_id;
    String d,k;
    public boolean[] checkboxStatus;
    DatabaseHandler dh=new DatabaseHandler();

    public Customer_Butcher_List_adapter(ArrayList<Customer_Butcher_List_Model> zwm, Context context) {
        this.zwm = zwm;
        this.context = context;
        this.checkboxStatus = new boolean[zwm.size()];
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3,fav_;
        // TextView personAge;
        private TextView fav_btn;
        public static MKLoader progress_loader;
        ImageView personPhoto;
LinearLayout lt1,lt2;
ProgressBar gallery_progressbar;
        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            tv=   (TextView) itemView.findViewById(R.id.textView14);
            tv1=   (TextView) itemView.findViewById(R.id.shp_nme);
            tv2=   (TextView) itemView.findViewById(R.id.dlvry_info);
            tv3=   (TextView) itemView.findViewById(R.id.dscnts);
            fav_btn=   (TextView) itemView.findViewById(R.id.fvts);
            fav_=   (TextView) itemView.findViewById(R.id.fav);
            v=   (ImageView) itemView.findViewById(R.id.imageView8);
            lt1= (LinearLayout) itemView.findViewById(R.id.lt1);
            lt2= (LinearLayout) itemView.findViewById(R.id.lt2);
            gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
        }
    }


    @Override
    public int getItemCount() {
        return zwm.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.customer_butcher_list_item_layout, viewGroup, false);
        ViewHolder pvh = new ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder personViewHolder, final int i) {
        Cursor rs=dh.checkIfAdded(context,zwm.get(i).getButcher_logo(),zwm.get(i).getShop_name(),zwm.get(i).butcher_discount,zwm.get(i).getDelivery_info(),zwm.get(i).getAdded_by(),zwm.get(i).getCat_id());
        System.out.println("rs  :  "+rs);
        if (rs.getCount()!=0)//checking if already added to FAV
        {
            //Toast.makeText(context, "Already Added", Toast.LENGTH_SHORT).show();
            personViewHolder.fav_btn.setBackgroundResource(R.drawable.ic_favorite_border_red_24dp);
        }
        else {
            personViewHolder.fav_btn.setBackgroundResource(R.drawable.ic_favorite_border_white_24dp);
        }
       // d=zwm.get(i).getShop_name();
       // k=zwm.get(i).getDelivery_info();

        Typeface myFont7 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.fav_.setTypeface(myFont7);
        String s = zwm.get(i).getButcher_logo();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
      personViewHolder.tv1.setTypeface(myFont2);
             personViewHolder.tv1.setText(zwm.get(i).getShop_name());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+zwm.get(i).getShop_name());

        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont3);
        personViewHolder.tv2.setText(zwm.get(i).getDelivery_info());

        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
      personViewHolder.tv3.setTypeface(myFont4);
        personViewHolder.tv3.setText(zwm.get(i).getDistance());


if (zwm.get(i).getButcher_discount().equalsIgnoreCase("")){

    personViewHolder.tv.setVisibility(View.GONE);
}else {
    Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "AlexBrush-Regular.ttf");
    personViewHolder.tv.setTypeface(myFont5);
        /*zwm.get(i).getShop_description()*/
    personViewHolder.tv.setText(zwm.get(i).getButcher_discount());
    personViewHolder.tv.setTextColor(Color.parseColor("#d62041"));
}
        //  String img=enm.get(i).getNews_img();
        System.out.println("ooo" + s);
     //   Picasso.with(context).load(s).noFade().into(personViewHolder.v);
        try {
            new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .build()
                    .load(s)
                    .noFade()
                    .into(personViewHolder.v, new Callback() {
                        @Override
                        public void onSuccess() {
                            personViewHolder.progress_loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });


    }catch (Exception e){
        e.printStackTrace();
    }

        personViewHolder.lt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                added_id=zwm.get(i).getAdded_by();
                cat_id=zwm.get(i).getCat_id();
                System.out.println("<<<<<<<<<<<<<<<<,subin>>>>>>>>>>>>>"+added_id+cat_id);
                Intent i=new Intent(context,Customer_Butcher_Sublist_Fragment.class);
                i.putExtra("add_id",added_id);
                i.putExtra("cat_id",cat_id);
                // i.putExtra("shp_nam",d);
                //  i.putExtra("del_info",k);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                //i.putExtra("value",status);
                context.startActivity(i);
                //  dialog.setContentView(R.layout.loc_popup);
             /*   new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(context.getResources().getString(R.string.location))
                        .setContentText(context.getResources().getString(R.string.DidyouChoosetherightlocation))
                        .setCustomImage(R.drawable.shop)
                        .setConfirmText(context.getResources().getString(R.string.Yes))
                        .setCancelText(context.getResources().getString(R.string.No))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Customer_Butcher_List_Fragment.changeindex();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                sDialog.dismiss();
                                       *//* .setTitleText("Deleted!")
                                        .setContentText("Your imaginary file has been deleted!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*//*
                            }
                        })
                        .show();
*/

            }
        });
        personViewHolder.tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                added_id=zwm.get(i).getAdded_by();
                cat_id=zwm.get(i).getCat_id();
                View popupView = LayoutInflater.from(view.getRootView().getContext()).inflate(R.layout.festival_popup_layout, null);
                final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                //  dialog.setContentView(R.layout.loc_popup);
              //  final Window window = dialog.getWindow();


                popupWindow.setAnimationStyle(R.style.DialogAnimation);
               // popupWindow.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                popupWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                TextView offer=(TextView) popupView.findViewById(R.id.offer);
                Button buy_now= (Button) popupView.findViewById(R.id.button2);
                ImageView dropDownImage = (ImageView) popupView.findViewById(R.id.clse);
                dropDownImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                    }
                });

                offer.setText(zwm.get(i).getDiscount_desc());




                buy_now.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i=new Intent(context,Customer_Butcher_Sublist_Fragment.class);
                        i.putExtra("add_id",added_id);
                        i.putExtra("cat_id",cat_id);
                        // i.putExtra("shp_nam",d);
                        //  i.putExtra("del_info",k);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        //i.putExtra("value",status);
                        context.startActivity(i);
                        popupWindow.dismiss();
                    }
                });


              /*  new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("Festival Offer")
                        .setContentText(zwm.get(i).getDiscount_desc())

                        .setConfirmText("Buy Now")

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                Intent i=new Intent(context,Customer_Butcher_Sublist_Fragment.class);
                                i.putExtra("add_id",added_id);
                                i.putExtra("cat_id",cat_id);
                                // i.putExtra("shp_nam",d);
                                //  i.putExtra("del_info",k);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                //i.putExtra("value",status);
                                context.startActivity(i);

                                sweetAlertDialog.dismiss();
                            }
                        })
                 .show();*/
                popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            }
        });
        personViewHolder.lt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                added_id=zwm.get(i).getAdded_by();
                cat_id=zwm.get(i).getCat_id();
                System.out.println("<<<<<<<<<<<<<<<<,subin>>>>>>>>>>>>>"+added_id+cat_id);
            /*    //  dialog.setContentView(R.layout.loc_popup);
                new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(context.getResources().getString(R.string.location))
                        .setContentText(context.getResources().getString(R.string.DidyouChoosetherightlocation))
                        .setCustomImage(R.drawable.shop)
                        .setConfirmText(context.getResources().getString(R.string.Yes))
                        .setCancelText(context.getResources().getString(R.string.No))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Customer_Butcher_List_Fragment.changeindex();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                sDialog.dismiss();
                                       *//* .setTitleText("Deleted!")
                                        .setContentText("Your imaginary file has been deleted!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*//*
                            }
                        })
                        .show();*/
                Intent i=new Intent(context,Customer_Butcher_Sublist_Fragment.class);
                i.putExtra("add_id",added_id);
                i.putExtra("cat_id",cat_id);
                // i.putExtra("shp_nam",d);
                //  i.putExtra("del_info",k);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                //i.putExtra("value",status);
                context.startActivity(i);
            }
        });
     personViewHolder.fav_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    ///////////////////////////////////////////////// checking if already added ---start//////////////////////
                    Cursor rs=dh.checkIfAdded(context,zwm.get(i).getButcher_logo(),zwm.get(i).getShop_name(),zwm.get(i).butcher_discount,zwm.get(i).getShop_addrs(),zwm.get(i).getAdded_by(),zwm.get(i).getCat_id());
                    System.out.println("rs  :  "+rs);

                    System.out.println("checked shop name : "+zwm.get(i).getShop_name());

                    if (rs.getCount()!=0)//checking if already added to FAV
                    {
                        new SweetAlertDialog(view.getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                .setTitleText(context.getResources().getString(R.string.AlreadyinFavourites))
                                .setContentText(context.getResources().getString(R.string.Wontbeabletoaddagain))
                                .setCustomImage(R.drawable.logo)
                                .showCancelButton(true)
                                .setConfirmText(context.getResources().getString(R.string.ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                       /* Intent i=new Intent(context,Favorites.class);
                                        // i.putExtra("add_id",added_id);
                                        //  i.putExtra("cat_id",cat_id);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        //i.putExtra("value",status);
                                        context.startActivity(i);*/
                                        sweetAlertDialog.cancel();
                                    }
                                })
                                .show();
                        final Snackbar snackbar = Snackbar
                                .make(view, context.getResources().getString(R.string.AlreadyinFavourites), Snackbar.LENGTH_LONG)
                                .setAction(context.getResources().getString(R.string.View), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent i=new Intent(context,Favorites.class);
                                        // i.putExtra("add_id",added_id);
                                        //  i.putExtra("cat_id",cat_id);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        //i.putExtra("value",status);
                                        context.startActivity(i);




                                    }
                                });
                        snackbar.setActionTextColor(Color.RED);
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                       personViewHolder.fav_btn.setBackgroundResource(R.drawable.ic_favorite_border_red_24dp);
                        notifyDataSetChanged();

                    }
                    ///////////////////////////////////////////////// checking if already added ---end///////////////////////
                    else{
                        ///////////////////////////////////////////////// inserting ---start////////////////////////////////////
                        dh.InsertIntoSqlite(context,zwm.get(i).getButcher_logo(),zwm.get(i).getShop_name(),zwm.get(i).getShop_addrs(),zwm.get(i).getButcher_discount(),zwm.get(i).getAdded_by(),zwm.get(i).getCat_id());

                        new SweetAlertDialog(view.getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(context.getResources().getString(R.string.AddedinFavourites))
                                //.setContentText("Won't be able to add again!")
                                .setConfirmText(context.getResources().getString(R.string.ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                     /*   Intent i=new Intent(context,Favorites.class);
                                        // i.putExtra("add_id",added_id);
                                        //  i.putExtra("cat_id",cat_id);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        //i.putExtra("value",status);
                                        context.startActivity(i);*/
                                        sweetAlertDialog.cancel();
                                    }
                                })
                                .showCancelButton(true)
                                .show();
                        final Snackbar snackbar = Snackbar
                                .make(view, context.getResources().getString(R.string.AddedinFavourites), Snackbar.LENGTH_LONG)
                                .setAction(context.getResources().getString(R.string.View), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent i=new Intent(context,Favorites.class);
                                       // i.putExtra("add_id",added_id);
                                      //  i.putExtra("cat_id",cat_id);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        //i.putExtra("value",status);
                                        context.startActivity(i);




                                    }
                                });
                        snackbar.setActionTextColor(Color.RED);
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();

                        personViewHolder.fav_btn.setBackgroundResource(R.drawable.ic_favorite_border_red_24dp);
                        notifyDataSetChanged();
                        ///////////////////////////////////////////////// inserting ---end////////////////////////////////////
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                    com.nispok.snackbar.Snackbar.with(view.getContext()) // context
                            .text("Failed!") // text to display
                            .show((Activity) view.getContext());

                }
            }
        });
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}