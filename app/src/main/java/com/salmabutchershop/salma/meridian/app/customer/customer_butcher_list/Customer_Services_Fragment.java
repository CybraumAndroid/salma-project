package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andremion.counterfab.CounterFab;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.NetworkCheckingClass;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Contact_Our_Team;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Contact_Our_Team1;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Model;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_adapter;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Favorites;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.database.OrderNew_Activity;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.myaccount.My_Account_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.notify.PushNotifiction;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.FeedBack;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.NewsUpdates;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 9/29/2016.
 */

public class Customer_Services_Fragment extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.OnConnectionFailedListener {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;
    //  static ArrayList<> upm;
    Customer_Services_adapter adapter;
    ProgressBar progress;
    String result, galry_image, Dat;
    static ArrayList<Customer_Services_Model> csm;
    Customer_Services_Model cm;
    String cat_id, main_cat_id, category_name, cat_image;
    NavigationView navigationView;
    String Reg_id, fullname, typ, latest_version;
    String userid;
    TextView t2, t3;
    ImageView ntfy;
    static TextView txtCount;
    String Wurl, flag_id,enable_popup;
    private static long back_pressed;
    private static final int TIME_DELAY = 2000;
    String strVersion = "";
    public static MKLoader progress_loader;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    //ProgressBar progress;
    String str;
    private static final String API_KEY = "AIzaSyAo27AsUvSaSFEQ5OZWGc4l2SczpZhwnL4";
    private static final String RADIOUS = "&radius";
    //String cat_id;
    static String cid;
    public GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    SharedPreferences preferencesd;
    public static CounterFab counterFab;
    String social_status,social_network,social_user_id="",social_name,social_email,social_gender,facebook_login_status,social_lat,social_long,refreshedToken,social_country,social_city;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
         counterFab = (CounterFab) findViewById(R.id.counter_fab);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
        preferencesd = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        userid = preferencesd.getString("user_id", null);
        cid = preferencesd.getString("city_id", null);

        final DatabaseHelper dataHelper = new DatabaseHelper(Customer_Services_Fragment.this);
        final int si = (int) dataHelper.getProfilesCount();

        counterFab.setCount(si);
        try{
            social_user_id=preferencesd.getString("social_user_id", null);
        }catch (Exception e){e.printStackTrace();}

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("778644506647-fdq786qefvscbvjggjd3kbdtk12t4j7c.apps.googleusercontent.com")
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();


        /*social_lat = preferencesd.getString("latitude", null);
        social_long = preferencesd.getString("longitude", null);
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        social_country = preferencesd.getString("cntry", null);
        social_city = preferencesd.getString("citys", null);*/
try{
        SharedPreferences preferences = getSharedPreferences("SocialPref", MODE_PRIVATE);
        social_status=preferences.getString("sociallogin",null);
        facebook_login_status=preferences.getString("facebook",null);
        System.out.println("social_status : "+social_status);
        if (social_status.equalsIgnoreCase("true")) {
            if(facebook_login_status.equalsIgnoreCase("true")){
                social_network="facebook";

            }else{
                social_network="google";

            }

            social_user_id=preferencesd.getString("user_id", null);
            social_email=preferencesd.getString("social_email", null);
            social_gender=preferencesd.getString("social_gender", null);
            social_name=preferencesd.getString("social_name", null);
            social_lat = preferencesd.getString("latitude", null);
            social_long = preferencesd.getString("longitude", null);
            refreshedToken = FirebaseInstanceId.getInstance().getToken();
            social_country = preferencesd.getString("cntry", null);
            social_city = preferencesd.getString("citys", null);



            if (DetectConnection
                    .checkInternetConnection(getApplicationContext())) {


                new SendSocialRegistration().execute();
            } else {
                com.nispok.snackbar.Snackbar.with(Customer_Services_Fragment.this) // context
                        .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                        .show(Customer_Services_Fragment.this);


            }

        }}catch (Exception e){e.printStackTrace();}

        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<Customerloginid1>>>>>>>>>>>>>>>>>>>>>>" + userid);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //changeOverflowMenu();
        // setSupportActionBar(toolbar);
        // getSupportActionBar().setIcon (R.mipmap.ic_launcher);
        getCurrentVersionInfo();
        txtCount = (TextView) findViewById(R.id.txtCount);


       // FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        counterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (si > 0) {
                    Intent in = new Intent(Customer_Services_Fragment.this, OrderNew_Activity.class);
                    startActivity(in);
                    finish();
                } else {
                    Snackbar.make(view,R.string.Cart_is_empty, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        fullname = myPrefs.getString("fullname", null);
        typ = myPrefs.getString("usertype", null);
        flag_id = myPrefs.getString("flag_id", null);
        t2 = (TextView) header.findViewById(R.id.nmae);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        t2.setTypeface(myFont7);
        t2.setText(fullname);
        t3 = (TextView) header.findViewById(R.id.textView);
        Typeface myFont9 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        t3.setTypeface(myFont9);
        t3.setText(typ);
        // Toast.makeText(this, myPrefs.getString("username", ""), Toast.LENGTH_LONG).show();

        if (userid != null) {   // condition true means user is already login
            //  navigationView.getMenu().getItem(6).setIcon(R.drawable.zayed_logout);


            navigationView.getMenu().getItem(5).setTitle(getResources().getString(R.string.logout));
            navigationView.getMenu().getItem(4).setVisible(true);
            //  navigationView.getMenu().getItem(6).setIcon(R.drawable.login);
            System.out.println("logoutt");


        } else {
            navigationView.getMenu().getItem(4).setVisible(false);
            navigationView.getMenu().getItem(5).setTitle(getResources().getString(R.string.signin));
            System.out.println("loginnn");



        }

        if(preferencesd.getString("social_user_id", null)!=null||preferencesd.getString("social_user_id", null)!=""){
            navigationView.getMenu().getItem(5).setTitle(getResources().getString(R.string.logout));
            navigationView.getMenu().getItem(4).setVisible(true);
        }
        ntfy = (ImageView) findViewById(R.id.imageView4);
        ntfy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtCount.setVisibility(View.GONE);
                Intent i = new Intent(Customer_Services_Fragment.this, PushNotifiction.class);
                startActivity(i);
                finish();
            }
        });
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context = getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 2));


        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {


            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Customer_Services_Fragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Customer_Services_Fragment.this);


        }
        // new DownloadData1().execute();

    }

    private String getCurrentVersionInfo() {


        PackageInfo packageInfo;
        try {
            packageInfo = getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(
                            getApplicationContext().getPackageName(),
                            0
                    );
            strVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        return strVersion;
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

            intent.putExtra("EXIT", true);
            startActivity(intent);
// dialog.dismiss();
            finishAffinity(); ;
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        com.nispok.snackbar.Snackbar.with(Customer_Services_Fragment.this) // context
                .text(R.string.Press_again_to_exit_from_salma)  // text to display
                .show(Customer_Services_Fragment.this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            if (userid != null||social_user_id!=null||social_user_id!="") {   // condition true means user is already login

                Intent i = new Intent(Customer_Services_Fragment.this, My_Account_Fragment.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                System.out.println("logoutt");


            } else {
                new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.drawable.logo)
                        .setTitleText(getResources().getString(R.string.Pleaseregister))
                        .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                        .setConfirmText(getResources().getString(R.string.Yes))
                        .setCancelText(getResources().getString(R.string.No))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(i);
                                finish();
                                sDialog.dismiss();

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


                System.out.println("loginnn");


            }

        } else if (id == R.id.nav_manage) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(Customer_Services_Fragment.this, Contact_Our_Team1.class);
            startActivity(i);
            finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        } else if (id == R.id.nav_gallery) {
            Intent i = new Intent(Customer_Services_Fragment.this, Favorites.class);
            startActivity(i);
            finish();
            // overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
        } else if (id == R.id.news) {
            Intent i = new Intent(Customer_Services_Fragment.this, NewsUpdates.class);
            startActivity(i);
            finish();
            //overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
        } else if (id == R.id.nav_slideshow) {
            Intent i = new Intent(Customer_Services_Fragment.this, FeedBack.class);
            startActivity(i);
            finish();
            // overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
        } else if (id == R.id.nav_manage) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(Customer_Services_Fragment.this, Contact_Our_Team1.class);
            startActivity(i);
            finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        } else if (id == R.id.nav_Terms) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(Customer_Services_Fragment.this, com.salmabutchershop.salma.meridian.app.customer.sidebar.Terms_Conditions.class);
            startActivity(i);
            finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        } else if (id == R.id.nav_Rate) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salmabutchershop.salma.meridian.app"));
            startActivity(i);
            // finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        } else if (id == R.id.nav_logout) {
            // Toast.makeText(getApplicationContext(), "hiiiiiiiiii moto", Toast.LENGTH_SHORT).show();
            // Toast.makeText(getApplicationContext(),"test5",Toast.LENGTH_SHORT).show();

//            SharedPreferences settings = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//            settings.edit().clear().commit();
            if (navigationView.getMenu().getItem(5).getTitle() == getResources().getString(R.string.logout)) {
                new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.drawable.logo)
                        .setTitleText(getResources().getString(R.string.logout))
                        .setContentText(getResources().getString(R.string.AreyouSurewanttoLogoutnow))
                        .setConfirmText(getResources().getString(R.string.Yes))
                        .setCancelText(getResources().getString(R.string.No))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                logout();
                                sDialog.dismiss();

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            } else {


                Intent intlog = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intlog);
                finish();
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

            }
        } else if (id == R.id.nav_lang) {
            try {

                new SweetAlertDialog(Customer_Services_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Chooseyourlanguage))
                        // .setContentText("Won't be able to recover this file!")

                        .setCustomImage(R.drawable.logo)
                        .setCancelText(getResources().getString(R.string.Arabic))
                        .setConfirmText(getResources().getString(R.string.English))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                Locale locale2 = new Locale("ar");
                                Locale.setDefault(locale2);
                                Configuration config2 = new Configuration();
                                config2.locale = locale2;
                                getBaseContext().getResources().updateConfiguration(config2, null);
                                Intent thisIntent = getIntent();
                                startActivity(thisIntent);
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {


                                Locale locale2 = new Locale("en");
                                Locale.setDefault(locale2);
                                Configuration config2 = new Configuration();
                                config2.locale = locale2;
                                getBaseContext().getResources().updateConfiguration(config2, null);
                                Intent thisIntent = getIntent();
                                startActivity(thisIntent);
                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        progress_loader.setVisibility(View.VISIBLE);
        NetworkCheckingClass networkCheckingClass = new NetworkCheckingClass(getApplicationContext());
        boolean i = networkCheckingClass.ckeckinternet();
        if (i) {
            //  Toast.makeText(getApplicationContext(), "Social Media", Toast.LENGTH_SHORT).show();\
            if (social_status.equalsIgnoreCase("true")) {
                if(facebook_login_status.equalsIgnoreCase("true")){
                    LoginManager.getInstance().logOut();

                }else{
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    // ...

                                    mGoogleApiClient.disconnect();
                                    mGoogleApiClient.connect();
                                }
                            });


                }



            }


            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/logout.php?", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    System.out.println("reee" + response);
                    System.out.println("kkk" + response);

                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    userid = preferences.getString("user_id", null);
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<Customerloginid2>>>>>>>>>>>>>>>>>>>>>>" + userid);

                    preferences.edit().remove("user_id").commit();
                    preferences.edit().remove("social_user_id").commit();
                    progress.setVisibility(View.GONE);
                    Intent i = new Intent(Customer_Services_Fragment.this, LoginActivity.class);
                    startActivity(i);
                    finishAffinity();
                    // overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    if (social_status.equalsIgnoreCase("true")) {
                        params.put("user_id", preferencesd.getString("social_user_id", null));
                        System.out.println("social_user_id inside logout service : "+preferencesd.getString("social_user_id", null));
                        System.out.println("user_id inside logout service : "+userid);
                    }else {

                        params.put("user_id", userid);
                    }



                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            com.nispok.snackbar.Snackbar.with(Customer_Services_Fragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Customer_Services_Fragment.this);

        }
    }

    public static void changeindex() {
        txtCount.setVisibility(View.VISIBLE);
        txtCount.setText("1");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);

            String l = getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>" + l);
            if (l.equalsIgnoreCase("English")) {
                Wurl = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/product_cat.php";
            } else {
                Wurl = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/product_cat.php";
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
            if (result != null && !result.isEmpty() && !result.equals("null")) {


                JSONArray mArray;
                csm = new ArrayList<Customer_Services_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        cm = new Customer_Services_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //3 Log.d("OutPut", mJsonObject.getString("image"));

                        //  String ,,,,,,,;


                        cat_id = mJsonObject.getString("cat_id");
                        main_cat_id = mJsonObject.getString("main_cat_id");
                        category_name = mJsonObject.getString("category_name");
                        cat_image = mJsonObject.getString("cat_image");
                        latest_version = mJsonObject.getString("latest_version");
                        enable_popup= mJsonObject.getString("enable_popup");
                        // String sponsor_id,event_id,name,email,phone,photo,address,website,content;
                        cm.setSubcat_id(cat_id);
                        cm.setPro_cat_id(main_cat_id);
                        cm.setSubcat_name(category_name);
                        cm.setSub_image(cat_image);
                        cm.setLatest_version(latest_version);
                        cm.setEnable_popup(enable_popup);


                        csm.add(cm);

                        System.out.println("<<<oo>>>>" + cm);

                        adapter = new Customer_Services_adapter(csm, Customer_Services_Fragment.this);
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (latest_version.equalsIgnoreCase(strVersion)) {

                } else {
                    if (enable_popup.equalsIgnoreCase("no")){

                    }else {
                        displayPopup();
                    }
                }
            } else {
                if(!((Activity) Customer_Services_Fragment.this).isFinishing()) {
                    new SweetAlertDialog(Customer_Services_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(getResources().getString(R.string.Sorry))
                            .setContentText(getResources().getString(R.string.NoProducts))
                            .setConfirmText(getResources().getString(R.string.ok))
                            .setCustomImage(R.drawable.logo)

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/
                                    ;
                                    sDialog.dismiss();
                                }
                            })
                            .show();
                    System.out.println("nulllllllllllllllllllllllllllllllll");
                }
            }
            recyclerview.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            cat_id = csm.get(position).getSubcat_id();
                            if (cat_id.equalsIgnoreCase("6")) {

                                new SweetAlertDialog(Customer_Services_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(getResources().getString(R.string.Sorry))
                                        .setContentText(getResources().getString(R.string.Wearerapidlyexpandingwewillletyouknowwhenourshopinavailableinyourarea))
                                        .setConfirmText(getResources().getString(R.string.contactus))
                                        .setCustomImage(R.drawable.logo)
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                Intent i = new Intent(getApplicationContext(), Contact_Our_Team.class);


                                                startActivity(i);
                                                finish();
                                                sDialog.dismiss();

                                            }
                                        })
                                        .show();

                            } else {
                                new SweetAlertDialog(Customer_Services_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                                        .setCustomImage(R.drawable.logo)
                                        .setTitleText(getResources().getString(R.string.location))
                                        .setContentText(getResources().getString(R.string.PleasechooseLocation))
                                        .setCancelText(getResources().getString(R.string.Other)+"\n"+getResources().getString(R.string.location))
                                        .setConfirmText(getResources().getString(R.string.Current)+"\n"+getResources().getString(R.string.location))
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                final Dialog dialog = new Dialog(Customer_Services_Fragment.this);
                                                final Window window = dialog.getWindow();
                                                window.requestFeature(Window.FEATURE_NO_TITLE);
                                                window.setContentView(R.layout.loc_test);
                                                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                                                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                                                AutoCompleteTextView autoCompView = (AutoCompleteTextView) dialog.findViewById(R.id.autoCompleteTextView);
                                                //tc= (TextView).p findViewById(R.id.tc);
                                                autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter1(Customer_Services_Fragment.this, R.layout.list_item));
                                                autoCompView.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                                                    }
                                                });
                                                //autoCompView.setOnItemClickListener(Customer_Butcher_List_Fragment.this);
                                                autoCompView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                        str = (String) adapterView.getItemAtPosition(i);
                                                        Intent is = new Intent(getApplicationContext(), Customer_Butcher_List_Fragment.class);
                                                        is.putExtra("cat_id", cat_id);
                                                        is.putExtra("value",str);
                                                        startActivity(is);
                                                        finish();

                                                        // Toast.makeText(Customer_Butcher_List_Fragment.this, str, Toast.LENGTH_SHORT).show();
                                                        //  new getLatLongFromGivenAddress1().execute;
                                                        // new DownloadData3().execute();

                                                        dialog.dismiss();
                                                    }
                                                });
                                                dialog.show();
                                                //finish();
                                                sweetAlertDialog.dismiss();
                                            }
                                        })
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                         /*       Intent is = new Intent(getApplicationContext(), DashboardActivity1.class);

                                                startActivity(is);
                                                sDialog.dismiss();*/

                                                Intent i = new Intent(getApplicationContext(), Customer_Butcher_List_Fragment.class);
                                                i.putExtra("cat_id", cat_id);
                                                i.putExtra("value","1");
                                                startActivity(i);
                                                finish();
                                                sDialog.dismiss();
                       /*     .setTitleText("We are rapidly expanding!")
                            .setContentText("We will let you \n know when our shop is available in your Area")
                            .setConfirmText("OK")
                            .setConfirmClickListener(null)
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*/

                                            }

                                        })
                                        .show();



                            }

                        }

                    })
            );


        }


    }

    private void displayPopup() {
        if (!((Activity) Customer_Services_Fragment.this).isFinishing()) {
            new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("New Update is Available")
                    .setContentText("Update Now ?")
                    .setConfirmText("Yes")
                    .setCancelText("No")
                    .setCustomImage(R.drawable.logo)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salmabutchershop.salma.meridian.app"));
                            startActivity(i);


                            sDialog.dismiss();

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
        }
    }
    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components="+"country:"+cid);
            sb.append("&radius=3000");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            trustEveryone();
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    private static void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }
    public class GooglePlacesAutocompleteAdapter1 extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter1(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
    private class SendSocialRegistration extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
            progress_loader.setVisibility(View.GONE);
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL("http://app.salmacorp.com/json/customer/social-login.php"); // here is your URL path


                System.out.println("social_type : "+social_network);
                System.out.println("uniqueid : "+social_user_id);
                System.out.println("fullname : "+social_name);
                System.out.println("email : "+social_name);
                System.out.println("token : "+refreshedToken);
                System.out.println("country : "+social_country);
                System.out.println("city : "+social_city);
                System.out.println("longitude : "+social_long);
                System.out.println("latitude : "+social_lat);


                System.out.println("social_user_id : "+preferencesd.getString("social_user_id", null));
                String s_u_id=preferencesd.getString("social_user_id", null);

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("social_type",social_network);
                postDataParams.put("uniqueid",s_u_id);
                postDataParams.put("fullname", social_name);
                postDataParams.put("email", social_email);
                postDataParams.put("token",refreshedToken);
                postDataParams.put("country",social_country);
                postDataParams.put("city", social_city);
                postDataParams.put("longitude",social_long);
                postDataParams.put("latitude",social_lat);
                postDataParams.put("type","1");
                postDataParams.put("phone","");
                postDataParams.put("location","");
                postDataParams.put("profilepic","");
                postDataParams.put("username","");
                postDataParams.put("password","");





                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    System.out.println("registration result : " + sb.toString());
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {

            progress_loader.setVisibility(View.GONE);
       /*   Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();
*/
            try {
                System.out.println("registration result : " + result);
                JSONObject jsonObject=new JSONObject(result);
                String status=jsonObject.getString("status");
                if(status.equalsIgnoreCase("true")){
                    navigationView.getMenu().getItem(5).setTitle(getResources().getString(R.string.logout));
                    navigationView.getMenu().getItem(4).setVisible(true);

                    String id=jsonObject.getString("id");
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("user_id", id);
                    userid=id;
                    editor.commit();

                }

                if(status.equalsIgnoreCase("success")){
                    navigationView.getMenu().getItem(5).setTitle(getResources().getString(R.string.logout));
                    navigationView.getMenu().getItem(4).setVisible(true);

                    JSONObject jsonObjdata=jsonObject.getJSONObject("data");

                    String id=jsonObjdata.getString("id");
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("user_id", id);
                    userid=id;
                    editor.commit();
                }



                System.out.println("------------------------Social_user_id and user id in variables-----------------------");
                System.out.println("social_user_id : "+social_user_id);
                System.out.println("user_id : "+userid);

                System.out.println("------------------------Social_user_id and user id in sharedpreference-----------------------");
                System.out.println("social_user_id : "+preferencesd.getString("social_user_id", null));
                System.out.println("user_id : "+preferencesd.getString("user_id", null));




            }catch (Exception e){
                e.printStackTrace();
            }

        }


    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        System.out.println("result.toString()"+result.toString());
        return result.toString();
    }
}