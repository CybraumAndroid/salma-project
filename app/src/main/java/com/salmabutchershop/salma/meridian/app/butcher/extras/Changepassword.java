package com.salmabutchershop.salma.meridian.app.butcher.extras;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.nispok.snackbar.Snackbar;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.R.id.edt_oldp;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/2/2016.
 */

public class Changepassword extends Activity {
    EditText Old_p,New_p,Cnf_p;
    String Old_p_S,New_p_S,Cnf_p_S;
    Button submit;
    ProgressBar progress;
    String Reg_id;
    boolean edittexterror = false;
    static String filename = "null", filenamepath = "null";
    public static MKLoader progress_loader;
    LinearLayout password_eye_old_show,password_eye_old_hide,password_eye_new_show,password_eye_new_hide,password_eye_cnf_show,password_eye_cnf_hide;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        Old_p= (EditText) findViewById(edt_oldp);
        New_p= (EditText) findViewById(R.id.edt_newp);
        Cnf_p= (EditText) findViewById(R.id.edt_cnp);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Changepassword.this.goBack();
            }
        });
        final Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");

        password_eye_old_show= (LinearLayout) findViewById(R.id.password_eye_old_show);
        password_eye_old_hide=(LinearLayout) findViewById(R.id.password_eye_old_hide);
        password_eye_new_show= (LinearLayout) findViewById(R.id.password_eye_new_show);
        password_eye_new_hide=(LinearLayout) findViewById(R.id.password_eye_new_hide);
        password_eye_cnf_show= (LinearLayout) findViewById(R.id.password_eye_cnf_show);
        password_eye_cnf_hide=(LinearLayout) findViewById(R.id.password_eye_cnf_hide);

        password_eye_old_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_old_show.setVisibility(View.GONE);
                password_eye_old_hide.setVisibility(View.VISIBLE);
                Old_p.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                Old_p.setTypeface(myFont1);
                Old_p.setSelection(Old_p.length());
            }
        });

        password_eye_old_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_old_hide.setVisibility(View.GONE);
                password_eye_old_show.setVisibility(View.VISIBLE);

                Old_p.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                Old_p.setTypeface(myFont1);
                Old_p.setSelection(Old_p.length());

            }
        });
        password_eye_new_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_new_show.setVisibility(View.GONE);
                password_eye_new_hide.setVisibility(View.VISIBLE);
                New_p.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                New_p.setTypeface(myFont1);
                New_p.setSelection(New_p.length());
            }
        });

        password_eye_new_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_new_hide.setVisibility(View.GONE);
                password_eye_new_show.setVisibility(View.VISIBLE);

                New_p.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                New_p.setTypeface(myFont1);
                New_p.setSelection(New_p.length());

            }
        });
        password_eye_cnf_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_cnf_show.setVisibility(View.GONE);
                password_eye_cnf_hide.setVisibility(View.VISIBLE);
                Cnf_p.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                Cnf_p.setTypeface(myFont1);
                Cnf_p.setSelection(Cnf_p.length());
            }
        });

        password_eye_cnf_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_cnf_hide.setVisibility(View.GONE);
                password_eye_cnf_show.setVisibility(View.VISIBLE);

                Cnf_p.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                Cnf_p.setTypeface(myFont1);
                Cnf_p.setSelection(Cnf_p.length());

            }
        });
//password eye--end

        submit= (Button) findViewById(R.id.submit);

        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_idb", null);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)
                    Old_p_S = Old_p.getText().toString();
                    New_p_S = New_p.getText().toString();
                    Cnf_p_S = Cnf_p.getText().toString();

                    if (Old_p_S.matches("") || New_p_S.matches("") || Cnf_p_S.matches("")) {
                        Snackbar.with(Changepassword.this) // context
                                .text(R.string.empty_fields) // text to display
                                .show(Changepassword.this);
                        /*Snackbar.with(Changepassword.this) // context
                                .text("Empty Fields!") // text to display
                                .show(Changepassword.this);
                       *//* com.chootdev.csnackbar.Snackbar.with(Changepassword.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Empty Fields!")
                                .duration(Duration.SHORT)
                                .show();*/
                        edittexterror = true;
                        //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

                    } else if (Old_p_S.isEmpty()) {
                        Old_p.setError(getResources().getString(R.string.OldPassword));
                        edittexterror = true;
                    } else if (New_p.getText().toString().isEmpty()) {
                        New_p.setError(getResources().getString(R.string.NewPassword));
                        edittexterror = true;
                    } else if (Cnf_p.getText().toString().isEmpty()) {
                        Cnf_p.setError(getResources().getString(R.string.NewPassword));
                        edittexterror = true;
                    } else if (!Cnf_p_S.equalsIgnoreCase(New_p_S)) {

                        Cnf_p.setError("MisMatch");
                        edittexterror = true;
                    }
                    else if (filenamepath != null) {
                        filename = filenamepath;


                    } else {
                        filename = "null";

                    }

                    if (edittexterror == false) {
                        new SendPostRequest3().execute();

                    }

                   /* alertDialog.show();
                        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL);
                        nbutton.setTextColor(getResources().getColor(R.color.dot_dark_screen1));
                        nbutton.setGravity(Gravity.CENTER);
                        nbutton.setBackgroundColor(getResources().getColor(R.color.dot_light_screen1));


                    } else {

                        new SendPostRequest3().execute();


                    }*/

                } else {
                    Snackbar.with(Changepassword.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off))  // text to display
                            .show(Changepassword.this);
                   /* Snackbar.with(Changepassword.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(Changepassword.this);*/
                   /* com.chootdev.csnackbar.Snackbar.with(Changepassword.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/


                }

            }
        });
    }

    private void goBack() {
        Intent i=new Intent(getApplicationContext(),MainFragment.class);
        startActivity(i);
        finish();
    }

    private class SendPostRequest3 extends AsyncTask<String, Void, String> {

        public void onPreExecute() {
            progress_loader.setVisibility(View.VISIBLE);
        }

        public String doInBackground(String... arg0) {

            try {
             //   http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/changepassword.php?user_id=152&c_password=123&new_password=1234
                URL url = new URL(SERVER+"json/butcher/changepassword.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();

               /* c_password
                        new_password
                user_id*/

              //
                postDataParams.put("c_password", Old_p_S);
                postDataParams.put("new_password", New_p_S);
                postDataParams.put("user_id", Reg_id);
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
               /*Toast.makeText(LoginActivityRetailer.this, result,
                Toast.LENGTH_LONG).show();*/
            progress_loader.setVisibility(View.GONE);


            if (result.equals("\"failed\"")) {
                progress_loader.setVisibility(View.GONE);
                new SweetAlertDialog(Changepassword.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
             /*   MaterialStyledDialog dialog = new MaterialStyledDialog(Changepassword.this)
                        .setTitle("Failed!")
                        .setDescription("Invalid Credentials")
                        .setIcon(R.drawable.ic_error_white_24dp)
                        .withIconAnimation(true)
                        .withDialogAnimation(true)
                        .setHeaderColor(R.color.colorPrimary)
                        .setCancelable(true)
                        .setPositive(getResources().getString(R.string.ok), new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {


                                dialog.dismiss();
                                //finish();

                            }
                        })
                        .build();

                dialog.show();*/


                //  String named = jsonObj.getString("name");

            } else if(result.equals("\"success\"")){


                progress_loader.setVisibility(View.GONE);
                new SweetAlertDialog(Changepassword.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.AWESOME))
                        .setContentText(getResources().getString(R.string.SuccessfullyChangedPassword))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                SharedPreferences preferences= getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                //    String     userid = preferences.getString("user_idb", null);
                                preferences.edit().clear().commit();
                                Intent i=new Intent(getApplicationContext(),ButcherLogin.class);
                                startActivity(i);
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
                Snackbar.with(Changepassword.this) // context
                        .text(getResources().getString(R.string.SuccessfullyChangedPassword)) // text to display
                        .show(Changepassword.this);
               /* com.chootdev.csnackbar.Snackbar.with(Changepassword.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("Successfully Changed Password")
                        .duration(Duration.SHORT)
                        .show();*/




            }
            else {
                new SweetAlertDialog(Changepassword.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }
        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Intent inn = new Intent(getApplicationContext(), MainFragment.class);
            startActivity(inn);
            finish();
        } else {

            Intent inn = new Intent(getApplicationContext(), MainFragment.class);
            startActivity(inn);
            finish();
        }
    }
}
