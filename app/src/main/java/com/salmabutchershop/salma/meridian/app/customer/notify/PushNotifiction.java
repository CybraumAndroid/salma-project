package com.salmabutchershop.salma.meridian.app.customer.notify;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.firebase.messaging.FirebaseMessaging;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_my_order;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.myaccount.history.My_Coustomer_history_details;
import com.salmabutchershop.salma.meridian.app.firebase.app.Config;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.ContentValues.TAG;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

/**
 * Created by libin on 1/7/2017.
 */

public class PushNotifiction extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;

    Fragment fragment;
    View view;
    ProgressBar progress;
    RecyclerView recyclerview;
    String not_id,not_title,not_desc,order_id,status,not_date;

    PushNotifiction_adapter adapter;
    static ArrayList<PushNotifiction_Model> pem;
    PushNotifiction_Model pe;
    String id,event_id,exhibitors;
    String tag = "events";
    String result,Dat,time_diff;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    public static MKLoader progress_loader;
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.push_notification_layout);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        progress = (ProgressBar)findViewById(R.id.progress_bar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Dat= myPrefs.getString("user_id", null);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahhhhhhhhhhhhhhh>>>>>>>>>" +Dat);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter);
        Intent intent = getIntent();
        String message = intent.getStringExtra("message");

        try{
            if(!message.isEmpty()){
                System.out.println("<<<<<<<<<sharedname>>>>" + message);
                swipeRefreshLayout.setRefreshing(true);

            }}
        catch (Exception e){
            e.printStackTrace();
        }
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    //   Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                    System.out.println("<<<<<<<<<sharedname>>>>" + message);
                   // txtRegId .setText(message);
                }
            }
        };

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                startActivity(i);
                finish();
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                    new DownloadData().execute();
                } else {
                    com.nispok.snackbar.Snackbar.with(PushNotifiction.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(PushNotifiction.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/

                    //	Toast.makeText(getActivity(),
                    //	getResources().getString(R.string.Sorry) +
                    //	getResources().getString(R.string.cic),
                    //	Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(PushNotifiction.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(PushNotifiction.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/

            //	Toast.makeText(getActivity(),
            //	getResources().getString(R.string.Sorry) +
            //	getResources().getString(R.string.cic),
            //	Toast.LENGTH_SHORT).show();
        }



    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

    /*    if (!TextUtils.isEmpty(regId))
           // txtRegId .setText("Firebase Reg Id: " + regId);

        else
           // txtRegId .setText("Firebase Reg Id is not received yet!");*/
    }


    private void goBack() {
        super.onBackPressed();
    }

    private class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER_CUSTOMER+"notification.php?user_id="+Dat
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            // String sam = result.trim();
            swipeRefreshLayout.setRefreshing(false);
            progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
        /*    if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                //  mCountryModel1 = new ArrayList<>();
                pem = new ArrayList<PushNotifiction_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                        pe = new PushNotifiction_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //Log.d("OutPut", mJsonObject.getString("doctor_publish"));


                        not_id = mJsonObject.getString("not_id");
                        not_title = mJsonObject.getString("not_title");
                        not_desc = mJsonObject.getString("not_desc");
                        order_id = mJsonObject.getString("order_id");
                        status = mJsonObject.getString("status");
                        not_date = mJsonObject.getString("not_date");
                        time_diff= mJsonObject.getString("time_diff");

                        pe.setNot_id(not_id);
                        pe.setNot_title(not_title);
                        pe.setNot_desc(not_desc);
                        pe.setStatus(status);
                        pe.setNot_date(not_date);
                        pe.setOrder_id(order_id);
                        pe.setTime_diff(time_diff);

                        pem.add(pe);

                        System.out.println("" + pe);


                        adapter = new PushNotifiction_adapter(pem, getApplicationContext());
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                if(!((Activity) PushNotifiction.this).isFinishing())
                {
                    //show dialog

                new SweetAlertDialog(PushNotifiction.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.drawable.logo)
                        .setTitleText(getResources().getString(R.string.No_Notifications))
                       /* .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                        .setConfirmText(getResources().getString(R.string.Yes))*/
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                                startActivity(i);
                                finish();
                                sDialog.dismiss();

                            }
                        })
                       /* .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })*/
                        .show();
                }
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

            recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i=new Intent(getApplicationContext(),My_Coustomer_history_details.class);
                                i.putExtra("order_id",pem.get(position).getOrder_id());
                                startActivity(i);
                               /* android.support.v4.app.Fragment fragment = new Zayed_News_Event_Desc_Fragment();

                                FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                                        .beginTransaction();
                                transaction.replace(R.id.frame_container, fragment, tag);
                                // titleStack.add(tag);
                                Bundle args = new Bundle();
                                args.putInt("pos", position);
                                // args.putString("desc", up_evnt_desc);
                                //  args.putString("desimg", up_evnt_img);
                                //  System.out.println(">>>>>depppppppppppppppppppppppp<<<" + dept_id);
                                System.out.println(">>>>><<<" + position);
                                fragment.setArguments(args);
                                transaction.addToBackStack(tag).commit();*/
                            }
                        })
                );
            }
        }
    }

