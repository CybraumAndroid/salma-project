package com.salmabutchershop.salma.meridian.app.customer.address;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Rashid on 11/10/2016.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
private ArrayList<imageModel>imageNameList;
    private Context context1;
    DatabaseHandler_N dh=new DatabaseHandler_N();

    public DataAdapter(Context context2, ArrayList<imageModel>imageNames){
        this.imageNameList=imageNames;
        context1=context2;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String eventId=Integer.toString(imageNameList.get(position).getId());
        holder.eventId.setText(eventId);
        Picasso.with(context1)
                .load(imageNameList.get(position).getImage())
                .resize(1000,1000)
                .into(holder.imgView1);

        holder.fav_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i=new Intent(context1,Favorites.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("name",imageNameList.get(position).getImage());
                context1.startActivity(i);*/

                System.out.println("url "+imageNameList.get(position).getImage());
                try {
                        ///////////////////////////////////////////////// checking if already added ---start//////////////////////
                        Cursor rs=dh.checkIfAdded(context1,imageNameList.get(position).getImage());
                        System.out.println("rs  :  "+rs);
                        if (rs.getCount()!=0)//checking if already added to FAV
                        {
                            Toast.makeText(context1, "Already Added", Toast.LENGTH_SHORT).show();
                        }
                        ///////////////////////////////////////////////// checking if already added ---end///////////////////////
                    else{
                        ///////////////////////////////////////////////// inserting ---start////////////////////////////////////
                        dh.InsertIntoSqlite(context1,imageNameList.get(position).getImage(),imageNameList.get(position).getEvent_id(),imageNameList.get(position).extra_data);
                        Toast.makeText(context1, "Added", Toast.LENGTH_SHORT).show();
                        ///////////////////////////////////////////////// inserting ---end////////////////////////////////////
                        }
                    }
                catch(Exception e){
                        e.printStackTrace();
                        Toast.makeText(context1, "Failed", Toast.LENGTH_SHORT).show();
                    }
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageNameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
private ImageView imgView1;
    private Button fav_btn;
        private TextView eventId;
    public ViewHolder(View itemView) {
        super(itemView);
        imgView1=(ImageView)itemView.findViewById(R.id.imgView1);
        fav_btn=(Button)itemView.findViewById(R.id.button1);
        eventId=(TextView)itemView.findViewById(R.id.eventId);
    }

}}

