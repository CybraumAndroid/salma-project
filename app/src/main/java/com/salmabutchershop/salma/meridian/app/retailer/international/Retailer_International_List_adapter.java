package com.salmabutchershop.salma.meridian.app.retailer.international;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Model;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_adapter;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.DatabaseHandler;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

/**
 * Created by libin on 12/7/2016.
 */
public class Retailer_International_List_adapter extends RecyclerView.Adapter<Retailer_International_List_adapter.ViewHolder> {


    private ArrayList<Retailer_International_List_Model> rim;
    Context context;

    DatabaseHandler dh=new DatabaseHandler();

    public Retailer_International_List_adapter(ArrayList<Retailer_International_List_Model> rim, Context context) {
        this.rim = rim;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;
        private TextView fav_btn;
        ImageView personPhoto;
        ProgressBar gallery_progressbar;
        public static MKLoader progress_loader;
        LinearLayout lt1,lt2;
        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

          tv=   (TextView) itemView.findViewById(R.id.ads);
            tv1=   (TextView) itemView.findViewById(R.id.com_nam);
            tv2=   (TextView) itemView.findViewById(R.id.phne);
            tv3=   (TextView) itemView.findViewById(R.id.eml);
            gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
            v=   (ImageView) itemView.findViewById(R.id.com_logo);

        }
    }


    @Override
    public int getItemCount() {
        return rim.size();
    }


    @Override
    public Retailer_International_List_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.international_list_item_layout, viewGroup, false);
        Retailer_International_List_adapter.ViewHolder pvh = new Retailer_International_List_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Retailer_International_List_adapter.ViewHolder personViewHolder, final int i) {

        String s = rim.get(i).getLogo();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv1.setTypeface(myFont2);
        personViewHolder.tv1.setText(rim.get(i).getCom_name());


        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont3);
        personViewHolder.tv2.setText(rim.get(i).getPhon_no());

        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv3.setTypeface(myFont4);
        personViewHolder.tv3.setText(rim.get(i).getEmail());

        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont5);
        personViewHolder.tv.setText(rim.get(i).getAddress());
        //  String img=enm.get(i).getNews_img();
        System.out.println("ooo" + s);
        try {


        Picasso.with(context).load(s).resize(150, 150).noFade()//.into(personViewHolder.v);
                .into(personViewHolder.v, new Callback() {
                    @Override
                    public void onSuccess() {
                       personViewHolder.progress_loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}