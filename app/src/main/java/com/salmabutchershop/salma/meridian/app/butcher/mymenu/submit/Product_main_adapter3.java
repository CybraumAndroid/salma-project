package com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Product_mymenu_Model;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct.Product_Services_Model;

import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 4/30/2017.
 */

class Product_main_adapter3 extends RecyclerView.Adapter<Product_main_adapter3.ViewHolder> {


    List<Product_mymenu_Model> pmm;
    Context context;
    TextView qty;
    private int uprange = 20;
    private int downrange = 1;
    private int values = 1;
    private int lastPosition = -1;
    public Product_main_adapter3(ArrayList<Product_mymenu_Model> pmm, Context context) {
        this.pmm = pmm;
        this.context = context;

    }

    public Product_main_adapter3(BeefDetail beefDetail) {
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv, tv1, tv2, tv3;
        // TextView personAge;
        TextView title, qty, plus, minus, cart_txt;

        ImageView personPhoto;
        ProgressBar gallery_progressbar;
        LinearLayout ltx;
        public static MKLoader progress_loader;
        ViewHolder(View itemView) {
            super(itemView);

            tv = (TextView) itemView.findViewById(R.id.menu_nme);

            // gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            // progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
            // v = (ImageView) itemView.findViewById(R.id.beef_img);

        }
    }


    @Override
    public int getItemCount() {
        return pmm.size();
    }


    @Override
    public Product_main_adapter3.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.butcher_mymenu_item_layout3, viewGroup, false);
        Product_main_adapter3.ViewHolder pvh = new Product_main_adapter3.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Product_main_adapter3.ViewHolder personViewHolder, final int i) {



        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont2);
        personViewHolder.tv.setText(pmm.get(i).getType_name());
        Animation animation = AnimationUtils.loadAnimation(context,
                (i > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        personViewHolder.itemView.startAnimation(animation);
        lastPosition = i;

    }
    //System.out.println("ooo" + s);
     /*   try {


            Picasso.with(context).load(s).noFade()//.into(personViewHolder.v);
                    .into(personViewHolder.v, new Callback() {
                        @Override
                        public void onSuccess() {
                            personViewHolder.progress_loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });

        }catch (Exception e){
            e.printStackTrace();
        }

    }*/



    private void showpopup(View view) {

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public long getCount() {
        final DatabaseHelper dataHelper = new DatabaseHelper(context);
        int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        return  s;
    }
}