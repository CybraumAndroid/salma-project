package com.salmabutchershop.salma.meridian.app.customer.address;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.TimeZone;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Changepassword;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_my_order;
import com.salmabutchershop.salma.meridian.app.butcher.orderdetail.Butcher_MyOrder_New_Detail;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.CartModel;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.database.JsonModel;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 11/11/2016.
 */

public class AddressFragment extends AppCompatActivity {
    EditText fnames,lnames,citys,dlvry_area,street,building,floor,apartment_no,mob_no,email,loctn;
    Button submt;
    String name;
    Float price;
    String fnames_s,lnames_s,citys_s,dlvry_area_s,street_s,building_s,floor_s,apartment_no_s,mob_no_s,email_s,loctn_s,delivery_ads_s;
    RadioGroup radio_group;
    RadioButton radio_home,radio_work,radio_newh;
    String radio_home_s;
    ArrayList<CartModel> arrayListcart=new ArrayList<>();
    ArrayList<JsonModel>   array_lstcart;
    ArrayList<String> array_item=new ArrayList<>();
    ArrayList<String> array_qty=new ArrayList<>();
    ArrayList<String> array_product=new ArrayList<>();
    ArrayList<String> array_price_qty=new ArrayList<>();
    ArrayList<String> array_price=new ArrayList<>();
    ArrayList<String> array_butcher=new ArrayList<>();
    ArrayList<String> array_type=new ArrayList<>();
    ArrayList<String> array_tot=new ArrayList<>();
    ArrayList<String> some_jsn=new ArrayList<>();
    String sme_jsn;
    JsonModel s;
    SQLiteDatabase db;
    ProgressBar progress;
    JSONArray jsonArray1 = new JSONArray();
    String csv,csd,csf,csg,cbf,cpt,cty,ctt;
    boolean edittexterror = false;
    static String filename = "null", filenamepath = "null";
    private DatabaseHandler_N dh;
    private Button viewFav;
    private String[] values;
    String fn,ln,ci,buil,flo,apt,mob,em, loc,dla;
    LinearLayout Lx,Ly;
    String Reg_id,inst;
    String c_names,c_email,c_phone,c_location;
String Instructions,sub_tt,loc_ads;
    String city,state,postalCode,knownName,ads;
    double latitude;
    double longitude;
    String typeoforge;
    float grnd;
    String tt,ttnw;
   Float outlet_tot;
    String fultot;
    String Gtot;
    float gtot=0,cart_tot2=0;
    String refreshedToken,but_id,fbse_id;
    String grand_total,lng,lat,Cur_cy;
    String butcher_id,citysss;
    public static MKLoader progress_loader;
    String type_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adress_new_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        //   sub_tot= getIntent().getExtras().getInt("sub_tot");
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
     //   SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        // final String token = SharedPrefManager.getInstance(getApplicationContext()).getDeviceToken();
        //  token = SharedPrefManager.getInstance(this).getDeviceToken();

        fnames = (EditText) findViewById(R.id.us_nam);
        lnames = (EditText) findViewById(R.id.edt_lastname);

        citys = (EditText) findViewById(R.id.edt_city);
        dlvry_area = (EditText) findViewById(R.id.edt_dvry_area);
        street = (EditText) findViewById(R.id.edt_strt);
        building = (EditText) findViewById(R.id.edt_bldng);
        floor = (EditText) findViewById(R.id.edt_flr);
        apartment_no = (EditText) findViewById(R.id.edt_aptno);
        mob_no = (EditText) findViewById(R.id.edt_mble);
        email = (EditText) findViewById(R.id.edt_phone);
        loctn= (EditText) findViewById(R.id.edt_lctn);
        radio_group= (RadioGroup) findViewById(R.id.radio_group);
        fultot=getIntent().getExtras().getString("fultot");
        inst=getIntent().getExtras().getString("inst");
        System.out.println("-----------------------ads-----------------"+inst);
        if (getIntent().getExtras().getString("inst")!=null&&getIntent().getExtras().getString("inst").isEmpty()){
            inst=getIntent().getExtras().getString("inst");

        }else{

        }
        Intent intent = getIntent();
        but_id = intent.getStringExtra("but_id");
        System.out.println("-----------------------but_idassssssssss-----------------"+but_id);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);
        Gtot= myPrefs.getString("Grand_Total", null);
        fbse_id = myPrefs.getString("fbse_id", null);
        lng = myPrefs.getString("longitude", null);
        lat = myPrefs.getString("latitude", null);
        citysss=myPrefs.getString("citys", null);
        Cur_cy= myPrefs.getString("currency", null);
        System.out.println(">>>>>>subinlang>>>>>>>>>" + lng);
        System.out.println(">>>>>>subinlong>>>>>>>>>" + lat);
        System.out.println("<<<<<<<<<<<<<<<refnew>>>>>>>>"+fbse_id);
        loc_ads= myPrefs.getString("loc_ads", null);
        ads=myPrefs.getString("ads",null);
       city=myPrefs.getString("city",null);
        state=myPrefs.getString("state",null);
        postalCode=myPrefs.getString("postal",null);
        knownName=myPrefs.getString("knwn",knownName);

        c_names= myPrefs.getString("c_name", null);

        c_email= myPrefs.getString("c_email", null);
        c_phone= myPrefs.getString("c_phone", null);
        c_location= myPrefs.getString("c_location", null);
        if (Reg_id != null && !Reg_id.isEmpty()){
            fnames.setText(c_names);
            citys.setText(citysss);
            // lnames.setText(string2);
            mob_no.setText(c_phone);
            email.setText(c_email);
           // loctn.setText(c_location);
        }
        System.out.println("-----------------------but_idNNNNNNNNNNNN-----------------"+but_id);
        grand_total=getIntent().getExtras().getString("sub_tt");
     //   SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        //SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);


         System.out.println("bbbbbbbbbbbbbbbbbbbb"+c_names);
        System.out.println("cccccccccccccccccccc"+c_email);
        System.out.println("bbbbbbbbbbbbbbbbbbbb"+c_phone);
        System.out.println("cccccccccccccccccccc"+c_location);
        radio_home= (RadioButton) findViewById(R.id.home);
        radio_work= (RadioButton) findViewById(R.id.work);
        radio_newh= (RadioButton) findViewById(R.id.newh);

        Lx= (LinearLayout) findViewById(R.id.lx);
        Ly= (LinearLayout) findViewById(R.id.ly);
        if(getIntent().getExtras().getString("id_nam").equalsIgnoreCase("libin")){
            name = getIntent().getExtras().getString("name");
            price = getIntent().getExtras().getFloat("price");
            if(getIntent().getExtras().getString("inst")!=null&&getIntent().getExtras().getString("inst").isEmpty()){
                inst= getIntent().getExtras().getString("inst");
                sub_tt=getIntent().getExtras().getString("sub_tt");
            }
            else {
               /* inst= getIntent().getExtras().getString("inst");
                sub_tt=getIntent().getExtras().getString("sub_tt");*/
            }
          //  Lx.setVisibility(View.VISIBLE);
            Ly.setVisibility(View.VISIBLE);
        }
        else {

            fn= getIntent().getExtras().getString("fnames");
            ln= getIntent().getExtras().getString("lnames");
            ci= getIntent().getExtras().getString("citys");
            buil= getIntent().getExtras().getString("buildings");
            flo= getIntent().getExtras().getString("floors");
            apt= getIntent().getExtras().getString("apartrments");
            mob= getIntent().getExtras().getString("mobile");
            em= getIntent().getExtras().getString("phoe");
            loc= getIntent().getExtras().getString("loc");
            dla= getIntent().getExtras().getString("dlvry_area");
            fnames.setText(fn);
            lnames.setText(ln);
            citys .setText(ci);
            dlvry_area.setText(dla);
            street .setText(ci);
            building .setText(buil);
            floor.setText(flo);
            apartment_no.setText(apt);
            mob_no .setText(mob);
            email .setText(em);
         //  Lx.setVisibility(View.GONE);
            Ly.setVisibility(View.GONE);
        }

        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            String j =(String) b.get("name");

        }
        System.out.println("mappppppppppppppppppppppppppp" + loc_ads);
        if (loc_ads != null && !loc_ads.isEmpty()){
            loctn.setText(""+ads+"\n"+""+city+","+state+""+","+postalCode);
        }
        //  names= myPrefs.getString("fullname", null);
       // typ= myPrefs.getString("usertype", null);
        ImageView back= (ImageView) findViewById(R.id.back_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             finish();
            }
        });
    loctn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent i = new Intent(getApplicationContext(),DashboardActivity.class);
        startActivityForResult(i, 2);
                  }
            });
        submt = (Button) findViewById(R.id.button);
        submt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(radio_home.isChecked())
                {
                    radio_home_s=radio_home.getText().toString();
                }
                else if(radio_work.isChecked())
                {
                    radio_home_s=radio_work.getText().toString();
                }

                else if(radio_newh.isChecked())
                {
                    radio_home_s=radio_newh.getText().toString();
                }

                fnames_s= fnames.getText().toString();
                lnames_s = lnames.getText().toString();
                citys_s=citys.getText().toString();
                dlvry_area_s= dlvry_area.getText().toString();
                street_s = street.getText().toString();
               building_s = building.getText().toString();
                floor_s= floor.getText().toString();
                apartment_no_s= apartment_no.getText().toString();
                mob_no_s = mob_no.getText().toString();
                email_s= email.getText().toString();
               loctn_s = loctn.getText().toString();
                edittexterror = false;


                if (fnames.getText().toString().isEmpty()
                        || citys.getText().toString().isEmpty()
                        || street.getText().toString().isEmpty()
                        || building.getText().toString().isEmpty()
                        || mob_no.getText().toString().isEmpty()
                        || email.getText().toString().isEmpty()
                       ) {
                    com.nispok.snackbar.Snackbar.with(AddressFragment.this) // context
                            .text(getResources().getString(R.string.empty_fields)) // text to display
                            .show(AddressFragment.this);
               /*     com.chootdev.csnackbar.Snackbar.with(AddressFragment.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Field Empty!")
                            .duration(Duration.SHORT)
                            .show();*/
                    edittexterror = true;
                    //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

                }  else if (email.getText().toString().isEmpty()) {
                    email.setError(getResources().getString(R.string.Enter_Phone));
                    edittexterror = true;
                } else if (fnames.getText().toString().isEmpty()) {
                    fnames.setError(getResources().getString(R.string.Enter_Full_name));
                    edittexterror = true;
                } /*else if (lnames.getText().toString().isEmpty()) {
                    lnames.setError("Enter Last Name");
                    edittexterror = true;
                }*/ else if (citys.getText().toString().isEmpty()) {
                    citys.setError(getResources().getString(R.string.Enter_city));
                    edittexterror = true;
                }else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()) {
                    email.setError(getResources().getString(R.string.Invalid_Email));
                    edittexterror = true;
                } else if (street.getText().toString().isEmpty()) {
                    street.setError(getResources().getString(R.string.enter_street));
                    edittexterror = true;
                } /*else if (dlvry_area.getText().toString().isEmpty()) {
                   dlvry_area.setError("Enter Delivery Area");
                    edittexterror = true;
                }*/ else if (building.getText().toString().isEmpty()) {
                   building.setError(getResources().getString(R.string.enter_building));
                    edittexterror = true;
                } else if (filenamepath != null) {
                    filename = filenamepath;


                } else {
                    filename = "null";

                }

                if (edittexterror == false) {
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new SendPostRequest().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(AddressFragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(AddressFragment.this);
                       /* com.chootdev.csnackbar.Snackbar.with(AddressFragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();*/

                    }


                }
            }
        });

        final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
        db = dataHelper.getReadableDatabase();
        db.beginTransaction();
        try {
            String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
            System.out.println("" + selectQuery);
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    CartModel cartmodl = new CartModel();
                    final int outlet_id = cursor.getInt(cursor.getColumnIndex("outlet_id"));
                    String outlet_name = cursor.getString(cursor.getColumnIndex("outlet_name"));
                    Float outlet_qty = cursor.getFloat(cursor.getColumnIndex("outlet_qty"));
                    final Float outlet_price = cursor.getFloat(cursor.getColumnIndex("outlet_price"));
                    final String price_quantity=cursor.getString(cursor.getColumnIndex("price_quantity"));
                  outlet_tot = cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                  grnd = cursor.getFloat(cursor.getColumnIndex("total"));
                    String pro_id= cursor.getString(cursor.getColumnIndex("pro_id"));
                    butcher_id=cursor.getString(cursor.getColumnIndex("butcher_id"));
                    type_id=cursor.getString(cursor.getColumnIndex("type_id"));
                    System.out.println("<<<<<<<<<<<<<BUTCHER_ID>>>>>>>>>>>>>>" +butcher_id);
                    s = new JsonModel();

                    System.out.println("course2" + outlet_name);
                    System.out.println("sub_coursecoursereg" + outlet_qty);
                    System.out.println("sub_coursecourseregid" + outlet_price);
                    System.out.println("sub_coursecourseregid" + outlet_tot);
                    System.out.println("sub_coursecourseregid" + grnd);
                    System.out.println("sub_coursecourseregid" + price_quantity);

                    array_product.add(pro_id);
                    array_item.add(outlet_name);
                    array_qty.add(String.valueOf(outlet_qty));
                    array_price.add(String.valueOf(outlet_price));
                    array_butcher.add(String.valueOf(butcher_id));
                    array_price_qty.add(price_quantity);

                    array_type.add(type_id);
                    array_tot.add(String.valueOf(outlet_tot));

                   csv = array_product.toString().replace("[", "").replace("]", "")
                            .replace(", ", ",").replace(",",",");
                    System.out.println("<<<<<<<<<<<<<csv>>>>>>>>>>>>>>>" +csv);
                    csf= array_item.toString().replace("[", "").replace("]", "")
                            .replace(", ", ",").replace(",",",");
                    System.out.println("<<<<<<<<<<<<<csf>>>>>>>>>>>>>>>" +csf);
                    csd = array_qty.toString().replace("[", "").replace("]", "")
                            .replace(", ", ",").replace(",",",");
                    System.out.println("<<<<<<<<<<<<<csd>>>>>>>>>>>>>>>" +csd);
                    csg= array_price.toString().replace("[", "").replace("]", "")
                            .replace(", ", ",").replace(",",",");
                    System.out.println("<<<<<<<<<<<<<csg>>>>>>>>>>>>>>>" +csg);

                    cbf=array_butcher.toString().replace("[","").replace("]","").replace(",",",");

                    cpt=array_price_qty.toString().replace("[","").replace("]","").replace(",",",").replace(" ","");

                    cty=array_type.toString().replace("[","").replace("]","").replace(",",",").replace(",",",");

                    ctt=array_tot.toString().replace("[","").replace("]","").replace(",",",").replace(" ","");

                    array_lstcart=new ArrayList<>();
                    s.setProductid(csv);
                    s.setProduct(csf);
                    s.setQty(csd);
                    s.setPrice(csg);
                    s.setTotal(grnd);
                    s.setButcher_id(cbf);
                    s.setPrice_quantity(cpt);
                    s.setType_id(cty);
                    s.setOut_tt(ctt);
                    array_lstcart.add(s);

                    ArrayList<Float> obj = new ArrayList<Float>();
                    for (JsonModel s: array_lstcart) {
                        obj.add(s.getTotal());
                    }
               cart_tot2 += cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                    System.out.println("<<<<<<<<<<<<<Sum>>>>>>>>>>>>>>>" +s.getTotal());
                    Gson gson = new Gson();
                  sme_jsn=gson.toJson(array_lstcart);


                    System.out.println("<<<<<<<<<<<<<new my json>>>>>>>>>>>>>>>" +sme_jsn);


                }

            }
            db.setTransactionSuccessful();

        } catch (SQLiteException e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();
            // End the transaction.
            db.close();
            // Close database
        }
    }

    private void goBack() {
        super.onBackPressed();
    }

    public class SendPostRequest extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
            progress_loader.setVisibility(View.VISIBLE);
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL(SERVER+"json/customer/order.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

 tt= String.valueOf(cart_tot2);
                ttnw= String.valueOf(outlet_tot);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>"+tt);
            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>"+ttnw);
                System.out.println("-----------------------<<<<<<<<<<<<<<<<<<<<<<<???????????????????????s-----------------"+but_id);
                postDataParams.put("fname",fnames_s);
                postDataParams.put("lname", lnames_s);
                postDataParams.put("city", citys_s);
                postDataParams.put("butcher_id",but_id);
                postDataParams.put("deliveryarea", dlvry_area_s);
                postDataParams.put("addresstype", radio_home_s);
                postDataParams.put("street", street_s);
                postDataParams.put("building", building_s);
                postDataParams.put("floor",floor_s);
                postDataParams.put("apartno", apartment_no_s);
                postDataParams.put("mobile", mob_no_s);
                postDataParams.put("email", email_s);
                postDataParams.put("addtional_direc",loctn_s);
                postDataParams.put("user_id", Reg_id);
                postDataParams.put("servicefee","free");
                postDataParams.put("containercharges","2");
                postDataParams.put("deliverycharge","free");
                postDataParams.put("subtotal",tt);
                postDataParams.put("total",tt);
                postDataParams.put("instruction",inst);
                postDataParams.put("device_token",refreshedToken);
                postDataParams.put("payment_type","Cash On Delivery");
                postDataParams.put("order_time",currentDateTimeString);
                postDataParams.put("someJSON",sme_jsn);
                postDataParams.put("latitude",lat);
                postDataParams.put("longitude",lng);
                postDataParams.put("currency",Cur_cy);


System.out.println("<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>"+inst);
                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
            progress_loader.setVisibility(View.GONE);
//            Toast.makeText(getApplicationContext(),result,
//                    Toast.LENGTH_LONG).show();
            if (getIntent().getExtras().getString("id_nam").equalsIgnoreCase("libin")) {
                dh = new DatabaseHandler_N();
                dh.CreateDatas(getApplicationContext());
                values = new String[12];
                values[0] = fnames_s;
                values[1] = lnames_s;
                values[2] = citys_s;
                values[3] = dlvry_area_s;
                values[4] = street_s;
                values[5] = radio_home_s;
                values[6] = building_s;
                values[7] = floor_s;
                values[8] = apartment_no_s;

                values[9] = mob_no_s;
                values[10] = email_s;
                values[11] = "dubai";
                dh.InsertDatas(getApplicationContext(), values);
                if (result.equalsIgnoreCase("\"success\"")) {
                    new SweetAlertDialog(AddressFragment.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getResources().getString(R.string.AWESOME))
                            .setTitleText(getResources().getString(R.string.AWESOME))
                            .setContentText(getResources().getString(R.string.OrderplacedSuccessfully))
                            .setConfirmText(getResources().getString(R.string.ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                                    dataHelper.removeAll();



                                    Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                                    startActivity(i);
                                    finish();

                                    sweetAlertDialog.dismiss();

                                }
                            })
                            .show();


                } else {
                    new SweetAlertDialog(AddressFragment.this, SweetAlertDialog.ERROR_TYPE)
                           // .setTitleText("Oops...")
                            .setContentText(getResources().getString(R.string.Somethingwentwrong))
                            .show();

                }

            } else if (getIntent().getExtras().getString("id_nam").equalsIgnoreCase("antony")) {
                if (result.equalsIgnoreCase("\"success\"")) {
                    // Toast.makeText(getApplicationContext(),"Upload Success",Toast.LENGTH_SHORT).show();

                    new SweetAlertDialog(AddressFragment.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getResources().getString(R.string.AWESOME))
                          //  .setTitleText(getResources().getString(R.string.AWESOME))
                            .setContentText(getResources().getString(R.string.OrderplacedSuccessfully))
                            .setConfirmText(getResources().getString(R.string.ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                                    dataHelper.removeAll();



                                    Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                                    startActivity(i);
                                    finish();

                                    sweetAlertDialog.dismiss();

                                }
                            })
                            .show();


                } else {
                    com.nispok.snackbar.Snackbar.with(AddressFragment.this) // context
                            .text(getResources().getString(R.string.Somethingwentwrong)) // text to display
                            .show(AddressFragment.this);

                }

            }
        }



    }
    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == 2){

            if(requestCode==2)
            {
                typeoforge=data.getStringExtra("rm");
             loctn.setText(typeoforge);
                Log.e(typeoforge," rm");
            }

        }
    }
    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}


