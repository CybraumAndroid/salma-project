package com.salmabutchershop.salma.meridian.app.customer.login;

/**
 * Created by libin on 5/13/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akshay on 1/2/15.
 */
public class PeopleAdapter extends ArrayAdapter<Loc_Model> {

    Context context;
    int resource, textViewResourceId;
    List<Loc_Model> items, tempItems, suggestions;

    public PeopleAdapter(Context context, int resource, int textViewResourceId, List<Loc_Model> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<Loc_Model>(items); // this makes the difference.
        suggestions = new ArrayList<Loc_Model>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_people, parent, false);
     //  TextView lbl_name= (TextView) view.findViewById(R.id.lbl_name);
            final Loc_Model people = items.get(position);
            if (people != null) {
                TextView lblName = (TextView) view.findViewById(R.id.lbl_name);
                if (lblName != null)
                    lblName.setText(people.getCity());
//                lblName.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        System.out.println("latitudeeee>>>>>>>>>>"+people.getCity());
//                    }
//                });
            }

        }


        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((Loc_Model) resultValue).getCity();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Loc_Model people : tempItems) {
                    if (people.getCity().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Loc_Model> filterList = (ArrayList<Loc_Model>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Loc_Model people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}