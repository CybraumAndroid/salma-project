package com.salmabutchershop.salma.meridian.app.retailer.login;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.CustomPagerAdapter;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.OnSwipeTouchListener;
import com.salmabutchershop.salma.meridian.app.retailer.RetailerMainPage;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;

/**
 * Created by libin on 2/14/2017.
 */

public class Exploreappretailer extends Activity {
    CustomPagerAdapter mCustomPagerAdapter;
   LinearLayout mViewPager;
String name;
    TextView Name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.explore_app_layout);
        SharedPreferences preferencesd= getSharedPreferences("MyPref", MODE_PRIVATE);
        name = preferencesd.getString("fullnameR", null);
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<Customerloginid1>>>>>>>>>>>>>>>>>>>>>>"+name );

        final CardView cardView = (CardView) findViewById(R.id.card);
        final Button button = (Button) findViewById(R.id.button);
        button.setOnTouchListener(new View.OnTouchListener() {
            ObjectAnimator o1 = ObjectAnimator.ofFloat(cardView, "cardElevation", 2, 8)
                    .setDuration
                            (80);
            ObjectAnimator o2 = ObjectAnimator.ofFloat(cardView, "cardElevation", 8, 2)
                    .setDuration
                            (80);

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        o1.start();
                        Intent inn= new Intent(getApplicationContext(),Fresh_Frozen_Retailer.class);
                        startActivity(inn);
                        finish();
                        overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        Intent inn1= new Intent(getApplicationContext(),Fresh_Frozen_Retailer.class);
                        startActivity(inn1);
                        finish();
                        overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                        o2.start();
                        break;
                }
                return false;
            }
        });
        Name= (TextView) findViewById(R.id.textView14);
        Name.setText("Hi !"+ name);
        mViewPager= (LinearLayout) findViewById(R.id.pager);
     mViewPager.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                // Whatever
                Intent inn= new Intent(getApplicationContext(),Fresh_Frozen_Retailer.class);
                startActivity(inn);
                finish();
                overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
            }
        });


    }
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

            intent.putExtra("EXIT", true);
            startActivity(intent);
// dialog.dismiss();
            finishAffinity(); ;
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        com.nispok.snackbar.Snackbar.with(Exploreappretailer.this) // context
                .text("Press Again to Exit from SALMA ") // text to display
                .show(Exploreappretailer.this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }
}
