package com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;


import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Product_mymenu_Model;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct.Butcher_main_Product;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct.Product_main_adapter;

import com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct.SpinAdapter;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.address.AddressFragment;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.newintro.GPSTracker1;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Currency;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.ContentValues.TAG;
import static com.salmabutchershop.salma.meridian.app.R.id.imageView;


/**
 * Created by libin on 12/3/2016.
 */

public class Butcher_Product_Submit extends Activity {
    ImageView Up_img,Camera,Browse;
    TextView Disc_per,Plus,Minus,Disc,Desc,Dlvry,Dlvry_It,Qt_minus,Qt_plus;
    EditText Qty;
    private static final int PICK_FILE_REQUEST = 1;
    private static final int REQUEST_CODE = 1;
    String selectedFilePath,quantity_measure;
    static String filename = "null", filenamepath = "null";
    ProgressBar progress;
    Button submit;
    private int uprange = 20;
    private int downrange = 1;
    private int values = 0;
    //private int valuesqt = 1;
    String l,m,n,k;
    TextView title;
    Bitmap bmp;
    EditText entered_amount;
    String pro_ids,type_ids,pro_nam,type_name,type_image,category ;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE=100;
    private static final int MEDIA_TYPE_IMAGE=1;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String ImageLoc = "locationKey";

    private static final String IMAGE_DIRECTORY_NAME="camera_pics";
    private Uri fileUri;
    private Uri imgUriInsharedPref;
    private Bitmap bitmap2;
    private ImageView imgPreview;
    Button btnCapture,imgGallery;

    BitmapFactory.Options options1;

    private static final int CAMERA_REQUEST = 1888;
    String   type_id;
   // String filename;
    LinearLayout Pl_us,Mi_nus;
    String tot;
    //TextView tottext;
    String result, galry_image,Dat,Dat1;
    static ArrayList<Product_mymenu_Model> pmm;
    Product_mymenu_Model pm;
    Spinner spinner0;
    ArrayList<String> divisonlist;
   // String    dptmt;
    TextView Dlvry_Y,Dlvry_N;
    String clicked="null";
    EditText Description;
    TextView tottext;
    String Desc_ed,Qty_ed,Disc_ed,Price_ed,Disc_eds;
    DecimalFormat df = new DecimalFormat("#.##");
    Float totalaed=0f,currentaed=0f;
    Float totalaedAftrPlusClk=0f;
String userid,fullname,typ,cvc;
/*___________________________________________________________________________________________________________________________________________________________*/
    Float DiscntValues[];
    int i=0;
/*___________________________________________________________________________________________________________________________________________________________*/
String    dptmt,typen,typens;
    String type_old_price="52";

    /*^^^^^^^^^^^^^^^^^^^^^^^^start^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
    private float valuesqt = 500;
    float newvalues=500;
    String measure="gm";
    /*^^^^^^^^^^^^^^^^^^^^^^end^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    String Wurl;
    String tt,m_price;
    String tyh;
    EditText Min_price;
    public static MKLoader progress_loader,progress_loader1;
    Locale current;
    String symbol ;
    Spinner measure_spinner;
    SpinAdapter_cuttings spinAdapter_cuttings;
    ArrayList<String> divisonlist1;
    RecyclerView recyclerview,recyclerview1;
LinearLayout menu_lay,menu_cat;
    Product_main_adapter3 adapter3;
    TextView cuttings_name;
    final int PIC_CROP = 3;
    private Uri picUri;
    Uri selectedFileUri;
    String desc,Cntry,currency;
    private String selectedImagePath;
  Uri croped_image_uri;
    Bitmap  photo;
    TextView Cntry_text;
    GPSTracker1 gps;
    String Country,cnty;
    Double latitude,longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_submit_layout);
        Intent intent = getIntent();
        l = intent.getStringExtra("cat_id");
        m=intent.getStringExtra("pro_name");
        k=intent.getStringExtra("pro_id");

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Butcher_Product_Submit.this.goBack();
            }
        });


        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + l);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        progress_loader1=(MKLoader)findViewById(R.id.progress_loader1);
        if (ContextCompat.checkSelfPermission(Butcher_Product_Submit.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Butcher_Product_Submit.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Butcher_Product_Submit.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            gps = new GPSTracker1(Butcher_Product_Submit.this);

            // check if GPS enabled
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
              longitude = gps.getLongitude();
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(Butcher_Product_Submit.this, Locale.getDefault());

                try {
                              /*  lt= Double.valueOf(C_lat);
                                lg= Double.valueOf(C_long);*/
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    // progress.setVisibility(View.VISIBLE);
                    if (!addresses.isEmpty()) {
                        Country = addresses.get(0).getCountryName();

                        cvc = addresses.get(0).getCountryCode();
                        System.out.println("-----------------knwn-----------------------" + cvc);
                        // textinfo.setText("" + address + "\n" + "" + city + "\n" + "" + state + "\n" + "" + postalCode + "\n" + "" + knownName);
                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();

                        editor.putString("city_id", cvc);
                        editor.putString("cvc", cvc);
                        // editor.putString("latitude", String.valueOf(latitude));
                        editor.commit();
                        progress.setVisibility(View.GONE);
                    }
                    {
                        System.out.println("----------------------------------------");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("longitude", String.valueOf(longitude));
                editor.putString("latitude", String.valueOf(latitude));
                editor.commit();
                // \n is for new line
                // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }

        }
        cuttings_name= (TextView) findViewById(R.id.cuttings_name);
        Cntry_text= (TextView) findViewById(R.id.textView39);
        measure_spinner=(Spinner)findViewById(R.id.measure_spinner);
        String[] spinner_vals=new String[]{"gm","kg"};
        divisonlist1 = new ArrayList<String>();
        divisonlist1.add(0,"gm");
        divisonlist1.add(1,"kg");
        ArrayAdapter<String> spinnerAdapter2 = new ArrayAdapter<String>(this,
                R.layout.patientreferal_spinner_item1, R.id.txt, divisonlist1);

        measure_spinner.setAdapter(spinnerAdapter2);

        recyclerview1 = (RecyclerView) findViewById(R.id.recyclerview1);
        recyclerview1.setHasFixedSize(true);
        Context context = getApplicationContext();
        // LinearLayoutManager llm = new LinearLayoutManager(context);
/*        llm.setOrientation(LinearLayoutManager.HORIZONTAL);*/
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview1.setLayoutManager(layoutManager1);
        recyclerview1.setAdapter(adapter3);
        menu_lay= (LinearLayout) findViewById(R.id.menu_lay);
        menu_cat= (LinearLayout) findViewById(R.id.menu_cat);
        menu_cat.setVisibility(View.GONE);
        menu_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu_cat.setVisibility(View.VISIBLE);
               // recyclerview.setVisibility(View.GONE);
            }
        });
/*
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,spinner_vals);
        measure_spinner.setAdapter(adapter);
        measure_spinner.setSelection(0);*/
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        userid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullnameb", null);
        cvc=myPrefs.getString("cvc",cvc);
        cnty=myPrefs.getString("cnty",null);
        Cntry = myPrefs.getString("cntry", null);
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>"+Cntry);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);

      Min_price= (EditText) findViewById(R.id.min_price);
        Up_img= (ImageView) findViewById(R.id.up_img);
        Description= (EditText) findViewById(R.id.desc);
        Camera=(ImageView) findViewById(R.id.camera);
        Browse= (ImageView) findViewById(R.id.brwse);
      //  title= (TextView) findViewById(R.id.header_text);

        Disc_per= (TextView) findViewById(R.id.disc_per);
        Plus= (TextView) findViewById(R.id.plus);
        Minus= (TextView) findViewById(R.id.minus);

        Disc= (TextView) findViewById(R.id.disc);
        Desc= (TextView) findViewById(R.id.desc);
        Dlvry= (TextView) findViewById(R.id.dlvry);
        Dlvry_It= (TextView) findViewById(R.id.dlvry_it);
        tottext= (TextView) findViewById(R.id.textView36);

        entered_amount=(EditText)findViewById(R.id.entered_amount);

        tottext.setText("10");
        entered_amount.setText("10");
        currentaed=Float.parseFloat(tottext.getText().toString());
        System.out.println("current aed : at begining"+currentaed);


        entered_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
try {
    values = 0;
    Disc_per.setText(values + "% off");
    tottext.setText(entered_amount.getText().toString());
    currentaed = Float.parseFloat(entered_amount.getText().toString());
    totalaed = Float.parseFloat(entered_amount.getText().toString());
    DiscntValues[0]=totalaed;
    i=0;
}catch (Exception e){
    e.printStackTrace();
}

            }
        });



        Qt_minus= (TextView) findViewById(R.id.qt_minus);
        Qt_plus= (TextView) findViewById(R.id.qt_plus);
        Qty= (EditText) findViewById(R.id.qty);
/*___________________________________________________________________________________________________________________________________________________________*/
///////////////////////////////////// storing discount values --start //////////////////////////////////
        DiscntValues=new Float[21];//array size =1+max discount value
        DiscntValues[0]=Float.parseFloat(tottext.getText().toString());

///////////////////////////////////// storing discount values --end //////////////////////////////////
/*___________________________________________________________________________________________________________________________________________________________*/
        totalaed=Float.parseFloat(tottext.getText().toString());





        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();

        } else {
            com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                    .text("No internet connection!") // text to display
                    .show(Butcher_Product_Submit.this);


        }
      //  spinner0 = (Spinner)findViewById(R.id.spinner4);
    /*  LinearLayout dropDownImage= (LinearLayout) findViewById(R.id.spin);
        dropDownImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinner0.performClick();
            }
        });*/
    //    spinner0.setPrompt("           "+getResources().getString(R.string.TypeofCuttings)+"    ");

     /*   spinner0.setGravity(Gravity.CENTER);
        spinner0.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                dptmt = spinner0.getSelectedItem().toString();
                typen=pmm.get(arg2).getType_name();
                typens=pmm.get(arg2).getType_id();
              tyh=pmm.get(arg2).getType_image();
               // title.setText(pmm.get(arg2).getType_name());
              //  Picasso.with(getApplicationContext()).load(pmm.get(arg2).getType_image()).noFade().noFade().into(Up_img);
                try {


                new Picasso.Builder(Butcher_Product_Submit.this)
                        .downloader(new OkHttpDownloader(Butcher_Product_Submit.this, Integer.MAX_VALUE))
                        .build()
                        .load(pmm.get(arg2).getType_image())
                        .noFade()
                        .into(Up_img, new Callback() {
                            @Override
                            public void onSuccess() {
                                progress_loader1.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                // personViewHolder.progress_loader.setVisibility(View.GONE);
                            }
                        });
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {


                    //new Acquie_Location_Services_Fragment.DownloadData2().execute();

                } else {
                    com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                            .text("No internet connection!") // text to display
                            .show(Butcher_Product_Submit.this);

                }




            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });*/

        Pl_us= (LinearLayout) findViewById(R.id.pl_us);

        Mi_nus= (LinearLayout) findViewById(R.id.mi_nus);

        Pl_us.setOnClickListener(new View.OnClickListener() {
            Float aed=0f;

            public void onClick(View v) {


              //  totalaed=Float.parseFloat(tottext.getText().toString());
                if(values==20){


                }else {

                    if (values >= 0 && values <= uprange) {
                        values++;
/*
                        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n substring : "+(tottext.getText().toString().substring(4))+"\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
*/
/*___________________________________________________________________________________________________________________________________________________________*/
                       // totalaed=Float.parseFloat(tottext.getText().toString());
                        aed=totalaed-(totalaed*values/100);
                        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"+aed);
                        tottext.setText(Float.toString(aed));
                        i++;
                        DiscntValues[i]=aed;
/*___________________________________________________________________________________________________________________________________________________________*/





                        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                        System.out.println("inside plus click");
                        System.out.println("values : "+values);
                        System.out.println("totalaed : "+totalaed);
                        System.out.println("aed after discount : "+aed);
                        System.out.println("discount Array : \n");
                        for(int j=0;j<DiscntValues.length;j++)
                            System.out.println("elemet at position "+j+" : "+DiscntValues[j]+"\n");
                        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");



                    }
                    if (values > uprange) {
                        values = 0;
/*
                        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n substring : "+(tottext.getText().toString().substring(4))+"\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
*/
/*___________________________________________________________________________________________________________________________________________________________*/
                       // totalaed=Float.parseFloat(tottext.getText().toString());
                        aed=totalaed-(totalaed*values/100);
                        tottext.setText(Float.toString(aed));

                        i++;
                        DiscntValues[i]=aed;
/*___________________________________________________________________________________________________________________________________________________________*/




                        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                        System.out.println("inside plus click");
                        System.out.println("values : "+values);
                        System.out.println("totalaed : "+totalaed);
                        System.out.println("aed after discount : "+aed);
                        System.out.println("discount Array : \n");
                        for(int j=0;j<DiscntValues.length;j++)
                            System.out.println("elemet at position "+j+" : "+DiscntValues[j]+"\n");
                        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

                    }
                }
                //Qty.setText("" + values + " kg");
                Disc_per.setText("" + values + "% off");








            }
        });
      Mi_nus.setOnClickListener(new View.OnClickListener() {
          Float aed=0f;
            public void onClick(View v) {
              //  totalaed=Float.parseFloat(tottext.getText().toString());
                if(values==0){


                }else {
                    if (values >= 0 && values <= uprange) {
                        values--;
/*
                        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n substring : "+(tottext.getText().toString().substring(4))+"\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
*/



/*___________________________________________________________________________________________________________________________________________________________*/

                        /*aed=totalaed+(totalaed*values/100);
                        tottext.setText("AED "+Float.toString(aed));*/
                       i--;
                        if(i<0){

                        }else {
                            tottext.setText(DiscntValues[i].toString());
/*___________________________________________________________________________________________________________________________________________________________*/


                            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                            System.out.println("inside minus click");
                            System.out.println("values : " + values);
                            System.out.println("totalaed : " + totalaed);
                            System.out.println("aed after discount : " + aed);
                            System.out.println("DiscntValues[" + i + "] : " + DiscntValues[i]);
                            System.out.println("i : " + i);
                            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

                        }
                    }
                    if (values < 0) {
                        values = uprange;
/*
                        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n substring : "+(tottext.getText().toString().substring(4))+"\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
*/
/*___________________________________________________________________________________________________________________________________________________________*/

                        /*aed=totalaed+(totalaed*values/100);
                        tottext.setText("AED "+Float.toString(aed));*/

                       i--;
                        if(i<0){

                        }else {
                            tottext.setText(DiscntValues[i].toString());
/*___________________________________________________________________________________________________________________________________________________________*/

                            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                            System.out.println("inside minus click");
                            System.out.println("values : " + values);
                            System.out.println("totalaed : " + totalaed);
                            System.out.println("aed after discount : " + aed);
                            System.out.println("DiscntValues[" + (DiscntValues.length - i) + "] : " + DiscntValues[DiscntValues.length - i]);
                            System.out.println("i : " + i);
                            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                        }
                    }
                }

                //qty.setText(values + " kg");

                Disc_per.setText(values + "% off");





            }
        });

        Qt_plus.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


/*^^^^^^^^^^^^^^^^^^^^^^^^start^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

               // totalaed=Float.parseFloat(tottext.getText().toString());


                if(newvalues==20000.0)
                {

                }

                else{

                    measure=" kg";
                    newvalues=newvalues+500;
                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                    valuesqt=newvalues/1000;
                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+valuesqt+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                    Qty.setText("" + valuesqt + measure);



                    ////////////////////////////////////////////////////
                    if(measure.equals(" g")) {

                        System.out.println("current aed : (in qt plus) "+currentaed);
                        float no_of_grams=newvalues/500;
                        System.out.println(currentaed + "*" + no_of_grams + "=" + (currentaed * no_of_grams));
                        tot = String.valueOf(currentaed * no_of_grams);
                    }
                    else{
                        System.out.println("current aed : (in qt plus) "+currentaed);
                        float no_of_grams=newvalues/500;

                        System.out.println(currentaed + "*" + no_of_grams + "=" + (currentaed * no_of_grams));
                        tot = String.valueOf(currentaed * no_of_grams);
                    }
                    tottext.setText(tot);
                    values=0;
                    Disc_per.setText(values + "% off");

                    totalaed=Float.parseFloat(tottext.getText().toString());
/*___________________________________________________________________________________________________________________________________________________________*/
                    i=0;
                    for(int j=0;j<DiscntValues.length;j++)
                        DiscntValues[j]=0f;
                    DiscntValues[0]=Float.parseFloat(entered_amount.getText().toString());
/*___________________________________________________________________________________________________________________________________________________________*/



                }
/*^^^^^^^^^^^^^^^^^^^^^^^^end^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

            }
        });
        Qt_minus.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
/*^^^^^^^^^^^^^^^^^^^^^^^^start^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
              //  totalaed=Float.parseFloat(tottext.getText().toString());
                if(newvalues==500.0 )
                {
                    measure=" g";
                    Qty.setText(newvalues + measure);
                }

                else{

                    measure=" kg";
                    newvalues=newvalues-500;
                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                    valuesqt=newvalues/1000;
                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+valuesqt+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");

                    if(valuesqt==0.5){
                        measure=" g";
                        Qty.setText(newvalues + measure);
                    }
                    else {
                        measure=" kg";
                        Qty.setText(valuesqt + measure);
                    }
                    ////////////////////////////////////////////////////
                    if(measure.equals(" g")) {
                        System.out.println("current aed : (in qt minus) "+currentaed);
                        float no_of_grams=newvalues/500;
                        System.out.println(currentaed + "*" + no_of_grams + "=" + (currentaed * no_of_grams));
                        tot = String.valueOf(currentaed * no_of_grams);
                    }
                    else{
                        System.out.println("current aed : (in qt minus) "+currentaed);
                        float no_of_grams=newvalues/500;

                        System.out.println(currentaed + "*" + no_of_grams + "=" + (currentaed * no_of_grams));
                        tot = String.valueOf(currentaed * no_of_grams);
                    }
                    tottext.setText(tot);
                    values=0;
                    Disc_per.setText(values + "% off");
                }



                totalaed=Float.parseFloat(tottext.getText().toString());
/*___________________________________________________________________________________________________________________________________________________________*/
                i=0;
                for(int j=0;j<DiscntValues.length;j++)
                    DiscntValues[j]=0f;
                DiscntValues[0]=Float.parseFloat(tottext.getText().toString());
/*___________________________________________________________________________________________________________________________________________________________*/



/*^^^^^^^^^^^^^^^^^^^^^^^^end^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

            }
        });
        submit= (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int flag=0;
                try{
                    quantity_measure = measure_spinner.getSelectedItem().toString();
                    System.out.println("quantity_measure : " + quantity_measure);
                    if (quantity_measure.trim().equalsIgnoreCase("gm")) {
                        quantity_measure = "gm";
                        if (Integer.parseInt(Qty.getText().toString()) < 200) {
                        /*Qty.setText("500");*/
                            flag=1;
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.please_add_minimum_200gm),Toast.LENGTH_SHORT).show();

                        }
                        if (Integer.parseInt(Qty.getText().toString()) >=1000) {
                            int new_quan=Integer.parseInt(Qty.getText().toString())/1000;
                            Qty.setText(Integer.toString(new_quan));
                            measure_spinner.setSelection(1);
                        }
                    } else {
                        quantity_measure = "kg";
                        if (Integer.parseInt(Qty.getText().toString()) == 0) {

                            flag=1;
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.kg_is_not_a_valid_measure),Toast.LENGTH_SHORT).show();
                        }
                        if(Integer.parseInt(Qty.getText().toString())>20){
                            flag=1;
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.maximun_quantity_is_20kg),Toast.LENGTH_SHORT).show();
                        }
                    }}catch (Exception e){
                    e.printStackTrace();
                }

                if(flag==0) {
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {

                        filename = filenamepath;
                        int selectedId = radioSexGroup.getCheckedRadioButtonId();

                        // find the radiobutton by returned id
                        radioSexButton = (RadioButton) findViewById(selectedId);
                      /*  getCurrencySymbol("IN");*/
                       /* Currency c=Currency.getInstance(new Locale("en",null));
                        System.out.println("Symbol is "+c.getSymbol());
                        System.out.println("Symbol is "+c.getCurrencyCode());*/
                /*   current = getResources().getConfiguration().locale;
                   *//*     String locale = Butcher_Product_Submit.this.getResources().getConfiguration().locale.getCountry();
                        Log.i("locale",locale);*//*
                        TelephonyManager tm = (TelephonyManager)Butcher_Product_Submit.this.getSystemService(Context.TELEPHONY_SERVICE);
                        String countryCodeValue = tm.getNetworkCountryIso();
                       // symbol = currency.getSymbol().replaceAll("\\w", "");
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<currencycode>>>>>>>>>>>>>>>>>>>>>"+current);
                        Currency curr = java.util.Currency.getInstance(countryCodeValue);
                        System.out.println("Currency code:" + curr.getCurrencyCode());
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<currenvvvcycode>>>>>>>>>>>>>>>>>>>>>"+curr);
                     System.out.println("<<<<<<<<<<<<<<<<<<<<<<<currencysymbol>>>>>>>>>>>>>>>>>>>>>"+curr);*/
                      /*  Toast.makeText(Butcher_Product_Submit.this,
                                radioSexButton.getText(), Toast.LENGTH_SHORT).show();*/
                        clicked = radioSexButton.getText().toString();
                      Desc_ed = Description.getText().toString();
                        Qty_ed = Qty.getText().toString();
                        Disc_ed = Disc_per.getText().toString();
                        Price_ed = tottext.getText().toString();
                        m_price = Min_price.getText().toString();
                        Disc_eds = Desc_ed.replaceAll(" ", "%20");
                       desc=Description.getText().toString();
                        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -start
                        quantity_measure = measure_spinner.getSelectedItem().toString();
                        if (quantity_measure.trim().equalsIgnoreCase("gm")) {
                            quantity_measure = "gm";
                        } else {
                            quantity_measure = "kg";
                        }
                        Qty_ed = Qty_ed + " " + quantity_measure;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -end
                        System.out.println("--------------------------------" + Price_ed);
                        if (Qty.getText().toString().isEmpty() || Disc_per.getText().toString().isEmpty() || tottext.getText().toString().isEmpty()) {
                            com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                                    .text(getResources().getString(R.string.SelectQuantity)) // text to display
                                    .show(Butcher_Product_Submit.this);


                        } else if (Price_ed.isEmpty()) {
                            com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                                    .text("Amount Empty!") // text to display
                                    .show(Butcher_Product_Submit.this);

                        } else {
                            if (selectedFilePath != null && !selectedFilePath.isEmpty())

                            {
                                uploadVideo1();


                                new SendPostRequest1().execute();
                                // tt="";
                            } else {
                                new SweetAlertDialog(Butcher_Product_Submit.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(getResources().getString(R.string.MyMenu))
                                        .setContentText(getResources().getString(R.string.Areyousurewanttosetimageasdefault))
                                        .setConfirmText(getResources().getString(R.string.Yes))
                                        .setCancelText(getResources().getString(R.string.No))
                                        .setCustomImage(R.drawable.logo)
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {


                                                new SendPostRequest1().execute();
                                                sDialog.dismiss();
                                            }
                                        })
                                        .show();

                            }

                        }

                    } else {
                        com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Butcher_Product_Submit.this);


                    }

                }

            }
        });
        Browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED) {

                    requestPermissions();
                }
                showFileChooser();
            }
        });
Camera.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        checkAndRequestPermissions();
        captureImage();
    }
});
    }

    /*public static String getCurrencySymbol(String countryCode) {
        String curr=Currency.getInstance(new Locale(null, countryCode)).getCurrencyCode();
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<jjjjjjjjjjjjjjjjjjjjjjjj>>>>>>>>>>>>>>>>>>>>>"+curr);
        return Currency.getInstance(new Locale(null, countryCode)).getCurrencyCode();


    }*/

    @Override
    protected void onStart() {
        super.onStart();

        checkAndRequestPermissions();


    }
    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void captureImage() {
        try {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        System.out.println("--------------------------------");
        System.out.println("FILE URI INSIDE captureImage() : " + fileUri);
        System.out.println("--------------------------------");

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support capturing images!";
           // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }
      /*  try {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
  fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        System.out.println("--------------------------------");
        System.out.println("FILE URI INSIDE captureImage() : " + fileUri);
        System.out.println("--------------------------------");

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(takePhotoIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support capturing images!";
           // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }*/
    }
    public Uri getOutputMediaFileUri(int type){
      return Uri.fromFile(getOutputMediaFile(type));
    }
    private File getOutputMediaFile(int type) {

        //////////////
        // External sdcard location
        File mediaStorageDir=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);
        System.out.println("--------------------------------");
        System.out.println("MEDIA STORAGE DIR : "+mediaStorageDir);
        System.out.println("--------------------------------");
        Up_img.setImageBitmap(bmp);
        // Create the storage directory if it does not exist
        if(!mediaStorageDir.exists()){
            if(!mediaStorageDir.mkdirs()){
                Log.d(IMAGE_DIRECTORY_NAME,"Oops!failed to create"+IMAGE_DIRECTORY_NAME+" directory");
                System.out.println("--------------------------------");
                System.out.println("MEDIA STORAGE DIR NOT EXISTS");
                System.out.println("--------------------------------");
                return null;
            }
        }
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if(type==MEDIA_TYPE_IMAGE){
            mediaFile=new File(mediaStorageDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
           // Uri selectedFileUri = data.getData();
            //Uri selectedFileUri = data.getData();
            Uri external = Uri.fromFile(mediaFile);
/*selectedFilePath=mediaFile.getAbsolutePath();*/

          selectedFilePath = Vis_FilePath.getPath(getApplicationContext(),external);
            System.out.println("--------------selected------------------"+selectedFilePath);
           bmp = BitmapFactory.decodeFile(selectedFilePath);
            Log.i(TAG,"Selected File Path:" + selectedFilePath);

            Bitmap myBitmap = BitmapFactory.decodeFile(mediaFile.getAbsolutePath());
            Up_img.setImageBitmap(bmp);
            System.out.println("--------------------------------");
            System.out.println("MEDIA FILE : "+mediaFile);
            System.out.println("--------------------------------");
            if(selectedFilePath != null && !selectedFilePath.equals("")){
                // textViewResponse.setText(selectedFilePath);
                filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
            }else{
                com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                        .text("Cannot upload file to server!") // text to display
                        .show(Butcher_Product_Submit.this);

            }
            Up_img.setImageBitmap(bmp);


        }
        else{
            return null;
        }
        return mediaFile;
    }


    private void goBack() {

            Intent i=new Intent(getApplicationContext(),MainFragment.class);


            startActivity(i);
            // finish();
            finish();

    }

    private void showFileChooser() {
        try{
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("image/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent,"Choose File to Vis_Upload.."),PICK_FILE_REQUEST);
        } catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = getResources().getString(R.string.whoops_your_device_doesnt_support_capturing);
            // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                if(data == null){
                    //no data present

                    return;
                }


                Uri selectedFileUri = data.getData();
                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(),selectedFileUri);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                Log.i(TAG,"Selected File Path:" + selectedFilePath);

                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    // textViewResponse.setText(selectedFilePath);
                    filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                    try {


                    CropImage.activity(selectedFileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }else{
                    com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                            .text("Cannot upload file to server!") // text to display
                            .show(Butcher_Product_Submit.this);
                   /* Snackbar.with(Butcher_Product_Submit.this,null)
                            .type(Type.SUCCESS)
                            .message("Cannot upload file to server!")
                            .duration(Duration.LONG)
                            .show();*/
                  //  Toast.makeText(getApplicationContext(),"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                }
                Up_img.setImageBitmap(bmp);
            }
            else if(requestCode == PIC_CROP) {
                if (resultCode == RESULT_OK && data != null) {
                    try {
                        Uri selectedFileUri = data.getData();
                        String path = data.getStringExtra("imageuri");
                        System.out.println("selectedFileUri : " + croped_image_uri);
                        String selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), croped_image_uri);
                        Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
// BitmapFactory.Options options=new BitmapFactory.Options();
// downsizing image as it throws OutOfMemory Exception for larger
// images
// options.inSampleSize = 8;

// final Bitmap bitmap= BitmapFactory.decodeFile(picUri.getPath(),options);
// Bitmap thePic = (Bitmap) data.getExtras().get("data");
//get the returned data
/* Bundle extras = data.getExtras();
//get the cropped bitmap
Bitmap thePic = (Bitmap) extras.get("return-data");*/
//display the returned cropped image
                        Up_img.setImageBitmap(bmp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                  /*  Uri selectedImageUri = data.getData();
                    if (Build.VERSION.SDK_INT < 19) {
                        selectedImagePath = getPath(selectedImageUri);
                        Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath);
                        Up_img.setImageBitmap(bitmap);

                    }
                    else {
                        ParcelFileDescriptor parcelFileDescriptor;
                        try {
                            parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedImageUri, "r");
                            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
                            parcelFileDescriptor.close();
                            Up_img.setImageBitmap(image);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {

                            e.printStackTrace();
                        }
                    }*/
                   /* if (Build.VERSION.SDK_INT < 19) {
                        Uri selectedFileUri = data.getData();
                        // System.out.println("selectedImage "+selectedImage);
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(selectedFileUri, filePathColumn, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        selectedFilePath = cursor.getString(columnIndex);
                        cursor.close();
                        System.out.println("smallImagePath" + selectedFilePath);
                        Up_img.setImageBitmap(BitmapFactory.decodeFile(selectedFilePath));
                        //encodeImage();
                    }else {
                        Uri selectedFileUri = data.getData();
                        String selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), selectedFileUri);
                        Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                        Up_img.setImageBitmap(bmp);
                    }*/
                  //  BitmapFactory.Options options=new BitmapFactory.Options();
// downsizing image as it throws OutOfMemory Exception for larger
// images
               //     options.inSampleSize = 8;

                   // final Bitmap bitmap= BitmapFactory.decodeFile(picUri.getPath(),options);
                    //   Bitmap thePic = (Bitmap) data.getExtras().get("data");
                    //get the returned data
                  /*  Bundle extras = data.getExtras();
                    //get the cropped bitmap
                    Bitmap thePic = (Bitmap) extras.get("return-data");*/
                    //display the returned cropped image

                }


              /*  Bundle extras = data.getExtras();
                //get the cropped bitmap
                Bitmap thePic = (Bitmap) extras.get("data");
                //display the returned cropped image
                Up_img.setImageBitmap(thePic);
                if(resultCode==RESULT_OK) {
                    previewCapturedImage();
                }*/
            }
            else if(requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
                if(resultCode==RESULT_OK){
                    //previewCapturedImage();
                    Uri uri = fileUri;
             //  photo = (Bitmap) data.getExtras().get("data");
                   // Log.d("uriGallery", fileUri.toString());
               //  performCrop(fileUri);
                  //  performcrop();
                   // previewCapturedImage();
try {


                    CropImage.activity(fileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
}catch (NullPointerException e){
    e.printStackTrace();
}
                }
               /* if(resultCode==RESULT_OK){
                 //   Uri uri = fileUri;
                    //carry out the crop operation
                    checkAndRequestPermissions();
                  //  previewCapturedImage();
                   beginCrop(selectedFileUri);
                  //  performCrop();
                 //   Log.d("picUri", uri.toString());
                   // handleCrop(resultCode,data);

                }
*/
                
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            try {


            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                ExifInterface exif;
                int angle = 0;
                try {
                    exif = new ExifInterface(resultUri.getPath());
                    int orientation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL);



                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                        angle = 90;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                        angle = 180;
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Matrix matrix1 = new Matrix();

                //set image rotation value to 45 degrees in matrix.
                matrix1.postRotate(angle);
                System.out.println("selectedFileUri : "+resultUri);
               selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), resultUri);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    // textViewResponse.setText(selectedFilePath);
                    filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                }else{
                    com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                            .text(getResources().getString(R.string.cannot_upload_file_to_server)) // text to display
                            .show(Butcher_Product_Submit.this);

                }
                Up_img.setImageBitmap(bmp);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
            }catch (NullPointerException e){
                e.printStackTrace();
            }
        }

    }

    private void performcrop() {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        Bitmap croppedBmp = Bitmap.createBitmap(photo, 0, 0, width / 2,
                photo.getHeight());

        Up_img.setImageBitmap(croppedBmp);
    }

    private String getPath(Uri selectedFileUri) {
        if( selectedFileUri == null ) {
            return null;
        }
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(selectedFileUri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return selectedFileUri.getPath();
    }


      private void beginCrop(Uri d) {
          Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
       //   getApplicationContext().getContentResolver().delete(destination.getPath(), null, null);
          Crop.of(d, destination).asSquare().start(this);
        //  Up_img.setImageURI(destination);
          BitmapFactory.Options options=new BitmapFactory.Options();
  
  // downsizing image as it throws OutOfMemory Exception for larger
  // images
          options.inSampleSize = 8;
  
  
  
          final Bitmap bitmap=BitmapFactory.decodeFile(destination.getPath(),options);
          Up_img.setImageBitmap(bitmap);
          selectedFilePath = Vis_FilePath.getPath(getApplicationContext(),destination);
  
      }
   /* private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            Up_img.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }*/
    private void performCrop(Uri uri) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            System.out.println("fileUri : "+uri);
            cropIntent.setDataAndType(uri, "image/*");
            croped_image_uri=uri;
            //set crop properties
            cropIntent.putExtra("imageuri",uri);
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = getResources().getString(R.string.whoops_your_device_doesnt_support_the_crop_action);
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private void previewCapturedImage() {
        try{
          //  imgPreview.setVisibility(View.VISIBLE);
            BitmapFactory.Options options=new BitmapFactory.Options();
// downsizing image as it throws OutOfMemory Exception for larger
// images
            options.inSampleSize = 8;



            final Bitmap bitmap=BitmapFactory.decodeFile(fileUri.getPath(),options);
           Up_img.setImageBitmap(bitmap);
            System.out.println("--------------------------------");
            System.out.println("FILE URI IN PREVIEW : "+fileUri);
            System.out.println("--------------------------------");
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(Butcher_Product_Submit.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE);
        // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {



                } else {
                    com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                            .text(getResources().getString(R.string.permission_not_granted)) // text to display
                            .show(Butcher_Product_Submit.this);

                    finish();


                }
                return;
            }

        }


    }



    private void uploadVideo1() {
        class UploadVideo extends AsyncTask<Void, Void, String> {

            ProgressDialog uploading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress_loader.setVisibility(View.VISIBLE);
             uploading = ProgressDialog.show(Butcher_Product_Submit.this, getResources().getString(R.string.uploading), getResources().getString(R.string.please_wait), false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progress_loader.setVisibility(View.GONE);
                uploading.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {
                Vis_Upload u = new Vis_Upload();
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<md>>>>>>>>>>>>>>>>>>>>>>>>>" + selectedFilePath);
                String msg = u.uploadVideo(selectedFilePath);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<mdeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>>>>>>>>>>" + msg);

                return msg;
            }
        }
        UploadVideo uv = new UploadVideo();
        uv.execute();
    }


    private class SendPostRequest1 extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
            progress_loader.setVisibility(ProgressBar.VISIBLE);
            System.out.println("nameeeeeeeeeeeeeeeeeeee"+dptmt);
           // Log.e("clicked",clicked);
        }

        public String doInBackground(String... arg0) {

            try {
String js=filename.replaceAll(" ","");


                URL url = new URL("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/butcher-addproduct.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();

                postDataParams.put("butcher_id",userid);
                postDataParams.put("category",category);
                postDataParams.put("type_id", typens);
                postDataParams.put("pro_id",k);
                postDataParams.put("pro_type_name",typens);
                postDataParams.put("typ_prize",Price_ed);
                postDataParams.put("typ_qnty",Qty_ed);
                postDataParams.put("typ_unit",measure);
                postDataParams.put("typ_currency",currency);
                postDataParams.put("typ_discount",""+values);
                postDataParams.put("typ_discount_unit","%");
                postDataParams.put("typ_desc",desc);
                postDataParams.put("typ_delivery",clicked);
                postDataParams.put("type_old_price",totalaed);
                postDataParams.put("min_order_amount",m_price);
                postDataParams.put("productphoto",js);
             postDataParams.put("country_code",cvc);
                postDataParams.put("productphoto_default",tyh);

                Log.e("clicked",clicked);
                Log.e("params",postDataParams.toString());


                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
            progress_loader.setVisibility(ProgressBar.GONE);
            /*Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();*/
            if (result.contentEquals("\"success\"")){

                new SweetAlertDialog(Butcher_Product_Submit.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.AWESOME))
                        .setContentText(getResources().getString(R.string.Menussuccessfullyadded))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog
                                        .setTitleText(getResources().getString(R.string.AWESOME))
                                        .setContentText(getResources().getString(R.string.Menussuccessfullyadded))
                                        .setConfirmText(getResources().getString(R.string.ok))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                Intent inn= new Intent(getApplicationContext(),Butcher_main_Product.class);
                                                startActivity(inn);
                                                finish();
                                                sweetAlertDialog.dismiss();
                                            }
                                        })
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                            }
                        })
                        .show();




            }
            else if(result.contentEquals("\"failed\"")){
                new SweetAlertDialog(Butcher_Product_Submit.this, SweetAlertDialog.ERROR_TYPE)
                        //.setTitleText("Oops...")
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .show();

            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);
            String lk=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<langlibin>>>>>"+lk);
            if (lk.equalsIgnoreCase("English")) {
                System.out.println("<<<<<<libin>>>>>"+l);
                System.out.println("<<<<<<libin>>>>>"+k);
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/product_type.php?sub_id="+l+"&pro_id="+k+"&country="+cnty;
                System.out.println("<<<<<<libin>>>>>"+"http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/product_type.php?sub_id="+l+"&pro_id="+k+"country"+Cntry);
            }
            else{
                System.out.println("<<<<<<libin2>>>>>"+l);
                System.out.println("<<<<<<libin2>>>>>"+k);
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/ar/product_type.php?sub_id="+l+"&pro_id="+k+"&country="+cnty;
            }



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {



                HttpClient httpclient = new DefaultHttpClient();

                HttpPost httppost = new HttpPost(Wurl);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
//            String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Butcher_Product_Submit.this);

            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                pmm = new ArrayList<Product_mymenu_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        pm = new Product_mymenu_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        // pro_id = mJsonObject.getString("pro_id");
                        type_id= mJsonObject.getString("type_id");
                        category =mJsonObject.getString("category");
                        ///pro_nam = mJsonObject.getString("pro_name");
                        type_name= mJsonObject.getString("type_name");
                        type_image = mJsonObject.getString("type_image");
                        currency= mJsonObject.getString("currency");

                        // pm.setPro_id(pro_id);
                        pm.setType_id(type_id);
                        //   pm.setPro_nam(pro_nam);
                        pm.setCategory(category);
                        pm.setType_name(type_name);
                        pm.setType_image(type_image);
                        pm.setCurrency(currency);

                        System.out.println("<<<galry_img>>>>" + type_image);


                        pmm.add(pm);
                        System.out.println("<<<oo>>>>" + pm);
                        adapter3= new Product_main_adapter3(pmm, getApplicationContext());
                        recyclerview1.setAdapter(adapter3);
                        divisonlist = new ArrayList<String>();
                        //Dep.add(dm);
                        for (Product_mymenu_Model pm : pmm) {
                            divisonlist.add(pm.getType_name());
                            //  divisonlist.add(dv.getBranch_id());
                        }
                        System.out.println(""+pm);
                        typen=pmm.get(0).getType_name();
                        typens=pmm.get(0).getType_id();
                        tyh=pmm.get(0).getType_image();
                        String cdt=pmm.get(0).getCurrency();
                        cuttings_name.setText(typen);
                        Cntry_text.setText(cdt);
                        try {


                            new Picasso.Builder(Butcher_Product_Submit.this)
                                    .downloader(new OkHttpDownloader(Butcher_Product_Submit.this, Integer.MAX_VALUE))
                                    .build()
                                    .load(pmm.get(0).getType_image())
                                    .noFade()
                                    .into(Up_img, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            progress_loader1.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError() {
                                            // personViewHolder.progress_loader.setVisibility(View.GONE);
                                        }
                                    });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        entered_amount.setHint(cdt);
                      //  spinner0.setPrompt("      "+getResources().getString(R.string.TypeofCuttings)+"    ");
                   //  spinner0.setGravity(Gravity.CENTER);
/*
                      ArrayAdapter<String> spinnerAdapter0 = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.simplespinner, R.id.txt, divisonlist);*/
                       /* final ArrayAdapter<String> spinnerAdapter0 = new ArrayAdapter<String>(
                                Butcher_Product_Submit.this,R.layout.simplespinner,divisonlist){
                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if(position%2 == 1) {
                                    // Set the item background color
                                    tv.setBackgroundColor(Color.parseColor("#D3D3D3"));
                                }
                                else {
                                    // Set the alternate item background color
                                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                                }
                                return view;
                            }
                        };*/
//                        ArrayAdapter<String> spinnerAdapter0 = new ArrayAdapter<String>(
//                                Butcher_Product_Submit.this,android.R.layout.simple_spinner_item,divisonlist);
//                        spinnerAdapter0.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                     //   spinner0.setPrompt("      "+getResources().getString(R.string.TypeofCuttings)+"    ");

                       // spinner0.setGravity(Gravity.CENTER);

                     /*   spinAdapter_cuttings= new SpinAdapter_cuttings(Butcher_Product_Submit.this,android.R.layout.simple_list_item_checked,pmm);

                        spinner0.setAdapter(spinAdapter_cuttings);*/
                     //   spinner0.setPrompt(getResources().getString(R.string.NoProducts));





                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                recyclerview1.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                menu_cat.setVisibility(View.GONE);
                              //  dptmt = spinner0.getSelectedItem().toString();
                                typen=pmm.get(position).getType_name();
                                typens=pmm.get(position).getType_id();
                                tyh=pmm.get(position).getType_image();
                                String cdt=pmm.get(position).getCurrency();
                                cuttings_name.setText(typen);
                                Cntry_text.setText(cdt);
                                // title.setText(pmm.get(arg2).getType_name());
                                //  Picasso.with(getApplicationContext()).load(pmm.get(arg2).getType_image()).noFade().noFade().into(Up_img);
                                try {


                                    new Picasso.Builder(Butcher_Product_Submit.this)
                                            .downloader(new OkHttpDownloader(Butcher_Product_Submit.this, Integer.MAX_VALUE))
                                            .build()
                                            .load(pmm.get(position).getType_image())
                                            .noFade()
                                            .into(Up_img, new Callback() {
                                                @Override
                                                public void onSuccess() {
                                                    progress_loader1.setVisibility(View.GONE);
                                                }

                                                @Override
                                                public void onError() {
                                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
                                                }
                                            });
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                if (DetectConnection
                                        .checkInternetConnection(getApplicationContext())) {


                                    //new Acquie_Location_Services_Fragment.DownloadData2().execute();

                                } else {
                                    com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                            .show(Butcher_Product_Submit.this);

                                }







                            }

                        })
                );

            }

            else{
                com.nispok.snackbar.Snackbar.with(Butcher_Product_Submit.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Butcher_Product_Submit.this);

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}