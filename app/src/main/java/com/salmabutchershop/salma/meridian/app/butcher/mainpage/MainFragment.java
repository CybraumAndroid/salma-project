package com.salmabutchershop.salma.meridian.app.butcher.mainpage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Changepassword;
import com.salmabutchershop.salma.meridian.app.butcher.extras.ExtrasFirstPage;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Profile_View_Activity;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Profile_update_butcher;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Terms_Conditions;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct.Butcher_main_Product;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_my_order;
import com.salmabutchershop.salma.meridian.app.butcher.sidebar.Butcher_FeedBack;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.Butcher_Wholseller_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.NetworkCheckingClass;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Contact_Our_Team;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.newintro.New_HttpHandler;
import com.salmabutchershop.salma.meridian.app.customer.newintro.New_ViewPagerAdapter;
import com.salmabutchershop.salma.meridian.app.customer.newintro.New_WelcomeActivity;
import com.salmabutchershop.salma.meridian.app.customer.newintro.ZayedModel;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.ContactUs;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.FeedBack;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.NewsUpdates;
import com.salmabutchershop.salma.meridian.app.retailer.RetailerMainPage;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

public class MainFragment extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    LinearLayout myOrder,purchase,myMenu,extras;
    NavigationView navigationView;
    TextView t2,t3;
    String Reg_id,fullname,typ,userid,latest_version;
    private ArrayList imageURL;
    ProgressBar progress;
    JSONArray array1;
    TextView tb;
    CountModel cm;
    public static MKLoader progress_loader,progress_loader1;
    String strVersion = "";
    String jsonString,shp_nmb,logo,valid;
    TextView But_Nam;
    ImageView search,nav_id;
    ImageView butcher_pfile;
    ImageView nav_img;
    String enable_popup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainfragment);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        progress_loader1=(MKLoader)findViewById(R.id.progress_loader1);
        But_Nam= (TextView) findViewById(R.id.butcher_nm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

         navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        tb= (TextView) findViewById(R.id.imageView2);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullname", null);
        typ= myPrefs.getString("usertype", null);
        shp_nmb= myPrefs.getString("shp_nmb", null);

        t2=(TextView)header. findViewById(R.id.nmae);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        t2.setTypeface(myFont7);
        t2.setText(fullname);
        t3=(TextView)header. findViewById(R.id.textView);

        Typeface myFont9 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        t3.setTypeface(myFont9);
        t3.setText(typ);
        getCurrentVersionInfo();
        if (userid != null) {   // condition true means user is already login
            //  navigationView.getMenu().getItem(6).setIcon(R.drawable.zayed_logout);
            navigationView.getMenu().getItem(6).setTitle(getResources().getString(R.string.logout));
            navigationView.getMenu().getItem(5).setVisible(true);

            System.out.println("logoutt");


        } else {
            //  navigationView.getMenu().getItem(6).setIcon(R.drawable.login);
            navigationView.getMenu().getItem(5).setVisible(false);
            navigationView.getMenu().getItem(6).setTitle(getResources().getString(R.string.signin));
            System.out.println("loginnn");



        }
      //navigationView.getMenu().getItem(7).setVisible(false);
        imageURL=new ArrayList();
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new FetchImage().execute();

        } else {
            com.nispok.snackbar.Snackbar.with(MainFragment.this) // context
                    .text(R.string.oops_your_connection_Seems_off) // text to display
                    .show(MainFragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(MainFragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/



        }
        View hView =  navigationView.inflateHeaderView(R.layout.butcher_nav_header_main);
        navigationView.removeHeaderView(navigationView.getHeaderView(0));
         nav_img= (ImageView)hView.findViewById(R.id.imageView);

        nav_id= (ImageView) findViewById(R.id.nav);
        butcher_pfile= (ImageView) findViewById(R.id.butcher_pfile);
        But_Nam= (TextView) findViewById(R.id.butcher_nme);
        But_Nam.setText(shp_nmb);
        myOrder=(LinearLayout)findViewById(R.id.my_order);
        purchase=(LinearLayout)findViewById(R.id.purchase);
        myMenu=(LinearLayout)findViewById(R.id.my_menu);
        extras=(LinearLayout)findViewById(R.id.extras);
        myOrder.setClickable(true);
        purchase.setClickable(true);
        myMenu.setClickable(true);
        extras.setClickable(true);
        nav_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
     /*   mSearchView.attachNavigationDrawerToMenuButton(drawer);*/
                drawer.openDrawer(GravityCompat.START);
            }
        });


        myOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid != null && !valid.isEmpty() && !valid.equals("null")) {


                if(valid.equalsIgnoreCase("no")){
                    if (!((Activity) MainFragment.this).isFinishing()) {
                        new SweetAlertDialog(MainFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                .setTitleText(getResources().getString(R.string.your_validity_expired))
                                .setContentText(getResources().getString(R.string.please_contact_salma_team_immediately))
                                .setConfirmText(getResources().getString(R.string.ok))
                                //  .setCancelText("No")
                                .setCustomImage(R.drawable.logo)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismiss();
                                    }
                                })

                                .show();
                    }
                    else {

                    }
                }else {
                    Intent i2 = new Intent(getApplicationContext(),Butcher_my_order.class);
                    // i2.putExtra("name","Butcher");
                    startActivity(i2);
                    finish();
                }
                }else {

                }
            }
        });
        purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid != null && !valid.isEmpty() && !valid.equals("null")) {
                    if (valid.equalsIgnoreCase("no")) {
                        if (!((Activity) MainFragment.this).isFinishing()) {
                            new SweetAlertDialog(MainFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(getResources().getString(R.string.your_validity_expired))
                                    .setContentText(getResources().getString(R.string.please_contact_salma_team_immediately))
                                    .setConfirmText(getResources().getString(R.string.ok))
                                    //  .setCancelText("No")
                                    .setCustomImage(R.drawable.logo)
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismiss();
                                        }
                                    })

                                    .show();
                        } else {

                        }
                    } else {
                        Intent i2 = new Intent(getApplicationContext(), Butcher_Wholseller_Fragment.class);
                        // i2.putExtra("name","Butcher");
                        startActivity(i2);
                        finish();
                    }
                }
else{

                    }
                }

        });

        myMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid != null && !valid.isEmpty() && !valid.equals("null")) {
                    if (valid.equalsIgnoreCase("no")) {
                        if (!((Activity) MainFragment.this).isFinishing()) {
                            new SweetAlertDialog(MainFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(getResources().getString(R.string.your_validity_expired))
                                    .setContentText(getResources().getString(R.string.please_contact_salma_team_immediately))
                                    .setConfirmText(getResources().getString(R.string.ok))
                                    //  .setCancelText("No")
                                    .setCustomImage(R.drawable.logo)
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                           /* Intent i2 = new Intent(getApplicationContext(), Butcher_main_Product.class);
                                            // i2.putExtra("name","Butcher");
                                            startActivity(i2);
                                            finish();*/
                                            sDialog.dismiss();
                                        }
                                    })

                                    .show();
                        } else {

                        }
                    } else {
                        Intent i2 = new Intent(getApplicationContext(), Butcher_main_Product.class);
                        // i2.putExtra("name","Butcher");
                        startActivity(i2);
                        finish();
                    }
                }

                else {

                    }



            }
        });

        extras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid != null && !valid.isEmpty() && !valid.equals("null")) {

                    if (valid.equalsIgnoreCase("no")) {
                        if (!((Activity) MainFragment.this).isFinishing()) {
                            new SweetAlertDialog(MainFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(getResources().getString(R.string.your_validity_expired))
                                    .setContentText(getResources().getString(R.string.please_contact_salma_team_immediately))
                                    .setConfirmText(getResources().getString(R.string.ok))
                                    //  .setCancelText("No")
                                    .setCustomImage(R.drawable.logo)
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismiss();
                                        }
                                    })

                                    .show();
                        }
                        else
                        {

                        }
                    } else {
                        Intent i2 = new Intent(getApplicationContext(), ExtrasFirstPage.class);
                        // i2.putExtra("name","Butcher");
                        startActivity(i2);
                        finish();
                    }
                }


                else {
                }



            }
        });



    }
    boolean doubleBackToExitPressedOnce = false;

 private String getCurrentVersionInfo() {


     PackageInfo packageInfo;
     try {
         packageInfo = getApplicationContext()
                 .getPackageManager()
                 .getPackageInfo(
                         getApplicationContext().getPackageName(),
                         0
                 );
         strVersion = packageInfo.versionName;
     } catch (PackageManager.NameNotFoundException e) {

     }
     return strVersion;
 }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            com.nispok.snackbar.Snackbar.with(MainFragment.this) // context
                    .text(R.string.Press_again_to_exit_from_salma) // text to display
                    .show(MainFragment.this);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
           /* Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            Intent i = new Intent(this,MainFragment.class);
            // set the new task and clear flags
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);


            intent.putExtra("EXIT", true);
            startActivity(intent);
            // dialog.dismiss();
            finish();*/
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.my_account) {
            // Handle the camera action
        } else if (id == R.id.international) {

        } else if (id == R.id.favorites) {

        } else if (id == R.id.feedback) {

            Intent i=new Intent(MainFragment.this,Butcher_FeedBack.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.news) {

            Intent i=new Intent(MainFragment.this, com.salmabutchershop.salma.meridian.app.butcher.sidebar.NewsUpdates.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.contact_us) {
            Intent i=new Intent(getApplicationContext(), com.salmabutchershop.salma.meridian.app.butcher.sidebar.Contact_Our_Team.class);
            // i.putExtra("cat_id",cat_id);
            //i.putExtra("value",status);

            startActivity(i);
            finish();
          /*  Intent i=new Intent(MainFragment.this, com.salmabutchershop.salma.meridian.app.butcher.sidebar.ContactUs.class);
            startActivity(i);
            finish();*/
        }
        else if(id==R.id.nav_update_profile){
            Intent i=new Intent(MainFragment.this,Profile_update_butcher.class);
            startActivity(i);
            finish();
        }
        else if(id==R.id.nav_view_profile){
            Intent i=new Intent(MainFragment.this,Profile_View_Activity.class);
            startActivity(i);
            finish();
        }
        else if(id==R.id.nav_Terms){
            Intent i=new Intent(MainFragment.this,Terms_Conditions.class);
            startActivity(i);
            finish();
        }
        else if(id == R.id.nav_Rate) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salmabutchershop.salma.meridian.app"));
            startActivity(i);
        //    finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        }
        else if(id==R.id.change_password){
            Intent i=new Intent(MainFragment.this,Changepassword.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.nav_logout) {
            // Toast.makeText(getApplicationContext(),"test5",Toast.LENGTH_SHORT).show();

//            SharedPreferences settings = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//            settings.edit().clear().commit();
            if (navigationView.getMenu().getItem(6).getTitle() == getResources().getString(R.string.logout)) {
                new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.drawable.logo)
                        .setTitleText(getResources().getString(R.string.logout))
                        .setContentText(getResources().getString(R.string.AreyouSurewanttoLogoutnow))
                        .setConfirmText(getResources().getString(R.string.Yes))
                        .setCancelText(getResources().getString(R.string.No))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                logout();
                                sDialog.dismiss();

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

            } else {


                Intent intlog = new Intent(getApplicationContext(), ButcherLogin.class);
                startActivity(intlog);
                finish();
            }
        }
        else if(id==R.id.nav_lang){
            try {

                new SweetAlertDialog(MainFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Chooseyourlanguage))
                        // .setContentText("Won't be able to recover this file!")

                        .setCustomImage(R.drawable.logo)
                        .setCancelText(getResources().getString(R.string.Arabic))
                        .setConfirmText(getResources().getString(R.string.English))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                    /*sDialog*/
                             /*   SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("flag_id", flag);
                                editor.commit();*/
                                Locale locale2 = new Locale("ar");
                                Locale.setDefault(locale2);
                                Configuration config2 = new Configuration();
                                config2.locale = locale2;
                                getBaseContext().getResources().updateConfiguration(config2, null);
                                Intent thisIntent = getIntent();
                                startActivity(thisIntent);
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                             /*   SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("flag_id", flag);
                                editor.commit();*/

                                Locale locale2 = new Locale("en");
                                Locale.setDefault(locale2);
                                Configuration config2 = new Configuration();
                                config2.locale = locale2;
                                getBaseContext().getResources().updateConfiguration(config2, null);
                                Intent thisIntent = getIntent();
                                startActivity(thisIntent);
                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            }
            catch (Exception e){
                e.printStackTrace();
            }




        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        NetworkCheckingClass networkCheckingClass=new NetworkCheckingClass(getApplicationContext());
        boolean i= networkCheckingClass.ckeckinternet();
        progress_loader.setVisibility(View.VISIBLE);
        if(i) {
            //  Toast.makeText(getApplicationContext(), "Social Media", Toast.LENGTH_SHORT).show();\




            StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER+"json/butcher/logout.php?", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    System.out.println("reee"+response);
                    System.out.println("kkk"+response);
                    progress.setVisibility(View.GONE);
                    SharedPreferences preferences= getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    userid = preferences.getString("user_idb", null);
                    preferences.edit().clear().commit();
                    Intent  i = new Intent(MainFragment.this,ButcherLogin.class);
                    startActivity(i);
                    finish();

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userid);




                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else {
            com.nispok.snackbar.Snackbar.with(MainFragment.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(MainFragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(MainFragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/
        }
    }

    private class FetchImage extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                New_HttpHandler h = new New_HttpHandler();
                jsonString = h.makeServiceCall(SERVER+"json/butcher/order_counts.php?butcher_id="+userid);
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress_loader.setVisibility(ProgressBar.GONE);
            if (jsonString != null && !jsonString.isEmpty() && !jsonString.equals("null")) {
                try {
                    array1 = new JSONArray(jsonString);

                    for (int i = 0; i < array1.length(); i++) {
                        JSONObject obj = array1.getJSONObject(i);
                        cm = new CountModel();
                        latest_version = obj.getString("latest_version");
                        logo = obj.getString("logo");
                        enable_popup= obj.getString("enable_popup");
                        valid = obj.getString("valid");
                        cm.total = obj.getString("total");
                        cm.accepted = obj.getString("accepted");
                        cm.pending = obj.getString("pending");
                        cm.latest_version = obj.getString("latest_version");
                        cm.logo= obj.getString("latest_version");

                        imageURL.add(cm);


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {


                    new Picasso.Builder(MainFragment.this)
                            .downloader(new OkHttpDownloader(MainFragment.this, Integer.MAX_VALUE))
                            .build()
                            .load(logo)
                            .noFade()
                            .into(butcher_pfile, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progress_loader1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }
              /*  try {


                    new Picasso.Builder(MainFragment.this)
                            .downloader(new OkHttpDownloader(MainFragment.this, Integer.MAX_VALUE))
                            .build()
                            .load(logo)
                            .noFade()
                            .into(nav_img, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progress_loader1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }*/

                if (latest_version.equalsIgnoreCase(strVersion)) {

                } else {
                    if (enable_popup.equalsIgnoreCase("no")){

                    }else {
                        displayPopup();
                    }

                }

            } else {
                System.out.println("nodots");
            }
            try {

if(valid.equalsIgnoreCase("no")){
    if (!((Activity) MainFragment.this).isFinishing()) {
        new SweetAlertDialog(MainFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText(getResources().getString(R.string.your_validity_expired))
                .setContentText(getResources().getString(R.string.please_contact_salma_team_immediately))
                .setConfirmText(getResources().getString(R.string.ok))
              //  .setCancelText("No")
                .setCustomImage(R.drawable.logo)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                      sDialog.dismiss();
                    }
                })

                .show();
    }
}else {

}

         if(cm.pending != null && !cm.pending.isEmpty() && !cm.pending.equals("null")&&!cm.pending.equalsIgnoreCase("")) {
             if (cm.pending.equalsIgnoreCase("0")) {

                 tb.setVisibility(View.GONE);
             } else {
                 tb.setVisibility(View.VISIBLE);
                 tb.setText(cm.pending);

             }
         }
         else {

         }
      /*  myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);*/


        }catch(
        Exception e)

        {
            e.printStackTrace();
        }
    }
    }

    private void displayPopup() {
  if (!((Activity) MainFragment.this).isFinishing()) {
            new SweetAlertDialog(MainFragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText(getResources().getString(R.string.new_update_available))
                    .setContentText(getResources().getString(R.string.Pleaseupdate))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setCustomImage(R.drawable.logo)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salmabutchershop.salma.meridian.app"));
                            startActivity(i);


                            sDialog.dismiss();

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
        }
    }
    }

