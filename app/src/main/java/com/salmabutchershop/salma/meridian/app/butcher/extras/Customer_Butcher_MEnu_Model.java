package com.salmabutchershop.salma.meridian.app.butcher.extras;

/**
 * Created by libin on 3/24/2017.
 */

class Customer_Butcher_MEnu_Model {
    public String getButcher_id() {
        return butcher_id;
    }

    public void setButcher_id(String butcher_id) {
        this.butcher_id = butcher_id;
    }

    public String getTyp_id() {
        return typ_id;
    }

    public void setTyp_id(String typ_id) {
        this.typ_id = typ_id;
    }

    public String getPro_cutting_type_id() {
        return pro_cutting_type_id;
    }

    public void setPro_cutting_type_id(String pro_cutting_type_id) {
        this.pro_cutting_type_id = pro_cutting_type_id;
    }

    public String getPro_cutting_type_name() {
        return pro_cutting_type_name;
    }

    public void setPro_cutting_type_name(String pro_cutting_type_name) {
        this.pro_cutting_type_name = pro_cutting_type_name;
    }

    public String getPro_cat_id() {
        return pro_cat_id;
    }

    public void setPro_cat_id(String pro_cat_id) {
        this.pro_cat_id = pro_cat_id;
    }

    public String getPro_cat_name() {
        return pro_cat_name;
    }

    public void setPro_cat_name(String pro_cat_name) {
        this.pro_cat_name = pro_cat_name;
    }

    public String getPro_sub_cat_id() {
        return pro_sub_cat_id;
    }

    public void setPro_sub_cat_id(String pro_sub_cat_id) {
        this.pro_sub_cat_id = pro_sub_cat_id;
    }

    public String getPro_sub_cat_name() {
        return pro_sub_cat_name;
    }

    public void setPro_sub_cat_name(String pro_sub_cat_name) {
        this.pro_sub_cat_name = pro_sub_cat_name;
    }

    public String getTyp_old_price() {
        return typ_old_price;
    }

    public void setTyp_old_price(String typ_old_price) {
        this.typ_old_price = typ_old_price;
    }

    public String getTyp_prize() {
        return typ_prize;
    }

    public void setTyp_prize(String typ_prize) {
        this.typ_prize = typ_prize;
    }

    public String getTyp_qnty() {
        return typ_qnty;
    }

    public void setTyp_qnty(String typ_qnty) {
        this.typ_qnty = typ_qnty;
    }

    public String getTyp_unit() {
        return typ_unit;
    }

    public void setTyp_unit(String typ_unit) {
        this.typ_unit = typ_unit;
    }

    public String getTyp_currency() {
        return typ_currency;
    }

    public void setTyp_currency(String typ_currency) {
        this.typ_currency = typ_currency;
    }

    public String getTyp_discount() {
        return typ_discount;
    }

    public void setTyp_discount(String typ_discount) {
        this.typ_discount = typ_discount;
    }

    public String getTyp_discount_unit() {
        return typ_discount_unit;
    }

    public void setTyp_discount_unit(String typ_discount_unit) {
        this.typ_discount_unit = typ_discount_unit;
    }

    public String getTyp_desc() {
        return typ_desc;
    }

    public void setTyp_desc(String typ_desc) {
        this.typ_desc = typ_desc;
    }

    public String getTyp_delivery() {
        return typ_delivery;
    }

    public void setTyp_delivery(String typ_delivery) {
        this.typ_delivery = typ_delivery;
    }

    public String getTyp_image() {
        return typ_image;
    }

    public void setTyp_image(String typ_image) {
        this.typ_image = typ_image;
    }

    String butcher_id,typ_id,pro_cutting_type_id,pro_cutting_type_name,pro_cat_id,
            pro_cat_name,pro_sub_cat_id,pro_sub_cat_name,typ_old_price,typ_prize,
            typ_qnty,typ_unit,typ_currency,typ_discount,typ_discount_unit,typ_desc,typ_delivery,typ_image;
}
