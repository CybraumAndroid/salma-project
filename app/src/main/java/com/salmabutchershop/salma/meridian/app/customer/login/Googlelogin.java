package com.salmabutchershop.salma.meridian.app.customer.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;

/**
 * Created by libin on 1/3/2017.
 */

public class Googlelogin extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

//Signin button
private SignInButton signInButton;

//Signing Options
private GoogleSignInOptions gso;

//google api client
public GoogleApiClient mGoogleApiClient;

//Signin constant to check the activity result
private int RC_SIGN_IN = 100;

//TextViews
public TextView textViewName;
public TextView textViewEmail;
public NetworkImageView profilePhoto;
        private ProgressDialog mProgressDialog;
//Image Loader
private ImageLoader imageLoader;

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_layout);

        //Initializing Views
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);
        profilePhoto = (NetworkImageView) findViewById(R.id.profileImage);

        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
                .requestIdToken("778644506647-fdq786qefvscbvjggjd3kbdtk12t4j7c.apps.googleusercontent.com")
        .build();

        //Initializing signinbutton
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
        .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
        .build();

        //Setting onclick listener to signing button
        signInButton.setOnClickListener(this);
        }


//This function will option signing intent
private void signIn() {
        //Creating an intent


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
        }

@Override
public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        //Calling a new function to handle signin
                GoogleSignInAccount acct = result.getSignInAccount();
                System.out.println("<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>"+acct.getDisplayName());
                //Displaying name and email
                textViewName.setText(acct.getDisplayName());
                textViewEmail.setText(acct.getEmail());
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("user_id", acct.getId());
                editor.putString("c_name",acct.getDisplayName());
                editor.putString("c_email",acct.getEmail());
                editor.putString("social_name",acct.getGivenName());
                editor.putString("social_email",acct.getEmail());
                editor.putString("social_gender","");
                editor.putString("social_profile_image",acct.getPhotoUrl().toString());
                editor.commit();
               /* Person person  = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                if (person != null) {
                        Log.i(TAG, "--------------------------------");
                        Log.i(TAG, "Display Name: " + person.getDisplayName());
                        Log.i(TAG, "Gender: " + person.getGender());
                        Log.i(TAG, "About Me: " + person.getAboutMe());
                        Log.i(TAG, "Birthday: " + person.getBirthday());
                        Log.i(TAG, "Current Location: " + person.getCurrentLocation());
                        Log.i(TAG, "Language: " + person.getLanguage());
                } else {
                        Log.e(TAG, "Error!");
                }*/

       handleSignInResult(result);
        }
        }


//After the signing we are calling this function
private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
        //Getting google account
                Log.d("TAG", "handleSignInResult:" + result.isSuccess());

        GoogleSignInAccount acct = result.getSignInAccount();

        //Displaying name and email
        textViewName.setText(acct.getDisplayName());
        textViewEmail.setText(acct.getEmail());
System.out.println("<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>"+result.isSuccess());
        //Initializing image loader
        imageLoader = CustomVolleyRequest.getInstance(this.getApplicationContext())
        .getImageLoader();
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("user_id", acct.getId());
                editor.putString("c_name",acct.getGivenName());
                editor.putString("c_email",acct.getEmail());
                editor.putString("social_name",acct.getGivenName());
                editor.putString("social_email",acct.getEmail());
                editor.putString("social_gender","");
                editor.commit();



                SharedPreferences preferences2 = getSharedPreferences("SocialPref", MODE_PRIVATE);
                SharedPreferences.Editor editor2 = preferences2.edit();
                editor2.putString("sociallogin","true");
                editor2.putString("google", "true");
                editor2.putString("facebook","false");
                editor2.commit();

                // imageLoader.get(acct.getPhotoUrl().toString(),
       // ImageLoader.getImageListener(profilePhoto,
       // R.mipmap.ic_launcher,
     //   R.mipmap.ic_launcher));

        //Loading image
     //   profilePhoto.setImageUrl(acct.getPhotoUrl().toString(), imageLoader);
                Intent inn= new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                startActivity(inn);
        } else {
        //If login fails
      //  Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
        }

@Override
public void onClick(View v) {
        if (v == signInButton) {
        //Calling signin
        signIn();
        }
        }
       

        @Override
        public void onStart() {
                super.onStart();

                OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
                if (opr.isDone()) {
                        // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                        // and the GoogleSignInResult will be available instantly.
                       // Log.d(TAG, "Got cached sign-in");
                        GoogleSignInResult result = opr.get();
                        handleSignInResult(result);
                } else {
                        // If the user has not previously signed in on this device or the sign-in has expired,
                        // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                        // single sign-on will occur in this branch.
                        showProgressDialog();
                        opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                                @Override
                                public void onResult(GoogleSignInResult googleSignInResult) {
                                        hideProgressDialog();
                                        handleSignInResult(googleSignInResult);
                                }
                        });
                }
        }

        private void hideProgressDialog() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.hide();
                }
        }

        private void showProgressDialog() {
                if (mProgressDialog == null) {
                        mProgressDialog = new ProgressDialog(this);
                        mProgressDialog.setMessage("loading");
                        mProgressDialog.setIndeterminate(true);
                }

                mProgressDialog.show();
        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        }

}
