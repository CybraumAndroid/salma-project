package com.salmabutchershop.salma.meridian.app.customer.myaccount;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.address.DatabaseHandler_N;


import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 12/13/2016.
 */
public class NewDataAdapter_NS extends RecyclerView.Adapter <NewDataAdapter_NS.ViewHolder>{
    private ArrayList<com.salmabutchershop.salma.meridian.app.customer.address.DataValues_N> DataValues;
    String inst;
    private Context context1;
    DatabaseHandler_N dh=new DatabaseHandler_N();

    public NewDataAdapter_NS(Context context2, ArrayList<com.salmabutchershop.salma.meridian.app.customer.address.DataValues_N> dataValues, String inst){
        this.DataValues=dataValues;
        this.inst=inst;
        context1=context2;

    }
    @Override
    public NewDataAdapter_NS.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_data_row_ns,parent,false);
        return new NewDataAdapter_NS.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NewDataAdapter_NS.ViewHolder holder, final int position){
        holder.primary_id.setText(""+DataValues.get(position).id);
        holder.name.setText(DataValues.get(position).fname+" "+DataValues.get(position).lname);
        holder.city.setText(DataValues.get(position).city);
        holder.street.setText(DataValues.get(position).addresstype);
        holder.ad_typ.setText(DataValues.get(position).street);
        holder.building.setText("Bldng: "+DataValues.get(position).building);
        holder.floor.setText("Floor: "+DataValues.get(position).floor);
        holder.apart_no.setText("Apartment: "+DataValues.get(position).apartno);
        holder.mobile.setText("Mob: "+DataValues.get(position).mobile);
        holder.email.setText("Email: "+DataValues.get(position).phone);
        System.out.println("<<<<<<<<<<<<<<<<<<>>>>>>>>>>"+DataValues.get(position).addresstype);
      /*  holder.deliver_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("<<<<<<<<<<<<<<<<<<>>>>>>>>>>"+DataValues.get(position).addresstype);
                System.out.println("url "+inst);
                Intent i = new Intent(context1, AddressFragment.class);
                i.putExtra("id_nam","antony");
                i.putExtra("fnames",DataValues.get(position).fname);
                i.putExtra("lnames",DataValues.get(position).lname);
                i.putExtra("citys",DataValues.get(position).city);
                i.putExtra("buildings",DataValues.get(position).building);
                i.putExtra("floors",DataValues.get(position).floor);
                i.putExtra("apartrments",DataValues.get(position).apartno);
                i.putExtra("dlvry_area",DataValues.get(position).deliveryarea);
                i.putExtra("mobile",DataValues.get(position).mobile);
                i.putExtra("phoe",DataValues.get(position).phone);
                i.putExtra("loc",DataValues.get(position).addtional_direc);
                i.putExtra("inst",inst);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //   i.putExtra("sub_tot",foo);
                //System.out.println("detailll6"+gtot);
                context1. startActivity(i);

            }
        });
*/
        holder.clse_lt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //System.out.println("url "+DataValues.get(position).getImage());


                try {
                    new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Are you sure?")
                            .setContentText("want to delete this Address ?")
                            .setCustomImage(R.drawable.logo)
                            .setConfirmText("Yes")
                           // .setCancelText("No")
                          /*  .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })*/
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    dh.DeleteViewN(context1,holder);

                                    //  Toast.makeText(context1, "Deleted", Toast.LENGTH_SHORT).show();


                                    ///////////////// REMOVING ITEM FROM RECYCLE LIST --START/////////////////
                                    DataValues.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position,DataValues.size());
                                    sweetAlertDialog
                                            .setTitleText("Deleted!")
                                            .setContentText("Your Address has been deleted!")
                                            .setConfirmText("OK")

                                            .setConfirmClickListener(null)
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                }
                            })
                            .show();
                   /* new AlertDialog.Builder(view.getRootView().getContext())
                            .setTitle("Delete Item")
                            .setMessage("Are you sure want to delete Address?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    dh.DeleteViewN(context1,holder);

                                  //  Toast.makeText(context1, "Deleted", Toast.LENGTH_SHORT).show();


                                    ///////////////// REMOVING ITEM FROM RECYCLE LIST --START/////////////////
                                    DataValues.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position,DataValues.size());
                                    ///////////////// REMOVING ITEM FROM RECYCLE LIST --END/////////////////

                                }

                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
*/



                }
                catch(Exception e){
                    e.printStackTrace();

                    Toast.makeText(context1, "Failed", Toast.LENGTH_SHORT).show();
                }





            }
        });

    }

    @Override
    public int getItemCount(){
        return DataValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public TextView name,city,street,building,floor,apart_no,mobile,phone,primary_id,ad_typ,email;
        LinearLayout clse_lt;
        Button deliver_here;
        public ViewHolder(View itemView) {
            super(itemView);

            primary_id=(TextView)itemView.findViewById(R.id.pid);

            name=(TextView)itemView.findViewById(R.id.name);
            Typeface myFont1 = Typeface.createFromAsset(context1.getAssets(), "open-sans.bold.ttf");
            name.setTypeface(myFont1);

            city=(TextView)itemView.findViewById(R.id.city);
            Typeface myFont2 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            city.setTypeface(myFont2);

            street=(TextView)itemView.findViewById(R.id.street);
            Typeface myFont3 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            street.setTypeface(myFont3);

            building=(TextView)itemView.findViewById(R.id.building);
            Typeface myFont4 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            building.setTypeface(myFont4);

            floor=(TextView)itemView.findViewById(R.id.floor);
            Typeface myFont5 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            floor.setTypeface(myFont5);

            apart_no=(TextView)itemView.findViewById(R.id.apartNo);
            Typeface myFont6 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            apart_no.setTypeface(myFont6);


            mobile=(TextView)itemView.findViewById(R.id.mobile);
            Typeface myFont7 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            mobile.setTypeface(myFont7);

            ad_typ=(TextView)itemView.findViewById(R.id.ad_typ);
            Typeface myFont8 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            ad_typ.setTypeface(myFont8);

            email=(TextView)itemView.findViewById(R.id.eml);
            Typeface myFont10 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            email.setTypeface(myFont10);

            clse_lt=(LinearLayout) itemView.findViewById(R.id.clse_l);
            deliver_here=(Button) itemView.findViewById(R.id.button4);
            Typeface myFont9 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
            deliver_here.setTypeface(myFont9);
        }

    }
}