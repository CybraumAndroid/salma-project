package com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.NetworkCheckingClass;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.retailer.international.InternationalFragment;
import com.salmabutchershop.salma.meridian.app.retailer.login.LoginActivityRetailer;
import com.salmabutchershop.salma.meridian.app.retailer.login.RetailerChangePassword;
import com.salmabutchershop.salma.meridian.app.retailer.login.Retailer_FeedBack;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/7/2016.
 */

public class Fresh_Frozen_Retailer extends AppCompatActivity implements View.OnClickListener,SearchView.OnQueryTextListener,NavigationView.OnNavigationItemSelectedListener {
    LinearLayout Fresh,Frozen;
    RecyclerView recyclerview;
    public static MKLoader progress_loader,progress_loader1;
    Retailer_Froz_Fresh_adapter adapter;
    RVAdapter1 adapter1;
    ProgressBar progress;
    String result, galry_image,Dat;
    static ArrayList<Retailer_fresh_Model> rfm;
    Retailer_fresh_Model rm;
    String ids;
    String cat_id,subcat_id,subcat_name,cat_image;
    private FloatingSearchView mSearchView,mSearchView1;
    List<CountryModel1> mCountryModel1;
    CountryModel1 cm;
    String Wurl;
    NavigationView navigationView;
    TextView t2,t3,Ret_Nam;
    String Reg_id,fullname,typ,userid,latest_version,valid,logo;
    ImageView search,nav_id;
    String strVersion = "";
    ImageView butcher_pfile;
    String shop_name,enable_popup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retailer_home_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        progress_loader1=(MKLoader)findViewById(R.id.progress_loader1);
        butcher_pfile= (ImageView) findViewById(R.id.butcher_pfile);
        Ret_Nam= (TextView) findViewById(R.id.retailer_nm);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idL",null);
        fullname = myPrefs.getString("fullname", null);
        typ= myPrefs.getString("usertype", null);
        shop_name=myPrefs.getString("shp_nm",null);
        System.out.println("<<<<<<<<<<<<<<<<shop_name>>>>>>>>>>>>>>>>>>>>>>>>>"+shop_name);
        Ret_Nam.setText(shop_name);
      //  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

      /* final Toolbar toolbar = (Toolbar) findViwById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        // mDrawerToggle.syncState();
      /*  setSupportActionBar(toolbar);ewById(R.id.toolbar);*/
       // setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        // setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
       /* Toolbar toolbar = (Toolbar) findVie
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);*/
        Fresh= (LinearLayout) findViewById(R.id.frsh);
        Frozen= (LinearLayout) findViewById(R.id.frzn);
        Fresh.setOnClickListener(this);
        Frozen.setOnClickListener(this);
        nav_id= (ImageView) findViewById(R.id.nav);
        getCurrentVersionInfo();
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 2));

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)
            ids="1";
            new DownloadData1().execute();

        } else {
            com.nispok.snackbar.Snackbar.with(Fresh_Frozen_Retailer.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Fresh_Frozen_Retailer.this);


        }


     /*   ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);*/
       // toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
navigationView.getMenu().getItem(1).setVisible(true);


        nav_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
     /*   mSearchView.attachNavigationDrawerToMenuButton(drawer);*/
                drawer.openDrawer(GravityCompat.START);
            }
        });
        mSearchView = (FloatingSearchView)findViewById(R.id.floating_search_view);
        //recyclerview.setLayoutManager(layoutManager);
        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {
                try {
                    final List<CountryModel1> filteredModelList = filter(mCountryModel1, newQuery);
                    adapter1.setFilter(filteredModelList);
                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        });

    }

    private String getCurrentVersionInfo() {
        PackageInfo packageInfo;
        try {
            packageInfo = getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(
                            getApplicationContext().getPackageName(),
                            0
                    );
            strVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        return strVersion;
    }

    private void goBack() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.frsh:
                Fresh.setBackgroundResource(R.color.colorAccent);
                Frozen.setBackgroundResource(R.color.colorGray);
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)
                    ids="1";
                    new DownloadData1().execute();

                } else {
                    com.nispok.snackbar.Snackbar.with(Fresh_Frozen_Retailer.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(Fresh_Frozen_Retailer.this);
                   /* com.chootdev.csnackbar.Snackbar.with(this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/


                }
                break;
            case R.id.frzn:
                Fresh.setBackgroundResource(R.color.colorGray);
                Frozen.setBackgroundResource(R.color.colorAccent);
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)
ids="2";
                    new DownloadData1().execute();
                } else {
                    com.nispok.snackbar.Snackbar.with(Fresh_Frozen_Retailer.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(Fresh_Frozen_Retailer.this);


                }
                break;
            default:

        }

    }
    @Override
    public boolean onQueryTextChange(String newText) {
        final List<CountryModel1> filteredModelList = filter(mCountryModel1, newText);
        adapter1.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    private List<CountryModel1> filter(List<CountryModel1> models, String query) {
        query = query.toLowerCase();

        final List<CountryModel1> filteredModelList = new ArrayList<>();
        for (CountryModel1 model1 : models) {
            final String text = model1.getSubcat_name().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model1);
            }
        }
        return filteredModelList;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.my_account) {
            // Handle the camera action
        } else if (id == R.id.international) {
            Intent inter = new Intent(getApplicationContext(),InternationalFragment.class);
            startActivity(inter);
        }
        else if (id == R.id.news) {
            Intent i=new Intent(Fresh_Frozen_Retailer.this, com.salmabutchershop.salma.meridian.app.retailer.login.news.NewsUpdates.class);
            startActivity(i);
        }
        else if (id == R.id.favorites) {

        } else if (id == R.id.feedback) {
            Intent i=new Intent(Fresh_Frozen_Retailer.this,Retailer_FeedBack.class);
            startActivity(i);

        } else if (id == R.id.contact_us) {
            Intent i=new Intent(getApplicationContext(), com.salmabutchershop.salma.meridian.app.retailer.login.Contact_Our_Team.class);
            // i.putExtra("cat_id",cat_id);
            //i.putExtra("value",status);

            startActivity(i);
            finish();
          /*  Intent i=new Intent(Fresh_Frozen_Retailer.this, com.salmabutchershop.salma.meridian.app.retailer.login.ContactUs.class);
            startActivity(i);*/
        }
        else if(id ==R.id.nav_Terms) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i=new Intent(Fresh_Frozen_Retailer.this, com.salmabutchershop.salma.meridian.app.retailer.login.Terms_Conditions.class);
            startActivity(i);
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        }
        else if(id == R.id.nav_Rate) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salmabutchershop.salma.meridian.app"));
            startActivity(i);
            //finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        }
        else if(id == R.id.nav_update_profile) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i=new Intent(Fresh_Frozen_Retailer.this,Profile_update_retailer.class);
            System.out.println("retailerrrr clickkkkkkkkkkk");
            startActivity(i);
            finish();
          ///  finish();
            //finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        }
        else if(id == R.id.change_password) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i=new Intent(Fresh_Frozen_Retailer.this,RetailerChangePassword.class);
            System.out.println("retailerrrr clickkkkkkkkkkk");
            startActivity(i);
            ///  finish();
            //finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
        }


        else if(id == R.id.nav_view_profile) {
            // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            Intent i=new Intent(Fresh_Frozen_Retailer.this,Profile_View_Retailer.class);
            System.out.println("retailerrrr clickkkkkkkkkkk");
            startActivity(i);
            finish();
            //finish();
            //   overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

        }
        else if (id == R.id.nav_logout) {
            // Toast.makeText(getApplicationContext(),"test5",Toast.LENGTH_SHORT).show();

//            SharedPreferences settings = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//            settings.edit().clear().commit();
            if (navigationView.getMenu().getItem(6).getTitle() == getResources().getString(R.string.logout)) {
                System.out.println("kkk"+"log2");
                new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.drawable.logo)
                        .setTitleText(getResources().getString(R.string.logout))
                        .setContentText(getResources().getString(R.string.AreyouSurewanttoLogoutnow))
                        .setConfirmText(getResources().getString(R.string.Yes))
                        .setCancelText(getResources().getString(R.string.No))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                logout();
                                sDialog.dismiss();

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

            } else {

                System.out.println("kkk"+"log1");
                Intent intlog = new Intent(getApplicationContext(), LoginActivityRetailer.class);
                startActivity(intlog);
                finish();
            }
        }
        else if(id==R.id.nav_lang){

            try {

                new SweetAlertDialog(Fresh_Frozen_Retailer.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Chooseyourlanguage))
                        // .setContentText("Won't be able to recover this file!")

                        .setCustomImage(R.drawable.logo)
                        .setCancelText(getResources().getString(R.string.Arabic))
                        .setConfirmText(getResources().getString(R.string.English))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                    /*sDialog*/
                             /*   SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("flag_id", flag);
                                editor.commit();*/
                                Locale locale2 = new Locale("ar");
                                Locale.setDefault(locale2);
                                Configuration config2 = new Configuration();
                                config2.locale = locale2;
                                getBaseContext().getResources().updateConfiguration(config2, null);
                                Intent thisIntent = getIntent();
                                startActivity(thisIntent);
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                             /*   SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("flag_id", flag);
                                editor.commit();*/

                                Locale locale2 = new Locale("en");
                                Locale.setDefault(locale2);
                                Configuration config2 = new Configuration();
                                config2.locale = locale2;
                                getBaseContext().getResources().updateConfiguration(config2, null);
                                Intent thisIntent = getIntent();
                                startActivity(thisIntent);
                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            }
            catch (Exception e){
                e.printStackTrace();
            }


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
     /*   mSearchView.attachNavigationDrawerToMenuButton(drawer);*/
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        NetworkCheckingClass networkCheckingClass=new NetworkCheckingClass(getApplicationContext());
        boolean i= networkCheckingClass.ckeckinternet();
        progress_loader.setVisibility(View.VISIBLE);
        if(i) {
            //  Toast.makeText(getApplicationContext(), "Social Media", Toast.LENGTH_SHORT).show();\
            SharedPreferences preferences= getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
            userid = preferences.getString("user_idL", null);
            preferences.edit().clear().commit();
            System.out.println("userid"+userid);


            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/logout.php?", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    System.out.println("reee"+response);
                    System.out.println("kkk"+response);

progress.setVisibility(View.GONE);
                    Intent  i = new Intent(Fresh_Frozen_Retailer.this,SelectionPage.class);
                    startActivity(i);
                    finish();

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userid);




                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else {
            com.nispok.snackbar.Snackbar.with(Fresh_Frozen_Retailer.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Fresh_Frozen_Retailer.this);

        }
    }


    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);

            String l=getResources().getConfiguration().locale.getDisplayLanguage();
            http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/product_cat.php?cat_id=1&retailer_id=271
            System.out.println("<<<<<<lang>>>>>"+"http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/product_cat.php?cat_id="+ids+"&retailer_id="+userid);
            System.out.println("<<<<<<lang1>>>>>"+userid);
            if (l.equalsIgnoreCase("English")) {
                Wurl=SERVER+"json/retailer/product_cat.php?cat_id="+ids+"&retailer_id="+userid;
            }
            else{
                Wurl=SERVER+"json/retailer/ar/product_cat.php?cat_id="+ids+"&retailer_id="+userid;
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {



                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            progress_loader.setVisibility(ProgressBar.GONE);
          //  String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
         //   System.out.println(">>>>>>>>>>>>>>>" + sam);
         //   String value = result;
     /*       if
                    (result.equalsIgnoreCase("[]")) {
                new SweetAlertDialog(Fresh_Frozen_Retailer.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                sDialog.dismiss();
                            }
                        })
                        .show();
                *//*com.nispok.snackbar.Snackbar.with(Fresh_Frozen_Retailer.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Fresh_Frozen_Retailer.this);*//*

            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
              /*  rfm = new ArrayList<Retailer_fresh_Model>();*/
                mCountryModel1 = new ArrayList<>();
                mCountryModel1.clear();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        // rm = new Retailer_fresh_Model();
                        cm = new CountryModel1();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //3 Log.d("OutPut", mJsonObject.getString("image"));

                        //  String ,,,,,,,;


                        cat_id = mJsonObject.getString("cat_id");
                        subcat_id = mJsonObject.getString("subcat_id");
                        subcat_name = mJsonObject.getString("subcat_name");
                        cat_image = mJsonObject.getString("cat_image");
                        latest_version = mJsonObject.getString("latest_version");
                        valid = mJsonObject.getString("valid");
                        logo = mJsonObject.getString("retailer_logo");
                        enable_popup= mJsonObject.getString("enable_popup");
                        // String sponsor_id,event_id,name,email,phone,photo,address,website,content;
                        cm.setCat_id(cat_id);
                        cm.setSubcat_id(subcat_id);
                        cm.setSubcat_name(subcat_name);
                        cm.setCat_image(cat_image);
                        cm.setLatest_version(latest_version);
                        cm.setValid(valid);
                        cm.setEnable_popup(enable_popup);

                        mCountryModel1.add(cm);
                        System.out.println("<<<oo>>>>" + rm);

                        adapter1 = new RVAdapter1(mCountryModel1, getApplicationContext());
                        recyclerview.setAdapter(adapter1);



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {


                    new Picasso.Builder(Fresh_Frozen_Retailer.this)
                            .downloader(new OkHttpDownloader(Fresh_Frozen_Retailer.this, Integer.MAX_VALUE))
                            .build()
                            .load(logo)
                            .noFade()
                            .into(butcher_pfile, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progress_loader1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }
                try {


                if(valid.equalsIgnoreCase("no")){
                    if (!((Activity) Fresh_Frozen_Retailer.this).isFinishing()) {
                        new SweetAlertDialog(Fresh_Frozen_Retailer.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                .setTitleText("Your Validity Expired")
                                .setContentText("Please Contact Salma Team Immediately")
                                .setConfirmText(getResources().getString(R.string.ok))
                                //  .setCancelText("No")
                                .setCustomImage(R.drawable.logo)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismiss();
                                    }
                                })

                                .show();
                    }

                }else {

                }
                }catch (NullPointerException e){

                }
                try {
                if (latest_version.equalsIgnoreCase(strVersion)) {

                } else {
                    if (enable_popup.equalsIgnoreCase("no")){

                    }else {
                        displayPopup();
                    }
                }
                }catch (NullPointerException e){

                }


            }
            else{
                try {
                new SweetAlertDialog(Fresh_Frozen_Retailer.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })
                        .show();
                }catch (NullPointerException e){

                }

        /*        com.nispok.snackbar.Snackbar.with(Fresh_Frozen_Retailer.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Fresh_Frozen_Retailer.this);
*/

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

            recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {


                                if(valid.equalsIgnoreCase("no")){
                                    if (!((Activity) Fresh_Frozen_Retailer.this).isFinishing()) {
                                        new SweetAlertDialog(Fresh_Frozen_Retailer.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                                .setTitleText("Your Validity Expired")
                                                .setContentText("Please Contact Salma Team Immediately")
                                                .setConfirmText(getResources().getString(R.string.ok))
                                                //  .setCancelText("No")
                                                .setCustomImage(R.drawable.logo)
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismiss();
                                                    }
                                                })

                                                .show();
                                    }
                                }else {
                                    adapter1.setViews(position);
                                    System.out.println("positionc"+position);
                                }



                            }
                        })
                );


            }


        }

    private void displayPopup() {
        if (!((Activity) Fresh_Frozen_Retailer.this).isFinishing()) {
            new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("New Update is Available")
                    .setContentText("Update Now ?")
                    .setConfirmText("Yes")
                    .setCancelText("No")
                    .setCustomImage(R.drawable.logo)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salmabutchershop.salma.meridian.app"));
                            startActivity(i);


                            sDialog.dismiss();

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            com.nispok.snackbar.Snackbar.with(Fresh_Frozen_Retailer.this) // context
                    .text(R.string.Press_again_to_exit_from_salma)  // text to display
                    .show(Fresh_Frozen_Retailer.this);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        }
    }
    }

