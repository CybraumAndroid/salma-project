package com.salmabutchershop.salma.meridian.app.butcher.wholseller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.detail.Wholeseller_detail_Fragment;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.enquiry.EnquiryFragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.DashboardActivity1;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 9/29/2016.
 */

public class Butcher_Wholseller_Fragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;
  //  static ArrayList<UpcomingModel> upm;
 Butcher_Wholseller_adapter adapter1;
    ProgressBar progress;
    String result, galry_image,Dat,Dat1;
    static ArrayList<Butcher_Wholseller_Model> zwm;
    Butcher_Wholseller_Model wm;
    String event_id,winner_name,winner_image,award_name;
    String id,shop_name,name,address,address2,location,wholeseller_logo;
    String Wurl;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wholeseller_fragment_layoutfirst);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
       // Dat= getArguments().getString("desc");
       // Dat= getArguments().getString("des_id");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +Dat);
       /* GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);


        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });*/
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Butcher_Wholseller_Fragment.this.goBack();
            }
        });

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);
        /*recyclerview.setHasFixedSize(true);
        Context context=getActivity();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 3));*/

        //  GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        //recyclerview.setLayoutManager(layoutManager);
        //recyclerview.setOrientation(GridLayoutManager.VERTICAL);
        //recyclerview.setLayoutManager(layoutManager);
        // recyclerview.setAdapter( adapter );
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Butcher_Wholseller_Fragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Butcher_Wholseller_Fragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(Butcher_Wholseller_Fragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/


        }

    }

    private void goBack() {

      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();

    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);

//            pd = new ProgressDialog(Login_Activity.this);
//            pd.setTitle("Submitting...");
//            pd.setMessage("Please wait...");
//            pd.setCancelable(false);
//            pd.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {
                SharedPreferences myPrefs = Butcher_Wholseller_Fragment.this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                //SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

                String citys = myPrefs.getString("cvc", null);

                //String gh=us_typ.replaceAll(" ","%20");
//                System.out.println(">>>>>>sss>>>>>>>>>" + s);
//                System.out.println(">>>>>>>ttt>>>>>>>>" + t);
//                //System.out.println(">>>>>>>vvv>>>>>>>>" + v);
//                System.out.println(">>>>>>>gh>>>>>>>>" +gh);
                HttpClient httpclient = new DefaultHttpClient();
                String l=getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>"+SERVER+"json/butcher/wholeseller.php?country="+citys);
                System.out.println("<<<<<<lang>>>>>"+l);
                if (l.equalsIgnoreCase("English")) {
                    Wurl=SERVER+"json/butcher/wholeseller.php?country="+citys;
                }
                else{
                    Wurl=SERVER+"json/butcher/ar/wholeseller.php?country="+citys;
                }

                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            progress_loader.setVisibility(ProgressBar.GONE);
          //  String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
         //   System.out.println(">>>>>>>>>>>>>>>" + sam);
           // String value = result;
     /*       if
                    (result.equalsIgnoreCase("")) {
                new SweetAlertDialog(Butcher_Wholseller_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                sDialog.dismiss();
                            }
                        })
                        .show();
           *//*     com.nispok.snackbar.Snackbar.with(Butcher_Wholseller_Fragment.this) // context
                        .text("No Products!") // text to display
                        .show(Butcher_Wholseller_Fragment.this);*//*
               *//* com.chootdev.csnackbar.Snackbar.with(Butcher_Wholseller_Fragment.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("No Products!")
                        .duration(Duration.SHORT)
                        .show();*//*
               // Toast.makeText(getApplicationContext(), "No Events", Toast.LENGTH_SHORT).show();
            } else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                zwm = new ArrayList<Butcher_Wholseller_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        wm = new Butcher_Wholseller_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        id = mJsonObject.getString("id");
                        shop_name = mJsonObject.getString("shop_name");
                        name = mJsonObject.getString("name");
                        address = mJsonObject.getString("address");
                        address2 = mJsonObject.getString("address2");
                        location = mJsonObject.getString("location");
                        wholeseller_logo = mJsonObject.getString("wholeseller_logo");


                        //  event_id = mJsonObject.getString("event_id");

                        //  String o = mJsonObject.getString("specialty_other");
                        wm.setId(id);
                        wm.setShop_name(shop_name);
                        wm.setName(name);
                        wm.setAddress(address);
                        wm.setAddress2(address2);
                        wm.setLocation(location);
                        wm.setWholeseller_logo(wholeseller_logo);
                        //  wm.s

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        //   System.out.println("<<<oo>>>>" + o);
                        //   onComplete();

                        zwm.add(wm);
                        System.out.println("<<<oo>>>>" + wm);
/*
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerview.setLayoutManager(layoutManager);
                        recyclerview.setAdapter(adapter);*/
                        adapter1 = new Butcher_Wholseller_adapter(zwm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                        //adapter = new Event_adapter(upm, getActivity());
                        //  recyclerview.setAdapter(adapter);
                        //  Toast.makeText(getApplicationContext(), "Login Sucess", Toast.LENGTH_SHORT).show();
                        //status=5;
                        //   Intent i1 = new Intent(Participated_Events_Activity.this, HomeActivity.class);
                        //  i1.putExtra("fulname", k);
                        //   i1.putExtra("spclty", o);
                        // startActivity(i1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                new SweetAlertDialog(Butcher_Wholseller_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })
                        .show();
               /* com.nispok.snackbar.Snackbar.with(Butcher_Wholseller_Fragment.this) // context
                        .text("No Products!") // text to display
                        .show(Butcher_Wholseller_Fragment.this);*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
               /* ArrayAdapter<PastModel> adapter1 = new MyList();
                ListV.setAdapter(adapter1);
*/
                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                                Intent i = new Intent(getApplicationContext(),Wholeseller_detail_Fragment.class);
                                i.putExtra("id",zwm.get(position).getId());
                                i.putExtra("shopname",zwm.get(position).getShop_name());
                                startActivity(i);
                                finish();
                            }
                        })
                );
            }
           /* ListV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Lm=pns.get(i).getEvent_year();
                    System.out.println("<<<oo>lllllllllllllllllllll>>>"+ Lm);
                    new DownloadData2().execute();
                }
            });*/

        }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    }
