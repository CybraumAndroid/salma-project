package com.salmabutchershop.salma.meridian.app.acquirelocation;

/**
 * Created by libin on 1/30/2017.
 */
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.GPSTracker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.address.DashboardActivity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AndroidGPSTrackingActivity extends Activity {

    Button btnShowLocation;

    // GPSTracker class

    int locationMode;
    Context mContext;
    GPSTracker gps;
    double latitude;
    double longitude;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AndroidGPSTrackingActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


        } else {

            Toast.makeText(mContext, "You already granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(mContext, AndroidGPSTrackingActivity.this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                // \n is for new line
                System.out.println("----------------------------------------");
                System.out.println("inside cangetlocation");
                System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                System.out.println("----------------------------------------");


            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                int a = gps.showSettingsAlert(AndroidGPSTrackingActivity.this);

            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(mContext, AndroidGPSTrackingActivity.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();

                        // \n is for new line
                        System.out.println("----------------------------------------");
                        System.out.println("inside cangetlocation");
                        System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                        System.out.println("----------------------------------------");



                    } else {

                        int a = gps.showSettingsAlert(AndroidGPSTrackingActivity.this);


                    }

                } else {


                    Toast.makeText(mContext, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }


                return;
            }
        }

    }
}