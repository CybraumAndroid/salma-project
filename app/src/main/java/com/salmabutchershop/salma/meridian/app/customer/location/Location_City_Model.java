package com.salmabutchershop.salma.meridian.app.customer.location;

/**
 * Created by libin on 11/11/2016.
 */
public class Location_City_Model {
  String id,city,loc_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public String getLoc_name() {
        return loc_name;
    }

    public void setLoc_name(String loc_name) {
        this.loc_name = loc_name;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
