package com.salmabutchershop.salma.meridian.app.butcher.extras;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;

import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Model;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_adapter;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/20/2016.
 */

public class NotificationFragment extends Activity  {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

    Notfy_adapter adapter;
    ProgressBar progress;
    String result, galry_image,Dat;
    static ArrayList<Notfy_Model> nfm;
    Notfy_Model nm;
    String not_id,not_title,not_desc;
    String k,l;
    Button Beef,Mutton,Camel,Other;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        Intent intent = getIntent();
        k = intent.getStringExtra("add_id");
        l= intent.getStringExtra("cat_id");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +k);
        System.out.println(">>>>>>cattttttttttttttttttttttttttttttttttttttttttt>>>>>>>>>" +l);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
       // recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NotificationFragment.this.goBack();
            }
        });

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(NotificationFragment.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(NotificationFragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(NotificationFragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/

        }
        // new DownloadData1().execute();

    }

    private void goBack() {
        super.onBackPressed();
    }






    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER+"json/customer/notification.php"
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
         //   String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
            if
                    (result.equalsIgnoreCase("")) {
                com.nispok.snackbar.Snackbar.with(NotificationFragment.this) // context
                        .text(R.string.no_more_notification) // text to display
                        .show(NotificationFragment.this);
               /* com.chootdev.csnackbar.Snackbar.with(NotificationFragment.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("No more notifications")
                        .duration(Duration.SHORT)
                        .show();*/
               // Toast.makeText(getApplicationContext(), "No items", Toast.LENGTH_SHORT).show();
            }  else if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                nfm = new ArrayList<Notfy_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        nm = new Notfy_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);


                        not_id = mJsonObject.getString("not_id");
                        not_title = mJsonObject.getString("not_title");
                        not_desc = mJsonObject.getString("not_desc");

                        nm.setNot_id(not_id);
                        nm.setNot_title(not_title);
                        nm.setNot_desc(not_desc);


                        nfm.add(nm);
                        System.out.println("<<<oo>>>>" + nm);

                        adapter = new Notfy_adapter(nfm, getApplicationContext());
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }


                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                            }
                        })
                );
            }


        }
    }
