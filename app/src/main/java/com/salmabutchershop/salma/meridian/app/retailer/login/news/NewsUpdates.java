package com.salmabutchershop.salma.meridian.app.retailer.login.news;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/23/2016.
 */

public class NewsUpdates extends Activity  {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

    News_Updates_adapter adapter;
    ProgressBar progress;
    String result, galry_image,Dat;
    static ArrayList<NewsUpdates_Model> num;
    NewsUpdates_Model nm;
    String not_id,not_title,not_desc;
    String k,l;
    Button Beef,Mutton,Camel,Other;
    String news_id,news_title,news_desc,news_image;
    public static MKLoader progress_loader;
    String Ret_typ,userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newsupdatessidebar_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        Intent intent = getIntent();
        k = intent.getStringExtra("add_id");
        l= intent.getStringExtra("cat_id");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +k);
        System.out.println(">>>>>>cattttttttttttttttttttttttttttttttttttttttttt>>>>>>>>>" +l);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        // recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        ImageView back = (ImageView) findViewById(R.id.back_image);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        Ret_typ=myPrefs.getString("Ret_typ",null);
        userid=myPrefs.getString("user_idL",null);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NewsUpdates.this.goBack();
            }
        });

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(NewsUpdates.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(NewsUpdates.this);
           /* com.chootdev.csnackbar.Snackbar.with(NewsUpdates.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }
        // new DownloadData1().execute();

    }

    private void goBack() {
        super.onBackPressed();
    }






    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();
                System.out.println(">>>>>>>>>>bbbbbbbbbbbbbb>>>>>" +userid);
                System.out.println(">>>>>>>>>>bbbbbbbbbbbbbb>>>>>" +Ret_typ);
                String ss=Ret_typ.replaceAll(" ","");
              /*  retailer_id
                        retail_cat*/
             //   http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/newsupdates.php?retailer_id=307&retail_cat=Retail%20Shop
                HttpPost httppost = new HttpPost(SERVER+"json/retailer/newsupdates.php?retailer_id="+userid+"&retail_cat="+ss
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
//            String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
           // System.out.println(">>>>>>>>>>>>>>>" + sam);
         //   String value = result;
           /* if
                    (result.equalsIgnoreCase("[]")) {
                com.nispok.snackbar.Snackbar.with(NewsUpdates.this) // context
                        .text("No News") // text to display
                        .show(NewsUpdates.this);
                //Toast.makeText(getApplicationContext(), "No Events", Toast.LENGTH_SHORT).show();
            }*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                num = new ArrayList<NewsUpdates_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        nm = new NewsUpdates_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);


                        news_id = mJsonObject.getString("news_id");
                        news_title = mJsonObject.getString("news_title");
                        news_desc = mJsonObject.getString("news_desc");
                        news_image = mJsonObject.getString("news_image");

                        nm.setNews_id(news_id);
                        nm.setNews_title(news_title);
                        nm.setNews_desc(news_desc);
                        nm.setNews_image(news_image);


                        num.add(nm);
                        System.out.println("<<<oo>>>>" + nm);

                        adapter = new News_Updates_adapter(num, getApplicationContext());
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else{
                new SweetAlertDialog(NewsUpdates.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText("No News")
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                Intent i=new Intent(getApplicationContext(),Fresh_Frozen_Retailer.class);

                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .show();

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }


            recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
/*
                                String added_id=esm.get(position).getAdded_by();
                                String sub_id=esm.get(position).getSub_id();
                                String title=esm.get(position).getSubcat_name();
                                Intent i=new Intent(getApplicationContext(),BeefDetail.class);
                                i.putExtra("add_id",added_id);
                                i.putExtra("sub_id",sub_id);
                                i.putExtra("title",title);
                                //i.putExtra("value",status);
                                startActivity(i);
                                finish();*/
                            }
                        })
                );
            }


        }
    }
