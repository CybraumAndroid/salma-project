package com.salmabutchershop.salma.meridian.app.customer.notify;

/**
 * Created by libin on 1/7/2017.
 */
public class PushNotifiction_Model {
    String not_id,not_title,not_desc,order_id,status,not_date,time_diff;

    public String getTime_diff() {
        return time_diff;
    }

    public void setTime_diff(String time_diff) {
        this.time_diff = time_diff;
    }

    public String getNot_id() {
        return not_id;
    }

    public void setNot_id(String not_id) {
        this.not_id = not_id;
    }

    public String getNot_title() {
        return not_title;
    }

    public void setNot_title(String not_title) {
        this.not_title = not_title;
    }

    public String getNot_desc() {
        return not_desc;
    }

    public void setNot_desc(String not_desc) {
        this.not_desc = not_desc;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNot_date() {
        return not_date;
    }

    public void setNot_date(String not_date) {
        this.not_date = not_date;
    }
}
