package com.salmabutchershop.salma.meridian.app.customer.custom_er;

/**
 * Created by libin on 9/29/2016.
 */
public class Customer_Services_Model {
    public String getEnable_popup() {
        return enable_popup;
    }

    public void setEnable_popup(String enable_popup) {
        this.enable_popup = enable_popup;
    }

    String subcat_id,pro_cat_id,subcat_name,sub_image,latest_version,enable_popup;

    public String getSubcat_id() {
        return subcat_id;
    }

    public String getLatest_version() {
        return latest_version;
    }

    public void setLatest_version(String latest_version) {
        this.latest_version = latest_version;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getPro_cat_id() {
        return pro_cat_id;
    }

    public void setPro_cat_id(String pro_cat_id) {
        this.pro_cat_id = pro_cat_id;
    }

    public String getSubcat_name() {
        return subcat_name;
    }

    public void setSubcat_name(String subcat_name) {
        this.subcat_name = subcat_name;
    }

    public String getSub_image() {
        return sub_image;
    }

    public void setSub_image(String sub_image) {
        this.sub_image = sub_image;
    }
}
