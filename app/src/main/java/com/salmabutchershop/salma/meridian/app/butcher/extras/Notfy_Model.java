package com.salmabutchershop.salma.meridian.app.butcher.extras;

/**
 * Created by libin on 12/20/2016.
 */
public class Notfy_Model {
    public String getNot_id() {
        return not_id;
    }

    public void setNot_id(String not_id) {
        this.not_id = not_id;
    }

    public String getNot_title() {
        return not_title;
    }

    public void setNot_title(String not_title) {
        this.not_title = not_title;
    }

    public String getNot_desc() {
        return not_desc;
    }

    public void setNot_desc(String not_desc) {
        this.not_desc = not_desc;
    }

    String not_id,not_title,not_desc;
}
