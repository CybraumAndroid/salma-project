package com.salmabutchershop.salma.meridian.app.butcher.mymenu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/3/2016.
 */

public class Butcher_mymenu_product extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

    Product_mymenu_adapter adapter1;
    ProgressBar progress;
    String result, galry_image,Dat,Dat1;
    static ArrayList<Product_mymenu_Model> pmm;
    Product_mymenu_Model pm;
    String event_id,winner_name,winner_image,award_name;
    String k,l,m;
    String pro_id,type_id,pro_nam,type_name,type_image,category ;
    TextView title,qty,plus,minus,cart_txt;
    private int uprange = 20;
    private int downrange = 0;
    private int values = 0;
    TextView tv,tv1,tv2,tv3;
    ImageView v;
    Button button2;
    LinearLayout llm;
    static TextView cnt;
    String Reg_id,names,typ;
TextView titles;
    String Wurl;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mymenu_product_layout);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        Intent intent = getIntent();
        l = intent.getStringExtra("cat_id");
        m=intent.getStringExtra("pro_name");
        k=intent.getStringExtra("pro_id");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + l);
     titles= (TextView) findViewById(R.id.header_text);
        titles.setText(m);

        progress = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Butcher_mymenu_product.this.goBack();
            }
        });
        cnt= (TextView) findViewById(R.id.imageView2);

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Butcher_mymenu_product.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Butcher_mymenu_product.this);
          /*  com.chootdev.csnackbar.Snackbar.with(Butcher_mymenu_product.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/



        }

    }

    private void goBack() {
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
        /*super.onBackPressed();*/
    }

    public void functionToRun() {
        final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
        int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        cnt.setText(""+s);
    }

    public static void changeindex(int si) {
        cnt.setText(""+si);
    }


    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                String l=getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>"+l);
                if (l.equalsIgnoreCase("English")) {
                    Wurl=SERVER+"json/butcher/product_type.php?sub_id="+l+"&pro_id="+k;
                }
                else{
                    Wurl=SERVER+"json/butcher/ar/product_type.php?sub_id="+l+"&pro_id="+k;
                }
                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
           // String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
         //   System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
       /*     if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No items", Toast.LENGTH_SHORT).show();
            } else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                pmm = new ArrayList<Product_mymenu_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        pm = new Product_mymenu_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        // pro_id = mJsonObject.getString("pro_id");
                        type_id = mJsonObject.getString("type_id");
                        category = mJsonObject.getString("category");
                        ///pro_nam = mJsonObject.getString("pro_name");
                        type_name = mJsonObject.getString("type_name");
                        type_image = mJsonObject.getString("type_image");

                        // pm.setPro_id(pro_id);
                        pm.setType_id(type_id);
                        //   pm.setPro_nam(pro_nam);
                        pm.setCategory(category);
                        pm.setType_name(type_name);
                        pm.setType_image(type_image);

                        System.out.println("<<<galry_img>>>>" + galry_image);


                        pmm.add(pm);
                        System.out.println("<<<oo>>>>" + pm);


                        adapter1 = new Product_mymenu_adapter(pmm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }


            recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                String cat_id=pmm.get(position).getType_id();
                                Intent i=new Intent(getApplicationContext(),Butcher_Product_Submit.class);
                                i.putExtra("cat_id",cat_id);
                                i.putExtra("typ_img",pmm.get(position).getType_image());
                                i.putExtra("typ_nam",pmm.get(position).getType_name());
                                //i.putExtra("value",status);
                                startActivity(i);
                                finish();

                            }
                        })
                );
            }


        }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
       // finish();
        finish();
    }

}
