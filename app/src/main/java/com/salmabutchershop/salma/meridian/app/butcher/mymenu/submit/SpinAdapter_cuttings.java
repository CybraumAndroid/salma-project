package com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Product_mymenu_Model;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct.Product_Services_Model;

import java.util.ArrayList;

public class SpinAdapter_cuttings extends ArrayAdapter<Product_mymenu_Model> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private ArrayList<Product_mymenu_Model> values;

    public SpinAdapter_cuttings(Context context, int textViewResourceId,
                                ArrayList<Product_mymenu_Model> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
       return values.size();
    }

    public Product_mymenu_Model getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        /*final TextView label = new TextView(context);
        label.setTextColor(Color.WHITE);
        label.setBackgroundResource(android.R.color.darker_gray);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(values.get(position).getCategory_name());

        label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                label.setBackgroundResource(android.R.color.holo_red_dark);
            }
        });*/
        TextView item_name;
        ImageView th;
        final LinearLayout background;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.simplespinner1, parent, false);

            item_name=(TextView)v.findViewById(R.id.txt);
        th=(ImageView) v.findViewById(R.id.img);
        item_name.setText(values.get(position).getType_name());
        background=(LinearLayout)v.findViewById(R.id.spinner_item_background1);
        background.setBackgroundResource(R.drawable.spinner_bg);
        String l = context.getResources().getConfiguration().locale.getDisplayLanguage();
        System.out.println("<<<<<<lang>>>>>" + l);
        if (l.equalsIgnoreCase("English")) {
            th.setBackgroundResource(R.drawable.arrow_img);

        }

        else {
            th.setBackgroundResource(R.drawable.arrow_img1);

        }
        /*th.setBackgroundResource(R.drawable.arrow_img);*/


//background.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View view) {
//        background.setBackgroundResource(R.drawable.spinner_bg);
//
//    }
//});

        // And finally return your dynamic (or custom) view for each spinner item
        return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
            ViewGroup parent) {
        TextView item_name;
        ImageView th;
     final    LinearLayout background;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.simplespinner1, parent, false);

        item_name=(TextView)v.findViewById(R.id.txt);
        background=(LinearLayout)v.findViewById(R.id.spinner_item_background1);
        background.setBackgroundResource(R.drawable.spinner_bg);
        th=(ImageView) v.findViewById(R.id.img);

        item_name.setText(values.get(position).getType_name());
        String l = context.getResources().getConfiguration().locale.getDisplayLanguage();
        System.out.println("<<<<<<lang>>>>>" + l);

        if (l.equalsIgnoreCase("English")) {
            th.setBackgroundResource(R.drawable.arrow_img);

        }

        else {
            th.setBackgroundResource(R.drawable.arrow_img1);
           // background.setBackgroundResource(R.drawable.spinner_bg);
        }
//        background.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                background.setBackgroundResource(R.drawable.spinner_bg);
//
//            }
//        });

        // And finally return your dynamic (or custom) view for each spinner item
        return v;
    }
}
