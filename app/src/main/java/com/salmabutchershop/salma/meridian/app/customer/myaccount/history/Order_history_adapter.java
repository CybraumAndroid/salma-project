package com.salmabutchershop.salma.meridian.app.customer.myaccount.history;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.salmabutchershop.salma.meridian.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 10/17/2016.
 */
public class Order_history_adapter extends RecyclerView.Adapter<Order_history_adapter.ViewHolder> {


    List<Order_history_Model> eem;
    Context context;



    public Order_history_adapter(ArrayList<Order_history_Model> eem, Context context) {
        this.eem = eem;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3,tv4;
        // TextView personAge;

        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            tv=   (TextView) itemView.findViewById(R.id.pro_name);
            tv1=   (TextView) itemView.findViewById(R.id.pro_qty);
            tv2=   (TextView) itemView.findViewById(R.id.date);
            tv3=   (TextView) itemView.findViewById(R.id.ads);
            tv4=   (TextView) itemView.findViewById(R.id.status);
         ;
           // v=   (ImageView) itemView.findViewById(R.id.imageView13);
        }
    }


    @Override
    public int getItemCount() {
        return eem.size();
    }


    @Override
    public Order_history_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_history_item_layout, viewGroup, false);
        Order_history_adapter.ViewHolder pvh = new Order_history_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Order_history_adapter.ViewHolder personViewHolder, final int i) {



        Typeface myFont1 = Typeface.createFromAsset(context.getAssets(), "open-sans.bold.ttf");
        personViewHolder.tv.setTypeface(myFont1);
        personViewHolder.tv.setText(context.getResources().getString(R.string.OrderNo)+":"+eem.get(i).getOrder_id());

        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv1.setTypeface(myFont2);
        personViewHolder.tv1.setText(context.getResources().getString(R.string.Amount)+":"+"  AED "+eem.get(i).getSubtotal());

        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont3);
        personViewHolder.tv2.setText(eem.get(i).getOrder_date());
System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<+>>>>>>>>>>>>>>>>>>>>>>>"+eem.get(i).getOrder_time());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv3.setTypeface(myFont4);
        personViewHolder.tv3.setText(eem.get(i).getOrder_time());

        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv4.setTypeface(myFont5);
        personViewHolder.tv4.setText(eem.get(i).getOrder_status());


    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}