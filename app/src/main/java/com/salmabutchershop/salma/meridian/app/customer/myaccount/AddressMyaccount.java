package com.salmabutchershop.salma.meridian.app.customer.myaccount;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.address.AddressFragment;
import com.salmabutchershop.salma.meridian.app.customer.address.DataValues_N;
import com.salmabutchershop.salma.meridian.app.customer.address.DatabaseHandler_N;
import com.salmabutchershop.salma.meridian.app.customer.address.NewDataAdapter_N;
import com.salmabutchershop.salma.meridian.app.customer.address.ViewDatas_N;

import java.util.ArrayList;

/**
 * Created by libin on 12/13/2016.
 */
public class AddressMyaccount extends AppCompatActivity {
    private ProgressDialog prgDlg;
    private DatabaseHandler_N dh;
    ArrayList dataa;
    private RecyclerView rvNew;
    NewDataAdapter_NS adapterNew;
    Button Add_ads;
    String gtot,name,inst;
    Float price;
    DataValues_N dv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_myaccount_layout);
        dh=new DatabaseHandler_N();
        dataa=new ArrayList<>();
        rvNew=(RecyclerView)findViewById(R.id.recylerView2);
        RecyclerView.LayoutManager lm2=new LinearLayoutManager(getApplicationContext());
        rvNew.setLayoutManager(lm2);
        ImageView back= (ImageView) findViewById(R.id.back_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddressMyaccount.this.goBack();
            }
        });
        Add_ads= (Button) findViewById(R.id.button5);
        Add_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), AddressNewFragment.class);
                i.putExtra("id_nam","libin");
                i.putExtra("price", gtot);
                i.putExtra("name",name);
                i.putExtra("inst",inst);
                System.out.println("detailll6"+gtot);
                startActivity(i);
               // finish();
            }
        });
        new FetchDetails().execute();
    }

    private void goBack() {
        super.onBackPressed();
    }

    private class FetchDetails extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prgDlg=new ProgressDialog(AddressMyaccount.this);
            prgDlg.setMessage("Loading page...");
            prgDlg.setCancelable(false);
            prgDlg.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try{
                Cursor c=dh.selectAllDATAS(getApplicationContext());
                int count=c.getCount();
                if(count!=0)
                {
                    while (c.moveToNext()){
                       dv=new DataValues_N();
                        dv.id=c.getInt(0);
                        dv.fname=c.getString(1);
                        dv.lname=c.getString(2);
                        dv.city=c.getString(3);
                        dv.deliveryarea=c.getString(4);
                        dv.addresstype=c.getString(5);
                        dv.street=c.getString(6);
                        dv.building =c.getString(7);
                        dv.floor=c.getString(8);
                        dv.apartno=c.getString(9);
                        dv.mobile=c.getString(10);
                        dv.phone=c.getString(11);
                        dv.addtional_direc=c.getString(12);
                   /* if(inst.isEmpty()){

                    }else {
                        dv.inst=c.getString(13);
                    }*/

                        dataa.add(dv);
                        adapterNew=new NewDataAdapter_NS(getApplicationContext(),dataa,inst);
                    }
                }
                else {
                    if(!((Activity) getApplicationContext()).isFinishing()) {
                        Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_LONG).show();
                    }

                }}catch (Exception e){
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prgDlg.isShowing())
                prgDlg.dismiss();

            rvNew.setAdapter(adapterNew);

        }
    }

}

