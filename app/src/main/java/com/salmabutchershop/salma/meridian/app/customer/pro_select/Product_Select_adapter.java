package com.salmabutchershop.salma.meridian.app.customer.pro_select;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.address.Collect_From_here;
import com.salmabutchershop.salma.meridian.app.customer.address.DatabaseHelperCollect;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.database.OrderNew_Activity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by libin on 9/29/2016.
 */
public class Product_Select_adapter extends RecyclerView.Adapter<Product_Select_adapter.ViewHolder> {
    private float values = 500;
    float newvalues=500;
   /* private float values = 500;
    float newvalues=500;*/

    List<Product_Select_Model> zwm;
    Context context;
    TextView qty;
    private int uprange = 20;
    private int downrange = 1;
    //private int values = 1;
    String bid,prid,shop_ads;
    public Product_Select_adapter(ArrayList<Product_Select_Model> zwm, Context context) {
        this.zwm = zwm;
        this.context = context;

    }

    public Product_Select_adapter(BeefDetail beefDetail) {
    }
    String main_measure;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv, tv1, tv2, tv3;
        // TextView personAge;
        TextView title, qty, plus, minus, cart_txt;

        ImageView personPhoto;
ProgressBar gallery_progressbar;
        LinearLayout ltx;
        public static MKLoader progress_loader;
TextView oldp;
        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            ///  tv=   (TextView) itemView.findViewById(R.id.title);
            tv = (TextView) itemView.findViewById(R.id.beef_name);
            tv1=   (TextView) itemView.findViewById(R.id.beef_amtold);
            tv2 = (TextView) itemView.findViewById(R.id.beef_amt);
            tv3 = (TextView) itemView.findViewById(R.id.dlvry);

            v = (ImageView) itemView.findViewById(R.id.beef_img);
            //
            gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            cart_txt = (TextView) itemView.findViewById(R.id.text_cart);
            ltx = (LinearLayout) itemView.findViewById(R.id.ltx);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
        }
    }


    @Override
    public int getItemCount() {
        return zwm.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_select_item_old_layout, viewGroup, false);
        ViewHolder pvh = new ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder personViewHolder, final int i) {


        String s = zwm.get(i).getPro_image();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont2);
        personViewHolder.tv.setText(zwm.get(i).getPro_name());

         Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
          personViewHolder.tv1.setTypeface(myFont3);
        if(zwm.get(i).getDiscount().equalsIgnoreCase("0")){
            personViewHolder.tv1.setVisibility(View.GONE);
        }
        else {
            personViewHolder.tv1.setVisibility(View.VISIBLE);
            personViewHolder.tv1.setText(zwm.get(i).getCurrency()+" "+zwm.get(i).getType_old_price());
        }

       /* if (zwm.get(i).getType_old_price().isEmpty()){
            personViewHolder.tv1.setVisibility(View.GONE);
        }*/

/*if (zwm.get(i).getOld_price().isEmpty()){

}*/
       /* else {*/
   /* Typeface myFont9 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
    personViewHolder.tv1.setTypeface(myFont9);
    personViewHolder.tv1.setText("AED:"+zwm.get(i).getOld_price());*/
       // }
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont4);
        personViewHolder.tv2.setText(zwm.get(i).getCurrency()+" "+ zwm.get(i).getPrice() + "/"+zwm.get(i).getPrice_quantity());
        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv3.setTypeface(myFont5);
        personViewHolder.tv3.setText("" + zwm.get(i).getDiscount() + zwm.get(i).getDiscount_unit()+" off");

        //  String img=enm.get(i).getNews_img();
        System.out.println("ooo" + s);
      //  Picasso.with(context).load(s).noFade().into(personViewHolder.v);
        try {


        new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                .build()
                .load(s)
                .noFade()
               // .into(personViewHolder.v);
         .into(personViewHolder.v, new Callback() {
            @Override
            public void onSuccess() {

                personViewHolder.progress_loader.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
        }catch (Exception e){
            e.printStackTrace();
        }
        personViewHolder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  String s = zwm.get(i).getPro_image();
                Intent i = new Intent(context, ImagePopupFragment.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("pro_id",s);
                context.startActivity(i);*/
                String s = zwm.get(i).getPro_image();
                final Dialog dialog = new Dialog(view.getRootView().getContext());

                //  dialog.setContentView(R.layout.loc_popup);
                final Window window = dialog.getWindow();
                window.requestFeature(Window.FEATURE_NO_TITLE);
                window.setContentView(R.layout.imagepopup);
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                TouchImageView autoCompView = (TouchImageView) dialog.findViewById(R.id.imageView8);
                ImageButton close=(ImageButton) dialog.findViewById(R.id.close);
                try {


                    new Picasso.Builder(context)
                            .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                            .build()
                            .load(s)
                            .noFade()
                            // .into(personViewHolder.v);
                            .into(autoCompView, new Callback() {
                                @Override
                                public void onSuccess() {

                                   // progress_loader.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {


                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }


close.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        dialog.dismiss();
    }
});
                dialog.show();


            }
        });
        personViewHolder.ltx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {


                values=200;
                newvalues=200;
                bid=zwm.get(i).getButcher_id();
                prid=zwm.get(i).getPro_id();
                // Toast.makeText(context,"hiiii",Toast.LENGTH_SHORT).show();
                View popupView = LayoutInflater.from(view.getRootView().getContext()).inflate(R.layout.cart_poup_layout, null);
                final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
               // personViewHolder.ltx.setGravity(Gravity.BOTTOM);
                //popupWindow.setAnimationStyle(R.style.DialogAnimation);
                popupWindow.setAnimationStyle(R.style.DialogAnimation);
                ImageButton btnDismiss = (ImageButton) popupView.findViewById(R.id.close);
                LinearLayout clse_lt = (LinearLayout) popupView.findViewById(R.id.clse_lt);
                TextView plus = (TextView) popupView.findViewById(R.id.textView5);
                TextView minus = (TextView) popupView.findViewById(R.id.textView6);
                TextView beefname = (TextView) popupView.findViewById(R.id.beef_name);
                TextView beeamt = (TextView) popupView.findViewById(R.id.beef_amt);
                TextView dlv = (TextView) popupView.findViewById(R.id.dlvry_it);
                TextView info = (TextView) popupView.findViewById(R.id.textViewVersion);
                ImageView cart_img= (ImageView) popupView.findViewById(R.id.cart_img);
                final ProgressBar gallery_progressbar=(ProgressBar) popupView.findViewById(R.id.gallery_progressbar);
                String s = zwm.get(i).getPro_image();
                try {


                new Picasso.Builder(context)
                        .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                        .build()
                        .load(s)
                        .noFade()
                        // .into(personViewHolder.v);
                        .into(cart_img, new Callback() {
                            @Override
                            public void onSuccess() {
                                gallery_progressbar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                }catch (Exception e){
                    e.printStackTrace();
                }
                info.setText(zwm.get(i).getDescription());
                dlv.setText(zwm.get(i).getDelivery_info());
                beefname.setText(zwm.get(i).getPro_name());
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<CURRENCY@@@@@@@@>>>>>>>>>>>>>>>>>>>>>"+zwm.get(i).getCurrency());
                beeamt.setText(zwm.get(i).getCurrency()+" "+ zwm.get(i).getPrice() + "/"+zwm.get(i).getPrice_quantity());
                    main_measure=zwm.get(i).getPrice_quantity().substring(Math.max(zwm.get(i).getPrice_quantity().length() - 2, 0)).trim();
                qty = (TextView) popupView.findViewById(R.id.textView4);
                    values=Float.parseFloat(zwm.get(i).getPrice_quantity().substring(0,zwm.get(i).getPrice_quantity().length() - 2).trim());
                    newvalues=values;

                    if(main_measure.equalsIgnoreCase("kg")) {
                        qty.setText(values + " kg");
                    }else {
                        qty.setText(values + " g");
                    }
                Button submit = (Button) popupView.findViewById(R.id.button2);
                btnDismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            popupWindow.dismiss();


                    }
                });
                clse_lt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                            popupWindow.dismiss();

                    }
                });


                    if(main_measure.equalsIgnoreCase("kg")){
                        values = newvalues;
                    }else {
                        values = newvalues / 1000;
                    }


                    plus.setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {
                            String measure="";
                            if(main_measure.equalsIgnoreCase("kg")){
                                measure=" kg";
////////////////////////////////////////////////////////////////////////////////////////////////////////
                                qty.setText(values + " kg");
                                if(newvalues==20000.0)
                                {

                                }

                                else{

                                    measure=" kg";
                                    newvalues=newvalues+Float.parseFloat(".5");
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    values=newvalues;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+values+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    qty.setText("" + values + measure);
                                }
////////////////////////////////////////////////////////////////////////////////////////////////////////

                            }else{

                                if(newvalues==20000.0)
                                {

                                }
                                else if(newvalues<950){
                                    measure=" g";
                                    newvalues=newvalues+50;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    values=newvalues/1000;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+values+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    qty.setText("" + newvalues + measure);
                                }

                                else{

                                    measure=" kg";
                                    newvalues=newvalues+50;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    values=newvalues/1000;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+values+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    qty.setText("" + values + measure);
                                }
                            }


                        }
                    });
                    minus.setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {

                            String measure="";

                            if(main_measure.equalsIgnoreCase("kg")) {
                                measure = " kg";
                                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                                qty.setText(values + " kg");
                                if(newvalues==200.0 )
                                {
           /*qty.setText(newvalues + " g");*/
                                }
                                else if(newvalues==0.5){

                                }

                                else{

                                    measure=" kg";
                                    newvalues=newvalues-Float.parseFloat(".5");
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    values=newvalues;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+values+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");



                                    qty.setText(values + " kg");
                                }

                                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }else{
                                if(newvalues==200.0 )
                                {
           /*qty.setText(newvalues + " g");*/
                                }
                                else if(newvalues<=1000){
                                    newvalues=newvalues-50;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    values=newvalues/1000;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+values+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    qty.setText(newvalues + " g");
                                }

                                else{

                                    measure=" kg";
                                    newvalues=newvalues-50;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ newvalues : "+newvalues+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                    values=newvalues/1000;
                                    System.out.println("\\\\\\\\\\\\\\\\\\\\ converted values : "+values+" \\\\\\\\\\\\\\\\\\\\\\\\\\\\");

                                    if(values==0.2){
                                        qty.setText(newvalues + " g");
                                    }
                                    else
                                        qty.setText(values + " kg");
                                }
                            }



                        }
                    });

                submit.setOnClickListener(new View.OnClickListener() {
                    String type_id=zwm.get(i).getTyp_id();
                    int x = 30;
                  String j=zwm.get(i).getPrice();
String shop_ads=zwm.get(i).getShop_addrs();
                    String Shop_name=zwm.get(i).getShop_name();
                    String Shop_email=zwm.get(i).getEmail();
                    String Shop_Phone=zwm.get(i).getPhone();

                  Float foo = Float.parseFloat(zwm.get(i).getPrice());
                   int pro_id = Integer.parseInt(zwm.get(i).getPro_id());
                    String lat=zwm.get(i).getLatitude();
                    String lng=zwm.get(i).getLongitude();


                Float  fds=Float.parseFloat("0");
                     //System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+zwm.get(i).getButcher_id());
                    @Override
                    public void onClick(View view) {
                        if (zwm.get(i).getDelivery_info().equalsIgnoreCase("no")) {

                                popupWindow.dismiss();

                            new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(context.getResources().getString(R.string.SorryWillnotbeabletodeliverthisItem))
                                    //.setContentText("Won't be able to deliver this Item!")
                                    .setContentText(context.getResources().getString(R.string.PleaseCollectitfromourshop))
                                    .setCustomImage(R.drawable.logo)
                                    .setCancelText(context.getResources().getString(R.string.No))
                                    .setConfirmText(context.getResources().getString(R.string.Yes))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            if (zwm.get(i).getPrice().equalsIgnoreCase("null")){

                                            }else {
                                                if (values > 0) {


                                              /*      //checking if butcher added quantity is kg or g

                                                    String kg_or_g = (zwm.get(i).getPrice_quantity().substring(Math.max(zwm.get(i).getPrice_quantity().length() - 2, 0))).trim();//getting 'g' or 'kg'
                                                    String value_befr_kg_or_g=(zwm.get(i).getPrice_quantity().substring(0,zwm.get(i).getPrice_quantity().length() - 2)).trim(); // getting the amount value before charector'g' or 'kg'
                                                    System.out.println("kg_or_g : "+kg_or_g);
                                                    System.out.println("value_befr_kg_or_g : "+value_befr_kg_or_g);
                                                    float price_for_1kg=0f,price_for_1_gram=0f,total=0f;

                                                    float price_to_multiply,quantity_to_multiply;
                                                    float inc_or_dec_quantity=values;

                                                    if(kg_or_g.equalsIgnoreCase("kg")){
                                                        //if kg_or_g=kg

                                                        if(value_befr_kg_or_g.contentEquals("1")){//if 1 there is no need to calculate price for 1 kg
                                                            if(values==200.0){
                                                                values=values/1000;
                                                                quantity_to_multiply=values;
                                                                price_to_multiply=price_for_1_gram;
                                                                total=quantity_to_multiply * price_to_multiply;
                                                            }
                                                           else if (values <=1000.0) {
                                                                price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));

                                                                price_to_multiply = price_for_1kg/2;
                                                                quantity_to_multiply = Float.parseFloat(".5");
                                                                total=price_to_multiply;//half price
                                                            }else {
                                                                price_for_1kg = Float.parseFloat(zwm.get(i).getPrice());
                                                                System.out.println(" inside value_befr_kg_or_g.contentEquals(\"1\") ");
                                                                System.out.println("price_for_1kg : " + price_for_1kg);
                                                                price_to_multiply = price_for_1kg;
                                                                quantity_to_multiply = inc_or_dec_quantity;
                                                                total = quantity_to_multiply * price_to_multiply;
                                                            }
                                                        }else {
                                                            //finding price for 1kg
                                                            if(values==200.0){
                                                                values=values/1000;
                                                                quantity_to_multiply=values;
                                                                price_to_multiply=price_for_1_gram;
                                                                total=quantity_to_multiply * price_to_multiply;
                                                            }
                                                            else if (values <=1000.0) {
                                                                price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));

                                                                price_to_multiply = price_for_1kg/2;
                                                                quantity_to_multiply = Float.parseFloat(".5");
                                                                total=price_to_multiply;//half price
                                                            }else {
                                                                price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));
                                                                System.out.println(" inside 'finding price for 1kg' ");
                                                                System.out.println("price_for_1kg : " + price_for_1kg);
                                                                price_to_multiply = price_for_1kg;
                                                                quantity_to_multiply = inc_or_dec_quantity;
                                                                total=quantity_to_multiply * price_to_multiply;
                                                            }
                                                        }

                                                    }
                                                    else{
                                                        //if kg_or_g=g

                                                        System.out.println(" inside 'if kg_or_g=g' ");

                                                        price_for_1_gram=(Float.parseFloat(zwm.get(i).getPrice()))/(Float.parseFloat(value_befr_kg_or_g));
                                                        System.out.println("price_for_1_gram : "+price_for_1_gram);
                                                        if(values==200.0){
                                                            values=values/1000;
                                                            quantity_to_multiply=values;
                                                            price_to_multiply=price_for_1_gram;
                                                            total=quantity_to_multiply * price_to_multiply;
                                                        }
                                                       else if (values <=1000.0) {
                                                            price_to_multiply=price_for_1_gram;
                                                            quantity_to_multiply=inc_or_dec_quantity;
                                                            System.out.println("inside 'values == 500.0'");
                                                            System.out.println("price_to_multiply : "+price_to_multiply);
                                                            System.out.println("quantity_to_multiply : "+quantity_to_multiply);
                                                            total=quantity_to_multiply * price_to_multiply;
                                                        }
                                                        else{
                                                            System.out.println("inside else loop of 'if(values == 500.0)'");
                                                            //converting kg to kg
                                                            quantity_to_multiply=inc_or_dec_quantity*1000;
                                                            price_to_multiply=price_for_1_gram;

                                                            System.out.println("price_to_multiply : "+price_to_multiply);
                                                            System.out.println("quantity_to_multiply : "+quantity_to_multiply);
                                                            total=quantity_to_multiply * price_to_multiply;
                                                        }

                                                    }


*/



                                                    String kg_or_g = (zwm.get(i).getPrice_quantity().substring(Math.max(zwm.get(i).getPrice_quantity().length() - 2, 0))).trim();//getting 'g' or 'kg'
                                                    String value_befr_kg_or_g=(zwm.get(i).getPrice_quantity().substring(0,zwm.get(i).getPrice_quantity().length() - 2)).trim(); // getting the amount value before charector'g' or 'kg'
                                                    System.out.println("kg_or_g : "+kg_or_g);
                                                    System.out.println("value_befr_kg_or_g : "+value_befr_kg_or_g);
                                                    float price_for_1kg=0f,price_for_1_gram=0f,total=0f;

                                                    float price_to_multiply,quantity_to_multiply;
                                                    float inc_or_dec_quantity=values;

                                                    if(kg_or_g.equalsIgnoreCase("kg")){
                                                        //if kg_or_g=kg
                                                        if(value_befr_kg_or_g.contentEquals("1")){//if 1 there is no need to calculate price for 1 kg
                                                            if (values == 500.0) {
                                                                price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));

                                                                price_to_multiply = price_for_1kg/2;
                                                                quantity_to_multiply = Float.parseFloat(".5");
                                                                total=price_to_multiply;//half price
                                                            }
                                                            else {
                                                                price_for_1kg = Float.parseFloat(zwm.get(i).getPrice());
                                                                System.out.println(" inside value_befr_kg_or_g.contentEquals(\"1\") ");
                                                                System.out.println("price_for_1kg : " + price_for_1kg);
                                                                price_to_multiply = price_for_1kg;
                                                                quantity_to_multiply = inc_or_dec_quantity;
                                                                total = quantity_to_multiply * price_to_multiply;
                                                            }
                                                        }else {
                                                            //finding price for 1kg
                                                            if (values == 500.0) {
                                                                price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));

                                                                price_to_multiply = price_for_1kg/2;
                                                                quantity_to_multiply = Float.parseFloat(".5");
                                                                total=price_to_multiply;//half price
                                                            }else {
                                                                price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));
                                                                System.out.println(" inside 'finding price for 1kg' ");
                                                                System.out.println("price_for_1kg : " + price_for_1kg);
                                                                price_to_multiply = price_for_1kg;
                                                                quantity_to_multiply = inc_or_dec_quantity;
                                                                total=quantity_to_multiply * price_to_multiply;
                                                            }
                                                        }

                                                    }
                                                    else{
                                                        //if kg_or_g=g

                                                        System.out.println(" inside 'if kg_or_g=g' ");

                                                        price_for_1_gram=(Float.parseFloat(zwm.get(i).getPrice()))/(Float.parseFloat(value_befr_kg_or_g));
                                                        System.out.println("price_for_1_gram : "+price_for_1_gram);

                                                        if (values == 500.0) {
                                                            price_to_multiply=price_for_1_gram;
                                                            quantity_to_multiply=inc_or_dec_quantity;
                                                            System.out.println("inside 'values == 500.0'");
                                                            System.out.println("price_to_multiply : "+price_to_multiply);
                                                            System.out.println("quantity_to_multiply : "+quantity_to_multiply);
                                                            total=quantity_to_multiply * price_to_multiply;
                                                        }
                                                        else{
                                                            System.out.println("inside else loop of 'if(values == 500.0)'");
                                                            //converting kg to kg
                                                            quantity_to_multiply=inc_or_dec_quantity*1000;
                                                            price_to_multiply=price_for_1_gram;

                                                            System.out.println("price_to_multiply : "+price_to_multiply);
                                                            System.out.println("quantity_to_multiply : "+quantity_to_multiply);
                                                            total=quantity_to_multiply * price_to_multiply;
                                                        }

                                                    }




                                                        popupWindow.dismiss();

                                                    float tot = values * foo;
                                                    float gtot = tot + 2;
                                                    final DatabaseHelperCollect dataHelper = new DatabaseHelperCollect(context);
                                                  /*  if (values<=1000.0) {
                                                        values = values / 1000;//gram to kg
                                                    }*/

                                                    if (values == 500.0) {
                                                        values = values / 1000;//gram to kg
                                                    }
                                                    String email_id = zwm.get(i).getButcher_id();
                                                   /* if(dataHelper.isExist(email_id)){
                                                        /// Toast.makeText(context,"Already in", Toast.LENGTH_SHORT).show();
                                                        // fds=0f;
                                                        fds=Float.parseFloat("0");
                                                    }else {
                                                        //  Toast.makeText(context,"Already out", Toast.LENGTH_SHORT).show();
                                                        // Float foo = Float.parseFloat(zwm.get(i).getPrice());
                                                        fds=Float.parseFloat("0");
                                                    }*/
                                                    dataHelper.insertData(personViewHolder.tv.getText().toString(), values, foo, total,total /*gtot*/, zwm.get(i).getPro_id(),zwm.get(i).getButcher_id(),fds,zwm.get(i).getShop_name(),zwm.get(i).getPrice_quantity(),kg_or_g,value_befr_kg_or_g,price_for_1kg,price_for_1_gram,type_id);
                                              //      dataHelper.insertData(personViewHolder.tv.getText().toString(), values, foo,total,total /*gtot*2*/,zwm.get(i).getPro_id(),zwm.get(i).getButcher_id(),fds,zwm.get(i).getShop_name(),zwm.get(i).getPrice_quantity(),kg_or_g,value_befr_kg_or_g,price_for_1kg,price_for_1_gram);
                                                    //  final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                                                    //  Toast.makeText(context,zwm.get(i).getSub_id(),Toast.LENGTH_SHORT).show();
                                                    int si = (int) dataHelper.getProfilesCount();
                                                    System.out.println("counttttttttttttttttttttttttt" + si);
                                                    // BeefDetail.changeindex(si);
                                                    Intent i = new Intent(context, Collect_From_here.class);
                                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    i.putExtra("but_id",bid);
                                                    i.putExtra("pro_id",prid);
                                                    i.putExtra("shop_ads",shop_ads);
                                                    i.putExtra("Shop_name",Shop_name);
                                                    i.putExtra("Shop_email",Shop_email);
                                                    i.putExtra("Shop_phone",Shop_Phone);
                                                    i.putExtra("lat",lat);
                                                    i.putExtra("lng",lng);
                                                    context.startActivity(i);




                                                } else {
                                                    Toast.makeText(context, context.getResources().getString(R.string.SelectQuantity), Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                            sDialog.dismiss();
                                                   /* .setTitleText("Deleted!")
                                                    .setContentText("Your imaginary file has been deleted!")
                                                    .setConfirmText("OK")
                                                    .setConfirmClickListener(null)
                                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*/
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();



                        } else {
                            if (values > 0) {

                                //checking if butcher added quantity is kg or g

                                String kg_or_g = (zwm.get(i).getPrice_quantity().substring(Math.max(zwm.get(i).getPrice_quantity().length() - 2, 0))).trim();//getting 'g' or 'kg'
                                String value_befr_kg_or_g=(zwm.get(i).getPrice_quantity().substring(0,zwm.get(i).getPrice_quantity().length() - 2)).trim(); // getting the amount value before charector'g' or 'kg'
                                System.out.println("kg_or_g : "+kg_or_g);
                                System.out.println("value_befr_kg_or_g : "+value_befr_kg_or_g);
                                float price_for_1kg=0f,price_for_1_gram=0f,total=0f;

                                float price_to_multiply,quantity_to_multiply;
                                float inc_or_dec_quantity=values;

                                if(kg_or_g.equalsIgnoreCase("kg")){
                                    //if kg_or_g=kg
                                    if(value_befr_kg_or_g.contentEquals("1")){//if 1 there is no need to calculate price for 1 kg
                                        if (values == 500.0) {
                                            price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));

                                            price_to_multiply = price_for_1kg/2;
                                            quantity_to_multiply = Float.parseFloat(".5");
                                            total=price_to_multiply;//half price
                                        }
                                        else {
                                            price_for_1kg = Float.parseFloat(zwm.get(i).getPrice());
                                            System.out.println(" inside value_befr_kg_or_g.contentEquals(\"1\") ");
                                            System.out.println("price_for_1kg : " + price_for_1kg);
                                            price_to_multiply = price_for_1kg;
                                            quantity_to_multiply = inc_or_dec_quantity;
                                            total = quantity_to_multiply * price_to_multiply;
                                        }
                                    }else {
                                        //finding price for 1kg
                                        if (values == 500.0) {
                                            price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));

                                            price_to_multiply = price_for_1kg/2;
                                            quantity_to_multiply = Float.parseFloat(".5");
                                            total=price_to_multiply;//half price
                                        }else {
                                            price_for_1kg = (Float.parseFloat(zwm.get(i).getPrice())) / (Float.parseFloat(value_befr_kg_or_g));
                                            System.out.println(" inside 'finding price for 1kg' ");
                                            System.out.println("price_for_1kg : " + price_for_1kg);
                                            price_to_multiply = price_for_1kg;
                                            quantity_to_multiply = inc_or_dec_quantity;
                                            total=quantity_to_multiply * price_to_multiply;
                                        }
                                    }

                                }
                                else{
                                    //if kg_or_g=g

                                    System.out.println(" inside 'if kg_or_g=g' ");

                                    price_for_1_gram=(Float.parseFloat(zwm.get(i).getPrice()))/(Float.parseFloat(value_befr_kg_or_g));
                                    System.out.println("price_for_1_gram : "+price_for_1_gram);

                                    if (values == 500.0) {
                                        price_to_multiply=price_for_1_gram;
                                        quantity_to_multiply=inc_or_dec_quantity;
                                        System.out.println("inside 'values == 500.0'");
                                        System.out.println("price_to_multiply : "+price_to_multiply);
                                        System.out.println("quantity_to_multiply : "+quantity_to_multiply);
                                        total=quantity_to_multiply * price_to_multiply;
                                    }
                                    else{
                                        System.out.println("inside else loop of 'if(values == 500.0)'");
                                        //converting kg to kg
                                        quantity_to_multiply=inc_or_dec_quantity*1000;
                                        price_to_multiply=price_for_1_gram;

                                        System.out.println("price_to_multiply : "+price_to_multiply);
                                        System.out.println("quantity_to_multiply : "+quantity_to_multiply);
                                        total=quantity_to_multiply * price_to_multiply;
                                    }

                                }





                                    popupWindow.dismiss();

                                float tot = values * foo;
                                float gtot = tot + 2;
                                final DatabaseHelper dataHelper = new DatabaseHelper(context);
                                if (values == 500.0) {
                                    values = values / 1000;//gram to kg
                                }
                                String email_id = zwm.get(i).getButcher_id();
                                if(dataHelper.isExist(email_id)){
                                   /// Toast.makeText(context,"Already in", Toast.LENGTH_SHORT).show();
                                    // fds=0f;
                                  fds=Float.parseFloat("0");
                                }else {
                                  //  Toast.makeText(context,"Already out", Toast.LENGTH_SHORT).show();
                                   // Float foo = Float.parseFloat(zwm.get(i).getPrice());
                                    fds=Float.parseFloat("0");
                                }
                              /* if (boolean dataHelper.isExist(email_id))*/
                            // dataHelper.getdet(butcherid).toString().contentEquals(zwm.get(i).getButcher_id()))
                               // final DatabaseHelper dataHelper = new DatabaseHelper(context);
                              /*  db = dataHelper.getReadableDatabase();
                                db.beginTransaction();
                                try {
                                    String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                    System.out.println("" + selectQuery);
                                    Cursor cursor = db.rawQuery(selectQuery, null);

                                    if (cursor.getCount() > 0) {
                                        while (cursor.moveToNext()) {

                                            butcher_id=cursor.getString(cursor.getColumnIndex("butcher_id"));

                                        }

                                    }
                                    db.setTransactionSuccessful();

                                } catch (SQLiteException e) {
                                    e.printStackTrace();

                                } finally {
                                    db.endTransaction();
                                    // End the transaction.
                                    db.close();
                                    // Close database
                                }
                            }*/
                                System.out.println("quantity_to_multiply * price_to_multiply : = "+total);


                                dataHelper.insertData(personViewHolder.tv.getText().toString(), values, foo, total,total /*gtot*/, zwm.get(i).getPro_id(),zwm.get(i).getButcher_id(),fds,zwm.get(i).getShop_name(),zwm.get(i).getPrice_quantity(),kg_or_g,value_befr_kg_or_g,price_for_1kg,price_for_1_gram,type_id);
                                //  final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                                //  Toast.makeText(context,zwm.get(i).getSub_id(),Toast.LENGTH_SHORT).show();
                                int si = (int) dataHelper.getProfilesCount();
                                System.out.println("counttttttttttttttttttttttttt" + si);
                                BeefDetail.changeindex(si);



                                new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(context.getResources().getString(R.string.SuccessfullyaddedtoCart))
                                        .setContentText(context.getResources().getString(R.string.DoyouwanttocontinueShopping))
                                        .setCancelText(context.getResources().getString(R.string.No))
                                        .setConfirmText(context.getResources().getString(R.string.Yes))
                                        .setCustomImage(R.drawable.cart_img)

                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {

                                                Intent i1 = new Intent(context, OrderNew_Activity.class);
                                                i1.putExtra("but_id",zwm.get(i).getButcher_id());
                                                i1.putExtra("del_charge",zwm.get(i).getDelivery_charge());
                                                i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                context.startActivity(i1);
                                                //dialog.dismiss();
                                                sDialog.dismiss();

                                            }
                                        })
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                              Intent i1 = new Intent(context, Customer_Butcher_List_Fragment.class);
                                                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+zwm.get(i).getButcher_id());
                                                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+zwm.get(i).getPro_id());
                                               i1.putExtra("value","1");
                                                i1.putExtra("cat_id",zwm.get(i).getCat_id());
                                                i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                context.startActivity(i1);
                                                sweetAlertDialog.dismiss();
                                            }
                                        })

                                     .show();







                            } else {
                                com.nispok.snackbar.Snackbar.with(context) // context
                                        .text(context.getResources().getString(R.string.SelectQuantity)) // text to display
                                        .show((Activity) context);
                               // Toast.makeText(context, "Select Quantity", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
                // popupWindow.showAsDropDown(popupView, 0, 0);
                popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
                }catch (NullPointerException e){
                    e.printStackTrace();
                }


            }
        });



    }



    private void showpopup(View view) {

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public long getCount() {
        final DatabaseHelper dataHelper = new DatabaseHelper(context);
        int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        return  s;
    }
}