package com.salmabutchershop.salma.meridian.app.customer.address;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.icu.text.DateFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.CartModel;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.database.JsonModel;
import com.salmabutchershop.salma.meridian.app.customer.database.OrderNew_Activity;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 12/13/2016.
 */

public class Collect_From_here extends AppCompatActivity {
    Button Cfm;
    //private Context context;
    String name;
    int qty;
    // int tot,cart_tot=0;
    int price;
    int container_charge=0;
    private Context context;
    SQLiteDatabase db;
    TextView tv_tot,grand,tt_grand;
    String cart;
    //int gtot=0,cart_tot2=0;
    Button cnte;
    int cnt;
    int l=2;
    TextView ADD;

    String sub_tt;
    ArrayList<CartModel> arrayListcart=new ArrayList<>();
    ///ArrayList<JsonModel>   array_lstcart=new ArrayList<>();
    String dlvry_shd;
    JsonModel s;

    String inst="lib";
    float gtot=0,cart_tot2=0;
    RadioGroup radio_group,radio_group_patient;
    RadioButton radioMale,radioFemale,radioMales,radioFemales;
    String rads,radm;
    LinearLayout L1;
    float tot,cart_tot=0;
    Float m;
    ProgressBar progress;
    ArrayList<JsonModel>   array_lstcart;
    ArrayList<String> array_item=new ArrayList<>();
    ArrayList<String> array_qty=new ArrayList<>();
    ArrayList<String> array_product=new ArrayList<>();
    ArrayList<String> array_price=new ArrayList<>();
    ArrayList<String> But_id=new ArrayList<>();
    ArrayList<String> array_tot=new ArrayList<>();
    String csv,csd,csf,csg,cbf,cty,ctt,cpt;
    String sme_jsn;
    EditText Name,Email,Phone;
    boolean edittexterror = false;
    String names,phno,email;
    String j,k,lk;
    String Reg_id;
    String refreshedToken;
    String tt;
    CartModel cartmodl;
    Gson gson;
    TextView shop_ads;
    String Shop_ads,Shop_name,Shop_email,Shop_phone;
    TextView shop_name,shop_eml,shop_phone;
    String lat,lng;
    String c_names,c_email,c_phone;
    Cursor cursor;
    public static MKLoader progress_loader;
    String Cur_cy;
    float subtotal=0f;
    String currentDateTimeString;
    ArrayList<String> array_type=new ArrayList<>();
    String type_id,instS;
    ArrayList<String> array_price_qty=new ArrayList<>();
    EditText instE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collectdromherenew);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        Cfm= (Button) findViewById(R.id.cfm);
        grand=(TextView)findViewById(R.id.textView20);
        tt_grand=(TextView)findViewById(R.id.textView2);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        shop_ads= (TextView) findViewById(R.id.city);
        shop_eml= (TextView) findViewById(R.id.eml);
        shop_phone= (TextView) findViewById(R.id.mobile);
        shop_name= (TextView) findViewById(R.id.textView42);
        instE= (EditText) findViewById(R.id.inst);
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        // final String token = SharedPrefManager.getInstance(getApplicationContext()).getDeviceToken();
        //  token = SharedPrefManager.getInstance(this).getDeviceToken();
        System.out.println("<<<<<<<<<<<<<<<ref>>>>>>>>"+refreshedToken);
        Intent intent = getIntent();
        k = intent.getStringExtra("but_id");
        j= intent.getStringExtra("pro_id");
       Shop_ads=intent.getStringExtra("shop_ads");
        Shop_name=intent.getStringExtra("Shop_name");
        Shop_email=intent.getStringExtra("Shop_email");
        Shop_phone=intent.getStringExtra("Shop_phone");
        lat=intent.getStringExtra("lat");
        lng=intent.getStringExtra("lng");
        shop_ads.setText(Shop_ads);
        shop_name.setText(Shop_name);
        shop_eml.setText("Email: "+Shop_email);
        shop_phone.setText("Phone: "+Shop_phone);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);
        c_names= myPrefs.getString("c_name", null);
        Cur_cy= myPrefs.getString("currency", null);
        c_email= myPrefs.getString("c_email", null);
        c_phone= myPrefs.getString("c_phone", null);
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg"+cart_tot2);
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg"+Cur_cy);
        tv_tot=(TextView)findViewById(R.id.textView20);
        Name= (EditText) findViewById(R.id.name);
        Name.setText(c_names);

        Email= (EditText) findViewById(R.id.email);
        Email.setText(c_email);
        Phone= (EditText) findViewById(R.id.phone);
        Phone.setText(c_phone);
        if (Reg_id != null && !Reg_id.isEmpty()){

        }
        else {
            new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText(getResources().getString(R.string.Pleaseregister))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                            sDialog.dismiss();

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();

        }
        Cfm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)

names=Name.getText().toString();
                    instS=instE.getText().toString();;
                    email=Email.getText().toString();
                    phno=Phone.getText().toString();
                    if(Name.getText().toString().isEmpty()||Email.getText().toString().isEmpty()||Phone.getText().toString().isEmpty()){
                        com.nispok.snackbar.Snackbar.with(Collect_From_here.this) // context
                        .text(getResources().getString(R.string.empty_fields)) // text to display
                                .show(Collect_From_here.this);
                      /*  com.chootdev.csnackbar.Snackbar.with(Collect_From_here.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Field Empty!")
                                .duration(Duration.SHORT)
                                .show();*/
                        edittexterror = true;
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email.getText().toString().trim()).matches()) {
                        Email.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (Name.getText().toString().isEmpty()) {
                      Name.setError(getResources().getString(R.string.Enter_Full_name));
                        edittexterror = true;
                    } else if (Phone.getText().toString().isEmpty()) {
                        Phone.setError(getResources().getString(R.string.Enter_Phone));
                        edittexterror = true;
                    } else if (!android.util.Patterns.PHONE.matcher(Phone.getText().toString().trim()).matches()) {
                        Phone.setError(getResources().getString(R.string.Invalid_Phone));
                        edittexterror = true;
                    }
                    if (edittexterror == false) {
                        if (Reg_id != null && !Reg_id.isEmpty()){
                        new SendPostRequest1().execute();
                    }

                    else {
                        new SweetAlertDialog(Collect_From_here.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                .setTitleText(getResources().getString(R.string.Pleaseregister))
                                .setCustomImage(R.drawable.logo)
                                .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                                .setConfirmText(getResources().getString(R.string.Yes))
                                .setCancelText(getResources().getString(R.string.No))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(i);

                                        sDialog.dismiss();

                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();

                    }
                    }


                } else {
                    com.nispok.snackbar.Snackbar.with(Collect_From_here.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Collect_From_here.this);
            /*        com.chootdev.csnackbar.Snackbar.with(context,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/


                }
            }
        });
        ImageView back= (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Collect_From_here.this.goBack();
            }
        });
        tot = qty * price;
        gtot=gtot+tot;

        context = this;

        final DatabaseHelperCollect dataHelper = new DatabaseHelperCollect(context);
        //  dataHelper.insertData(name, qty, price, tot,gtot);
        final TableLayout tableLayout = (TableLayout) findViewById(R.id.tl);


        TableRow rowHeader = new TableRow(context);


        rowHeader.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 130));
        rowHeader.setBackgroundResource(R.drawable.text_bg);
        View line = new View(this);
        line.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, 80));
        line.setBackgroundColor(Color.rgb(51, 51, 51));
        rowHeader.addView(line);
      //  String[] headerText = {"Products", "Qty", "Price(AED)", "Total(AED)",""};
        String[] headerText = {getResources().getString(R.string.products), getResources().getString(R.string.quantity)+"(kg)", getResources().getString(R.string.price)+"("+Cur_cy+")", getResources().getString(R.string.total)+"("+Cur_cy+")",""};
        tableLayout.setStretchAllColumns(true);
        for (String c : headerText)
        {
            TextView tv = new TextView(this);
            tv.setTextColor(Color.parseColor("#FFFFFF"));
            tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, 70));
            tv.setGravity(Gravity.CENTER_HORIZONTAL);

            tv.setTextSize(15);
            tv.setText(c);

            rowHeader.addView(tv);


        }
        tableLayout.addView(rowHeader);


        db = dataHelper.getReadableDatabase();
        db.beginTransaction();
        try {
            String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
            System.out.println("" + selectQuery);
           cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() >0) {
                while (cursor.moveToNext()) {
                   cartmodl=new CartModel();
                   /* final int outlet_id = cursor.getInt(cursor.getColumnIndex("outlet_id"));
                    String outlet_name = cursor.getString(cursor.getColumnIndex("outlet_name"));
                    final Float outlet_qty = cursor.getFloat(cursor.getColumnIndex("outlet_qty"));
                    final Float outlet_price = cursor.getFloat(cursor.getColumnIndex("outlet_price"));
                    final Float outlet_tot = cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                    Float grnd = cursor.getFloat(cursor.getColumnIndex("total"));
                    String pro_id= cursor.getString(cursor.getColumnIndex("pro_id"));
                    final String price_quantity=cursor.getString(cursor.getColumnIndex("price_quantity"));
                    String kg_or_g=cursor.getString(cursor.getColumnIndex("kg_or_g"));
                    String value_befr_kg_or_g=cursor.getString(cursor.getColumnIndex("value_befr_kg_or_g"));
                    final Float price_for_1kg=cursor.getFloat(cursor.getColumnIndex("price_for_1kg"));
                    final Float price_for_1_gram=cursor.getFloat(cursor.getColumnIndex("price_for_1_gram"));*/
                    final int outlet_id = cursor.getInt(cursor.getColumnIndex("outlet_id"));
                    String outlet_name = cursor.getString(cursor.getColumnIndex("outlet_name"));
                    final Float outlet_qty = cursor.getFloat(cursor.getColumnIndex("outlet_qty"));
                    final Float outlet_price = cursor.getFloat(cursor.getColumnIndex("outlet_price"));
                    final Float outlet_tot = cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                    Float grnd = cursor.getFloat(cursor.getColumnIndex("total"));
                  //  String pro_id= cursor.getString(cursor.getColumnIndex("pro_id"));
//                    final Float delvc= cursor.getFloat(cursor.getColumnIndex("fds"));
                    final String price_quantity=cursor.getString(cursor.getColumnIndex("price_quantity"));
                    String kg_or_g=cursor.getString(cursor.getColumnIndex("kg_or_g"));
                    String value_befr_kg_or_g=cursor.getString(cursor.getColumnIndex("value_befr_kg_or_g"));
                    final Float price_for_1kg=cursor.getFloat(cursor.getColumnIndex("price_for_1kg"));
                    final Float price_for_1_gram=cursor.getFloat(cursor.getColumnIndex("price_for_1_gram"));
                    subtotal+=outlet_tot;
                    String s= String.valueOf(outlet_tot);
                    //  s=new JsonModel();
                    System.out.println("<<<<<<<<<<<<outlettot>>>>>>>>>>>>" +s);
                    System.out.println("course2" + outlet_name);
                    System.out.println("sub_coursecoursereg" +outlet_qty);
                    System.out.println("sub_coursecourseregid" + outlet_price);
                    System.out.println("sub_coursecourseregid" + outlet_tot);
                    System.out.println("sub_coursecourseregid" + grnd);
                    final TableRow row = new TableRow(context);
                    row.setBackgroundResource(R.drawable.layer_nw);
                    row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 100));
                    row.setMinimumHeight(50);
                    //tr.addView(view);
                    String[] colText = {"" + outlet_name, "" + outlet_qty, "" + outlet_price, "" + outlet_tot};

////////////////////////////////////////////////////////////////////////// 1ST CHANGE START /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    TextView outlet_nameTV=new TextView(this);
                    outlet_nameTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,100));
                    outlet_nameTV.setGravity(Gravity.CENTER);
                    outlet_nameTV.setTextSize(14);
                  //  outlet_nameTV.setTextColor(Integer.parseInt("#000000"));
                    outlet_nameTV.setText(outlet_name);


                    final EditText outlet_qtyTV=new EditText(this);
                    outlet_qtyTV.setBackgroundResource(R.drawable.border);
                    outlet_qtyTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,80));
                    outlet_qtyTV.setGravity(Gravity.CENTER);
                    outlet_qtyTV.setTextSize(14);
                    outlet_qtyTV.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

                    outlet_qtyTV.setText(Float.toString(outlet_qty));


                    TextView outlet_priceTV=new TextView(this);
                    outlet_priceTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,100));
                    outlet_priceTV.setGravity(Gravity.CENTER);
                    outlet_priceTV.setTextSize(14);
                   // outlet_priceTV.setTextColor(Integer.parseInt("#000000"));
                    outlet_priceTV.setText(Float.toString(outlet_price)+"/"+price_quantity);

                    final TextView outlet_totTV=new TextView(this);
                    Typeface font5= Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
                    outlet_totTV.setTypeface(font5);
                    outlet_totTV.setTextColor(Color.parseColor("#000000"));
                    outlet_totTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,100));
                    outlet_totTV.setGravity(Gravity.CENTER);
                    outlet_totTV.setTextSize(16);
                    Locale.setDefault(Locale.US);
                    outlet_totTV.setText(Float.toString(Float.valueOf(new DecimalFormat("##.##").format(outlet_tot))));
                   /* final TextView outlet_totTV=new TextView(this);
                    outlet_totTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,100));
                    outlet_totTV.setGravity(Gravity.CENTER);
                    outlet_totTV.setTextSize(14);
                  //  outlet_totTV.setTextColor(Integer.parseInt("#000000"));
                    outlet_totTV.setText(Float.toString(Float.valueOf(new DecimalFormat("##.##").format(outlet_tot))));
*/

                    row.addView(outlet_nameTV);
                    row.addView(outlet_qtyTV);
                    row.addView(outlet_priceTV);
                    row.addView(outlet_totTV);
                    tableLayout.addView(row);



                    outlet_qtyTV.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            //Toast.makeText(getApplicationContext(), " aftertextchanged", Toast.LENGTH_SHORT).show();


                          /*  try{
                                if (outlet_qtyTV.getText().length() == 0) {


                                } else if (Float.parseFloat(outlet_qtyTV.getText().toString()) == 0) {
                                    outlet_qtyTV.setText("0.5");
                                } else {





                                    Float new_qty = Float.parseFloat(outlet_qtyTV.getText().toString());
                                    if (new_qty == 500.0) {
                                        new_qty = new_qty / 1000;
                                    }
                                    System.out.println("new_qty :"+new_qty);
                                    System.out.println("outlet_price :"+outlet_price);
                                    System.out.println("outlet_qty :"+outlet_qty);


                                    Float divider=0f;
                                    String kg_or_g = (price_quantity.substring(Math.max(price_quantity.length() - 2, 0))).trim();//getting 'g' or 'kg'
                                    if(kg_or_g.contentEquals("g")){
                                        System.out.println("inside 'g'");
                                        divider=Float.parseFloat(price_quantity.substring(0,price_quantity.length() - 2).trim());
                                        divider/=1000;
                                        System.out.println("divider : "+divider);
                                    }else{
                                        divider=Float.parseFloat(price_quantity.substring(0,price_quantity.length() - 2).trim());
                                        System.out.println("divider : "+divider);
                                    }



                                    System.out.println("price_quantity.substring(0,price_quantity.length() - 2) :"+price_quantity.substring(0,price_quantity.length() - 2));
                                    Float new_total = (new_qty * outlet_price)/divider;
                                    outlet_totTV.setText(Float.toString(Float.valueOf(new DecimalFormat("##.##").format(new_total))));
                                    System.out.println("////////////////////////////////\nNew Total :" + new_total + "\n/////////////////////////////////////");
                                    dataHelper.UpdateAmount(outlet_id, new_qty, new_total);
                                    cnt = tableLayout.getChildCount();


                                    db = dataHelper.getReadableDatabase();
                                    try {
                                        cart_tot2 = 0;
                                        String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                        System.out.println("" + selectQuery);
                                        Cursor cursor = db.rawQuery(selectQuery, null);

                                        if (cursor.getCount() > 0) {
                                            while (cursor.moveToNext()) {


                                                cart_tot2 += cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                                                System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ncartttotal" + cart_tot2 + "\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                            }

                                        }
                                        tv_tot.setText(Cur_cy+" "+Float.toString(Float.parseFloat(new DecimalFormat("##.##").format(cart_tot2))));
                                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg"+cart_tot2);
                                        tt_grand.setText(Cur_cy+" "+Float.toString(Float.parseFloat(new DecimalFormat("##.##").format(cart_tot2))));

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }*/

                            try{
                                if (outlet_qtyTV.getText().length() == 0) {


                                }/* else if (Float.parseFloat(outlet_qtyTV.getText().toString()) == 0) {
                                    outlet_qtyTV.setText("0.2");
                                }*/ else {

                                    Float new_qty = Float.parseFloat(outlet_qtyTV.getText().toString());
                                  /*  if (new_qty == 500.0) {
                                        new_qty = new_qty / 1000;
                                    }*/

                                    System.out.println("new_qty :"+new_qty);
                                    System.out.println("outlet_price :"+outlet_price);
                                    System.out.println("outlet_qty :"+outlet_qty);

                                    float divider=0f;
                                    Float new_total=0f;
                                    String kg_or_g = (price_quantity.substring(Math.max(price_quantity.length() - 2, 0))).trim();//getting 'g' or 'kg'
                                    if(kg_or_g.contentEquals("gm")){
                                        System.out.println("inside 'gm'");
                                        divider=Float.parseFloat(price_quantity.substring(0,price_quantity.length() - 2).trim());
                                        divider/=1000;
                                        new_total= new_qty * outlet_price/divider;
                                        System.out.println("new_total = "+"("+new_qty+"*"+outlet_price+")/"+divider+" = "+new_total);
                                        System.out.println("divider : "+divider);
                                    }else{
                                        divider=Float.parseFloat(price_quantity.substring(0,price_quantity.length() - 2).trim());
                                        new_total= new_qty * outlet_price/divider;
                                        System.out.println("new_total = "+"("+new_qty+"*"+outlet_price+")/"+divider+" = "+new_total);
                                        System.out.println("divider : "+divider);
                                    }


                                    System.out.println("price_quantity.substring(0,price_quantity.length() - 2) :"+price_quantity.substring(0,price_quantity.length() - 2));

                                    outlet_totTV.setText(Float.toString(Float.valueOf(new DecimalFormat("##.##").format(new_total))));
                                    System.out.println("////////////////////////////////\nNew Total :" + new_total + "\n/////////////////////////////////////");
                                    dataHelper.UpdateAmount(outlet_id, new_qty, new_total);
                                    cnt = tableLayout.getChildCount();


                                    db = dataHelper.getReadableDatabase();
                                    try {
                                        cart_tot2 = 0;
                                        String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                        System.out.println("" + selectQuery);
                                        Cursor cursor = db.rawQuery(selectQuery, null);

                                        if (cursor.getCount() > 0) {
                                            while (cursor.moveToNext()) {
                                                cart_tot2 += cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                                                System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ncartttotal" + cart_tot2 + "\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                            }

                                        }

                                        tv_tot.setText(Cur_cy+"  "+Float.toString(Float.parseFloat(new DecimalFormat("##.##").format(cart_tot2))));
                                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg"+cart_tot2);

                                        tt_grand.setText(Cur_cy+"  "+Float.toString(Float.parseFloat(new DecimalFormat("##.##").format(cart_tot2))));

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                               /* if(((outlet_qtyTV.getText().toString()).indexOf("."))==-1){

                                    outlet_qtyTV.setText((outlet_qtyTV.getText().toString())+".0");

                                }
                                else*/ if((outlet_qtyTV.getText().toString()=="0.5")){
                                    outlet_totTV.setText(outlet_qtyTV.getText().toString()+0.5);
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }




////////////////////////////////////////////////////////////// 1ST CHANGE END ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////
/* }
}*/

////////////////////////////////////


                        }

                    });


                    ImageView views = new ImageView(Collect_From_here.this);
                    views.setLayoutParams(new TableRow.LayoutParams(40,40));
                    row.setGravity(Gravity.CENTER);
                    views.setImageResource(R.drawable.ic_close_black_24dp);
                    row.addView(views);
                    cnt = tableLayout.getChildCount();

                    if (cnt < 2) {
                        tableLayout.removeView(row);
                        cart_tot = 0;
                        cart = Float.toString(cart_tot);
                        tv_tot.setText(Cur_cy+" "+new DecimalFormat("##.##").format(Float.parseFloat(cart)));
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg1"+cart);
                        m=cart_tot+container_charge;
                        sub_tt=Float.toString(cart_tot);
                        tt_grand.setText(Cur_cy+" "+new DecimalFormat("##.##").format(Float.parseFloat(sub_tt)));
                        System.out.println("count" + cnt);


                    } else {

                        cart_tot = cart_tot + outlet_tot;
                        cart = Float.toString(cart_tot);
                        tv_tot.setText(Cur_cy+" "+new DecimalFormat("##.##").format(Float.parseFloat(cart)));
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg2"+cart);
                        m=cart_tot+container_charge;
                        sub_tt=Float.toString(cart_tot);
                        tt_grand.setText(Cur_cy+" "+new DecimalFormat("##.##").format(Float.parseFloat(sub_tt)));
                        System.out.println("count" + cnt);
                        System.out.println("outlettotal" + outlet_tot);
                        System.out.println("cartttotal" + cart_tot);

                    }
//////////////////////////////////////////////////////////////////////////////// 2ND CHANGE START /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    views.setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {
                            new SweetAlertDialog(Collect_From_here.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setCustomImage(R.drawable.logo)
                                    .setTitleText(getResources().getString(R.string.Delete))
                                    .setContentText(getResources().getString(R.string.AreyousureWanttodeletetheproduct))
                                    //  .setContentText("Want to delete this cart item ?")
                                    .setConfirmText(getResources().getString(R.string.Yes))
                                    .setCancelText(getResources().getString(R.string.No))
                                    .showCancelButton(true)

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            tableLayout.removeView(row);


                                            dataHelper.delete(outlet_id);
                                            db = dataHelper.getReadableDatabase();
                                            try {
                                                cart_tot2=0;
                                                String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                                System.out.println("" + selectQuery);
                                                Cursor cursor = db.rawQuery(selectQuery, null);

                                                if (cursor.getCount() > 0) {
                                                    while (cursor.moveToNext()) {


                                                        cart_tot2+= cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                                                        System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ncartttotal" + cart_tot2+"\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                                        final DatabaseHelper dataHelper = new DatabaseHelper(context);
                                                        int si = (int) dataHelper.getProfilesCount();
                                                        BeefDetail.changeindex1(si);
                                                    }

                                                }
                                                else{
                                                    final DatabaseHelper dataHelper = new DatabaseHelper(context);
                                                    int si = (int) dataHelper.getProfilesCount();
                                                    BeefDetail.changeindex1(si);
                                                    goBack();
                                                }
                                                tv_tot.setText(Cur_cy+"  "+Float.toString(cart_tot2));
                                                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg3"+cart_tot2);
                                                tt_grand.setText(Cur_cy+"  "+Float.toString(cart_tot2));

                                               // BeefDetail.changeindex1(si);
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }



                                            sDialog

                                                    .setTitleText(getResources().getString(R.string.Deleted))
                                                    .setContentText(getResources().getString(R.string.YourCartItemhasbeen))
                                                    .setConfirmText(getResources().getString(R.string.ok))
                                                    .setConfirmClickListener(null)
                                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                            sDialog.dismiss();
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();




                   /* ImageView views = new ImageView(Collect_From_here.this);
                    views.setLayoutParams(new TableRow.LayoutParams(25,25));
                    row.setGravity(Gravity.CENTER);
                    views.setImageResource(R.drawable.close);
                    row.addView(views);
                    cnt = tableLayout.getChildCount();

                    if (cnt < 2) {
                        tableLayout.removeView(row);
                        cart_tot = 0;
                        cart = Float.toString(cart_tot);
                        tv_tot.setText(new DecimalFormat("##.##").format(Float.parseFloat(cart)));
                        m=cart_tot+container_charge;
                        sub_tt=Float.toString(cart_tot);
                        tt_grand.setText(new DecimalFormat("##.##").format(Float.parseFloat(sub_tt)));
                        System.out.println("count" + cnt);


                    } else {

                        cart_tot = cart_tot + outlet_tot;
                        cart = Float.toString(cart_tot);
                        tv_tot.setText(new DecimalFormat("##.##").format(Float.parseFloat(cart)));
                        m=cart_tot+container_charge;
                        sub_tt=Float.toString(cart_tot);
                        tt_grand.setText(new DecimalFormat("##.##").format(Float.parseFloat(sub_tt)));
                        System.out.println("count" + cnt);
                        System.out.println("outlettotal" + outlet_tot);
                        System.out.println("cartttotal" + cart_tot);

                    }
//////////////////////////////////////////////////////////////////////////////// 2ND CHANGE START /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    views.setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {

                            new SweetAlertDialog(Collect_From_here.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setCustomImage(R.drawable.logo)
                                    .setTitleText(getResources().getString(R.string.Delete))
                                    .setContentText(getResources().getString(R.string.AreyousureWanttodeletetheproduct))
                                    //  .setContentText("Want to delete this cart item ?")
                                    .setConfirmText(getResources().getString(R.string.Yes))
                                    .setCancelText(getResources().getString(R.string.No))
                                    .showCancelButton(true)
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            tableLayout.removeView(row);


                                            dataHelper.delete(outlet_id);
                                            db = dataHelper.getReadableDatabase();
                                            try {
                                                cart_tot2=0;
                                                String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                                System.out.println("" + selectQuery);
                                                Cursor cursor = db.rawQuery(selectQuery, null);

                                                if (cursor.getCount() > 0) {
                                                    while (cursor.moveToNext()) {


                                                        cart_tot2+= cursor.getInt(cursor.getColumnIndex("outlet_tot"));
                                                        System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ncartttotal" + cart_tot2+"\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                                    }

                                                }
                                                else{
                                                    goBack();
                                                }
                                                tv_tot.setText(Float.toString(cart_tot2*2));
                                                tt_grand.setText(Float.toString(cart_tot2*2));

                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }

sweetAlertDialog.dismiss();

                                        }
                                    });*/





                           /* new AlertDialog.Builder(context)
                                    .setTitle("Delete Item")
                                    .setMessage("Are you sure you want to delete this Item?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            tableLayout.removeView(row);


                                            dataHelper.delete(outlet_id);
                                            db = dataHelper.getReadableDatabase();
                                            try {
                                                cart_tot2=0;
                                                String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                                System.out.println("" + selectQuery);
                                                Cursor cursor = db.rawQuery(selectQuery, null);

                                                if (cursor.getCount() > 0) {
                                                    while (cursor.moveToNext()) {


                                                        cart_tot2+= cursor.getInt(cursor.getColumnIndex("outlet_tot"));
                                                        System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ncartttotal" + cart_tot2+"\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                                    }

                                                }
                                                else{
                                                    goBack();
                                                }
                                                tv_tot.setText(Float.toString(cart_tot2*2));
                                                tt_grand.setText(Float.toString(cart_tot2*2));

                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }




                                            dialog.dismiss();
                                        }

                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                            dialog.dismiss();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();*/


                        }
                    });


                }
            }
            db.setTransactionSuccessful();

        } catch (SQLiteException e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();
            // End the transaction.
            db.close();
            // Close database
        }
    }
    @Override
    public void onBackPressed() {

        if (cursor.getCount() >0) {
            new SweetAlertDialog(Collect_From_here.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("")
                    .setTitleText(getResources().getString(R.string.Exit))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.DoyouwanttosaveyourCartitem))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            final DatabaseHelperCollect dataHelper = new DatabaseHelperCollect(Collect_From_here.this);

                            // final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                            dataHelper.removeAll();

                           /* Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                            startActivity(i);*/
                            finish();
                            sweetAlertDialog.dismiss();
                            sweetAlertDialog.onBackPressed();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            finish();

                        }
                    })
                    .show();

        }
        else {
            finish();
        }
      /*  if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Snackbar.with(OrderNew_Activity.this,null)
                    .type(DisplayContext.Type.SUCCESS)

                    .message("Press Again to Exit from Zulekha !")
                    .duration(android.support.design.widget.Snackbar.Duration.LONG)

                    .show();
        }
        back_pressed = System.currentTimeMillis();*/
        //Do the Logics Here
    }
    private void goBack() {

        if (cursor.getCount() > 0) {
            new SweetAlertDialog(Collect_From_here.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("")
                    .setTitleText(getResources().getString(R.string.Exit))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.DoyouwanttosaveyourCartitem))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            final DatabaseHelperCollect dataHelper = new DatabaseHelperCollect(Collect_From_here.this);

                            // final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                            dataHelper.removeAll();

                            /*Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                            startActivity(i);*/
                            finish();

                            sweetAlertDialog.dismiss();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            finish();

                        }
                    })
                    .show();
        } else {
            finish();
        }
    }
    private class SendPostRequest1 extends AsyncTask<String, Void, String> {

        public void onPreExecute() {
            progress_loader.setVisibility(View.VISIBLE);
            gson=new Gson();
            final DatabaseHelperCollect dataHelper = new DatabaseHelperCollect(context);
            db = dataHelper.getReadableDatabase();
            db.beginTransaction();
            try {
                String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                System.out.println("" + selectQuery);
                Cursor cursor = db.rawQuery(selectQuery, null);

                if (cursor.getCount() > 0) {
                    cart_tot2=0;
                    while (cursor.moveToNext()) {
                        cartmodl = new CartModel();
                        final int outlet_id = cursor.getInt(cursor.getColumnIndex("outlet_id"));
                        String outlet_name = cursor.getString(cursor.getColumnIndex("outlet_name"));
                        Float outlet_qty = cursor.getFloat(cursor.getColumnIndex("outlet_qty"));
                        final Float outlet_price = cursor.getFloat(cursor.getColumnIndex("outlet_price"));
                        final Float outlet_tot = cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                        Float grnd = cursor.getFloat(cursor.getColumnIndex("total"));
                        String pro_id = cursor.getString(cursor.getColumnIndex("pro_id"));
                        final String price_quantity=cursor.getString(cursor.getColumnIndex("price_quantity"));
                        String but_id=cursor.getString(cursor.getColumnIndex("butcher_id"));
                        type_id=cursor.getString(cursor.getColumnIndex("type_id"));
                        System.out.println("grandtotalll" + grnd);
                        cartmodl.setId(outlet_id);
                        cartmodl.setNam(outlet_name);
                        cartmodl.setQty(outlet_qty);
                        cartmodl.setPrice(outlet_price);
                        cartmodl.setGrnd(grnd);
                        cartmodl.setBut_id(but_id);
                        cartmodl.setPrice_quantity(price_quantity);
                        cartmodl.setType_id(type_id);
                        cartmodl.setOut_tt(String.valueOf(outlet_qty));

                        // arrayListcart.add(cartmodl);
                        arrayListcart.add(cartmodl);

                        s = new JsonModel();
                        array_product.add(pro_id);
                        array_item.add(outlet_name);
                        array_qty.add(String.valueOf(outlet_qty));
                        array_price.add(String.valueOf(outlet_price));
                        array_type.add(type_id);
                        But_id.add(but_id);
                        array_price_qty.add(price_quantity);
                        array_tot.add(String.valueOf(outlet_tot));
                        csv = array_product.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",").replace(",",",");
                        System.out.println("<<<<<<<<<<<<<csv>>>>>>>>>>>>>>>" + csv);
                        csf = array_item.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",").replace(",",",");
                        System.out.println("<<<<<<<<<<<<<csf>>>>>>>>>>>>>>>" + csf);
                        csd = array_qty.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",").replace(",",",");
                        System.out.println("<<<<<<<<<<<<<csd>>>>>>>>>>>>>>>" + csd);
                        csg = array_price.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",").replace(",",",");
                        cbf=But_id.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",").replace(",",",");
                        cpt=array_price_qty.toString().replace("[","").replace("]","").replace(",",",").replace(" ","");
                        cty=array_type.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",").replace(",",",");
                        System.out.println("<<<<<<<<<<<<<csg>>>>>>>>>>>>>>>" + csg);
ctt=array_tot.toString().replace("[", "").replace("]", "")
        .replace(", ", ",").replace(" ","");
                        array_lstcart = new ArrayList<>();
                        s.setProductid(csv);
                        s.setProduct(csf);
                        s.setQty(csd);
                        s.setPrice(csg);
                        s.setButcher_id(cbf);
                        s.setPrice_quantity(cpt);
                        s.setType_id(cty);
                        s.setOut_tt(ctt);
                        array_lstcart.add(s);
                        String added="";
                        System.out.println("outlet_tot : "+cursor.getFloat(cursor.getColumnIndex("outlet_tot")));
                        cart_tot2+= cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                        System.out.println("<<<<<<<<<<<<<Sum>>>>>>>>>>>>>>>" + cart_tot2);

                        sme_jsn = gson.toJson(array_lstcart);


                        System.out.println("<<<<<<<<<<<<<new my json>>>>>>>>>>>>>>>" + sme_jsn);


                        System.out.println("<<<<<<<<<<<<product_id>>>>>>>>>>>>" + pro_id);
                        System.out.println("course2" + outlet_name);
                        System.out.println("sub_coursecoursereg" + outlet_qty);
                        System.out.println("sub_coursecourseregid" + outlet_price);
                        System.out.println("sub_coursecourseregid" + outlet_tot);
                        System.out.println("sub_coursecourseregid" + grnd);

                    }

                }
                db.setTransactionSuccessful();

            } catch (SQLiteException e) {
                e.printStackTrace();

            } finally {
                db.endTransaction();
                // End the transaction.
                db.close();
                // Close database
            }
        }
        @RequiresApi(api = Build.VERSION_CODES.N)
        public String doInBackground(String... arg0) {

            try {

                URL url = new URL("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/request.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


               // JSONObject postDataParams = new JSONObject();
                try {

                    currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            } catch (NoClassDefFoundError e) {
                e.printStackTrace();

            }

                tt= String.valueOf(cart_tot2);

                System.out.println("<<<<<<<<<<<<<anvinraj>>>>>>>>>>>>>>" +tt);
                postDataParams.put("butcher_id",k);
                postDataParams.put("user_id",Reg_id);
                postDataParams.put("proid", j);
                postDataParams.put("fullname",names);
                postDataParams.put("address", "");
                postDataParams.put("phoneno",phno);
                postDataParams.put("email", email);
                postDataParams.put("delivery","No");
                postDataParams.put("subtotal",tt);
                postDataParams.put("total",tt);
                postDataParams.put("device_token",refreshedToken);
                postDataParams.put("order_time",currentDateTimeString);
                postDataParams.put("currency",Cur_cy);
                postDataParams.put("instruction",instS);
                postDataParams.put("someJSON",sme_jsn);


                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
            progress_loader.setVisibility(View.GONE);
           /* Toast.makeText(getApplicationContext(),result,
                    Toast.LENGTH_LONG).show();*/

                if(result.equalsIgnoreCase("\"success\"")){

                    new SweetAlertDialog(Collect_From_here.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(getResources().getString(R.string.AWESOME))
                            .setContentText(getResources().getString(R.string.OrderplacedSuccessfully))
                            .setCustomImage(R.drawable.logo)
                            .setConfirmText(getResources().getString(R.string.ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    final DatabaseHelperCollect dataHelper = new DatabaseHelperCollect(context);

                                    // final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                                    dataHelper.removeAll();
                                    sDialog
                                            .setTitleText(getResources().getString(R.string.ThankYou))
                                            .setContentText(getResources().getString(R.string.PleaseCollectitfromourshop))
                                            .setConfirmText(getResources().getString(R.string.ok))
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                                    Intent i = new Intent(getApplicationContext(),Collect_final_Screen.class);
                                                    i.putExtra("lat",lat);
                                                    i.putExtra("lng",lng);
                                                    i.putExtra("Shop_ads",Shop_ads);
                                                    i.putExtra("Email",Shop_email);
                                                    i.putExtra("Phone",Shop_phone);
                                                    startActivity(i);
                                                    finish();
                                                    sweetAlertDialog.dismiss();
                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                }
                            })
                            .show();

                }
                else {
                    com.nispok.snackbar.Snackbar.with(Collect_From_here.this) // context
                            .text("Order not placed!") // text to display
                            .show(Collect_From_here.this);

                }


        }




    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

}
