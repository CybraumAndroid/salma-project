package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Rashid on 11/11/2016.
 */

public class DataAdapter_2 extends RecyclerView.Adapter<DataAdapter_2.ViewHolder>{
    private ArrayList<imageModel> imageNameList;
Context context1;
DatabaseHandler dh=new DatabaseHandler();




    public DataAdapter_2(Context context2, ArrayList<imageModel>imageNames){
        this.imageNameList=imageNames;
        context1=context2;
        System.out.println("||||||||||||||||||     Inside dataadpater_2");

       for(imageModel im:imageNameList)
       {
           System.out.println("|||||  url in adapter : "+im.getImage());
       }
    }
    @Override
    public DataAdapter_2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_2,parent,false);
        return new DataAdapter_2.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        System.out.println("|||||||||||    Inside on bind viewholder : "+imageNameList.get(position).getImage());
        String primaryId=Integer.toString(imageNameList.get(position).getId());
        holder.PrimaryId.setText(primaryId);
        Typeface myFont2 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
       holder.tv1.setTypeface(myFont2);
       holder.tv1.setText(imageNameList.get(position).getShop_name());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+imageNameList.get(position).getShop_name());

        Typeface myFont3 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
     holder.tv2.setTypeface(myFont3);
holder.tv2.setText(imageNameList.get(position).getDelivery_info());

        Typeface myFont4 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
        holder.tv3.setTypeface(myFont4);
        holder.tv3.setText(imageNameList.get(position).getButcher_discount());

        Picasso.with(context1)
                .load(imageNameList.get(position).getImage())
                .resize(1000,1000)
                .into(holder.imgView2);
holder.clse_lt.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(final View view) {
        System.out.println("url "+imageNameList.get(position).getImage());


        try {
            new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText(context1.getResources().getString(R.string.Delete))
                  .setContentText(context1.getResources().getString(R.string.Areyousurewanttodeleteyourfavouriteshop))
                    .setConfirmText(context1.getResources().getString(R.string.Yes))
                    .setCustomImage(R.drawable.logo)
                   // .setCancelText(context1.getResources().getString(R.string.No))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            dh.DeleteView(context1,holder);
                            com.nispok.snackbar.Snackbar.with(view.getContext()) // context
                                    .text("Deleted!") // text to display
                                    .show((Activity) view.getContext());

                            imageNameList.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position,imageNameList.size());
                            sDialog
                                    .setTitleText("")
                                    .setContentText(context1.getResources().getString(R.string.YourFavouriteButcherhasbeendeleted))
                                    .setConfirmText(context1.getResources().getString(R.string.ok))

                                    .setConfirmClickListener(null)
                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        }
                    })
                    .show();



        }
        catch(Exception e){
            e.printStackTrace();
            com.nispok.snackbar.Snackbar.with(view.getContext()) // context
                    .text("Failed!") // text to display
                    .show((Activity) view.getContext());

        }





    }
});
        holder.lt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String added_id=imageNameList.get(position).getAdded_by();
                String cat_id=imageNameList.get(position).getCat_id();
                //String cat_id=imageNames.get(position).g;

                Intent i=new Intent(context1,Customer_Butcher_Sublist_Fragment.class);
                i.putExtra("add_id",added_id);
                i.putExtra("cat_id",cat_id);
               i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //i.putExtra("value",status);
               context1.startActivity(i);
            }
        });
        holder.lt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String added_id=imageNameList.get(position).getAdded_by();
                String cat_id=imageNameList.get(position).getCat_id();
                //String cat_id=imageNames.get(position).g;

                Intent i=new Intent(context1,Customer_Butcher_Sublist_Fragment.class);
                i.putExtra("add_id",added_id);
                i.putExtra("cat_id",cat_id);
             i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //i.putExtra("value",status);
                context1.startActivity(i);
            }
        });
        holder.lt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String added_id=imageNameList.get(position).getAdded_by();
                String cat_id=imageNameList.get(position).getCat_id();
                //String cat_id=imageNames.get(position).g;

                Intent i=new Intent(context1,Customer_Butcher_Sublist_Fragment.class);
                i.putExtra("add_id",added_id);
                i.putExtra("cat_id",cat_id);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //i.putExtra("value",status);
                context1.startActivity(i);
            }
        });
    holder.rmv_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                /*Intent i=new Intent(context1,Favorites.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("name",imageNameList.get(position).getImage());
                context1.startActivity(i);*/

                System.out.println("url "+imageNameList.get(position).getImage());


                try {

                    new SweetAlertDialog(view.getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(context1.getResources().getString(R.string.Delete))
                        .setContentText(context1.getResources().getString(R.string.Areyousurewanttodeleteyourfavouriteshop))
                            .setCustomImage(R.drawable.logo)
                            .setConfirmText(context1.getResources().getString(R.string.Yes))
                            //.setCancelText(context1.getResources().getString(R.string.No))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    dh.DeleteView(context1,holder);


                                    imageNameList.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position,imageNameList.size());
                                    sDialog

                                            .setTitleText("")
                                            .setContentText(context1.getResources().getString(R.string.YourFavouriteButcherhasbeendeleted))
                                            .setConfirmText(context1.getResources().getString(R.string.ok))
                                            .setConfirmClickListener(null)
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);



                                }
                            })
                           /* .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })*/
                            .show();






                }
                catch(Exception e){
                    e.printStackTrace();

                }







            }
        });
    }


    @Override
    public int getItemCount() {
        return imageNameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView imgView2;
        public TextView rmv_btn;
        public TextView PrimaryId;
        public RecyclerView rv2;
        TextView tv,tv1,tv2,tv3;
        LinearLayout clse_lt,lt1,lt2,lt3;
        public ViewHolder(View itemView) {
            super(itemView);

            imgView2=(ImageView)itemView.findViewById(R.id.imageView8);
            rmv_btn=(TextView)itemView.findViewById(R.id.dlt);
            PrimaryId=(TextView)itemView.findViewById(R.id.PrimaryId);
            rv2=(RecyclerView)itemView.findViewById(R.id.recylerView2);
            tv1=   (TextView) itemView.findViewById(R.id.shp_nme);
            tv2=   (TextView) itemView.findViewById(R.id.dlvry_info);
            tv3=   (TextView) itemView.findViewById(R.id.dscnts);
         clse_lt=(LinearLayout) itemView.findViewById(R.id.clse_l);
            lt1= (LinearLayout) itemView.findViewById(R.id.lt1);
            lt2= (LinearLayout) itemView.findViewById(R.id.lt2);
            lt3= (LinearLayout) itemView.findViewById(R.id.lt3);
        }

    }
}
