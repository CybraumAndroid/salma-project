package com.salmabutchershop.salma.meridian.app.butcher.sidebar;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 12/23/2016.
 */
public class News_Updates_adapter extends RecyclerView.Adapter<News_Updates_adapter.ViewHolder> {


    List<NewsUpdates_Model> num;
    Context context;



    public News_Updates_adapter(ArrayList<NewsUpdates_Model> num, Context context) {
        this.num = num;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;
        public static MKLoader progress_loader;
        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            tv=   (TextView) itemView.findViewById(R.id.news_title);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
            tv1=   (TextView) itemView.findViewById(R.id.news_desc);
            v=   (ImageView) itemView.findViewById(R.id.imageView8);
        }
    }


    @Override
    public int getItemCount() {
        return num.size();
    }


    @Override
    public News_Updates_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_updates_item_layout, viewGroup, false);
        News_Updates_adapter.ViewHolder pvh = new News_Updates_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final News_Updates_adapter.ViewHolder personViewHolder, final int i) {


      String s = num.get(i).getNews_image();
        personViewHolder.tv.setText(num.get(i).getNews_title());
        Typeface myFont1 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
    personViewHolder.tv.setTypeface(myFont1);
        personViewHolder.tv1.setText(num.get(i).getNews_desc());
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv1.setTypeface(myFont2);

 ;
    // Picasso.with(context).load(s).noFade().into(personViewHolder.v);
        try {


            new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .build()
                    .load(s)
                    .noFade()
                    // .into(personViewHolder.v);
                    .into(personViewHolder.v, new Callback() {
                        @Override
                        public void onSuccess() {

                            personViewHolder.progress_loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}