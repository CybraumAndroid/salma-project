package com.salmabutchershop.salma.meridian.app.customer.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.address.DashboardActivity;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.newintro.GPSTracker1;
import com.salmabutchershop.salma.meridian.app.customer.newintro.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.facebook.login.widget.ProfilePictureView.TAG;

/**
 * Created by libin on 10/17/2016.
 */

public class Location_Services_Fragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;

    Fragment fragment;
    View view;
    /*ProgressBar*/ //progress;
    RecyclerView recyclerview, recyclerview1;
    String news_id, news_title, news_content, result, news_img, Dat;
    Location_Services_adapter adapter;
    static ArrayList<Location_Services_Model> eem;
    Location_Services_Model ee;
    String id, city;
    String tag = "events";
    Location_City_adapter adapter1;
    static ArrayList<Location_City_Model> lcm;
    Location_City_Model cm;
    String ids;
    String result1;
    ArrayList<String> divisonlist;
    Spinner spinner0, spinner1, spinner2;
    String dptmt, dvn_id, country_code, c_code;
    private PrefManager prefManager;
    // String flag="1";
    String flag_id;
    GPSTracker1 gps;
    double latitude;
    double longitude;
    String flag = "1";
    String address, state, postalCode, knownName, Country, Area, SubLocality;
    EditText Cntry, Cty, Ara;
    Button Next;
    TextView tc;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    ProgressBar progress;
    static String cvc;
    String str;
    private static final String API_KEY = "AIzaSyAo27AsUvSaSFEQ5OZWGc4l2SczpZhwnL4";
    LinearLayout LOC;
    String C_lat, C_long, loc_ads;
    ClearableAutoCompleteTextView autoCompView;
    Double lt;
    Double lg;
    String ss;
    List<Address> addresses = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.our_location_new);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        LOC = (LinearLayout) findViewById(R.id.imageView25);
        Cntry = (EditText) findViewById(R.id.editText1);
        Cty = (EditText) findViewById(R.id.editText2);
        Ara = (EditText) findViewById(R.id.editText3);
        Next = (Button) findViewById(R.id.next);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        c_code = myPrefs.getString("c_code", null);
        flag_id = myPrefs.getString("flag_id", null);
        C_lat = myPrefs.getString("latitude", null);
        C_long = myPrefs.getString("longitude", null);
        loc_ads = myPrefs.getString("loc_ads", null);
        Intent intent = getIntent();
        loc_ads = intent.getStringExtra("rm");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + loc_ads);
        autoCompView = (ClearableAutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        autoCompView.setText(loc_ads);
        tc = (TextView) findViewById(R.id.tc);
        System.out.println("<<<<<<<<<c_code>>>>>" + flag_id);
      /*  editor.putString("loc_ads",""+address+"\n"+","+city+"\n"+","+state+"\n"+","+postalCode+"\n"+","+knownName);*/
      /*  editor.putString("ads",address);
        editor.putString("city",city);
        editor.putString("state",state);
        editor.putString("postal",postalCode);
        editor.putString("knwn",knownName);*/
        System.out.println("<<<<<<<<<,.............................................................................................>>>>" + C_lat);
        System.out.println("<<<<<<<<<,.............................................................................................>>>>" + C_long);
        try {


            if (C_lat.equalsIgnoreCase("0.0") && C_long.equalsIgnoreCase("0.0")) {
                progress.setVisibility(View.VISIBLE);
                gps = new GPSTracker1(Location_Services_Fragment.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    System.out.println("<<<<<<<<<,.............................................................................................>>>>" + C_lat);
                    System.out.println("<<<<<<<<<,.............................................................................................>>>>" + C_long);
                    lt = latitude;
                    lg = longitude;
                }
            } else {
                progress.setVisibility(View.GONE);
            }
      /*  if (flag_id != null && !flag_id.isEmpty()) {
            Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
            startActivity(i1);
            finish();
        }*/
        } catch (NullPointerException e) {
        }


        LOC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(i1);
                finish();
            }
        });


        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            if (ContextCompat.checkSelfPermission(Location_Services_Fragment.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Location_Services_Fragment.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Location_Services_Fragment.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(Location_Services_Fragment.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {

                        new GeocodeAsyncTask().execute();

                    } else {
                        com.nispok.snackbar.Snackbar.with(Location_Services_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Location_Services_Fragment.this);


                    }
                    // \n is for new line
                    //    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(Location_Services_Fragment.this, Locale.getDefault());

                  /*  try {
                        lt = Double.valueOf(C_lat);
                        lg = Double.valueOf(C_long);
                        addresses = geocoder.getFromLocation(lt, lg, 1);
                        progress.setVisibility(View.VISIBLE);
                        if (!addresses.isEmpty()) {
                            Country = addresses.get(0).getCountryName();
                            System.out.println("-----------------------Country-----------------" + Country);
                            Cntry.setText(Country);
                            // address = addresses.get(0).getAddressLine(0);
                            //  System.out.println("-----------------------adsggg-----------------" + address);
                            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            city = addresses.get(0).getLocality();
                            System.out.println("------------------city----------------------" + city);
                            Cty.setText(city);
                            Area = addresses.get(0).getSubLocality();
                            state = addresses.get(0).getLocality();
                            System.out.println("---------------Area-------------------------" + Area);
                            address = addresses.get(0).getAddressLine(0);
                            if (autoCompView.getText().toString().equalsIgnoreCase("")) {
                                autoCompView.setText(address);
                            }
                            //  SubLocality=addresses.get(0).getSubLocality();
                            SubLocality = addresses.get(0).getAddressLine(0);
                            System.out.println("-----------------------SubLocality-----------------" + SubLocality);
                            System.out.println("-----------------------adsggg-----------------" + address);

                           *//* if(!Area.equalsIgnoreCase("")&&!Area.isEmpty()&&Area.equals(null)) {
                                autoCompView.setText(Area + "," + state);
                            }
                            else {
                                autoCompView.setHint("Type Your Lacality");
                            }*//*
                            /*//*//**//*    String country = addresses.get(0).getCountryName();
                            // System.out.println("-----------------cntry-----------------------" + country);*//**//*
                            postalCode = addresses.get(0).getPostalCode();
                            System.out.println("----------------postalcode------------------------" + postalCode);
                            knownName = addresses.get(0).getFeatureName();
                            System.out.println("-----------------knwn-----------------------" + knownName);
                            cvc = addresses.get(0).getCountryCode();
                            // textinfo.setText("" + address + "\n" + "" + city + "\n" + "" + state + "\n" + "" + postalCode + "\n" + "" + knownName);
                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();

                            editor.putString("city_id", cvc);
                            editor.putString("citys", city);
                            editor.putString("cntry", Country);
                            editor.putString("cvc", cvc);
                            editor.putString("SubLocality", SubLocality);
                            // editor.putString("latitude", String.valueOf(latitude));
                            editor.commit();
                            progress.setVisibility(View.GONE);
                        }
                        {
                            System.out.println("----------------------------------------");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }


            autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter1(Location_Services_Fragment.this, R.layout.list_item));
            autoCompView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                }
            });

            autoCompView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    str = (String) adapterView.getItemAtPosition(i);


                }
            });
            autoCompView.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {
                @Override
                public void onClear() {
                    autoCompView.setText("");
                    System.out.println("clearrrrrr");
                }
            });

        } else {
            com.nispok.snackbar.Snackbar.with(Location_Services_Fragment.this) // context
                    .text("No internet connection") // text to display
                    .show(Location_Services_Fragment.this);


        }

        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                  /*  SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("flag_id", flag);
                    editor.commit();


                    Locale locale2 = new Locale("en");
                    Locale.setDefault(locale2);
                    Configuration config2 = new Configuration();
                    config2.locale = locale2;
                    getBaseContext().getResources().updateConfiguration(config2, null);
                    Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
                    startActivity(i1);
                    finish();*/
                    Intent i2 = new Intent(getApplicationContext(), SelectionPage.class);
                    startActivity(i2);
                    finish();
                  /*  new SweetAlertDialog(Location_Services_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Choose Your Language")
                           // .setContentText("Won't be able to recover this file!")

                            .setCustomImage(R.drawable.logo)
                            .setCancelText("Arabic")
                            .setConfirmText("English")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();
                               *//*     Locale locale2 = new Locale("ar");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);*//*

                                    sDialog.dismiss();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();


                            *//*        Locale locale2 = new Locale("en");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);*//*
                                    Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
                                    startActivity(i1);
                                    finish();
                                   sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        if (str.equalsIgnoreCase("")) {
            Toast.makeText(this, getResources().getString(R.string.please_enter_correct_place), Toast.LENGTH_SHORT).show();
        } else {


            // Lx.setVisibility(View.VISIBLE);
            // startCountAnimation();
        }
    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        trustEveryone();
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=" + "country:" + cvc);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());


            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    private static void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

    public static void Stackloc(double latitude, double longitude) {

    }

    public class GooglePlacesAutocompleteAdapter1 extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter1(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }


    class GeocodeAsyncTask extends AsyncTask<Void, Void, Address> {

        String errorMessage = "";

        @Override
        protected void onPreExecute() {

            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Address doInBackground(Void... none) {
            Geocoder geocoder = new Geocoder(Location_Services_Fragment.this, Locale.getDefault());



           /* double latitude = latitude;
            double longitude = longitude;*/

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

               // country_code=geocoder.ge
            } catch (IOException ioException) {
                errorMessage = "Service Not Available";
                Log.e(TAG, errorMessage, ioException);
            } catch (IllegalArgumentException illegalArgumentException) {
                errorMessage = "Invalid Latitude or Longitude Used";
                Log.e(TAG, errorMessage + ". " +
                        "Latitude = " + latitude + ", Longitude = " +
                        longitude, illegalArgumentException);
            }


            if (addresses != null && addresses.size() > 0)
                return addresses.get(0);

            return null;
        }

        protected void onPostExecute(Address address) {
            if (address == null) {
                progress.setVisibility(View.INVISIBLE);

                // tvcurrentLOc.setText(errorMessage);
            } else {

                progress.setVisibility(View.VISIBLE);
                try{
             if(addresses != null && !addresses.isEmpty() && !addresses.equals("null")) {
                    Country = addresses.get(0).getCountryName();
                    System.out.println("-----------------------Country1111111111111111111-----------------" + Country);
                    Cntry.setText(Country);
                    // address = addresses.get(0).getAddressLine(0);
                    //  System.out.println("-----------------------adsggg-----------------" + address);
                    // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    city = addresses.get(0).getLocality();
                    System.out.println("------------------city11111111111111111----------------------" + city);
                    Cty.setText(city);
                    Area = addresses.get(0).getSubLocality();
                    state = addresses.get(0).getLocality();
                    System.out.println("---------------Area111111111111111111-------------------------" + Area);
                    /*address = addresses.get(0).getAddressLine(0);
                    if (autoCompView.getText().toString().equalsIgnoreCase("")) {
                        autoCompView.setText(address);
                    }*/
                    //  SubLocality=addresses.get(0).getSubLocality();
                    SubLocality = addresses.get(0).getAddressLine(0);
                    System.out.println("-----------------------SubLocality11111-----------------" + SubLocality);
                    System.out.println("-----------------------adsggg111111111111111-----------------" + address);

                       /* if(!Area.equalsIgnoreCase("")&&!Area.isEmpty()&&Area.equals(null)) {
                            autoCompView.setText(Area + "," + state);
                        }
                        else {
                            autoCompView.setHint("Type Your Lacality");
                        }*/
                    //*//*    String country = addresses.get(0).getCountryName();
                    // System.out.println("-----------------cntry-----------------------" + country);*//*
                    postalCode = addresses.get(0).getPostalCode();
                    System.out.println("----------------postalcode11111111111111------------------------" + postalCode);
                    knownName = addresses.get(0).getFeatureName();
                    System.out.println("-----------------knwn1111111111111-----------------------" + knownName);
                    cvc = addresses.get(0).getCountryCode();
                    // textinfo.setText("" + address + "\n" + "" + city + "\n" + "" + state + "\n" + "" + postalCode + "\n" + "" + knownName);
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("city_id",cvc);
                    editor.putString("citys",city);
                    editor.putString("cntry",Country);
                    editor.putString("cvc",cvc);
                    editor.putString("SubLocality", SubLocality);
                    // editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                    progress.setVisibility(View.GONE);

                }
                else {


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

            progress.setVisibility(View.INVISIBLE);

                //tvcurrentLOc.setText(addressName);
            }
        }
    }
}