package com.salmabutchershop.salma.meridian.app.customer.custom_er;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.FeedBack;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 3/2/2017.
 */

public class Contact_Our_Team1 extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    Button submit;
    EditText feedback;
    int selectedRadiobuttonId;
    private String USER_ID_KEY="user_id";
    private String MESSAGE_KEY="subject";
    private String SUBJECT_KEY="message";
    boolean edittexterror = false;
    private String feedbackMessage,subject;
    String  name,shop_name,location,phone,email,message,user_id;
    ProgressBar progress;
    EditText edtyourname,  edtshopname,  edtcountry , edtphone , edtemail,edtmsg,edtads1,edtads2;
    String nameS,locationS,phoneS,emailS,messgeS,userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_our_team_layout);
        edtyourname = (EditText) findViewById(R.id.edt_e);
        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtyourname.setTypeface(myFont1);

       /* edtshopname = (EditText) findViewById(R.id.edt_shopnamee);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtshopname.setTypeface(myFont2);
*/
        edtcountry = (EditText) findViewById(R.id.edt_countrylocation);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtcountry.setTypeface(myFont3);

        edtphone = (EditText) findViewById(R.id.edt_phone);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtphone .setTypeface(myFont4);

        edtemail= (EditText) findViewById(R.id.edt_email);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtemail.setTypeface(myFont5);

        edtmsg = (EditText) findViewById(R.id.feedback);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtmsg.setTypeface(myFont6);
        SharedPreferences preferencesd= getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        userid = preferencesd.getString("user_id", null);
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<Customerloginid1>>>>>>>>>>>>>>>>>>>>>>"+userid);
        if (userid != null && !userid.isEmpty()){
           // new FeedBack.PostFeedback().execute();
        }
        else {
            new SweetAlertDialog(Contact_Our_Team1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText(getResources().getString(R.string.Pleaseregister))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                            sDialog.dismiss();

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
        }


        submit=(Button)findViewById(R.id.submit);

        progress = (ProgressBar) findViewById(R.id.progress_bar);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                // i.putExtra("cat_id",cat_id);
                //i.putExtra("value",status);

                startActivity(i);
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    nameS=edtyourname.getText().toString();
                    locationS=edtcountry.getText().toString();
                    phoneS=edtphone.getText().toString();
                    emailS=edtemail.getText().toString();
                    messgeS=edtmsg.getText().toString();
                    if (nameS.isEmpty()  || edtcountry.getText().toString().trim().isEmpty() || edtphone.getText().toString().isEmpty()  || edtemail.getText().toString().isEmpty()) {
                        com.nispok.snackbar.Snackbar.with(Contact_Our_Team1.this) // context
                                .text(getResources().getString(R.string.empty_fields)) // text to display
                                .show(Contact_Our_Team1.this);
                       /* com.chootdev.csnackbar.Snackbar.with(Contact_Our_Team.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Empty Fields!")
                                .duration(Duration.SHORT)
                                .show();*/
                        edittexterror = true;
                        //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (edtemail.getText().toString().isEmpty()) {
                        edtemail.setError(getResources().getString(R.string.Enter_Email_Id));
                        edittexterror = true;
                    } else if (edtyourname.getText().toString().isEmpty()) {
                        edtyourname.setError(getResources().getString(R.string.Enter_Full_name));
                        edittexterror = true;
                    } else if (edtphone.getText().toString().isEmpty()) {
                        edtphone.setError(getResources().getString(R.string.Enter_Phone));
                        edittexterror = true;
                    } /*else if (edtshopname.getText().toString().isEmpty()) {
                        edtshopname.setError("Enter Shop Name");
                        edittexterror = true;
                    } */ else if (edtcountry.getText().toString().isEmpty()) {
                        edtcountry.setError(getResources().getString(R.string.Enter_country));
                        edittexterror = true;
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (!android.util.Patterns.PHONE.matcher(edtphone.getText().toString().trim()).matches()) {
                        edtphone.setError(getResources().getString(R.string.Invalid_Phone));
                        edittexterror = true;
                    }
/*                    else if (edtshopname.getText().toString().isEmpty()) {
                        edtshopname.setError("Enter Shopname");
                        edittexterror = true;
                    }*/
                    if (edittexterror == false) {
                        if (userid != null && !userid.isEmpty()){
                            // new FeedBack.PostFeedback().execute();
                            new PostFeedback().execute();
                        }
                        else {
                            new SweetAlertDialog(Contact_Our_Team1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(getResources().getString(R.string.Pleaseregister))
                                    .setCustomImage(R.drawable.logo)
                                    .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                                    .setConfirmText(getResources().getString(R.string.Yes))
                                    .setCancelText(getResources().getString(R.string.No))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(i);
                                            finish();
                                            sDialog.dismiss();

                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                        }

                    }

                } else {
                    com.nispok.snackbar.Snackbar.with(Contact_Our_Team1.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Contact_Our_Team1.this);
                  /*  com.chootdev.csnackbar.Snackbar.with(Contact_Our_Team.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/


                }





            }
        });

    }

    private void goBack() {
        super.onBackPressed();
    }


    class PostFeedback extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            //?name=subin&location=calicut&phone=999999999&email=subin@test.com&message=test%20msg..
            try{
                URL url = new URL("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/common/contact_request.php"); // here is your URL path

              /*  name
                        location
                phone
                        email
                message*/
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<Customerloginid1>>>>>>>>>>>>>>>>>>>>>>nnnnnnnnnnnnnn"+userid);
                JSONObject postDataParams = new JSONObject();

                postDataParams.put("name",nameS);

                postDataParams.put("location",locationS);
                postDataParams.put("phone",phoneS);
                postDataParams.put("email",emailS);
                postDataParams.put("message",messgeS);
                postDataParams.put("user_type","1");
                postDataParams.put("userid",userid);
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e)

            {
                return new String("Exception: " + e.getMessage());
            }


        }

        @Override
        protected void onPostExecute(String result) {


          //  Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            super.onPostExecute(result);
            progress.setVisibility(View.GONE);


            if (result.equals("\"failed\"")) {
                new SweetAlertDialog(Contact_Our_Team1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("")
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();



            } else {
                new SweetAlertDialog(Contact_Our_Team1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.ThankYou))
                        .setContentText(getResources().getString(R.string.Submitted))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                                startActivity(i);
                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();







            }


        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }

        return result.toString();
    }
        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event)  {
            if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                    && keyCode == KeyEvent.KEYCODE_BACK
                    && event.getRepeatCount() == 0) {
                Log.d("CDA", "onKeyDown Called");
                onBackPressed1();
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}
