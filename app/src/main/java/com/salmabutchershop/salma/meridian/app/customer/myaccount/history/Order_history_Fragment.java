package com.salmabutchershop.salma.meridian.app.customer.myaccount.history;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

/**
 * Created by libin on 10/17/2016.
 */

public class Order_history_Fragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;

    Fragment fragment;
    View view;
    ProgressBar progress;
    RecyclerView recyclerview;
    String order_id,pro_name,pro_qty,pro_price,order_date,order_status,
            city,deliveryarea,addresstype,street,building,floor,house_no;
    Order_history_adapter adapter;
    static ArrayList<Order_history_Model> eem;
    Order_history_Model ee;
String id,event_id,exhibitors;
    String tag = "events";
String result,Dat;
    String user_id,subtotal,order_time,email,instruction;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_history_layout);
        progress = (ProgressBar)findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Dat= myPrefs.getString("user_id", null);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahhhhhhhhhhhhhhh>>>>>>>>>" +Dat);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Order_history_Fragment.this.goBack();
            }
        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Order_history_Fragment.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Order_history_Fragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


            //	Toast.makeText(getActivity(),
            //	getResources().getString(R.string.Sorry) +
            //	getResources().getString(R.string.cic),
            //	Toast.LENGTH_SHORT).show();
        }



    }

    private void goBack() {
        super.onBackPressed();
    }

    private class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER_CUSTOMER+"history.php?user_id="+Dat
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            // String sam = result.trim();
            progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                //  mCountryModel1 = new ArrayList<>();
                eem = new ArrayList<Order_history_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                        ee = new Order_history_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //Log.d("OutPut", mJsonObject.getString("doctor_publish"));
//String user_id,subtotal,order_time,email,instruction;

                        order_id = mJsonObject.getString("order_id");
                        user_id = mJsonObject.getString("user_id");
                        subtotal = mJsonObject.getString("subtotal");
                        order_time = mJsonObject.getString("order_time");
                        order_date = mJsonObject.getString("order_date");
                        //  email=mJsonObject.getString("email");
                        // instruction=mJsonObject.getString("email");
                        order_status = mJsonObject.getString("order_status");
                        //   city= mJsonObject.getString("city");
                        //    deliveryarea= mJsonObject.getString("deliveryarea");
                        //   addresstype= mJsonObject.getString("addresstype");
                        //    street= mJsonObject.getString("street");
                        //     building= mJsonObject.getString("building");
                        //     floor= mJsonObject.getString("floor");
                        //    house_no=mJsonObject.getString("house_no");
                    /*    Newsdb_model city = new Newsdb_model();
                        city.setName(news_id);
                        city.setState(news_title);
                        city.setDescription(news_content);
                        city.setImg(news_img);
                        handler.addCity(city);*/

                        ee.setOrder_id(order_id);
                        // ee.setPro_name(pro_name);
                        //  ee.setPro_qty(pro_qty);
                        ee.setSubtotal(subtotal);
                        ee.setOrder_date(order_date);
                        ee.setOrder_status(order_status);
                        ee.setOrder_time(order_time);
                        ee.setCity(order_time);
                        // ee.setDeliveryarea(deliveryarea);
                        //  ee.setAddresstype(addresstype);
                        //  ee.setStreet(street);
                        //  ee.setBuilding(building);
                        //  ee.setFloor(floor);
//
                      /*  System.out.println("<<news_id>>>>" + news_id);

                        System.out.println("<< news_title>>>>" +news_title);
                        System.out.println("<< news_content>>>" +news_content);*/

                        eem.add(ee);

                        System.out.println("" + ee);


                        adapter = new Order_history_adapter(eem, getApplicationContext());
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                new SweetAlertDialog(Order_history_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoContent))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .show();
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }


            recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i=new Intent(getApplicationContext(),My_Coustomer_history_details.class);
                                i.putExtra("order_id",eem.get(position).getOrder_id());
                                startActivity(i);
                               /* android.support.v4.app.Fragment fragment = new Zayed_News_Event_Desc_Fragment();

                                FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                                        .beginTransaction();
                                transaction.replace(R.id.frame_container, fragment, tag);
                                // titleStack.add(tag);
                                Bundle args = new Bundle();
                                args.putInt("pos", position);
                                // args.putString("desc", up_evnt_desc);
                                //  args.putString("desimg", up_evnt_img);
                                //  System.out.println(">>>>>depppppppppppppppppppppppp<<<" + dept_id);
                                System.out.println(">>>>><<<" + position);
                                fragment.setArguments(args);
                                transaction.addToBackStack(tag).commit();*/
                            }
                        })
                );
            }
        }
    }

