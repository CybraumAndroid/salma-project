package com.salmabutchershop.salma.meridian.app.customer.database;

/**
 * Created by libin on 11/18/2016.
 */
public class CartModel {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice_quantity() {
        return price_quantity;
    }

    public void setPrice_quantity(String price_quantity) {
        this.price_quantity = price_quantity;
    }

    public Float getQty() {
        return qty;
    }

    public String getBut_id() {
        return but_id;
    }

    public void setBut_id(String but_id) {
        this.but_id = but_id;
    }

    public void setQty(Float qty) {
        this.qty = qty;
    }

    public Float getPrice() {
        return price;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getTot() {
        return tot;
    }

    public void setTot(Float tot) {
        this.tot = tot;
    }

    public Float getGrnd() {
        return grnd;
    }

    public String getOut_tt() {
        return out_tt;
    }

    public void setOut_tt(String out_tt) {
        this.out_tt = out_tt;
    }

    public void setGrnd(Float grnd) {
        this.grnd = grnd;
    }

    public String getNam() {
        return nam;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }



    String nam;
    int id;
           Float qty,price,tot,grnd;
    String but_id,price_quantity,type_id,out_tt;
}
