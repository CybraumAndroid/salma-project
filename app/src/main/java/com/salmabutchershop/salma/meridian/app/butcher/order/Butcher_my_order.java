package com.salmabutchershop.salma.meridian.app.butcher.order;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.MapFragment;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.butcher.orderdetail.Butcher_MyOrder_New_Detail;
import com.salmabutchershop.salma.meridian.app.butcher.orderdetail.Butcher_My_orderDetail;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.sidebar.FeedBack;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 11/24/2016.
 */

public class Butcher_my_order extends Activity  {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;
    //   static ArrayList<UpcomingModel> upm;
    Butcher_My_order_List_adapter adapter1;
    ProgressBar progress;
    String result, galry_image, Dat, Dat1;
    static ArrayList<Butcher_MyOrder_Model> bmo;
    Butcher_MyOrder_Model bm;

    String k;
    String id,pro_name,pro_type_name,city,order_status,order_date,order_item_id;
    String user_id,name,mobile,email,aditional_direction,subtotal,instruction,order_time;
    String userid,fullname;
    Button C_Order,Order_H;
    TextView T_Order,T_History;
    static  String AS;
    static  TextView tv;
    public static MKLoader progress_loader;
    private SwipeRefreshLayout swipeRefreshLayout;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.butcher_my_order_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
//tv= (TextView) findViewById(R.id.textView16);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        Intent intent = getIntent();
        k = intent.getStringExtra("cat_id");
        String message = intent.getStringExtra("message");

        try{
            if(!message.isEmpty()){
                System.out.println("<<<<<<<<<sharedname>>>>" + message);
                swipeRefreshLayout.setRefreshing(true);

            }}
        catch (Exception e){
            swipeRefreshLayout.setRefreshing(true);
            e.printStackTrace();

        }
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + k);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullnameb", null);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaHHHHHHHHHHHHHHHHHHaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +user_id);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Butcher_my_order.this,MainFragment.class);
                startActivity(i);
                finish();
            }
        });
AS="2";

        C_Order= (Button) findViewById(R.id.button8);
        Order_H= (Button) findViewById(R.id.button9);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                C_Order.setTextColor(Color.parseColor("#d62041"));
                Order_H.setTextColor(Color.parseColor("#A9A9A9"));
                C_Order.setBackgroundResource(R.drawable.divider_tab);
                Order_H.setBackgroundResource(R.color.md_divider_white);
                AS="2";
                new DownloadData1().execute();
            }
        });
        C_Order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                C_Order.setTextColor(Color.parseColor("#d62041"));
                Order_H.setTextColor(Color.parseColor("#A9A9A9"));
              C_Order.setBackgroundResource(R.drawable.divider_tab);
                Order_H.setBackgroundResource(R.color.md_divider_white);
                AS="2";
                new DownloadData1().execute();
            }
        });

        Order_H.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                C_Order.setTextColor(Color.parseColor("#A9A9A9"));
                Order_H.setTextColor(Color.parseColor("#d62041"));
                Order_H.setBackgroundResource(R.drawable.divider_tab);
                C_Order.setBackgroundResource(R.color.md_divider_white);
                AS="1";
                new DownloadData1().execute();

            }
        });
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Butcher_my_order.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Butcher_my_order.this);
          /*  com.chootdev.csnackbar.Snackbar.with(Butcher_my_order.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }


    }

    private void refreshItems() {
    }

    /* public void refresh(View view){          //refresh is onClick name given to the button
         onRestart();
     }*/
    private void onBackPressed1() {
        Intent thisIntent = getIntent();
        startActivity(thisIntent);
        finish();
    }



   /* public void yourDesiredMethod() {
        finish();
    }*/


    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();
System.out.println("gggggggggggg"+userid);
                System.out.println("<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>"+"http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/orders.php?butcher_id="+userid+"&accept_status="+AS);

                HttpPost httppost = new HttpPost(SERVER+"json/butcher/orders.php?butcher_id="+userid+"&accept_status="+AS//+k//+Dat
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
            swipeRefreshLayout.setRefreshing(false);
          //  String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
           // System.out.println(">>>>>>>>>>>>>>>" + sam);
          //  String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No Order", Toast.LENGTH_SHORT).show();
                recyclerview.setVisibility(View.GONE);
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                recyclerview.setVisibility(View.VISIBLE);
                JSONArray mArray;
               bmo = new ArrayList<Butcher_MyOrder_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        bm = new Butcher_MyOrder_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));

                       id = mJsonObject.getString("order_id");
                        user_id=mJsonObject.getString("user_id");
                        name=mJsonObject.getString("name");
                        city= mJsonObject.getString("city");
                        mobile= mJsonObject.getString("mobile");
                        email= mJsonObject.getString("email");
                        aditional_direction= mJsonObject.getString("aditional_direction");
                        subtotal= mJsonObject.getString("subtotal");
                        instruction= mJsonObject.getString("instruction");
                        order_date= mJsonObject.getString("order_date");
                        order_time= mJsonObject.getString("order_time");
                     //   pro_type_name= mJsonObject.getString("pro_type_name");
                     //   order_item_id= mJsonObject.getString("order_item_id");
                        order_status= mJsonObject.getString("order_status");




                        bm.setId(id);
                        bm.setUser_id(user_id);
                        bm.setName(name);
                        bm.setCity(city);
                        bm.setMobile(mobile);
                        bm.setEmail(email);
                        bm.setAditional_direction(aditional_direction);
                        bm.setSubtotal(subtotal);
                        bm.setInstruction(instruction);
                        bm.setOrder_date(order_date);
                        bm.setOrder_time(order_time);
                        bm.setOrder_status(order_status);
                        bmo.add(bm);
                        System.out.println("<<<oo>>>>" + bm);



                        adapter1 = new Butcher_My_order_List_adapter(bmo, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if(bmo.get(position).getOrder_status().equalsIgnoreCase("New")){
                                    String added_id=bmo.get(position).getUser_id();
                                    String s=bmo.get(position).getId();
                                    String o=bmo.get(position).getOrder_item_id();
                                    Intent i=new Intent(getApplicationContext(),Butcher_MyOrder_New_Detail.class);
                                    i.putExtra("add_id",added_id);
                                    i.putExtra("pr_id", s);
                                    i.putExtra("order_id",o);
                                  //  i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.putExtra("stat","1");
                                  startActivity(i);
                                    finish();
                                    // ((Butcher_my_order)context).finish();
                                    //((Butcher_my_order)context).yourDesiredMethod();
                                    //((Butcher_My_order_List_adapter).context).finish();
                                }else {
                                    String added_id=bmo.get(position).getUser_id();
                                    String s=bmo.get(position).getId();
                                    String o=bmo.get(position).getOrder_item_id();
                                    Intent i=new Intent(getApplicationContext(),Butcher_MyOrder_New_Detail.class);
                                    i.putExtra("add_id",added_id);
                                    i.putExtra("pr_id", s);
                                    i.putExtra("order_id",o);
                                  //  i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.putExtra("stat","2");
                                    startActivity(i);
                                    finish();
                                    // ((Butcher_my_order)context).finish();
           /* com.nispok.snackbar.Snackbar.with(view) // context
                    .text("Already Accepted") // text to display
                    .show(view);*/
                                    //  Toast.makeText(context,"Already Accepted",Toast.LENGTH_SHORT).show();

                                }
                            }
                        })
                );
            }
            else{
recyclerview.setVisibility(View.GONE);
                new SweetAlertDialog(Butcher_my_order.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoNewOrder))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })
                        .show();



                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

        }

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) < 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
        }

        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {

        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        finish();
      /*  Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);

        return;*/
    }

}