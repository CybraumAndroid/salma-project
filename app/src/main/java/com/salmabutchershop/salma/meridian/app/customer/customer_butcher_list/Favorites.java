package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.notify.PushNotifiction;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Favorites extends AppCompatActivity {
Button goBack;
    RecyclerView rv2;
    ProgressDialog prgDlg;
    static  ArrayList<imageModel> imageNames;
    DataAdapter_2 adapter2;
    DatabaseHandler dh=new DatabaseHandler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Favorites.this.goBack();
            }
        });
        rv2=(RecyclerView)findViewById(R.id.recylerView2);
        rv2.setHasFixedSize(true);

        RecyclerView.LayoutManager lm=new GridLayoutManager(getApplicationContext(),1);
        rv2.setLayoutManager(lm);
        new FetchDetails().execute();
        imageNames=new ArrayList<imageModel>();




    }


    class FetchDetails extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prgDlg=new ProgressDialog(Favorites.this);
            prgDlg.setMessage(getResources().getString(R.string.LOADING));
            prgDlg.setCancelable(false);
            prgDlg.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                ///////////////////////////////////////////////// selecting all rows  ---start////////////////////////////////////
                Cursor rs = dh.selectAllFAV(getApplicationContext());
                System.out.println("||||||||     Cursor size  : "+rs.getCount());

                if (rs.getCount()!=0)//checking if already added to FAV
                {
                    rs.moveToFirst();
                    do{
                        imageModel im = new imageModel();
                        im.id = rs.getInt(0);//this id is not event id,it's primary key in the table
                        im.image = rs.getString(1);
                        im.shop_name=rs.getString(2);
                        im.delivery_info=rs.getString(3);
                        im.butcher_discount=rs.getString(4);
                        im.added_by=rs.getString(5);
                        im.cat_id=rs.getString(6);
                        imageNames.add(im);
                        System.out.println("|||||||||||    image url : " + rs.getString(1));
                        System.out.println("|||||||||||    shop name : " + rs.getString(2));
                        System.out.println("|||||||||||    discount : " + rs.getString(3));
                        System.out.println("|||||||||||    dlvry : " + rs.getString(4));
                        adapter2 = new DataAdapter_2(getApplicationContext(), imageNames);
                    }while (rs.moveToNext());
                }
                else {
                    runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            new SweetAlertDialog(Favorites.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setCustomImage(R.drawable.logo)
                                    .setTitleText(getResources().getString(R.string.NoContent))
                       /* .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                        .setConfirmText(getResources().getString(R.string.Yes))*/
                                    .setConfirmText(getResources().getString(R.string.ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                                            startActivity(i);
                                            finish();
                                            sDialog.dismiss();

                                        }
                                    })
                       /* .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })*/
                                    .show();
                        }
                    });

                   /* com.chootdev.csnackbar.Snackbar.with(Favorites.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("No More Favourites!")
                            .duration(Duration.SHORT)
                            .show();*/
                   // finish();
                }




            }catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prgDlg.isShowing())
                prgDlg.dismiss();

            rv2.setAdapter(adapter2);
          /*  rv2.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                         public void onItemClick(View view, int position) {
                      *//* String added_id=imageNames.get(position).getAdded_by();
                            String cat_id=imageNames.get(position).getCat_id();
                           //String cat_id=imageNames.get(position).g;

                            Intent i=new Intent(getApplicationContext(),Customer_Butcher_Sublist_Fragment.class);
                            i.putExtra("add_id",added_id);
                           i.putExtra("cat_id",cat_id);
                           // i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            //i.putExtra("value",status);
                            startActivity(i);*//*

                        }
                    })
            );*/


        }
    }
    private void goBack() {
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}
