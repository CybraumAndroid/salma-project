package com.salmabutchershop.salma.meridian.app.customer.myaccount;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Vis_FilePath;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.myaccount.history.Order_history_Fragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.ContentValues.TAG;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

/**
 * Created by libin on 12/2/2016.
 */

public class My_Account_Fragment extends Activity {
    ImageView Edt_Img, Pfile;
    TextView Name, Ads,Phone,Location,Email;
    LinearLayout History, Change_pwd, Adss;
    String Reg_id, c_names, c_email, c_phone, c_location;
    static ArrayList<MyAccount_Profile_Model> eem;
    MyAccount_Profile_Model ee;
    public static MKLoader progress_loader, progress_loader1;
    String result, Dat,logo;
    ImageView profile_img_upload, profile_img;
    private static final int PICK_FILE_REQUEST = 1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    String selectedFilePath,cvc;
    private static final int REQUEST_CODE = 1;
    static String filename = "null", filenamepath = "null";
    String social_status="";
    LinearLayout editprofile;
    URL social_profile_url;
    SharedPreferences myPrefs;
    CountryCodePicker ccp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myaccount_layout);
        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
        progress_loader1 = (MKLoader) findViewById(R.id.progress_loader1);
         myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);


        c_names = myPrefs.getString("c_name", null);

        c_email = myPrefs.getString("c_email", null);
        c_phone = myPrefs.getString("c_phone", null);
        c_location = myPrefs.getString("c_location", null);

        editprofile=(LinearLayout)findViewById(R.id.editprofile);

        SharedPreferences preferences = getSharedPreferences("SocialPref", MODE_PRIVATE);
        social_status=preferences.getString("sociallogin",null);
        System.out.println("social_status : "+social_status);


        System.out.println("bbbbbbbbbbbbbbbbbbbb" + c_names);
        Edt_Img = (ImageView) findViewById(R.id.edt_img);
        Pfile = (ImageView) findViewById(R.id.pfile);
        Name = (TextView) findViewById(R.id.name);
        Phone=(TextView) findViewById(R.id.phone);
        Location=(TextView) findViewById(R.id.location);
        Email=(TextView) findViewById(R.id.email);


        if (social_status.equalsIgnoreCase("true")) {
           /* try {
                social_profile_url = new URL(myPrefs.getString("social_profile_image", null));
                System.out.println("social_profile_url : "+social_profile_url);
                Pfile.setBackground(null);
                new Picasso.Builder(My_Account_Fragment.this)
                        .downloader(new OkHttpDownloader(My_Account_Fragment.this, Integer.MAX_VALUE))
                        .build()
                        .load(String.valueOf(social_profile_url))
                        .noFade()
                        .into(Pfile, new Callback() {
                            @Override
                            public void onSuccess() {
                                progress_loader1.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                // personViewHolder.progress_loader.setVisibility(View.GONE);
                                progress_loader1.setVisibility(View.GONE);
                            }
                        });
            }catch (Exception e){
                e.printStackTrace();
            }*/

            Phone.setVisibility(View.GONE);
            Location.setVisibility(View.GONE);
        }else{
            Phone.setVisibility(View.VISIBLE);
            Location.setVisibility(View.VISIBLE);
        }


        if (Reg_id != null && !Reg_id.isEmpty()) {
            Name.setText(c_names);
            Phone.setText(c_phone);
            Location.setText(c_location);
            Email.setText(c_email);

        }
        Ads = (TextView) findViewById(R.id.ads);
        History = (LinearLayout) findViewById(R.id.history);
        Change_pwd = (LinearLayout) findViewById(R.id.chngepwd);
        Adss = (LinearLayout) findViewById(R.id.adss);

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                My_Account_Fragment.this.goBack();
            }
        });
       if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(My_Account_Fragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(My_Account_Fragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


            //	Toast.makeText(getActivity(),
            //	getResources().getString(R.string.Sorry) +
            //	getResources().getString(R.string.cic),
            //	Toast.LENGTH_SHORT).show();
        }
       /* Pfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED) {

                    requestPermissions();
                }
                showFileChooser();
            }
        });*/
        profile_img_upload = (ImageView)findViewById(R.id.profile_upload_icon);
       /* profile_img_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED) {

                    requestPermissions();
                }
                showFileChooser();
            }
        });*/
        History.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Order_history_Fragment.class);
                startActivity(i);
            }
        });
        Change_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (social_status.equalsIgnoreCase("true")) {
                    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                            .setContentText("You can't edit your profile via socialnetwork login")
                            .setConfirmText(getResources().getString(R.string.ok))

                            .setCustomImage(R.drawable.logo)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {


                                    // new PostRegistrationRetailer.SendPostRequest().execute();
                                    sDialog.dismiss();
                                }
                            })
                            .show();

                } else {
                    Intent i = new Intent(getApplicationContext(), Changepassword.class);
                    startActivity(i);
                }
            }
        });
        Adss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AddressMyaccount.class);
                startActivity(i);
            }
        });
        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (social_status.equalsIgnoreCase("true")) {
                    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                            .setContentText("You can't edit your profile via socialnetwork login")
                            .setConfirmText(getResources().getString(R.string.ok))

                            .setCustomImage(R.drawable.logo)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {


                                    // new PostRegistrationRetailer.SendPostRequest().execute();
                                    sDialog.dismiss();
                                }
                            })
                            .show();

                } else {
                    Intent i = new Intent(getApplicationContext(), EditProfile.class);
                    startActivity(i);
                }
            }
        });
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(My_Account_Fragment.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE);
        // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }
    private void showFileChooser() {
        try {

            if (social_status.equalsIgnoreCase("true")) {
                new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                        .setContentText("You can't edit your profile via socialnetwork login")
                        .setConfirmText(getResources().getString(R.string.ok))

                        .setCustomImage(R.drawable.logo)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {


                                // new PostRegistrationRetailer.SendPostRequest().execute();
                                sDialog.dismiss();
                            }
                        })
                        .show();

            } else {

                Intent intent = new Intent();
                //sets the select file to all types of files
                intent.setType("image/*");
                //allows to select data and return it
                intent.setAction(Intent.ACTION_GET_CONTENT);
                //starts new activity to select file and return data
                startActivityForResult(Intent.createChooser(intent, "Choose File to Vis_Upload.."), PICK_FILE_REQUEST);
            }
        } catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                if(data == null){
                    //no data present
                    return;
                }


                Uri selectedFileUri = data.getData();
                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(),selectedFileUri);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                Log.i(TAG,"Selected File Path:" + selectedFilePath);

                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    // textViewResponse.setText(selectedFilePath);

                    CropImage.activity(selectedFileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                    // logo_img.setImageBitmap(bmp);
                }else{
                    com.nispok.snackbar.Snackbar.with(My_Account_Fragment.this) // context
                            .text("Cannot upload file to server!") // text to display
                            .show(My_Account_Fragment.this);
                   /* Snackbar.with(Butcher_Product_Submit.this,null)
                            .type(Type.SUCCESS)
                            .message("Cannot upload file to server!")
                            .duration(Duration.LONG)
                            .show();*/
                    //  Toast.makeText(getApplicationContext(),"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                }

            }
           /* else if(requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
                if(resultCode==RESULT_OK){
                    previewCapturedImage();
                }*/

            //  }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                System.out.println("selectedFileUri : "+resultUri);

          selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), resultUri);
                filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                Pfile.setImageBitmap(bmp);
                if (selectedFilePath != null && !selectedFilePath.isEmpty())

                {
                    if (filenamepath != null) {
                        filename = filenamepath;


                    } else {
                        filename = "null";

                    }
                    uploadVideo1();


                    new SendPostRequest1().execute();
                    // tt="";
                } else {
                    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(getResources().getString(R.string.MyMenu))
                            .setContentText(getResources().getString(R.string.Areyousurewanttosetimageasdefault))
                            .setConfirmText(getResources().getString(R.string.Yes))
                            .setCancelText(getResources().getString(R.string.No))
                            .setCustomImage(R.drawable.logo)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {


                                   // new PostRegistrationRetailer.SendPostRequest().execute();
                                    sDialog.dismiss();
                                }
                            })
                            .show();

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void uploadVideo1() {
        class UploadVideo extends AsyncTask<Void, Void, String> {

            ProgressDialog uploading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
               // progress_loader.setVisibility(View.VISIBLE);
                //   uploading = ProgressDialog.show(PostRegistrationRetailer.this, "Salma", "Please wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //    progress_loader.setVisibility(View.GONE);
                //   uploading.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {
                Vis_Upload4 u = new Vis_Upload4();
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<md>>>>>>>>>>>>>>>>>>>>>>>>>" + selectedFilePath);
                String msg = u.uploadVideo(selectedFilePath);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<mdeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>>>>>>>>>>" + msg);

                return msg;
            }
        }
        UploadVideo uv = new UploadVideo();
        uv.execute();
    }
    @Override
    protected void onStart() {
        super.onStart();

        checkAndRequestPermissions();


    }
    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    private void goBack() {
        Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }

    private class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        //    progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER_CUSTOMER+"profile_picture.php?user_id="+Reg_id
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            // String sam = result.trim();
          //  progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
            }  else*/
            if (result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                //  mCountryModel1 = new ArrayList<>();
                eem = new ArrayList<MyAccount_Profile_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                        ee = new MyAccount_Profile_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //Log.d("OutPut", mJsonObject.getString("doctor_publish"));
//String user_id,subtotal,order_time,email,instruction;
                        logo = mJsonObject.getString("logo");
                        ee.setLogo(logo);
                        eem.add(ee);

                        System.out.println("" + ee);

                      /*
                        user_id = mJsonObject.getString("user_id");
                        subtotal = mJsonObject.getString("subtotal");
                        order_time = mJsonObject.getString("order_time");
                        order_date = mJsonObject.getString("order_date");
                        //  email=mJsonObject.getString("email");
                        // instruction=mJsonObject.getString("email");
                        order_status = mJsonObject.getString("order_status");
                     city= mJsonObject.getString("city");
                        //    deliveryarea= mJsonObject.getString("deliveryarea");
                        //   addresstype= mJsonObject.getString("addresstype");
                        //    street= mJsonObject.getString("street");
                        //     building= mJsonObject.getString("building");
                        //     floor= mJsonObject.getString("floor");
                        //    house_no=mJsonObject.getString("house_no");
                    *//*    Newsdb_model city = new Newsdb_model();
                        city.setName(news_id);
                        city.setState(news_title);
                        city.setDescription(news_content);
                        city.setImg(news_img);
                        handler.addCity(city);*//*

                        ee.setOrder_id(order_id);
                        // ee.setPro_name(pro_name);
                        //  ee.setPro_qty(pro_qty);
                        ee.setSubtotal(subtotal);
                        ee.setOrder_date(order_date);
                        ee.setOrder_status(order_status);
                        ee.setOrder_time(order_time);
                        ee.setCity(order_time);*/
                        // ee.setDeliveryarea(deliveryarea);
                        //  ee.setAddresstype(addresstype);
                        //  ee.setStreet(street);
                        //  ee.setBuilding(building);
                        //  ee.setFloor(floor);
//
                      /*  System.out.println("<<news_id>>>>" + news_id);

                        System.out.println("<< news_title>>>>" +news_title);
                        System.out.println("<< news_content>>>" +news_content);*/

                        eem.add(ee);

                        System.out.println("" + ee);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (social_status.equalsIgnoreCase("true")) {
                    try {
                        social_profile_url = new URL(myPrefs.getString("social_profile_image", null));
                        System.out.println("social_profile_url : " + social_profile_url);
                        Pfile.setBackground(null);
                        new Picasso.Builder(My_Account_Fragment.this)
                                .downloader(new OkHttpDownloader(My_Account_Fragment.this, Integer.MAX_VALUE))
                                .build()
                                .load(String.valueOf(social_profile_url))
                                .noFade()
                                .into(Pfile, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        progress_loader1.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        // personViewHolder.progress_loader.setVisibility(View.GONE);
                                        progress_loader1.setVisibility(View.GONE);
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else{
                try {
                    Pfile.setBackground(null);
                    new Picasso.Builder(My_Account_Fragment.this)
                            .downloader(new OkHttpDownloader(My_Account_Fragment.this, Integer.MAX_VALUE))
                            .build()
                            .load(logo)
                            .noFade()
                            .into(Pfile, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progress_loader1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
                                    progress_loader1.setVisibility(View.GONE);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            } else {
            /*    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoContent))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .show();*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
        }
    }

    private class SendPostRequest1 extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
            //progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        public String doInBackground(String... arg0) {
           /* if (filenamepath != null) {
                filename = filenamepath;


            } else {
                filename = "null";

            }*/
           // filename=filenamepath;
            String js=filename.replaceAll(" ","");

            try {

                URL url = new URL(SERVER_CUSTOMER+"profile_picture.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("user_id",Reg_id);
           /*     postDataParams.put("profilepic",fullname);
                postDataParams.put("phone",phone);
                postDataParams.put("email",email);
                postDataParams.put("address1",address1_s);
                postDataParams.put("address2",address2_s);
                postDataParams.put("shopname",shopname_s);
                postDataParams.put("country",username);
                postDataParams.put("city",password_s);
                postDataParams.put("token",refreshedToken);
                postDataParams.put("retailer_type",k);
                postDataParams.put("longitude",ltg);
                postDataParams.put("latitude",lts);*/
                postDataParams.put("profilepic",js);
                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {

           /*  Toast.makeText(getApplicationContext(), result,
              Toast.LENGTH_LONG).show();*/
            if (result.contentEquals("\"success\"")){

                new SweetAlertDialog(My_Account_Fragment.this,SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Profile Picture")
                        .setContentText("Uploaded Sucecssfully")
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                progress_loader.setVisibility(ProgressBar.GONE);
                                if (DetectConnection
                                        .checkInternetConnection(getApplicationContext())) {
                                    //Toast.makeText(getActivity(),
                                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                                    new DownloadData().execute();
                                } else {
                                    com.nispok.snackbar.Snackbar.with(My_Account_Fragment.this) // context
                                            .text("Oops Your Connection Seems Off..!") // text to display
                                            .show(My_Account_Fragment.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


                                    //	Toast.makeText(getActivity(),
                                    //	getResources().getString(R.string.Sorry) +
                                    //	getResources().getString(R.string.cic),
                                    //	Toast.LENGTH_SHORT).show();
                                }
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();



            }


            else if(result.contentEquals("\"already registerd\"")){
                new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.AlreadyRegistred))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");


            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        c_names = myPrefs.getString("c_name", null);
        c_email = myPrefs.getString("c_email", null);
        c_phone = myPrefs.getString("c_phone", null);
        c_location = myPrefs.getString("c_location", null);

        Name.setText(c_names);
        Phone.setText(c_phone);
        Location.setText(c_location);
        Email.setText(c_email);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            new DownloadData().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(My_Account_Fragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(My_Account_Fragment.this);
        }
    }

}