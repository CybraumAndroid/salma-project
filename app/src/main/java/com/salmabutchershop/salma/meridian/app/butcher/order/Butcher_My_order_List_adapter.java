package com.salmabutchershop.salma.meridian.app.butcher.order;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.orderdetail.Butcher_MyOrder_New_Detail;
import com.salmabutchershop.salma.meridian.app.butcher.orderdetail.Butcher_My_orderDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 11/24/2016.
 */
public class Butcher_My_order_List_adapter extends RecyclerView.Adapter<Butcher_My_order_List_adapter.ViewHolder> {


        List<Butcher_MyOrder_Model> bmo;
        Context context;



public Butcher_My_order_List_adapter(ArrayList<Butcher_MyOrder_Model> bmo, Context context) {
        this.bmo = bmo;
        this.context = context;

        }

public static class ViewHolder extends RecyclerView.ViewHolder {
    LinearLayout cv1;

    ImageView v;
    TextView tv,Name,Place,Estmtd_dlvry,Date,Name2;
    Button Nw;

    ImageView personPhoto;

    ViewHolder(View itemView) {
        super(itemView);

       // Name=   (TextView) itemView.findViewById(R.id.place);
       Name2=   (TextView) itemView.findViewById(R.id.order_id);
       Place=   (TextView) itemView.findViewById(R.id.place);
        Estmtd_dlvry=   (TextView) itemView.findViewById(R.id.time);
        Date=   (TextView) itemView.findViewById(R.id.date);
        Nw= (Button) itemView.findViewById(R.id.imageView16);




    }
}


    @Override
    public int getItemCount() {
        return bmo.size();
    }


    @Override
    public Butcher_My_order_List_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.butcher_my_order_item_layout, viewGroup, false);
        Butcher_My_order_List_adapter.ViewHolder pvh = new Butcher_My_order_List_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Butcher_My_order_List_adapter.ViewHolder personViewHolder, final int i) {

       /* Typeface myFont7 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.Name2.setTypeface(myFont7);
        personViewHolder.Name2.setText(bmo.get(i).getCity());
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.Name.setTypeface(myFont2);
        personViewHolder.Name.setText(bmo.get(i).getName());
        */
        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Name2.setTypeface(myFont3);
        personViewHolder.Name2.setText(context.getResources().getString(R.string.OrderNo)+":"+bmo.get(i).getId());

      /*  String[] parts = bmo.get(i).getOrder_date().split(" ");
        String part1 = parts[0]; // 004
        String part2 = parts[1]; // 03*/
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Place.setTypeface(myFont2);
        personViewHolder.Place.setText(bmo.get(i).getCity());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Estmtd_dlvry.setTypeface(myFont4);
        personViewHolder.Estmtd_dlvry.setText(bmo.get(i).getOrder_time());
        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Date.setTypeface(myFont5);
        personViewHolder.Date.setText(bmo.get(i).getOrder_date());
if(bmo.get(i).getOrder_status().equalsIgnoreCase("New")){
    personViewHolder.Nw.setBackgroundResource(R.drawable.neworder);
    personViewHolder.Nw.setText(context.getResources().getString(R.string.NewOrder));

}
        else {
    personViewHolder.Nw.setBackgroundResource(R.drawable.accepted);
    personViewHolder.Nw.setText(context.getResources().getString(R.string.Accepted));
        }
        //  String img=enm.get(i).getNews_img();

/*personViewHolder.Nw.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if(bmo.get(i).getOrder_status().equalsIgnoreCase("New")){
            String added_id=bmo.get(i).getUser_id();
           String s=bmo.get(i).getId();
            String o=bmo.get(i).getOrder_item_id();
            Intent i=new Intent(context,Butcher_MyOrder_New_Detail.class);
            i.putExtra("add_id",added_id);
            i.putExtra("pr_id", s);
            i.putExtra("order_id",o);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("stat","1");
            context.startActivity(i);
      // ((Butcher_my_order)context).finish();
           //((Butcher_my_order)context).yourDesiredMethod();
            //((Butcher_My_order_List_adapter).context).finish();
        }else {
            String added_id=bmo.get(i).getUser_id();
            String s=bmo.get(i).getId();
            String o=bmo.get(i).getOrder_item_id();
            Intent i=new Intent(context,Butcher_MyOrder_New_Detail.class);
            i.putExtra("add_id",added_id);
            i.putExtra("pr_id", s);
            i.putExtra("order_id",o);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("stat","2");
            context.startActivity(i);
         // ((Butcher_my_order)context).finish();
           *//* com.nispok.snackbar.Snackbar.with(view) // context
                    .text("Already Accepted") // text to display
                    .show(view);*//*
          //  Toast.makeText(context,"Already Accepted",Toast.LENGTH_SHORT).show();

        }
    }
});*/
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}