package com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Profile_View_Activity;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.squareup.picasso.Picasso;

public class Profile_View_Retailer extends AppCompatActivity {
    String Reg_id,result,logo,country,city,address,fullname,email,phone,shop_name;
    ImageView imag_logo;
    TextView txt_name,txt_city,txt_email,txt_address,txt_shopname,txt_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile__view_retailer);
        txt_name= (TextView) findViewById(R.id.txt_view_retailer_name);
        txt_phone= (TextView) findViewById(R.id.txt_view_retailer_phone);
        txt_email= (TextView) findViewById(R.id.txt_view_retailer_email);
        txt_address= (TextView) findViewById(R.id.txt_view_retailer_address);
        txt_shopname= (TextView) findViewById(R.id.txt_view_retailer_shopname);
        txt_city= (TextView) findViewById(R.id.txt_view_retailer_city);
        imag_logo= (ImageView) findViewById(R.id.imag_retailer_logo);


        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        fullname = myPrefs.getString("fullnameR", null);
        shop_name= myPrefs.getString("shp_nm", null);
        address = myPrefs.getString("ads", null);
        phone= myPrefs.getString("phne", null);
        email= myPrefs.getString("eml", null);
        city= myPrefs.getString("cityR", null);
        logo= myPrefs.getString("logoR", null);
        Reg_id= myPrefs.getString("user_idL", null);

        txt_name.setText(fullname);
        txt_shopname.setText(shop_name);
        txt_address.setText(address);
        txt_phone.setText(phone);
        txt_email.setText(email);
        txt_city.setText(city);
        Picasso.with(getApplicationContext()).load(logo).into(imag_logo);

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Profile_View_Retailer.this.goBack();
            }
        });
        //new DownloadData().execute();
    }
//    public class DownloadData extends AsyncTask<Void, Void, Void> {
//
//        ProgressDialog pd = null;
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            //    progress_loader.setVisibility(ProgressBar.VISIBLE);
//
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
////http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User
//
//            try {
//
//
//                HttpClient httpclient = new DefaultHttpClient();
//
//
//                HttpPost httppost = new HttpPost("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/view_profile.php?user_id="+Reg_id
//                );
//                HttpResponse response = httpclient.execute(httppost);
//                HttpEntity entity = response.getEntity();
//                InputStream is = entity.getContent();
//                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
//                StringBuilder sb = new StringBuilder();
//                String line = null;
//                while ((line = reader.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                is.close();
//                result = sb.toString();
//
//
//                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
//            } catch (Exception e) {
//                Log.e("Loading connection  :", e.toString());
//            }
//
//
//            return null;
//        }
//
//        protected void onPostExecute(Void args) {
//
//
//            System.out.println(">>>>>>>>>>>>>>>" + result);
//            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
//            String value = result;
//         /*   if
//                    (result.equalsIgnoreCase("")) {
//                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
//            }  else*/
//            if (result != null && !result.isEmpty() && !result.equals("null")) {
//                JSONArray mArray;
//                //  mCountryModel1 = new ArrayList<>();
//            //    eem = new ArrayList<MyAccount_Profile_Model>();
//                String  logos = null;
//                try {
//                    mArray = new JSONArray(result);
//                    for (int i = 0; i < mArray.length(); i++) {
//
//
//
//                        JSONObject mJsonObject = mArray.getJSONObject(i);
//
//                 logo = mJsonObject.getString("logo");
//                        System.out.println("logoooooooo"+logo);
//                        Picasso.with(getApplicationContext()).load(logo).into(Pfile);
//
//
//                        SharedPreferences myPrefs = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = myPrefs.edit();
//                        editor.putString("logob",logo);
//                        editor.commit();
//
//
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    new Picasso.Builder(Profile_View_Retailer.this)
//                            .downloader(new OkHttpDownloader(Profile_View_Retailer.this, Integer.MAX_VALUE))
//                            .build()
//                            .load(logo)
//                            .noFade()
//                            .into(Pfile, new Callback() {
//                                @Override
//                                public void onSuccess() {
//                                   // progress_loader1.setVisibility(View.GONE);
//                                }
//
//                                @Override
//                                public void onError() {
//                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
//                                }
//                            });
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//            } else {
//            /*    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
//                        .setTitleText(getResources().getString(R.string.Sorry))
//                        .setContentText(getResources().getString(R.string.NoContent))
//                        .setConfirmText(getResources().getString(R.string.ok))
//                        .setCustomImage(R.drawable.logo)
//
//                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(SweetAlertDialog sDialog) {
//                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);
//
//
//                                startActivity(i);
//                                finish()*//*;
//                                finish();
//                                sDialog.dismiss();
//                            }
//                        })
//                        .show();*/
//                System.out.println("nulllllllllllllllllllllllllllllllll");
//            }
//        }
//    }
private void goBack() {
    Intent i = new Intent(getApplicationContext(), Fresh_Frozen_Retailer.class);


    startActivity(i);
    // finish();
    finish();
}
}
