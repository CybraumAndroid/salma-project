package com.salmabutchershop.salma.meridian.app.retailer.international;

/**
 * Created by libin on 12/7/2016.
 */
public class Retailer_International_List_Model {
    String int_id,com_name,logo,country,city,con_person,designation,phon_no,email,address;

    public String getInt_id() {
        return int_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setInt_id(String int_id) {
        this.int_id = int_id;

    }

    public String getCom_name() {
        return com_name;
    }

    public void setCom_name(String com_name) {
        this.com_name = com_name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCon_person() {
        return con_person;
    }

    public void setCon_person(String con_person) {
        this.con_person = con_person;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPhon_no() {
        return phon_no;
    }

    public void setPhon_no(String phon_no) {
        this.phon_no = phon_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

