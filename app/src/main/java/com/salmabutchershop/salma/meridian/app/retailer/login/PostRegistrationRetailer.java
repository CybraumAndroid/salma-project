package com.salmabutchershop.salma.meridian.app.retailer.login;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Vis_FilePath;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Vis_Upload;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.address.HttpHandler;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.login.PostRegistration;
import com.salmabutchershop.salma.meridian.app.customer.newintro.GPSTracker1;
import com.salmabutchershop.salma.meridian.app.retailer.RetailerMainPage;
import com.salmabutchershop.salma.meridian.app.retailer.enquiry.RetailerEnquiryFragment;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.ContentValues.TAG;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;


/**
 * Created by libin on 11/18/2016.
 */

public class PostRegistrationRetailer extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView AlReg;
    EditText edtemail, edtphon, edtfullnam, edtusername, edtloctn, edtshopname, edtads1, edtads2;
    //  String email, phon, loc, pass, statusd, firstname, lastname, username, confirmpass;
    Button butsignup;
    boolean edittexterror = false;
    //String REGISTER_URL = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/register.php";
    ImageView profile_img_upload, profile_img;
    String filePath;
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    //    ProgressDialog pd;
    ProgressBar progress;

    Spinner spinner;
    static String filename = "null", filenamepath = "null";
    private static final int REQUEST_CODE = 1;
    String type, fullname, phone, email, location, username, password_s, address1_s, address2_s, shopname_s;
    String item;
    String result, refreshedToken, k;
    String Reg_id;
    TextView Tvb;
    public static MKLoader progress_loader;
    String ret_typ, Cntry, Cty;
    CountryCodePicker ccp;
    TextView Browse;
    private static final int PICK_FILE_REQUEST = 1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    String selectedFilePath, cvc;
    ImageView logo_img;
    String address, state, postalCode, knownName, Area, city, Country;
    GPSTracker1 gps;
    Double lt;
    Double lg;
    String lts, ltg;
    String ss;
    String SubLocality;
   // String vicinity_array[];
    //AutoCompleteTextView autoCompleteTextView;
  //  String autoCompleteTextViewss;
    double latitude;
    double longitude;
    String vicinity_array[],city_array_id[],city_array_city[],city_array_country_id[];
    AutoCompleteTextView autoCompleteTextView,edtpass;
    String autoCompleteTextViewss;
    String   starttext;
    String lng,lat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retailer_registration_layout);

        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_idL", null);
        Cntry = myPrefs.getString("cntry", null);
        Cty = myPrefs.getString("citys", null);
        cvc = myPrefs.getString("cvc", null);
        ss = myPrefs.getString("Ret_typ", null);
        SubLocality = myPrefs.getString("SubLocality", null);
        edtfullnam = (EditText) findViewById(R.id.edt_fullname);
        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtfullnam.setTypeface(myFont1);
        edtphon = (EditText) findViewById(R.id.edt_phone);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtphon.setTypeface(myFont2);
        edtemail = (EditText) findViewById(R.id.edt_email);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtemail.setTypeface(myFont3);
        edtloctn = (EditText) findViewById(R.id.edt_location);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtloctn.setTypeface(myFont4);
        edtusername = (EditText) findViewById(R.id.edt_username);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtusername.setTypeface(myFont5);
        edtpass = (AutoCompleteTextView) findViewById(R.id.edt_password);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtpass.setTypeface(myFont6);
        edtshopname = (EditText) findViewById(R.id.edt_shopnamee);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtshopname.setTypeface(myFont7);

        edtads1 = (EditText) findViewById(R.id.edt_ads1);
        Typeface myFont10 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtads1.setTypeface(myFont10);

        edtads2 = (EditText) findViewById(R.id.edt_ads2);
        Typeface myFont11 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtads2.setTypeface(myFont11);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setText(SubLocality);
        butsignup = (Button) findViewById(R.id.butsignup);
        Typeface myFont8 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        butsignup.setTypeface(myFont8);
        progress = (ProgressBar) findViewById(R.id.progress_bar);


        logo_img = (ImageView) findViewById(R.id.logo_img);
        Intent intent = getIntent();
        k = intent.getStringExtra("Retail_cat");

      /*  SharedPreferences myPrefs = PostRegistrationRetailer.this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

   System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + k);*/
/*        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();



        // editor.putString("usertype",usertype );

        editor.commit();*/
        // profile_img= (ImageView) findViewById(R.id.profile_image);

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            new getVicinity().execute();
            new getCountryList().execute();
            if (ContextCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistrationRetailer.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistrationRetailer.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("Ret_typ", k);
                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                    // \n is for new line
                    // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }
            lts = myPrefs.getString("latitude", null);
            ltg = myPrefs.getString("longitude", null);

        } else {
            com.nispok.snackbar.Snackbar.with(PostRegistrationRetailer.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(PostRegistrationRetailer.this);
        /*    com.chootdev.csnackbar.Snackbar.with(ButcherRegistration.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/

        }
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            if (ContextCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistrationRetailer.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistrationRetailer.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    // \n is for new line
                    //    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(PostRegistrationRetailer.this, Locale.getDefault());

                  /*  try {
                        lt = Double.valueOf(String.valueOf(latitude));
                        lg = Double.valueOf(String.valueOf(longitude));
                        addresses = geocoder.getFromLocation(lt, lg, 1);
                        progress_loader.setVisibility(View.VISIBLE);
                        if (!addresses.isEmpty()) {
                            Cntry = addresses.get(0).getCountryName();
                            System.out.println("-----------------------Country-----------------" + Country);
                            // Cntry.setText(Country);
                            // address = addresses.get(0).getAddressLine(0);
                            //  System.out.println("-----------------------adsggg-----------------" + address);
                            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            Cty = addresses.get(0).getLocality();
                            System.out.println("------------------city----------------------" + city);
                            //  Cty.setText(city);
                            Area = addresses.get(0).getSubLocality();
                            state = addresses.get(0).getLocality();
                            System.out.println("---------------Area-------------------------" + Area);
                            address = addresses.get(0).getAddressLine(0);
                               *//* if(autoCompView.getText().toString().equalsIgnoreCase("")){
                                    autoCompView.setText(address);
                                }*//*

                            System.out.println("-----------------------adsggg-----------------" + address);

                           *//* if(!Area.equalsIgnoreCase("")&&!Area.isEmpty()&&Area.equals(null)) {
                                autoCompView.setText(Area + "," + state);
                            }
                            else {
                                autoCompView.setHint("Type Your Lacality");
                            }*//*
                            /*//*//**//*    String country = addresses.get(0).getCountryName();
                            // System.out.println("-----------------cntry-----------------------" + country);*//**//*
                            postalCode = addresses.get(0).getPostalCode();
                            System.out.println("----------------postalcode------------------------" + postalCode);
                            knownName = addresses.get(0).getFeatureName();
                            System.out.println("-----------------knwn-----------------------" + knownName);
                            cvc = addresses.get(0).getCountryCode();
                            // textinfo.setText("" + address + "\n" + "" + city + "\n" + "" + state + "\n" + "" + postalCode + "\n" + "" + knownName);
                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();

                            editor.putString("city_id", cvc);
                            editor.putString("citys", city);
                            editor.putString("cntry", Country);
                            editor.putString("cvc", cvc);
                            editor.putString("Ret_typ", k);
                            // editor.putString("latitude", String.valueOf(latitude));
                            editor.commit();
                            progress_loader.setVisibility(View.GONE);
                        }
                        {
                            System.out.println("----------------------------------------");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }
            ccp = (CountryCodePicker) findViewById(R.id.ccp);
            ccp.setCountryForNameCode(cvc);
            ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected() {
                    //  Toast.makeText(PostRegistrationRetailer.this, "Updated " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
                }
            });
            if (Reg_id != null && !Reg_id.isEmpty()) {
                //Toast.makeText(getApplicationContext(), Reg_id, Toast.LENGTH_SHORT).show();


                Intent synin = new Intent(getApplicationContext(), Fresh_Frozen_Retailer.class);
                // synin.putExtra("brn_id",u);
                startActivity(synin);
                finish();
            }
            edtusername.setText(Cntry);
            edtpass.setText(Cty);
            TextView skip = (TextView) findViewById(R.id.txt_skips);

            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent inn = new Intent(getApplicationContext(), LoginActivityRetailer.class);
                    startActivity(inn);
                    finish();
                }
            });

            progress_loader.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    progress_loader.setVisibility(View.INVISIBLE);
                    return false;
                }
            });
            Tvb = (TextView) findViewById(R.id.txt_skip);
            Tvb.setText(k);
            Browse = (TextView) findViewById(R.id.brwse);
            Browse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED) {

                        requestPermissions();
                    }
                    showFileChooser();
                }
            });
            autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    String l = getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>" + l);

                    if (l.equalsIgnoreCase("English")) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (autoCompleteTextView.getRight() - autoCompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                autoCompleteTextView.setText("");
                                return true;
                            }
                        }
                    } else {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (autoCompleteTextView.getLeft() - autoCompleteTextView.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                                // your action here
                                autoCompleteTextView.setText("");
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });
            edtpass.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    String l = getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>" + l);
                    if (l.equalsIgnoreCase("English")) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtpass.getRight() - edtpass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                edtpass.setText("");
                                return true;
                            }
                        }
                    } else {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtpass.getLeft() - edtpass.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                                // your action here
                                edtpass.setText("");
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });
          /*  edtusername.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    String l = getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>" + l);
                    if (l.equalsIgnoreCase("English")) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtusername.getRight() - edtusername.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                edtusername.setText("");
                                return true;
                            }
                        }
                    } else {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtusername.getLeft() - edtusername.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                                // your action here
                                edtusername.setText("");
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });*/

            butsignup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    email = edtemail.getText().toString();
                    password_s = edtpass.getText().toString();
                    // type = edtcnfrmpass.getText().toString();
                    phone = edtphon.getText().toString();
                    fullname = edtfullnam.getText().toString();
                    address1_s = edtads1.getText().toString();
                    address2_s = edtads2.getText().toString();
                    shopname_s = edtshopname.getText().toString();
                    //  location = edtloctn.getText().toString();
                    username = edtusername.getText().toString();
                    autoCompleteTextViewss= autoCompleteTextView.getText().toString();
                    edittexterror = false;
                    if (edtemail.getText().toString().isEmpty() || edtpass.getText().toString().isEmpty() || edtfullnam.getText().toString().trim().isEmpty() || edtphon.getText().toString().isEmpty() || edtusername.getText().toString().isEmpty() || edtshopname.getText().toString().isEmpty()||autoCompleteTextView.getText().toString().isEmpty()) {
                        com.nispok.snackbar.Snackbar.with(PostRegistrationRetailer.this) // context
                                .text(getResources().getString(R.string.empty_fields))// text to display
                                .show(PostRegistrationRetailer.this);
                   /* com.chootdev.csnackbar.Snackbar.with(PostRegistrationRetailer.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Empty Fields!")
                            .duration(Duration.SHORT)
                            .show();*/
                        edittexterror = true;
                        //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (edtemail.getText().toString().isEmpty()) {
                        edtemail.setError(getResources().getString(R.string.Enter_Email_Id));
                        edittexterror = true;
                    } else if (edtpass.getText().toString().isEmpty()) {
                        edtpass.setError(getResources().getString(R.string.Enter_city));
                        edittexterror = true;
                    } else if (edtphon.getText().toString().isEmpty()) {
                        edtphon.setError(getResources().getString(R.string.Enter_Phone));
                        edittexterror = true;
                    } else if (edtfullnam.getText().toString().isEmpty()) {
                        edtfullnam.setError(getResources().getString(R.string.Enter_Full_name));
                        edittexterror = true;
                    } else if (edtusername.getText().toString().isEmpty()) {
                        edtusername.setError(getResources().getString(R.string.Enter_country));
                        edittexterror = true;
                    }
                 else if (autoCompleteTextView.getText().toString().isEmpty()) {
                        autoCompleteTextView.setError(getResources().getString(R.string.Enter_location));
                    edittexterror = true;
                }

                    else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (!android.util.Patterns.PHONE.matcher(edtphon.getText().toString().trim()).matches()) {
                        edtphon.setError(getResources().getString(R.string.Invalid_Phone));
                        edittexterror = true;
                    } else if (edtshopname.getText().toString().isEmpty()) {
                        edtshopname.setError(getResources().getString(R.string.Enter_shop_name));
                        edittexterror = true;
                    } else if (filenamepath != null) {
                        filename = filenamepath;


                    } else {
                        filename = "null";

                    }

                    if (edittexterror == false) {
                        if (DetectConnection
                                .checkInternetConnection(getApplicationContext())) {
                            if (selectedFilePath != null && !selectedFilePath.isEmpty())

                            {
                                uploadVideo1();


                                new SendPostRequest().execute();
                                // tt="";
                            } else {
                                new SweetAlertDialog(PostRegistrationRetailer.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(getResources().getString(R.string.MyMenu))
                                        .setContentText(getResources().getString(R.string.Areyousurewanttosetimageasdefault))
                                        .setConfirmText(getResources().getString(R.string.Yes))
                                        .setCancelText(getResources().getString(R.string.No))
                                        .setCustomImage(R.drawable.logo)
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {


                                                new SendPostRequest().execute();
                                                sDialog.dismiss();
                                            }
                                        })
                                        .show();

                            }


                        } else {
                            com.nispok.snackbar.Snackbar.with(PostRegistrationRetailer.this) // context
                                    .text("Oops Your Connection Seems Off..!") // text to display
                                    .show(PostRegistrationRetailer.this);
                       /* com.chootdev.csnackbar.Snackbar.with(PostRegistrationRetailer.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();*/

                        }


                    }


                }
            });
        }
    }

    private void uploadVideo1() {
        class UploadVideo extends AsyncTask<Void, Void, String> {

            ProgressDialog uploading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress_loader.setVisibility(View.VISIBLE);
                //   uploading = ProgressDialog.show(PostRegistrationRetailer.this, "Salma", "Please wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //    progress_loader.setVisibility(View.GONE);
                //   uploading.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {
                Vis_Upload1 u = new Vis_Upload1();
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<md>>>>>>>>>>>>>>>>>>>>>>>>>" + selectedFilePath);
                String msg = u.uploadVideo(selectedFilePath);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<mdeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>>>>>>>>>>" + msg);

                return msg;
            }
        }
        UploadVideo uv = new UploadVideo();
        uv.execute();
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(PostRegistrationRetailer.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE);
        // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    private void showFileChooser() {
        try {
            Intent intent = new Intent();
            //sets the select file to all types of files
            intent.setType("image/*");
            //allows to select data and return it
            intent.setAction(Intent.ACTION_GET_CONTENT);
            //starts new activity to select file and return data
            startActivityForResult(Intent.createChooser(intent, "Choose File to Vis_Upload.."), PICK_FILE_REQUEST);
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FILE_REQUEST) {
                if (data == null) {
                    //no data present
                    return;
                }


                Uri selectedFileUri = data.getData();
                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), selectedFileUri);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                Log.i(TAG, "Selected File Path:" + selectedFilePath);

                if (selectedFilePath != null && !selectedFilePath.equals("")) {
                    // textViewResponse.setText(selectedFilePath);
                    filenamepath = selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                    CropImage.activity(selectedFileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                    // logo_img.setImageBitmap(bmp);
                } else {
                    com.nispok.snackbar.Snackbar.with(PostRegistrationRetailer.this) // context
                            .text("Cannot upload file to server!") // text to display
                            .show(PostRegistrationRetailer.this);
                   /* Snackbar.with(Butcher_Product_Submit.this,null)
                            .type(Type.SUCCESS)
                            .message("Cannot upload file to server!")
                            .duration(Duration.LONG)
                            .show();*/
                    //  Toast.makeText(getApplicationContext(),"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                }

            }
           /* else if(requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
                if(resultCode==RESULT_OK){
                    previewCapturedImage();
                }*/

            //  }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                System.out.println("selectedFileUri : " + resultUri);
                selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), resultUri);
                Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                if (selectedFilePath != null && !selectedFilePath.equals("")) {
                    // textViewResponse.setText(selectedFilePath);
                    filenamepath = selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                } else {
                    com.nispok.snackbar.Snackbar.with(PostRegistrationRetailer.this) // context
                            .text("Cannot upload file to server!") // text to display
                            .show(PostRegistrationRetailer.this);

                }
                logo_img.setImageBitmap(bmp);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        checkAndRequestPermissions();


    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private boolean isValidPassword(String trim) {
        if (password_s != null && password_s.length() > 6) {
            return true;
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();


        if (item.equalsIgnoreCase("butchers")) {
            type = "3";
            edtads1.setVisibility(View.VISIBLE);
            edtads2.setVisibility(View.VISIBLE);
            edtshopname.setVisibility(View.VISIBLE);
            edtloctn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public class SendPostRequest extends AsyncTask<String, Void, String> {

        public void onPreExecute() {
            progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        public String doInBackground(String... arg0) {
            String js = filename.replaceAll(" ", "");

            try {

                URL url = new URL(SERVER+"json/retailer/retailer-registration.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("type", "2");
                postDataParams.put("fullname", fullname);
                postDataParams.put("phone", phone);
                postDataParams.put("email", email);
                postDataParams.put("address1", address1_s);
                postDataParams.put("address2", address2_s);
                postDataParams.put("shopname", shopname_s);
                postDataParams.put("country", username);
                postDataParams.put("city", password_s);
                postDataParams.put("token", refreshedToken);
                postDataParams.put("retailer_type", k);
                postDataParams.put("longitude", ltg);
                postDataParams.put("latitude", lts);
                postDataParams.put("profilepic", js);
                postDataParams.put("location",autoCompleteTextViewss);
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {

            // Toast.makeText(getApplicationContext(), result,
            //  Toast.LENGTH_LONG).show();
            if (result.contentEquals("\"success\"")) {

                new SweetAlertDialog(PostRegistrationRetailer.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getResources().getString(R.string.Retailsdetailssubmitted))
                        .setContentText(getResources().getString(R.string.Pleasewaitforconfirmation))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                progress_loader.setVisibility(ProgressBar.GONE);
                                Intent inn = new Intent(getApplicationContext(), LoginActivityRetailer.class);

                                startActivity(inn);
                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            } else if (result.contentEquals("\"already registerd\"")) {
                new SweetAlertDialog(PostRegistrationRetailer.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.AlreadyRegistred))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    private class getVicinity extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(View.VISIBLE);
            if (ContextCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistrationRetailer.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistrationRetailer.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String result;

            try {
                HttpHandler h = new HttpHandler();
                System.out.println("location : " + latitude + "," + longitude);
                String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lat+","+lng+"&radius=5000&type=all&keyword=&key=AIzaSyAo27AsUvSaSFEQ5OZWGc4l2SczpZhwnL4"; // here is your URL path
                System.out.println("url : " + url);

                result = h.makeServiceCall(url);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            System.out.println("result : " + result);


            try {
                if (result != null) {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray gmap_result = jsonObject.getJSONArray("results");
                    vicinity_array = new String[gmap_result.length()];
                    for (int i = 0; i < gmap_result.length(); i++) {
                        JSONObject results_object = gmap_result.getJSONObject(i);
                        String vicinity = results_object.getString("vicinity");
                        vicinity_array[i] = vicinity;
                    }
                    System.out.println("----------------- VICINITY ------------------------");
                    for (int i = 0; i < vicinity_array.length; i++) {
                        System.out.println("vicinity : " + vicinity_array[i]);
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (PostRegistrationRetailer.this, R.layout.customautocompleteview, R.id.autoCompleteItem, vicinity_array);
                    autoCompleteTextView.setAdapter(adapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            progress_loader.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Intent inn = new Intent(getApplicationContext(), RetailerMainPage.class);
            startActivity(inn);
            finish();
        } else {

            Intent inn = new Intent(getApplicationContext(), RetailerMainPage.class);
            startActivity(inn);
            finish();
        }
    }

    private class getCountryList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(View.VISIBLE);
            if (ContextCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistrationRetailer.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistrationRetailer.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistrationRetailer.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(PostRegistrationRetailer.this, Locale.getDefault());

                    try {
                              /*  lt= Double.valueOf(C_lat);
                                lg= Double.valueOf(C_long);*/
                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                        // progress.setVisibility(View.VISIBLE);
                        if (!addresses.isEmpty()) {
                            Country = addresses.get(0).getCountryName();

                            cvc = addresses.get(0).getCountryCode();

                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String result;

            try {
                HttpHandler h = new HttpHandler();

                String url = SERVER+"json/butcher/city.php?cid="+cvc; // here is your URL path
                System.out.println("url : " + url);

                result = h.makeServiceCall(url);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute (String result){

            System.out.println("result : " + result);


            try {
                if (result != null) {
                    JSONArray jsonArray=new JSONArray(result);
                    city_array_id=new String[jsonArray.length()];
                    city_array_city=new String[jsonArray.length()];
                    city_array_country_id=new String[jsonArray.length()];
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        city_array_id[i]=jsonObject.getString("id");
                        city_array_city[i]=jsonObject.getString("city");
                        city_array_country_id[i]=jsonObject.getString("country_id");

                    }
                    System.out.println("----------------- CITY ------------------------");
                    for (int i = 0; i < city_array_city.length; i++) {
                        System.out.println("city : " + city_array_city[i]);
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (PostRegistrationRetailer.this, R.layout.customautocompleteview, R.id.autoCompleteItem, city_array_city);
                    edtpass.setAdapter(adapter);
                    edtpass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            //TextView lbl_name= (TextView) view.findViewById(R.id.lbl_name);
                            starttext = String.valueOf(adapterView.getItemAtPosition(i));
                            System.out.println("name"+starttext);
                            new DownloadData3().execute();

                            // System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<lats>>>>>>>>>>>>"+lat1);
                            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<lats>>>>>>>>>>>>"+starttext);
                            //System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<lats>>>>>>>>>>>>"+lng1);


                        }
                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            progress_loader.setVisibility(View.GONE);
        }
    }

    private class DownloadData3 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;

        StringBuilder stringBuilder;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User


            String strnew = starttext.replaceAll(" ", "%20");
            String uri = "http://maps.google.com/maps/api/geocode/json?address="+strnew+"&sensor=false";
            HttpGet httpGet = new HttpGet(uri);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // JSONObject jsonObject = new JSONObject();
            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(View.GONE);
            // System.out.println(">>>>>>>>>>>>>>>" + result1);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());

                double lngd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                double latd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.d("latitude", "" + latd);
                Log.d("longitude", "" + lngd);
                lng = String.valueOf(lngd);
                lat = String.valueOf(latd);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlng>>>>>>>>>>>>>>>" + lng);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlat>>>>>>>>>>>>>>>" + lat);
                new getVicinity().execute();
                // new Customer_Butcher_List_Fragment.DownloadData1().execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}

