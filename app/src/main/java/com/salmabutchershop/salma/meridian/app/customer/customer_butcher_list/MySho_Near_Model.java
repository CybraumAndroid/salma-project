package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

/**
 * Created by libin on 5/8/2017.
 */

class MySho_Near_Model {
    String id,shop_nam1,address1;
Double latitude1,longitude1;

    public Double getLatitude1() {
        return latitude1;
    }

    public void setLatitude1(Double latitude1) {
        this.latitude1 = latitude1;
    }

    public Double getLongitude1() {
        return longitude1;
    }

    public void setLongitude1(Double longitude1) {
        this.longitude1 = longitude1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getShop_nam1() {
        return shop_nam1;
    }

    public void setShop_nam1(String shop_nam1) {
        this.shop_nam1 = shop_nam1;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }
}

