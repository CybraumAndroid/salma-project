package com.salmabutchershop.salma.meridian.app.customer.address;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;

import java.util.ArrayList;

public class ViewDatas_N extends AppCompatActivity {
private ProgressDialog prgDlg;
    private DatabaseHandler_N dh;
    ArrayList dataa;
    private RecyclerView rvNew;
    NewDataAdapter_N adapterNew;
    Button Add_ads;
    String gtot,name,inst,inste;
    Float price;
    String fultot,but_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_datas_n);
        name = getIntent().getExtras().getString("item_name");
        price = getIntent().getExtras().getFloat("price");
        inst= getIntent().getExtras().getString("inst");
        System.out.println("insttttttttttttttttttttt"+inst);
        fultot=getIntent().getExtras().getString("fultot");
        System.out.println("insttttttttttttttttttttt"+fultot);
        //i.putExtra("fultot",cart_tot2);
        if(getIntent().getExtras().getString("inst")!=null&&getIntent().getExtras().getString("inst").isEmpty()){
            inst= getIntent().getExtras().getString("inst");
            System.out.println("insttttttttttttttttttttt"+inst);
        }
        else {

        }
        Intent intent = getIntent();
        but_id = intent.getStringExtra("but_id");
        System.out.println("-----------------------but_idNNNNNNNNNNNN-----------------"+but_id);
        dh=new DatabaseHandler_N();
        dataa=new ArrayList<>();
        rvNew=(RecyclerView)findViewById(R.id.recylerView2);
        RecyclerView.LayoutManager lm2=new LinearLayoutManager(getApplicationContext());
        rvNew.setLayoutManager(lm2);
        ImageView back= (ImageView) findViewById(R.id.back_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });
        Add_ads= (Button) findViewById(R.id.button5);
        Add_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), AddressFragment.class);
                i.putExtra("id_nam","libin");
                i.putExtra("price", gtot);
                i.putExtra("name",name);
                i.putExtra("inst",inst);
                i.putExtra("fultot",fultot);
                i.putExtra("but_id",but_id);
                System.out.println("detailll6"+gtot);
                System.out.println("dbbbbbbbbbbbbbbbbbbbbb"+but_id);
                startActivity(i);
              //  finish();
            }
        });
        new FetchDetails().execute();
    }

    private void goBack() {
       finish();
    }

    class FetchDetails extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prgDlg=new ProgressDialog(ViewDatas_N.this);
            prgDlg.setMessage("Loading Image...");
            prgDlg.setCancelable(false);
            prgDlg.show();
        }

        @Override
        protected String doInBackground(String... strings) {
try{
            Cursor c=dh.selectAllDATAS(getApplicationContext());
            int count=c.getCount();
            if(count!=0)
            {
                while (c.moveToNext()){
                DataValues_N dv=new DataValues_N();
                    dv.id=c.getInt(0);
                    dv.fname=c.getString(1);
                    dv.lname=c.getString(2);
                    dv.city=c.getString(3);
                    dv.deliveryarea=c.getString(4);
                    dv.addresstype=c.getString(5);
                    dv.street=c.getString(6);
                    dv.building =c.getString(7);
                    dv.floor=c.getString(8);
                    dv.apartno=c.getString(9);
                    dv.mobile=c.getString(10);
                    dv.phone=c.getString(11);
                    dv.addtional_direc=c.getString(12);
                  //  dv.but_id=c.getString(13);

                   /* if(inst.isEmpty()){

                    }else {
                        dv.inst=c.getString(13);
                    }*/

                    dataa.add(dv);
                    adapterNew=new NewDataAdapter_N(getApplicationContext(),dataa,inst,but_id);
                }
            }
            else {
           /*     com.nispok.snackbar.Snackbar.with(ViewDatas_N.this) // context
                        .text("Field Empty!") // text to display
                        .show(ViewDatas_N.this);*/
               // Toast.makeText(getApplicationContext(),"No data",Toast.LENGTH_LONG).show();
            }}catch (Exception e){
    e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prgDlg.isShowing())
                prgDlg.dismiss();

            rvNew.setAdapter(adapterNew);

        }
    }

}
