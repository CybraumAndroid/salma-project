package com.salmabutchershop.salma.meridian.app.butcher.mymenu;

/**
 * Created by libin on 12/3/2016.
 */
public class Product_mymenu_Model {
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    String pro_id,type_id,pro_nam,type_name,type_image,category,currency ;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getPro_nam() {
        return pro_nam;
    }

    public void setPro_nam(String pro_nam) {
        this.pro_nam = pro_nam;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getType_image() {
        return type_image;
    }

    public void setType_image(String type_image) {
        this.type_image = type_image;
    }
}
