package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by libin on 9/29/2016.
 */
public class Customer_Butcher_Sublist_adapter extends RecyclerView.Adapter<Customer_Butcher_Sublist_adapter.ViewHolder> {


    List<Customer_Butcher_Sublist_Model>esm;
    Context context;



    public Customer_Butcher_Sublist_adapter(ArrayList<Customer_Butcher_Sublist_Model> esm, Context context) {
        this.esm = esm;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView vs;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;
        public static MKLoader progress_loader;
        ImageView personPhoto;
ProgressBar gallery_progressbar;
        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

       tv=   (TextView) itemView.findViewById(R.id.textViewName);
            gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            vs=   (ImageView) itemView.findViewById(R.id.imageView6);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
        }
    }


    @Override
    public int getItemCount() {
        return esm.size();
    }


    @Override
    public Customer_Butcher_Sublist_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sponsors_item_layout, viewGroup, false);
        Customer_Butcher_Sublist_adapter.ViewHolder pvh = new Customer_Butcher_Sublist_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Customer_Butcher_Sublist_adapter.ViewHolder personViewHolder, final int i) {

        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont3);
        String s = esm.get(i).getSub_image();
        personViewHolder.tv.setText(esm.get(i).getSubcat_name());


        //  String img=enm.get(i).getNews_img();
        System.out.println("oooyyyyyyyyyyyy>>>>>>" + s);
      //  Picasso.with(context)
        try {


        new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                .build()
                .load(s)
                .noFade()
                .into(personViewHolder.vs, new Callback() {
                    @Override
                    public void onSuccess() {
                        personViewHolder.progress_loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
        }catch (Exception e){
            e.printStackTrace();
        }
     //   try {
  // personViewHolder.vs.setTag(R.id.glide_tag,"hello");
      /*  Glide
                .with(context)
                .load(s)
                .into(personViewHolder.vs);*/
      /*  } catch(IllegalArgumentException ex) {
            Log.wtf("Glide-tag", String.valueOf(personViewHolder.v.getTag()));
        }*/
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}