package com.salmabutchershop.salma.meridian.app.customer.location;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 11/11/2016.
 */
public class Location_City_adapter extends RecyclerView.Adapter<Location_City_adapter.ViewHolder> {


    List<Location_City_Model> lcm;
    Context context;



    public Location_City_adapter(ArrayList<Location_City_Model> lcm, Context context) {
        this.lcm = lcm;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;

        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            tv=   (TextView) itemView.findViewById(R.id.evt_typ);
        //    cv1=(LinearLayout) itemView.findViewById(R.id.lt2);
           /* tv1=   (TextView) itemView.findViewById(R.id.evnt_frm);
            tv2=   (TextView) itemView.findViewById(R.id.evnt_to);
            tv3=   (TextView) itemView.findViewById(R.id.evnt_vnue);
         ;*/
            // v=   (ImageView) itemView.findViewById(R.id.imageView13);
        }
    }


    @Override
    public int getItemCount() {
        return lcm.size();
    }


    @Override
    public Location_City_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.location_city_item_layout, viewGroup, false);
        Location_City_adapter.ViewHolder pvh = new Location_City_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Location_City_adapter.ViewHolder personViewHolder, final int i) {


        // String s = eem.get(i).getExhibitors();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.tv.setTypeface(myFont2);
        personViewHolder.tv.setText(lcm.get(i).getCity());
        //   System.out.println("exhibtors" + s);
        /*String img=eem.get(i).getNews_img();
        System.out.println("ooo" + s);
        Picasso.with(context).load(img).resize(150, 150).into(personViewHolder.v);*/
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}