
package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


/**
 * Created by Rashid on 11/14/2016.
 */


public class DatabaseHandler {
    SQLiteDatabase db=null;

   public void DeleteView(Context context1, DataAdapter_2.ViewHolder holder){
        db = context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,img_url VARCHAR,shop_name VARCHAR,discount VARCHAR,dlvry_info VARCHAR,added_by VARCHAR,cat_id VARCHAR);");
        db.execSQL("DELETE FROM favourite where id="+holder.PrimaryId.getText());
        System.out.println("deleted button id :"+holder.rmv_btn+"\n image view id :"+holder.imgView2);
    }

    public void InsertIntoSqlite(Context context1,String imgUrl,String shop_name,String discount,String dlvry_info,String added_by,String cat_id)
    {
        db = context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
      //  db.execSQL("INSERT INTO favourite(img_url,shop_name,discount,dlvry_info) VALUES('" + imgUrl+shop_name+discount+dlvry_info+"')");
        db.execSQL("INSERT INTO favourite(img_url,shop_name,discount,dlvry_info,added_by,cat_id) VALUES('" + imgUrl + "','"+shop_name+"','"+discount+"','"+dlvry_info+"','"+added_by+"','"+cat_id+"')");
       // db.execSQL("INSERT INTO favourite(shop_name) VALUES('"+shop_name+"')");
      //  db.execSQL("INSERT INTO favourite(discount) VALUES('" + discount + "')");
      //  db.execSQL("INSERT INTO favourite(dlvry_info) VALUES('"+dlvry_info+"')");
    }

    public Cursor checkIfAdded(Context context1,String imgUrl,String shop_name,String discount,String dlvry_info,String added_by,String cat_id){
        db = context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,img_url VARCHAR,shop_name VARCHAR,discount VARCHAR,dlvry_info VARCHAR,added_by VARCHAR,cat_id VARCHAR);");
        Cursor rs = db.rawQuery("SELECT img_url FROM favourite where added_by='" + added_by + "'", null);
      //  Cursor rs= db.rawQuery("SELECT img_url FROM favourite where img_url='" + imgUrl + "'", null);
        return rs;
    }


    public Cursor selectAllFAV(Context context1)
    {
        db =context1.openOrCreateDatabase("FAV.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,img_url VARCHAR,shop_name VARCHAR,discount VARCHAR,dlvry_info VARCHAR,added_by VARCHAR,cat_id VARCHAR);");
        Cursor rs = db.rawQuery("SELECT * FROM favourite", null);
        return rs;
    }

    public Cursor Addedby(Context context1, String added_by) {

        db=context1.openOrCreateDatabase("FAV.db",Context.MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS favourite(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,added_by VARCHAR");
        Cursor rs=db.rawQuery("SELECT * FROM favourite", null);
        return rs;
    }

    {

    }
}
