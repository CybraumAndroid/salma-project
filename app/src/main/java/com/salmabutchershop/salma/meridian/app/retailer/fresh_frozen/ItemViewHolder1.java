package com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


/**
 * Created by Libin_Cybraum on 6/27/2016.
 */
public class ItemViewHolder1 extends RecyclerView.ViewHolder {

    public TextView name_TextView;
    public TextView iso_TextView;
    protected FrameLayout relativeLayoutOverYouTubeThumbnailView;

    TextView m;
    ImageView v;
    TextView tv,tv1,tv2,tv3;
ProgressBar gallery_progressbar;
    public ItemViewHolder1(View itemView) {
        super(itemView);
        itemView.setClickable(true);
        tv=   (TextView) itemView.findViewById(R.id.title);
           /* tv1=   (TextView) itemView.findViewById(R.id.evnt_frm);
            tv2=   (TextView) itemView.findViewById(R.id.evnt_to);
            tv3=   (TextView) itemView.findViewById(R.id.evnt_vnue);
         ;*/
        v=   (ImageView) itemView.findViewById(R.id.imageView8);
        //   playButton.setOnClickListener(this);
        gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
    }

    public void bind(final CountryModel1 countryModel1, final Context cx) {
        tv.setText(countryModel1.getSubcat_name());
        Picasso.with(cx).load(countryModel1.getCat_image()).noFade()//.into(v);
         .into(v, new Callback() {
            @Override
            public void onSuccess() {
                gallery_progressbar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
       // iso_TextView.setText(countryModel.getisoCode());




    }


}
