package com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen;

/**
 * Created by libin on 12/9/2016.
 */
public class CountryModel1 {
    public String getLatest_version() {
        return latest_version;
    }

    public void setLatest_version(String latest_version) {
        this.latest_version = latest_version;
    }


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getEnable_popup() {
        return enable_popup;
    }

    public void setEnable_popup(String enable_popup) {
        this.enable_popup = enable_popup;
    }

    String cat_id,subcat_id,subcat_name,cat_image,latest_version,valid,logo,enable_popup;

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getSubcat_name() {
        return subcat_name;
    }

    public void setSubcat_name(String subcat_name) {
        this.subcat_name = subcat_name;
    }

    public String getCat_image() {
        return cat_image;
    }

    public void setCat_image(String cat_image) {
        this.cat_image = cat_image;
    }


}
