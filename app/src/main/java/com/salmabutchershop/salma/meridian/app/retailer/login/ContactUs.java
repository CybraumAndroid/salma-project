package com.salmabutchershop.salma.meridian.app.retailer.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.address.HttpHandler;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

public class ContactUs extends AppCompatActivity {
    ProgressDialog prgDlg;
    TextView address,telephone,phone,email;
    private ContactModel cm;
    ProgressBar progress;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.layout);
        rl.setBackgroundColor(Color.parseColor("#ffffff"));

        address=(TextView)findViewById(R.id.address);

        telephone=(TextView)findViewById(R.id.telephone);

        phone=(TextView)findViewById(R.id.phone);
;
        email=(TextView)findViewById(R.id.email);

        cm=new ContactModel();

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ContactUs.this.goBack();
            }
        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)
            new FetchDetails().execute();

        } else {
            com.nispok.snackbar.Snackbar.with(ContactUs.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(ContactUs.this);
          /*  com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/



        }


    }

  /*  private void goBack() {
        super.onBackPressed();
    }*/


    class FetchDetails extends AsyncTask<String,String,String>{

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress_loader.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... strings) {

        HttpHandler h=new HttpHandler();
        String jsonString=h.makeServiceCall(SERVER+"json/customer/contactus.php");
        if(jsonString!=null){
            try{
                JSONArray valuesArray=new JSONArray(jsonString);
                for(int i=0;i<valuesArray.length();i++){
                    JSONObject jsonData=valuesArray.getJSONObject(i);

                    cm.id=Integer.parseInt(jsonData.getString("id"));
                    cm.address=jsonData.getString("address");
                    cm.telephone=jsonData.getString("telphone");
                    cm.mobile=jsonData.getString("mobile");
                    cm.email=jsonData.getString("email");

                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress_loader.setVisibility(View.GONE);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
      address.setTypeface(myFont2);
        address.setText(cm.getAddress());
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
      telephone.setTypeface(myFont3);
        telephone.setText("Tel : "+cm.getTelephone());
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
       phone.setTypeface(myFont4);
        phone.setText("Phone : "+cm.getMobile());
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
       email.setTypeface(myFont5);
        email.setText("Email : "+cm.getEmail());


    }
}
    private void goBack() {
     /*   Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);*/
        // finish();
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
   /*     Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);*/
        // finish();
        finish();
    }
}
