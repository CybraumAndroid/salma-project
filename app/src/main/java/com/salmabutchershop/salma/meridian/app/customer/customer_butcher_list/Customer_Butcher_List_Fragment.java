package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.salmabutchershop.salma.meridian.app.GPSTracker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.acquirelocation.GooglePlacesAutocompleteActivity;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;

import com.salmabutchershop.salma.meridian.app.customer.address.AddressFragment;
import com.salmabutchershop.salma.meridian.app.customer.address.Collect_From_here;
import com.salmabutchershop.salma.meridian.app.customer.address.DashboardActivity;
import com.salmabutchershop.salma.meridian.app.customer.address.DatabaseHelperCollect;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Contact_Our_Team;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_City_Model;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_City_adapter;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Model;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER_AR;


/**
 * Created by libin on 9/29/2016.
 */

public class Customer_Butcher_List_Fragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;
    //   static ArrayList<UpcomingModel> upm;
    Customer_Butcher_List_adapter adapter1;
    ProgressBar progress;
    String result, galry_image, Dat, Dat1;
    static ArrayList<Customer_Butcher_List_Model> zwm;
    Customer_Butcher_List_Model wm;
    String added_by, cat_id, butcher_logo, shop_name, delivery_info, butcher_discount;
    String k;
    Button Beef, Mutton, Camel, Other, Fish, Othe;
   static ImageView Loc_ic;
    LinearLayout NH;
    static ArrayList<Location_Services_Model> eem;
    Location_Services_Model ee;
    String id, city;
    ArrayList<String> divisonlist;
    ArrayList<String> divisonlist1;
    static Spinner loc;
    Spinner area;
    String ids;
    String dptmt, dvn_id, country_id, loc_name, shop_description;
    String result1;
    static ArrayList<Location_City_Model> lcm;
    Location_City_Model cm;

    String lng;
    String lat;

    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    //ProgressBar progress;

    private static final String API_KEY = "AIzaSyAo27AsUvSaSFEQ5OZWGc4l2SczpZhwnL4";
    private static final String RADIOUS = "&radius";
    TextView tc;
    String Wurl;
    static String cid;

    int locationMode;
    Context mContext;
    GPSTracker gps;
    double latitude;
    double longitude;
    String flag = "1";
    String str;
String shop_addrs;
    static LinearLayout Lct;
    public static MKLoader progress_loader;
    String discount_desc;
    Button map_icon_image;
    String distance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        tc = (TextView) findViewById(R.id.header_text);
       Lct= (LinearLayout) findViewById(R.id.lct);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        map_icon_image=(Button)findViewById(R.id.map_icon_image);
        // Dat= getArguments().getString("desc");
        // Dat= getArguments().getString("des_id");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Customer_Butcher_List_Fragment.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


        } else {

            gps = new GPSTracker(this, Customer_Butcher_List_Fragment.this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                // \n is for new line
                System.out.println("----------------------------------------");
                System.out.println("inside cangetlocation");
                System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                System.out.println("----------------------------------------");
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("longitude", String.valueOf(longitude));
                editor.putString("latitude", String.valueOf(latitude));
                editor.commit();

            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                int a = gps.showSettingsAlert(Customer_Butcher_List_Fragment.this);

            }
        }
        try {


        Intent intent = getIntent();
        k = intent.getStringExtra("cat_id");
        str=intent.getStringExtra("value");
        if (str.equalsIgnoreCase("1")){

        }else {

        }
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + k);
        }catch (NullPointerException e){

        }
       // String[] array = getIntent().getStringArrayExtra("array");


      //  RelativeLayout rl = (RelativeLayout) findViewById(R.id.layout);
//        rl.setBackgroundColor(Color.parseColor("#ffffff"));

        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        //SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        lng = myPrefs.getString("longitude", null);
        lat = myPrefs.getString("latitude", null);
        System.out.println(">>>>>>subinlang>>>>>>>>>" + lng);
        System.out.println(">>>>>>subinlong>>>>>>>>>" + lat);
        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        cid = myPrefs.getString("city_id", null);
        if (cid.isEmpty()&&cid==null){
            cid = myPrefs.getString("ccode", null);
        }else {
            cid = myPrefs.getString("cvc", null);
        }


        System.out.println(">>>>>>city_id>>>>>>>>>" +cid);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        try {


        if (k.equalsIgnoreCase("1")) {
            tc.setText(getResources().getString(R.string.findbeefshop));
        } else if (k.equalsIgnoreCase("2")) {
            tc.setText(getResources().getString(R.string.findmshop));
        } else if (k.equalsIgnoreCase("3")) {
            tc.setText(getResources().getString(R.string.findcshop));
        } else if (k.equalsIgnoreCase("4")) {
            tc.setText(getResources().getString(R.string.findchickenshop));
        } else if (k.equalsIgnoreCase("5")) {
            tc.setText(getResources().getString(R.string.findfishshop));
        }
        }catch (NullPointerException e){

        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customer_Butcher_List_Fragment.this.goBack();
            }
        });
        final String[] colors = {"#d62041", "#d62041", "#d62041"};


        AHBottomNavigation bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        try {
            if(getIntent().getStringExtra("from").toString().equalsIgnoreCase("addmoreitem"))
            {
                k="1";
                new DownloadData1().execute();
            }

            // bottomNavigation.
        }catch (Exception e){

        }
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.BeefMeat, R.drawable.beef_img, R.color.colorAccent);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.MuttonMeat, R.drawable.mutton, R.color.colorAccent);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.CamelMeat, R.drawable.camel, R.color.colorAccent);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.ChickenMeat, R.drawable.other, R.color.colorAccent);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.Fishes, R.drawable.fish, R.color.colorAccent);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);
        //  bottomNavigation.addItem(item6);
        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

// Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

        // Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

// Display color under navigation bar (API 21+)
// Don't forget these lines in your style-v21
// <item name="android:windowTranslucentNavigation">true</item>
// <item name="android:fitsSystemWindows">true</item>
        bottomNavigation.setTranslucentNavigationEnabled(true);

// Manage titles
        //  bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        //  bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

// Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

// Set current item programmatically
        bottomNavigation.setCurrentItem(1);


// Enable the translation of the FloatingActionButton
        //  bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);

// Change colors
        //  bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        //bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

// Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#d62041"));

        bottomNavigation.setAccentColor(Color.parseColor("#d62041"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        //  Enables Reveal effect
        bottomNavigation.setColored(true);
      //  bottomNavigation.setCurrentItem(Integer.parseInt(k) - 1);
try {


        bottomNavigation.setCurrentItem(Integer.parseInt(k) - 1);
        // bottomNavigation.
}catch (NumberFormatException e){
   // bottomNavigation.setCurrentItem(Integer.parseInt(k) - 1);
}
        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (position == 0) {
                    k = "1";
                    tc.setText(getResources().getString(R.string.findbeefshop));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_List_Fragment.this);
                      /*  com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();*/

                    }
                } else if (position == 1) {
                    k = "2";
                    tc.setText(getResources().getString(R.string.findmshop));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_List_Fragment.this);
                      /*  com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();
*/

                    }
                } else if (position == 2) {
                    k = "3";
                    tc.setText(getResources().getString(R.string.findcshop));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_List_Fragment.this);
                       /* com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();
*/
                    }
                } else if (position == 3) {
                    k = "4";
                    tc.setText(getResources().getString(R.string.findchickenshop));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_List_Fragment.this);
                       /* com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();*/


                    }
                } else if (position == 4) {
                    k = "5";
                    tc.setText(getResources().getString(R.string.findfishshop));

                  if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_List_Fragment.this);
                       /* com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();
*/
                }
                }
                return true;
            }
        });

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        // Vertical
      //  OverScrollDecoratorHelper.setUpOverScroll(recyclerview, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
     //   new HorizontalOverScrollBounceEffectDecorator(new RecyclerViewOverScrollDecorAdapter(recyclerview));
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerview.setLayoutManager(layoutManager1);

      //  recyclerview.setAdapter(adapter1);


        //End of BottomBar menu
        Loc_ic = (ImageView) findViewById(R.id.loc_ic);

        Lct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
         /*       Intent is = new Intent(getApplicationContext(), DashboardActivity1.class);
                *//*is.putExtra("cat_id", cat_id);
                is.putExtra("value",str);*//*
                startActivity(is);
                finish();*/
                final Dialog dialog = new Dialog(Customer_Butcher_List_Fragment.this);

                //  dialog.setContentView(R.layout.loc_popup);
                final Window window = dialog.getWindow();
                window.requestFeature(Window.FEATURE_NO_TITLE);
                window.setContentView(R.layout.loc_test);
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                AutoCompleteTextView autoCompView = (AutoCompleteTextView) dialog.findViewById(R.id.autoCompleteTextView);
                //tc= (TextView).p findViewById(R.id.tc);
                autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter1(Customer_Butcher_List_Fragment.this, R.layout.list_item));
                autoCompView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                    }
                });
                //autoCompView.setOnItemClickListener(Customer_Butcher_List_Fragment.this);
                autoCompView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        str = (String) adapterView.getItemAtPosition(i);

                        if (DetectConnection
                                .checkInternetConnection(Customer_Butcher_List_Fragment.this)) {
                            //Toast.makeText(getActivity(),
                            //	"You have Internet Connection", Toast.LENGTH_LONG)
                            new DownloadData3().execute();
                            //   new DownloadData().execute();

                        } else {
                            com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                    .show(Customer_Butcher_List_Fragment.this);


                        }


                        dialog.dismiss();
                    }
                });


                ImageView dropDownImage = (ImageView) dialog.findViewById(R.id.sp1);
                dropDownImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loc.performClick();
                    }
                });



                dialog.show();


            }
        });
        map_icon_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent is = new Intent(getApplicationContext(), DashboardActivity1.class);
                /*is.putExtra("cat_id", cat_id);
                is.putExtra("value",str);*/
                String s= String.valueOf(latitude);
                String sl= String.valueOf(longitude);
                is.putExtra("lat",lat);
                is.putExtra("lng",lng);
                is.putExtra("strnew",str);
                is.putExtra("cat_id",k);
                startActivity(is);
                finish();
            }
        });
        try{


        if (str.equalsIgnoreCase("1")){




        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Customer_Butcher_List_Fragment.this);


        }
        }else {
            new DownloadData3().execute();
        }
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + k);
        }catch (NullPointerException e){

        }
    }






    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void goBack() {

        Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);
        finish();
    }


    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components="+"country:"+cid);
            sb.append("&radius=3000");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            trustEveryone();
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    private static void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

    public static void changeindex() {
        Lct.performClick();
    }

    public class GooglePlacesAutocompleteAdapter1 extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter1(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;
//

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);

            String l = getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>" + l);
            System.out.println("<<<<<<lattitude>>>>>" + lng);
            System.out.println("<<<<<<longitude>>>>>" + lat);
            if (l.equalsIgnoreCase("English")) {
                Wurl =SERVER_CUSTOMER+"shopbycat.php?cat_id="+k+"&longitude="+lng+"&latitude="+lat;
                // Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat.php?cat_id="+k;
            } else {
                Wurl = SERVER_CUSTOMER_AR+"shopbycat.php?cat_id="+k+"&longitude="+lng+"&latitude="+lat;
                //  Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/shopbycat.php?cat_id="+k;
                System.out.println("<<<<<<longitude>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/shopbycat.php?cat_id="+k+"&longitude="+lng+"&latitude="+lat);
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl//+"&longitude="+lng+"&latitude="+lat
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            progress_loader.setVisibility(ProgressBar.GONE);
            // String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
       if(result != null && !result.isEmpty() && !result.equals("null")&&!result.equalsIgnoreCase("")) {
                JSONArray mArray;
                zwm = new ArrayList<Customer_Butcher_List_Model>();
                try {
                    recyclerview.setVisibility(View.VISIBLE);
                    System.out.println("<<<<<<Rejeesh>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/shopbycat.php?cat_id="+k+"&longitude="+lng+"&latitude="+lat);
                    System.out.println("<<<<<<Rejeesh>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat.php?cat_id="+k+"&longitude="+lng+"&latitude="+lat);
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        wm = new Customer_Butcher_List_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        id = mJsonObject.getString("id");
                        added_by = mJsonObject.getString("added_by");
                        cat_id = mJsonObject.getString("cat_id");
                        butcher_logo = mJsonObject.getString("butcher_logo");
                        shop_name = mJsonObject.getString("shop_name");
                        delivery_info = mJsonObject.getString("delivery_info");
                        butcher_discount = mJsonObject.getString("butcher_discount");
                        shop_addrs= mJsonObject.getString("shop_addrs");
                        shop_description = mJsonObject.getString("shop_description");
                        discount_desc= mJsonObject.getString("discount_desc");
                      distance=mJsonObject.getString("distance");
                       // butcher_discount
                        //  String o = mJsonObject.getString("specialty_other");
                        wm.setId(id);
                        wm.setAdded_by(added_by);
                        wm.setCat_id(cat_id);
                        wm.setButcher_logo(butcher_logo);
                        wm.setShop_name(shop_name);
                        wm.setDelivery_info(delivery_info);
                        wm.setShop_description(shop_description);
                        wm.setShop_addrs(shop_addrs);
                        wm.setButcher_discount(butcher_discount);
                        wm.setDiscount_desc(discount_desc);
                        wm.setDistance(distance);
                      //  wm.setD
                        //  wm.s

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        //   System.out.println("<<<oo>>>>" + o);
                        //   onComplete();

                        zwm.add(wm);
                        System.out.println("<<<oo>>>>" + wm);

                        adapter1 = new Customer_Butcher_List_adapter(zwm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
       else {

           System.out.println("<<<<<<Rejeeshttt>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat.php?cat_id=" + k + "&longitude=" + lng + "&latitude=" + lat);
           recyclerview.setVisibility(View.GONE);


           if (!((Activity) Customer_Butcher_List_Fragment.this).isFinishing()) {
               new SweetAlertDialog(Customer_Butcher_List_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                       .setCustomImage(R.drawable.logo)
                       .setTitleText(getResources().getString(R.string.Sorry))
                       .setContentText(getResources().getString(R.string.Wearerapidlyexpandingwewillletyouknowwhenourshopinavailableinyourarea))
                       .setConfirmText(getResources().getString(R.string.Change) + "     " + "\n" + getResources().getString(R.string.location))
                       .setCancelText(getResources().getString(R.string.contactus))
                       .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                           @Override
                           public void onClick(SweetAlertDialog sweetAlertDialog) {
                               Intent i = new Intent(getApplicationContext(), Contact_Our_Team.class);
                               // i.putExtra("cat_id",cat_id);
                               //i.putExtra("value",status);

                               startActivity(i);
                               finish();
                               sweetAlertDialog.dismiss();
                           }
                       })
                       .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                           @Override
                           public void onClick(SweetAlertDialog sDialog) {

                               Lct.performClick();

                               sDialog.dismiss();

                           }
                       })
                       .show();

           }
       }


            }

        }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(this, Customer_Butcher_List_Fragment.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();

                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();

                        editor.putString("longitude", String.valueOf(longitude));
                        editor.putString("latitude", String.valueOf(latitude));
                        editor.commit();
                        // \n is for new line
                        System.out.println("----------------------------------------");
                        System.out.println("inside cangetlocation");
                        System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                        System.out.println("----------------------------------------");




                        /*Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        int a = gps.showSettingsAlert(Customer_Butcher_List_Fragment.this);


                    }

                } else {
                    com.nispok.snackbar.Snackbar.with(Customer_Butcher_List_Fragment.this) // context
                            .text(getResources().getString(R.string.you_need_to_grant_permission)) // text to display
                            .show(Customer_Butcher_List_Fragment.this);

                }


                return;
            }
        }
    }

 class DownloadData3 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;

        StringBuilder stringBuilder;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User


            String strnew = str.replaceAll(" ", "%20");
            String uri = "http://maps.google.com/maps/api/geocode/json?address="+strnew+"&sensor=false";
            HttpGet httpGet = new HttpGet(uri);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
           stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

           // JSONObject jsonObject = new JSONObject();
            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result1);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());

                double lngd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                double latd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.d("latitude", "" + latd);
                Log.d("longitude", "" + lngd);
                lng = String.valueOf(lngd);
                lat = String.valueOf(latd);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlng>>>>>>>>>>>>>>>" + lng);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlat>>>>>>>>>>>>>>>" + lat);
                new DownloadData1().execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
        Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);
        finish();
    }



}