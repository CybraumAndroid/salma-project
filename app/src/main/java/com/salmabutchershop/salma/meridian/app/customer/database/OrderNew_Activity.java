package com.salmabutchershop.salma.meridian.app.customer.database;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.address.AddressFragment;
import com.salmabutchershop.salma.meridian.app.customer.address.DatabaseHandler_N;
import com.salmabutchershop.salma.meridian.app.customer.address.ViewDatas_N;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class OrderNew_Activity extends AppCompatActivity {

    String name;
    int qty;
   // int tot,cart_tot=0;
    int price;
    int container_charge=2;
    private Context context;
    SQLiteDatabase db;
    TextView tv_tot,grand,tt_grand;
    String cart;
    //int gtot=0,cart_tot2=0;
    Button cnte;
    int cnt;
    int l=2;
    TextView ADD;
float subtotal=0f;
    String sub_tt;
    ArrayList<CartModel> arrayListcart=new ArrayList<>();
    ArrayList<JsonModel>   array_lstcart=new ArrayList<>();
    String dlvry_shd;
    JsonModel s;
    private DatabaseHandler_N dh;
    String inste;
    float gtot=0,cart_tot2=0;
    RadioGroup radio_group,radio_group_patient;
    RadioButton radioMale,radioFemale,radioMales,radioFemales;
    String rads,radm,but_id;
    LinearLayout L1;
    float tot,cart_tot=0;
    Float m;
    EditText Inst;
    Button ADD_MORE;
    String Reg_id,min_order_amount,del_charge;
    Float mo;
    Cursor cursor;
    Float del_c;
    TextView del_v;
   float delk=0;
    Float delcc;

    String Cur_cy,cus_instruction;
    TextView curency1,currency2,currency3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        //  super.setTheme(android.R.style.Theme);
        setContentView(R.layout.tablelatest_layout);
        grand=(TextView)findViewById(R.id.textView20);
        tt_grand=(TextView)findViewById(R.id.textView2);
        Inst= (EditText) findViewById(R.id.inst);
del_v=(TextView)findViewById(R.id.delv2);
        curency1=(TextView)findViewById(R.id.textView5);
        currency2=(TextView)findViewById(R.id.cur2);
        currency3=(TextView)findViewById(R.id.cur3);
        Typeface font8= Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        tt_grand.setTypeface(font8);
        ADD_MORE= (Button) findViewById(R.id.add_more);
        SharedPreferences myPrefs = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);
        Cur_cy= myPrefs.getString("currency", null);

        cus_instruction= myPrefs.getString("cus_instruction", null);

        System.out.println("cus_instructionstarttt............"+ cus_instruction);
        Inst.setText(cus_instruction);



        Inst.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
// TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
// TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("cus_instruction",s.toString());
                System.out.println("cus_instruction............"+ Inst.getText().toString()+s);
                System.out.println();
                editor.commit();


            }

        });

        curency1.setText(Cur_cy);
        currency2.setText(Cur_cy);
        currency3.setText(Cur_cy);
        if (Reg_id != null && !Reg_id.isEmpty()){

        }
        else {
            new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText(getResources().getString(R.string.Pleaseregister))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
finish();
                            sDialog.dismiss();

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();

        }
            Intent intent = getIntent();
       but_id = intent.getStringExtra("but_id");
        min_order_amount= intent.getStringExtra("min_order_amount");
      /*  del_charge=intent.getStringExtra("del_charge");
        del_c= Float.valueOf(del_charge);
       String sd= String.valueOf(del_c);
        del_v.setText(String.valueOf(del_c));*/
        System.out.println("-----------------------but_id-----------------"+but_id);
        System.out.println("-----------------------min_order_amount-----------------"+min_order_amount);
        tv_tot=(TextView)findViewById(R.id.textView20);
        Typeface font7= Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        tv_tot.setTypeface(font7);

        radio_group= (RadioGroup) findViewById(R.id.radio_group);

        radioMale= (RadioButton) findViewById(R.id.radioMale);
        Typeface font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        radioMale.setTypeface(font);
        radioFemale= (RadioButton) findViewById(R.id.radioFemale);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        radioFemale.setTypeface(font1);
       radio_group
               .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        Log.d("chk", "id" + checkedId);

                        if (checkedId == R.id.radioMale) {
                            //some code
                        } else if (checkedId == R.id.radioFemale) {
                            com.nispok.snackbar.Snackbar.with(OrderNew_Activity.this) // context
                                    .text(getResources().getString(R.string.CurrentlyOnlinePaymentNotAvailable)) // text to display
                                    .show(OrderNew_Activity.this);

                            radioMale.setChecked(true);
                            radioFemale.setChecked(false);
                        }

                    }

                });
        if(radioMale.isChecked())
        {
            rads=radioMale.getText().toString();
           // L1.setVisibility(View.VISIBLE);
        }
        else if(radioFemale.isChecked())
        {
           rads=radioFemale.getText().toString();
        }

        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +qty);
        System.out.println(">>>>>>cattttttttttttttttttttttttttttttttttttttttttt>>>>>>>>>" +name);
        System.out.println(">>>>>>mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmttt>>>>>>>>>" +price);
        ImageView back= (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
        ADD_MORE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*OrderNew_Activity.this.goBack1();*/
                Intent i=new Intent(OrderNew_Activity.this, Customer_Butcher_List_Fragment.class);
                i.putExtra("from","addmoreitem");
                startActivity(i);
                finish();
            }
        });
        cnte=(Button)findViewById(R.id.button);
//        final int foo = Integer.parseInt(grand.getText().toString());
        cnte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dh=new DatabaseHandler_N();
                Cursor c=dh.selectAllDATAS(getApplicationContext());
                int count=c.getCount();
                inste=Inst.getText().toString();
              String  fultot=tt_grand.getText().toString();
Float ft= Float.valueOf(fultot);

               mo=1f;

/*if(ft>=mo) {*/
    System.out.println("counttttttttttttttttttttttttt" + Float.toString(cart_tot));
    System.out.println("fullllllllllllllllllllll" + Float.toString(cart_tot2));


    if (Reg_id != null && !Reg_id.isEmpty()){
    if (count > 0) {

        Intent i = new Intent(getApplicationContext(), ViewDatas_N.class);

        i.putExtra("price", gtot);
        i.putExtra("item_name", name);
        i.putExtra("inst", inste);
        i.putExtra("but_id", but_id);
        i.putExtra("fultot", Float.toString(cart_tot * 2));
        i.putExtra("sub_tt", grand.getText().toString());
        System.out.println("detailll6" + gtot);
        startActivity(i);
        finish();
    } else {
        Intent i = new Intent(getApplicationContext(), AddressFragment.class);
        i.putExtra("id_nam", "libin");
        i.putExtra("price", gtot);
        i.putExtra("item_name", name);
        i.putExtra("fultot", Float.toString(cart_tot * 2));
        i.putExtra("inst", inste);
        i.putExtra("but_id", but_id);
        System.out.println("detailll6" + gtot);
        startActivity(i);
        finish();
    }

    }

    else {
        new SweetAlertDialog(OrderNew_Activity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText(getResources().getString(R.string.Pleaseregister))
                .setCustomImage(R.drawable.logo)
                .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                .setConfirmText(getResources().getString(R.string.Yes))
                .setCancelText(getResources().getString(R.string.No))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(i);

                        sDialog.dismiss();

                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();

    }

/*else {
    new SweetAlertDialog(OrderNew_Activity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
            .setTitleText(getResources().getString(R.string.MinimumOrderisAED)+"20")
            .setCustomImage(R.drawable.logo)
            .setContentText(getResources().getString(R.string.PleasecompleteMinimumordertoproceedtothecart))
            .setConfirmText(getResources().getString(R.string.addmoreotems))
            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    finish();
                }
            })
            .showCancelButton(true)
            .show();
}*/

            }
        });
        tot = qty * price;
        gtot=gtot+tot;

        context = this;

        Gson gson=new Gson();
        final DatabaseHelper dataHelper = new DatabaseHelper(context);
        //  dataHelper.insertData(name, qty, price, tot,gtot);
        final TableLayout tableLayout = (TableLayout) findViewById(R.id.tl);


        TableRow rowHeader = new TableRow(context);


        rowHeader.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 100));
        rowHeader.setBackgroundResource(R.drawable.text_bg);
        View line = new View(this);
        line.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, 100));
        line.setBackgroundColor(Color.rgb(51, 51, 51));
        rowHeader.addView(line);
        String[] headerText = {getResources().getString(R.string.products), getResources().getString(R.string.quantity), getResources().getString(R.string.price)+"("+Cur_cy+")", getResources().getString(R.string.total)+"("+Cur_cy+")",""};
        tableLayout.setStretchAllColumns(true);
        for (String c : headerText)
        {
            TextView tv = new TextView(this);
            Typeface font9= Typeface.createFromAsset(getAssets(), "roboto.bold.ttf");
           tv.setTypeface(font9);

            tv.setTextColor(Color.parseColor("#000000"));
            tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, 100));
            tv.setGravity(Gravity.CENTER);

            tv.setTextSize(14);
            tv.setText(c);

            rowHeader.addView(tv);
        }

        tableLayout.addView(rowHeader);


        db = dataHelper.getReadableDatabase();
        db.beginTransaction();
        try {
            String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
            System.out.println("" + selectQuery);
          cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() >0) {
                while (cursor.moveToNext()) {
                    CartModel cartmodl=new CartModel();
                    final int outlet_id = cursor.getInt(cursor.getColumnIndex("outlet_id"));
                    String outlet_name = cursor.getString(cursor.getColumnIndex("outlet_name"));
                    final Float outlet_qty = cursor.getFloat(cursor.getColumnIndex("outlet_qty"));
                    final Float outlet_price = cursor.getFloat(cursor.getColumnIndex("outlet_price"));
                    final Float outlet_tot = cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                    Float grnd = cursor.getFloat(cursor.getColumnIndex("total"));
                    String pro_id= cursor.getString(cursor.getColumnIndex("pro_id"));
            final Float delvc= cursor.getFloat(cursor.getColumnIndex("fds"));
                    final String price_quantity=cursor.getString(cursor.getColumnIndex("price_quantity"));
                    String kg_or_g=cursor.getString(cursor.getColumnIndex("kg_or_g"));
                    String value_befr_kg_or_g=cursor.getString(cursor.getColumnIndex("value_befr_kg_or_g"));
                    final Float price_for_1kg=cursor.getFloat(cursor.getColumnIndex("price_for_1kg"));
                    final Float price_for_1_gram=cursor.getFloat(cursor.getColumnIndex("price_for_1_gram"));


                    subtotal+=outlet_tot;
                    System.out.println("+++++++++++++++++++++++++++++++++subtotal : "+subtotal);
                  // delk=0f;

                    delk =delk+delvc;
                    System.out.println("<<<<<<<<<<<<uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu>>>>>>>>>>>>" +Float.toString(delk));
                    del_v.setText(Float.toString(delk));
                    System.out.println("grandtotalll" + grnd);
                    cartmodl.setId(outlet_id);
                    cartmodl.setNam(outlet_name);
                    cartmodl.setQty(outlet_qty);
                    cartmodl.setPrice(outlet_price);
                    cartmodl.setGrnd(grnd);
                   // arrayListcart.add(cartmodl);
                    arrayListcart.add(cartmodl);
String s= String.valueOf(outlet_tot);
                    //  s=new JsonModel();
                    System.out.println("<<<<<<<<<<<<outlettot>>>>>>>>>>>>" +s);
                    System.out.println("course2" + outlet_name);
                    System.out.println("sub_coursecoursereg" +outlet_qty);
                    System.out.println("sub_coursecourseregid" + outlet_price);
                    System.out.println("sub_coursecourseregid" + outlet_tot);
                    System.out.println("sub_coursecourseregid" + grnd);


                    final TableRow row = new TableRow(context);
                    row.setBackgroundResource(R.drawable.layer_nw);
                    row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 100));
                    row.setMinimumHeight(50);
                    //tr.addView(view);
                    String[] colText = {"" + outlet_name, "" + outlet_qty, "" + outlet_price, "" + outlet_tot};

////////////////////////////////////////////////////////////////////////// 1ST CHANGE START /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    TextView outlet_nameTV=new TextView(this);
                    Typeface font2= Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
                    outlet_nameTV.setTypeface(font2);
                    outlet_nameTV.setTextColor(Color.parseColor("#000000"));
                    outlet_nameTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,100));
                    outlet_nameTV.setGravity(Gravity.CENTER);
                    outlet_nameTV.setTextSize(12);

                    outlet_nameTV.setText(outlet_name);


                    final EditText outlet_qtyTV=new EditText(this);
                    Typeface font3= Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
                    outlet_qtyTV.setTypeface(font3);
                    outlet_qtyTV.setTextColor(Color.parseColor("#000000"));
                    outlet_qtyTV.setBackgroundResource(R.drawable.border);

                    outlet_qtyTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,80));
                    outlet_qtyTV.setGravity(Gravity.CENTER);
                    outlet_qtyTV.setTextSize(16);
                    outlet_qtyTV.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    outlet_qtyTV.setText(Float.toString(outlet_qty));


                    TextView outlet_priceTV=new TextView(this);
                    Typeface font4= Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
                    outlet_priceTV.setTypeface(font4);
                    outlet_priceTV.setTextColor(Color.parseColor("#000000"));
                    outlet_priceTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,100));
                    outlet_priceTV.setGravity(Gravity.CENTER);
                    outlet_priceTV.setTextSize(16);
                    outlet_priceTV.setText(Float.toString(outlet_price)+"/"+price_quantity);


                    final TextView outlet_totTV=new TextView(this);
                    Typeface font5= Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
                    outlet_totTV.setTypeface(font5);
                    outlet_totTV.setTextColor(Color.parseColor("#000000"));
                    outlet_totTV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,100));
                    outlet_totTV.setGravity(Gravity.CENTER);
                    outlet_totTV.setTextSize(16);
                    Locale.setDefault(Locale.US);
                    outlet_totTV.setText(Float.toString(Float.valueOf(new DecimalFormat("##.##").format(outlet_tot))));


                    row.addView(outlet_nameTV);
                    row.addView(outlet_qtyTV);
                    row.addView(outlet_priceTV);
                    row.addView(outlet_totTV);
                    tableLayout.addView(row);



                    outlet_qtyTV.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            //Toast.makeText(getApplicationContext(), " aftertextchanged", Toast.LENGTH_SHORT).show();


                            try{
                                if (outlet_qtyTV.getText().length() == 0) {


                                }/* else if (Float.parseFloat(outlet_qtyTV.getText().toString()) == 0) {
                                    outlet_qtyTV.setText("0.2");
                                }*/ else {

                                    Float new_qty = Float.parseFloat(outlet_qtyTV.getText().toString());
                                    /*if (new_qty == 500.0) {
                                        new_qty = new_qty / 1000;
                                    }*/

                                    System.out.println("new_qty :"+new_qty);
                                    System.out.println("outlet_price :"+outlet_price);
                                    System.out.println("outlet_qty :"+outlet_qty);

                                    float divider=0f;
                                    Float new_total=0f;
                                    String kg_or_g = (price_quantity.substring(Math.max(price_quantity.length() - 2, 0))).trim();//getting 'gm' or 'kg'
                                    if(kg_or_g.contentEquals("gm")){
                                        System.out.println("inside 'gm'");
                                        divider=Float.parseFloat(price_quantity.substring(0,price_quantity.length() - 2).trim());
                                        divider/=1000;
                                        new_total= new_qty * outlet_price/divider;
                                        System.out.println("new_total = "+"("+new_qty+"*"+outlet_price+")/"+divider+" = "+new_total);

                                        System.out.println("divider : "+divider);
                                    }else{
                                        divider=Float.parseFloat(price_quantity.substring(0,price_quantity.length() - 2).trim());
                                        System.out.println("divider : "+divider);
                                        System.out.println("new_total = "+"("+new_qty+"*"+outlet_price+")/"+divider+" = "+new_total);
                                        new_total= new_qty * outlet_price/divider;
                                    }


                                    System.out.println("price_quantity.substring(0,price_quantity.length() - 2) :"+price_quantity.substring(0,price_quantity.length() - 2));

                                    outlet_totTV.setText(Float.toString(Float.valueOf(new DecimalFormat("##.##").format(new_total))));
                                    System.out.println("////////////////////////////////\nNew Total :" + new_total + "\n/////////////////////////////////////");
                                    dataHelper.UpdateAmount(outlet_id, new_qty, new_total);
                                    cnt = tableLayout.getChildCount();


                                    db = dataHelper.getReadableDatabase();
                                    try {
                                        cart_tot2 = 0;
                                        String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                        System.out.println("" + selectQuery);
                                        Cursor cursor = db.rawQuery(selectQuery, null);

                                        if (cursor.getCount() > 0) {
                                            while (cursor.moveToNext()) {
                                                cart_tot2 += cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                                                System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ncartttotal" + cart_tot2 + "\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                            }

                                        }

                                        tv_tot.setText(Float.toString(Float.parseFloat(new DecimalFormat("##.##").format(cart_tot2))));
                                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg"+cart_tot2);

                                        tt_grand.setText(Float.toString(Float.parseFloat(new DecimalFormat("##.##").format(cart_tot2))));

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                               /* if(((outlet_qtyTV.getText().toString()).indexOf("."))==-1){

                                    outlet_qtyTV.setText((outlet_qtyTV.getText().toString())+".0");

                                }
                                else*/ if((outlet_qtyTV.getText().toString()=="0.5")){
                                    outlet_totTV.setText(outlet_qtyTV.getText().toString()+0.5);
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }




                        }

                    });






                    ImageView views = new ImageView(OrderNew_Activity.this);
                    views.setLayoutParams(new TableRow.LayoutParams(40,40));
                    row.setGravity(Gravity.CENTER);
                    views.setImageResource(R.drawable.ic_close_black_24dp);
                    row.addView(views);
                    cnt = tableLayout.getChildCount();

                    if (cnt < 2) {
                        tableLayout.removeView(row);
                        cart_tot = 0;
                        cart = Float.toString(cart_tot);
                        tv_tot.setText(new DecimalFormat("##.##").format(Float.parseFloat(cart)));
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg1"+cart);
                        m=cart_tot+container_charge;
                        sub_tt=Float.toString(cart_tot);
                        tt_grand.setText(new DecimalFormat("##.##").format(Float.parseFloat(sub_tt)));
                        System.out.println("count" + cnt);


                    } else {

                        cart_tot = cart_tot + outlet_tot;
                        cart = Float.toString(cart_tot);
                        tv_tot.setText(new DecimalFormat("##.##").format(Float.parseFloat(cart)));
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg2"+cart);
                        m=cart_tot+container_charge;
                        sub_tt=Float.toString(cart_tot);
                        tt_grand.setText(new DecimalFormat("##.##").format(Float.parseFloat(sub_tt)));
                        System.out.println("count" + cnt);
                        System.out.println("outlettotal" + outlet_tot);
                        System.out.println("cartttotal" + cart_tot);

                    }
//////////////////////////////////////////////////////////////////////////////// 2ND CHANGE START /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    views.setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {
                            new SweetAlertDialog(OrderNew_Activity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setCustomImage(R.drawable.logo)
                                    .setTitleText(getResources().getString(R.string.Delete))
                                    .setContentText(getResources().getString(R.string.AreyousureWanttodeletetheproduct))
                                //  .setContentText("Want to delete this cart item ?")
                                    .setConfirmText(getResources().getString(R.string.Yes))
                                    .setCancelText(getResources().getString(R.string.No))
                                    .showCancelButton(true)

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            tableLayout.removeView(row);


                                            dataHelper.delete(outlet_id);
                                            db = dataHelper.getReadableDatabase();
                                            try {
                                                cart_tot2=0;
                                                String selectQuery = "SELECT * FROM " + DatabaseHelper.TABLE_OUTLET;
                                                System.out.println("" + selectQuery);
                                                Cursor cursor = db.rawQuery(selectQuery, null);

                                                if (cursor.getCount() > 0) {
                                                    while (cursor.moveToNext()) {


                                                        cart_tot2+= cursor.getFloat(cursor.getColumnIndex("outlet_tot"));
                                                        System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ncartttotal" + cart_tot2+"\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                                                        final DatabaseHelper dataHelper = new DatabaseHelper(context);
                                                        int si = (int) dataHelper.getProfilesCount();
                                                        BeefDetail.changeindex1(si);
                                                        Customer_Services_Fragment.counterFab.setCount(si);





                                                    }

                                                }
                                                else{
                                                    final DatabaseHelper dataHelper = new DatabaseHelper(context);
                                                    int si = (int) dataHelper.getProfilesCount();
                                                    BeefDetail.changeindex1(si);
                                                    Customer_Services_Fragment.counterFab.setCount(si);



                                                    goBack();
                                                }
                                                tv_tot.setText(Float.toString(cart_tot2));
                                                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<fg3"+cart_tot2);
                                                tt_grand.setText(Float.toString(cart_tot2));
                                                final DatabaseHelper dataHelper = new DatabaseHelper(context);
                                                int si = (int) dataHelper.getProfilesCount();
                                                Customer_Services_Fragment.counterFab.setCount(si);

                                                BeefDetail.changeindex1(si);
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }



                                            sDialog

                                                    .setTitleText(getResources().getString(R.string.Deleted))
                                                    .setContentText(getResources().getString(R.string.YourCartItemhasbeen))
                                                    .setConfirmText(getResources().getString(R.string.ok))
                                                    .setConfirmClickListener(null)
                                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                            sDialog.dismiss();
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();


                        }
                    });


                }
            }
            db.setTransactionSuccessful();

        } catch (SQLiteException e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();
            // End the transaction.
            db.close();
            // Close database
        }
    }

    private void goBack1() {

        Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
        startActivity(i);
        finish();

    }

    @Override
    public void onBackPressed() {

        if (cursor.getCount() >0) {
            new SweetAlertDialog(OrderNew_Activity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("")
                    .setTitleText(getResources().getString(R.string.Exit))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.DoyouwanttosaveyourCartitem))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                            dataHelper.removeAll();

                            int si = (int) dataHelper.getProfilesCount();
                            BeefDetail.changeindex1(si);
                            Customer_Services_Fragment.counterFab.setCount(si);

                            Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                            startActivity(i);
                            finish();

                            sweetAlertDialog.dismiss();
                            /*sweetAlertDialog.onBackPressed();*/
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            final DatabaseHelper dataHelper = new DatabaseHelper(context);
                            int si = (int) dataHelper.getProfilesCount();
                            BeefDetail.changeindex1(si);
                            Customer_Services_Fragment.counterFab.setCount(si);

                           Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                           startActivity(i);
                            finish();

                        }
                    })
                    .show();

        }
        else {
            finish();
        }
      /*  if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Snackbar.with(OrderNew_Activity.this,null)
                    .type(DisplayContext.Type.SUCCESS)

                    .message("Press Again to Exit from Zulekha !")
                    .duration(android.support.design.widget.Snackbar.Duration.LONG)

                    .show();
        }
        back_pressed = System.currentTimeMillis();*/
        //Do the Logics Here
    }
    private void goBack() {
        final DatabaseHelper dataHelper = new DatabaseHelper(context);
        int si = (int) dataHelper.getProfilesCount();
        if (si >0) {
            new SweetAlertDialog(OrderNew_Activity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("")
                    .setTitleText(getResources().getString(R.string.Exit))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.DoyouwanttosaveyourCartitem))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
                            dataHelper.removeAll();

                            int si = (int) dataHelper.getProfilesCount();
                            BeefDetail.changeindex1(si);
                            Customer_Services_Fragment.counterFab.setCount(si);

                            Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                            startActivity(i);
                            finish();

                            sweetAlertDialog.dismiss();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            final DatabaseHelper dataHelper = new DatabaseHelper(context);
                            int si = (int) dataHelper.getProfilesCount();
                            BeefDetail.changeindex1(si);
                            Customer_Services_Fragment.counterFab.setCount(si);
                            Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        }
                    })
                    .show();
        }
        else {
            finish();
        }

      // finish();
    }

}
