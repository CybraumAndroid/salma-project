package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;


import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER_AR;

/**
 * Created by libin on 9/29/2016.
 */

public class Customer_Butcher_Sublist_Fragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

    Customer_Butcher_Sublist_adapter adapter;
    ProgressBar progress;
    String result, galry_image,Dat;
    static ArrayList<Customer_Butcher_Sublist_Model> esm;
    Customer_Butcher_Sublist_Model sm;
    String added_by,cat_id,subcat_name,sub_image,sub_id;
    String k,l;
    Button Beef,Mutton,Camel,Other,Othe,Fish;
    LinearLayout other;
    public static MKLoader progress_loader;
    String Wurl;
    String shp_nam,del_info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sponsors_fragment_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        Intent intent = getIntent();
        k = intent.getStringExtra("add_id");
        l= intent.getStringExtra("cat_id");
       /* shp_nam=intent.getStringExtra("shp_nam");
        del_info=intent.getStringExtra("del_info");*/
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
      //  i.putExtra("shp_nam",d);
       // i.putExtra("del_info",k);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +shp_nam);
        System.out.println(">>>>>>cattttttttttttttttttttttttttttttttttttttttttt>>>>>>>>>" +del_info);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        other= (LinearLayout) findViewById(R.id.other);
        other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l = "6";
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                    new DownloadData1().execute();
                } else {
                    com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Customer_Butcher_Sublist_Fragment.this);
                 /*   com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/


                }

            }
        });
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customer_Butcher_Sublist_Fragment.this.goBack();
            }
        });
        final String[] colors = {"#d62041", "#d62041", "#d62041"};

       /* final SquareFragment fragment = new SquareFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("color", Color.parseColor(colors[0]));
        fragment.setArguments(bundle);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, fragment, "square")
                .commit();*/

        AHBottomNavigation bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        // AHBottomNavigationItem item1 = new AHBottomNavigationItem(getString(R.string.Beef), R.drawable.beef_img, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item2 = new AHBottomNavigationItem(getString(R.string.Mutton), R.drawable.mutton, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item3 = new AHBottomNavigationItem(getString(R.string.Camel), R.drawable.camel, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item4 = new AHBottomNavigationItem(getString(R.string.Chicken), R.drawable.other, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item5 = new AHBottomNavigationItem(getString(R.string.Fish), R.drawable.fish, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item6 = new AHBottomNavigationItem(getString(R.string.tab_3), R.drawable.fish, Color.parseColor(colors[0]));
// Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.BeefMeat, R.drawable.beef_img, R.color.colorAccent);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.MuttonMeat, R.drawable.mutton, R.color.colorAccent);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.CamelMeat, R.drawable.camel, R.color.colorAccent);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.ChickenMeat, R.drawable.other, R.color.colorAccent);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.Fishes, R.drawable.fish, R.color.colorAccent);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);
        //  bottomNavigation.addItem(item6);
        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

// Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

        // Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

// Display color under navigation bar (API 21+)
// Don't forget these lines in your style-v21
// <item name="android:windowTranslucentNavigation">true</item>
// <item name="android:fitsSystemWindows">true</item>
        bottomNavigation.setTranslucentNavigationEnabled(true);

// Manage titles
        //  bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        //  bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

// Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

// Set current item programmatically
        bottomNavigation.setCurrentItem(1);


// Enable the translation of the FloatingActionButton
        //  bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);

// Change colors
        //  bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        //bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

// Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#d62041"));

        bottomNavigation.setAccentColor(Color.parseColor("#d62041"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        //  Enables Reveal effect
        bottomNavigation.setColored(true);

        bottomNavigation.setCurrentItem(Integer.parseInt(l)-1);
        // bottomNavigation.

        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if(position==0){
                    l = "1";
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {

                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                                .text("Oops Your Connection Seems Off..!") // text to display
                                .show(Customer_Butcher_Sublist_Fragment.this);

                    }
                }
                else if (position==1){
                   l="2";
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {

                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_Sublist_Fragment.this);

                    }
                }
                else if (position==2){
                   l="3";
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_Sublist_Fragment.this);

                    }
                }
                else if (position==3){
                   l="4";
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_Sublist_Fragment.this);
                    }
                }
                else if (position==4){
                  l="5";
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {

                        com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Customer_Butcher_Sublist_Fragment.this);
                    }
                }
                return true;
            }
        });
        //End of BottomBar menu
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {

            com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Customer_Butcher_Sublist_Fragment.this);

        }
       // new DownloadData1().execute();

    }

    private void goBack() {
        super.onBackPressed();
    }




    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);
            String lk=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>"+lk);
            if (lk.equalsIgnoreCase("English")) {
                Wurl=SERVER_CUSTOMER+"butcher_subcat.php?added_by="+k+"&cat_id="+l;
            }
            else{
                Wurl=SERVER_CUSTOMER_AR+"butcher_subcat.php?added_by="+k+"&cat_id="+l;
            }


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();

                System.out.println("<<<<<<<<<<<<<<<<,subinkuriakose>>>>>>>>>>>>>"+k+l);
                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
          //  String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
         //   System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
           /* if
                    (result.equalsIgnoreCase("")) {
                recyclerview.setVisibility(View.GONE);
                com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                        .text("No Products!") // text to display
                        .show(Customer_Butcher_Sublist_Fragment.this);
                *//*Snackbar.with(Customer_Butcher_Sublist_Fragment.this,null)
                        .type(Type.SUCCESS)
                        .message("No Products!")
                        .duration(Duration.LONG)
                        .show();*//*
                //Toast.makeText(getApplicationContext(), "No Products", Toast.LENGTH_SHORT).show();
            }  else */if(result != null && !result.isEmpty() && !result.equals("null")) {
                recyclerview.setVisibility(View.VISIBLE);
                JSONArray mArray;
                esm = new ArrayList<Customer_Butcher_Sublist_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        sm = new Customer_Butcher_Sublist_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);


                        added_by = mJsonObject.getString("added_by");
                        cat_id = mJsonObject.getString("cat_id");
                        sub_id = mJsonObject.getString("sub_id");
                        subcat_name = mJsonObject.getString("subcat_name");
                        sub_image = mJsonObject.getString("sub_image");

                        sm.setAdded_by(added_by);
                        sm.setCat_id(cat_id);
                        sm.setSub_id(sub_id);
                        sm.setSubcat_name(subcat_name);
                        sm.setSub_image(sub_image);


                        esm.add(sm);
                        System.out.println("<<<oo>>>>" + sm);

                        adapter = new Customer_Butcher_Sublist_adapter(esm, getApplicationContext());
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else{
                recyclerview.setVisibility(View.GONE);
                if(!((Activity) Customer_Butcher_Sublist_Fragment.this).isFinishing())
                {
                    //show dialog

                new SweetAlertDialog(Customer_Butcher_Sublist_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })
                        .show();
                }
              /*  com.nispok.snackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this) // context
                        .text("No Products!") // text to display
                        .show(Customer_Butcher_Sublist_Fragment.this);*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                                String added_id=esm.get(position).getAdded_by();
                                String sub_id=esm.get(position).getSub_id();
                                String title=esm.get(position).getSubcat_name();
                                Intent i=new Intent(getApplicationContext(),BeefDetail.class);
                                i.putExtra("add_id",added_id);
                                i.putExtra("sub_id",sub_id);
                                i.putExtra("title",title);
                              /*  i.putExtra("shp_nam",shp_nam);
                                i.putExtra("del_info",del_info);*/
                              //  shp_nam=intent.getStringExtra("shp_nam");
                            //    del_info=intent.getStringExtra("del_info");
                                //i.putExtra("value",status);
                                startActivity(i);
                                finish();
                               // overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                              //  finish();
                            }
                        })
                );
            }


        }
    }

