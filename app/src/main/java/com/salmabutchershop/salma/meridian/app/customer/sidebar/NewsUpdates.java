package com.salmabutchershop.salma.meridian.app.customer.sidebar;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Notfy_Model;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Notfy_adapter;
import com.salmabutchershop.salma.meridian.app.butcher.extras.NotificationFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

/**
 * Created by libin on 12/23/2016.
 */

public class NewsUpdates extends Activity  {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

    News_Updates_adapter adapter;
    ProgressBar progress;
    String result, galry_image,Dat;
    static ArrayList<NewsUpdates_Model> num;
    NewsUpdates_Model nm;
    String not_id,not_title,not_desc;
    String k,l;
    Button Beef,Mutton,Camel,Other;
    String news_id,news_title,news_desc,news_image;
    public static MKLoader progress_loader;
    String Reg_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newsupdatessidebar_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.layout);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);
        rl.setBackgroundColor(Color.parseColor("#ffffff"));
        Intent intent = getIntent();
        k = intent.getStringExtra("add_id");
        l= intent.getStringExtra("cat_id");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +k);
        System.out.println(">>>>>>cattttttttttttttttttttttttttttttttttttttttttt>>>>>>>>>" +l);
        if (ContextCompat.checkSelfPermission(NewsUpdates.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(NewsUpdates.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NewsUpdates.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);


        } else {
            /*com.nispok.snackbar.Snackbar.with(NewsUpdates.this) // context
                    .text("You already granted permission!") // text to display
                    .show(NewsUpdates.this);*/
          /*  Snackbar.with(NewsUpdates.this, null)
                    .type(Type.SUCCESS)
                    .message("You already granted permission!")
                    .duration(Duration.SHORT)
                    .show();*/
        }
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        // recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NewsUpdates.this.goBack();
            }
        });

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(NewsUpdates.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(NewsUpdates.this);
          /*  com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/

        }
        // new DownloadData1().execute();

    }

  /*  private void goBack() {
        super.onBackPressed();
    }*/






    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER_CUSTOMER+"newsupdates.php?user_id="+Reg_id
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
         //   String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
           // String value = result;
           /* if
                    (result.equalsIgnoreCase("[]")) {
                Toast.makeText(getApplicationContext(), "No News", Toast.LENGTH_SHORT).show();
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                num = new ArrayList<NewsUpdates_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        nm = new NewsUpdates_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);


                        news_id = mJsonObject.getString("news_id");
                        news_title = mJsonObject.getString("news_title");
                        news_desc = mJsonObject.getString("news_desc");
                        news_image = mJsonObject.getString("news_image");

                        nm.setNews_id(news_id);
                        nm.setNews_title(news_title);
                        nm.setNews_desc(news_desc);
                        nm.setNews_image(news_image);


                        num.add(nm);
                        System.out.println("<<<oo>>>>" + nm);

                        adapter = new News_Updates_adapter(num, getApplicationContext());
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                new SweetAlertDialog(NewsUpdates.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.No_News))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


                                startActivity(i);
                                finish();
                                sDialog.dismiss();
                            }
                        })
                        .show();
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }



            recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                System.out.println("<<<<<<<<<<<<whatsapppimage>>>>>>>>>>>>>>>"+num.get(position).getNews_image());
                              /*  Uri bmpUri=  Uri.parse( num.get(position).getNews_image());
                                if (bmpUri != null) {
                                    // Construct a ShareIntent with link to image
                                    Intent shareIntent = new Intent();
                                    shareIntent.setAction(Intent.ACTION_SEND);
                                    shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                                    shareIntent.setType("image*//*");
                                    // Launch sharing dialog for image
                              startActivity(Intent.createChooser(shareIntent, "Share Image"));
                                } else {
                                    // ...sharing failed, handle error
                                }*/

                                }
                        })
                );
            }


        }
    private void goBack() {
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    }
