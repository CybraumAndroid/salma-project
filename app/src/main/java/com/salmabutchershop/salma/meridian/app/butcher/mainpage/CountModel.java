package com.salmabutchershop.salma.meridian.app.butcher.mainpage;

/**
 * Created by libin on 2/14/2017.
 */
public class CountModel {
    public String getTotal() {
        return total;
    }

    public String getLatest_version() {
        return latest_version;
    }

    public void setLatest_version(String latest_version) {
        this.latest_version = latest_version;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAccepted() {
        return accepted;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEnable_popup() {
        return enable_popup;
    }

    public void setEnable_popup(String enable_popup) {
        this.enable_popup = enable_popup;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    String total,accepted,pending,latest_version,logo,valid,enable_popup;;
}
