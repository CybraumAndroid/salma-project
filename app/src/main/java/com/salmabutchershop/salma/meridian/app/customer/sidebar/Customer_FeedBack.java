package com.salmabutchershop.salma.meridian.app.customer.sidebar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

public class Customer_FeedBack extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    Button submit;
    EditText feedback;
    int selectedRadiobuttonId;
    private String USER_ID_KEY="user_id";
    private String MESSAGE_KEY="subject";
    private String SUBJECT_KEY="message";
    boolean edittexterror = false;
    private String feedbackMessage,subject;
    String  name,shop_name,location,phone,email,message,user_id;
    ProgressBar progress;
    EditText edtyourname,  edtshopname,  edtcountry , edtphone , edtemail,edtmsg,edtads1,edtads2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_feedback_layout);
        edtyourname = (EditText) findViewById(R.id.edt_yourname);
        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        edtyourname.setTypeface(myFont1);

        edtshopname = (EditText) findViewById(R.id.edt_shopnamee);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        edtshopname.setTypeface(myFont2);

        edtcountry = (EditText) findViewById(R.id.edt_countrylocation);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        edtcountry.setTypeface(myFont3);

        edtphone = (EditText) findViewById(R.id.edt_phone);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        edtphone .setTypeface(myFont4);

        edtemail= (EditText) findViewById(R.id.edt_email);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        edtemail.setTypeface(myFont5);

        edtmsg = (EditText) findViewById(R.id.feedback);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        edtmsg.setTypeface(myFont6);


        RelativeLayout rl = (RelativeLayout) findViewById(R.id.layout);
        rl.setBackgroundColor(Color.parseColor("#ffffff"));

        submit=(Button)findViewById(R.id.submit);

        progress = (ProgressBar) findViewById(R.id.progress_bar);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              Customer_FeedBack.this.goBack();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    if (edtyourname.getText().toString().isEmpty() || edtshopname.getText().toString().isEmpty() || edtcountry.getText().toString().trim().isEmpty() || edtphone.getText().toString().isEmpty()  || edtemail.getText().toString().isEmpty()) {
                        com.nispok.snackbar.Snackbar.with(Customer_FeedBack.this) // context
                                .text(getResources().getString(R.string.empty_fields))// text to display
                                .show(Customer_FeedBack.this);
                       /* com.chootdev.csnackbar.Snackbar.with(Customer_FeedBack.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Empty Fields!")
                                .duration(Duration.SHORT)
                                .show();*/
                        edittexterror = true;
                        //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (edtemail.getText().toString().isEmpty()) {
                        edtemail.setError(getResources().getString(R.string.Enter_Email_Id));
                        edittexterror = true;
                    } else if (edtyourname.getText().toString().isEmpty()) {
                        edtyourname.setError(getResources().getString(R.string.Enter_Full_name));
                        edittexterror = true;
                    } else if (edtphone.getText().toString().isEmpty()) {
                        edtphone.setError(getResources().getString(R.string.Enter_Phone));
                        edittexterror = true;
                    } else if (edtshopname.getText().toString().isEmpty()) {
                        edtshopname.setError(getResources().getString(R.string.Enter_shop_name));
                        edittexterror = true;
                    }  else if (edtcountry.getText().toString().isEmpty()) {
                        edtcountry.setError(getResources().getString(R.string.Enter_country));
                        edittexterror = true;
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (!android.util.Patterns.PHONE.matcher(edtphone.getText().toString().trim()).matches()) {
                        edtphone.setError(getResources().getString(R.string.Invalid_Phone));
                        edittexterror = true;
                    }

                    else if (edtshopname.getText().toString().isEmpty()) {
                        edtshopname.setError(getResources().getString(R.string.Enter_shop_name));
                        edittexterror = true;
                    }
                    if (edittexterror == false) {
                        new PostFeedback().execute();
                    }

                } else {
                    com.nispok.snackbar.Snackbar.with(Customer_FeedBack.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(Customer_FeedBack.this);
                  /*  com.chootdev.csnackbar.Snackbar.with(Customer_FeedBack.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();
*/

                }





            }
        });

    }

    private void goBack() {
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }


    class PostFeedback extends AsyncTask<String,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
       progress.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try{
            URL url = new URL(SERVER_CUSTOMER+"feedback.php"); // here is your URL path


            JSONObject postDataParams = new JSONObject();

                postDataParams.put(name, "");
                postDataParams.put(shop_name,"");
                postDataParams.put(location, "");
                postDataParams.put(phone, "");
                postDataParams.put(email,"" );
                postDataParams.put(message, "");
                postDataParams.put(user_id, "");
            Log.e("params", postDataParams.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new
                        InputStreamReader(
                        conn.getInputStream()));

                StringBuffer sb = new StringBuffer("");
                String line = "";

                while ((line = in.readLine()) != null) {

                    sb.append(line);
                    break;
                }

                in.close();
                return sb.toString();

            } else {
                return new String("false : " + responseCode);
            }
        } catch (Exception e)

        {
            return new String("Exception: " + e.getMessage());
        }


        }

        @Override
        protected void onPostExecute(String result) {


            Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            super.onPostExecute(result);
           progress.setVisibility(View.GONE);


            if (result.equals("\"failed\"")) {

                new SweetAlertDialog(Customer_FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                       // .setTitleText("Oops...")
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
                //  String named = jsonObj.getString("name");

            } else {
                new SweetAlertDialog(Customer_FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.ThankYou))
                        .setContentText(getResources().getString(R.string.Feedbacksubmitted))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                                startActivity(i);
                                finish();
                            }
                        })
                        .show();





            }


        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}
