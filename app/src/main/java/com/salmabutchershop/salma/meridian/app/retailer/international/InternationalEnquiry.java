package com.salmabutchershop.salma.meridian.app.retailer.international;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.retailer.enquiry.RetailerEnquiryFragment;
import com.salmabutchershop.salma.meridian.app.retailer.login.LoginActivityRetailer;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 12/7/2016.
 */
public class InternationalEnquiry extends AppCompatActivity {
    TextView AlReg;
    EditText edtemail, edtphon, edtfullnam;
  //  public static MKLoader progress_loader;
    Button butsignup;
    boolean edittexterror = false;
    String REGISTER_URL = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/register.php";
    ImageView profile_img_upload, profile_img;
    String filePath;
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    //    ProgressDialog pd;
    ProgressBar progress;

    Spinner spinner;
    static String filename = "null", filenamepath = "null";
    private static final int REQUEST_CODE = 1;
    String type,fullname,phone,email_s,location,username,password_s;
    String item;
    String result;
    String token;
    String refreshedToken;
    String NameS,ShpnameS,AdsS,PhoneS,EmailS,EnqS;
    EditText Name,Shpname,Ads,Phone,Email,Enq;
    String ids,Reg_id;
    String cm_id,cm_nm,cm_ads,cm_ph,cm_em,cm_lg;
    TextView tv,tv1,tv2,tv3;
    ImageView v;
    String shp_nm,nam,ads,phne,emll;
    String selected_date;
    LinearLayout date_icon;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView selectdate;
    public static MKLoader progress_loader,progress_loader1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.international_enquiry_new);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        progress_loader1=(MKLoader)findViewById(R.id.progress_loader1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InternationalEnquiry.this.goBack();
            }
        });
        selectdate=(TextView)findViewById(R.id.selectdate);
        selectdate.setText("DD-MM-YYYY");
        date_icon=(LinearLayout)findViewById(R.id.date_icon);
        date_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectdate.performClick();
            }
        });
        selectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c=Calendar.getInstance();
                mYear=c.get(Calendar.YEAR);
                mMonth=c.get(Calendar.MONTH);
                mDay=c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog=new DatePickerDialog(InternationalEnquiry.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthofYear, int dayofMonth) {
                        SimpleDateFormat dfDate  = new SimpleDateFormat("yyyy-MM-dd");
                        String current_date=mYear+"-"+mMonth+"-"+mDay;
                        String selected_date=year+"-"+monthofYear+"-"+dayofMonth;
                        try {

                            if (dfDate.parse(selected_date).before(dfDate.parse(current_date)))
                            {
                                Toast.makeText(InternationalEnquiry.this, "can't select past date", Toast.LENGTH_LONG).show();
                            }
                            else {
                                selectdate.setText(dayofMonth + "-" + (monthofYear + 1) + "-" + year);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },mYear,mMonth,mDay);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());

                datePickerDialog.show();
            }
        });
     /*   i.putExtra("in_id",in_id);
        i.putExtra("cm_nm",cm_nm);
        i.putExtra("ads",ads);
        i.putExtra("ph",ph);
        i.putExtra("em",em);*/
        cm_id=getIntent().getStringExtra("in_id");
        cm_nm=getIntent().getStringExtra("cm_nm");
        cm_ads=getIntent().getStringExtra("ads");
        cm_ph=getIntent().getStringExtra("ph");
        cm_em=getIntent().getStringExtra("em");
        cm_lg=getIntent().getStringExtra("lg");
        tv=(TextView) findViewById(R.id.ads);
        tv.setText(cm_ads);
        tv1=(TextView) findViewById(R.id.com_nam);
        tv1.setText(cm_nm);
        tv2=(TextView)findViewById(R.id.phnes);
        tv2.setText(cm_ph);
        tv3=(TextView) findViewById(R.id.eml);
        tv3.setText(cm_em);

        v=   (ImageView) findViewById(R.id.com_logo);
      //  Picasso.with(InternationalEnquiry.this).load(cm_lg).noFade().into(v);
        try {


            Picasso.with(getApplicationContext()).load(cm_lg).noFade().into(v, new Callback() {
                @Override
                public void onSuccess() {
                    progress_loader1.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_idL", null);
        System.out.println("<<<<<<<<<<<<<<<ref>>>>>>>>"+refreshedToken);
        //  Toast.makeText(getApplicationContext(), refreshedToken , Toast.LENGTH_LONG).show();
        //  Toast.makeText(getApplicationContext(), refreshedToken , Toast.LENGTH_LONG).show();
        shp_nm= myPrefs.getString("shp_nm", null);
        nam= myPrefs.getString("nam", null);
        ads= myPrefs.getString("ads", null);
        phne= myPrefs.getString("phne", null);
        emll= myPrefs.getString("eml", null);;
        Name = (EditText) findViewById(R.id.edt_name);
        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Name.setTypeface(myFont1);
Name.setText(nam);
        Shpname = (EditText) findViewById(R.id.edt_shpnme);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Shpname.setTypeface(myFont2);
Shpname.setText(shp_nm);
        Ads = (EditText) findViewById(R.id.edt_ads);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Ads.setTypeface(myFont3);
Ads.setText(ads);
        Phone = (EditText) findViewById(R.id.edt_phone);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Phone.setTypeface(myFont4);
Phone.setText(phne);
        Email = (EditText) findViewById(R.id.edt_eml);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Email.setTypeface(myFont5);
Email.setText(emll);
        Enq = (EditText) findViewById(R.id.edt_enq);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Enq.setTypeface(myFont6);


        butsignup = (Button) findViewById(R.id.button6);
        Typeface myFont9 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        butsignup.setTypeface(myFont9);
        progress = (ProgressBar) findViewById(R.id.progress_bar);



        progress_loader.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                progress_loader.setVisibility(View.INVISIBLE);
                return false;
            }
        });

        butsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NameS=Name.getText().toString();
                ShpnameS=Shpname.getText().toString();
                AdsS=Ads.getText().toString();
                PhoneS=Phone.getText().toString();
                EmailS=Email.getText().toString();
                EnqS=Enq.getText().toString();
                selected_date=selectdate.getText().toString();

                edittexterror = false;
                if (Name.getText().toString().isEmpty()||Shpname.getText().toString().isEmpty()||Ads.getText().toString().isEmpty()
                        ||Phone.getText().toString().isEmpty()||Email.getText().toString().isEmpty()||Enq.getText().toString().isEmpty()) {
                    com.nispok.snackbar.Snackbar.with(InternationalEnquiry.this) // context
                            .text("Empty Fields!") // text to display
                            .show(InternationalEnquiry.this);
                   /* com.chootdev.csnackbar.Snackbar.with(InternationalEnquiry.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Empty Fields!")
                            .duration(Duration.SHORT)
                            .show();
*/
                    edittexterror = true;
                }

                else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email.getText().toString().trim()).matches()) {
                    Email.setError("Invalid Email");
                    edittexterror = true;
                } else if (Email.getText().toString().isEmpty()) {
                    Email.setError("Enter Email Id");
                    edittexterror = true;
                }
                else if (Shpname.getText().toString().isEmpty()) {
                    Shpname.setError("Enter Shopname");
                    edittexterror = true;
                }
                else if (Phone.getText().toString().isEmpty()) {
                    Phone.setError("Enter Phone number");
                    edittexterror = true;
                }
                else if (Ads.getText().toString().isEmpty()) {
                    Ads.setError("Enter Address");
                    edittexterror = true;
                }
                else if (Name.getText().toString().isEmpty()) {
                    Name.setError("Enter Name");
                    edittexterror = true;
                }
                else if(selected_date.equalsIgnoreCase("DD-MM-YYYY")){
                    com.nispok.snackbar.Snackbar.with(InternationalEnquiry.this) // context
                            .text(getResources().getString(R.string.selectdate)) // text to display
                            .show(InternationalEnquiry.this);
                    edittexterror = true;
                }
                else if (filenamepath != null) {
                    filename = filenamepath;


                } else {
                    filename = "null";

                }

                if (edittexterror == false) {
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        if (Reg_id != null && !Reg_id.isEmpty()){
                            new SendPostRequest().execute();
                        }
                        else {

                            new SweetAlertDialog(InternationalEnquiry.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getResources().getString(R.string.Pleaseregister))
                                    .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                                    .setConfirmText(getResources().getString(R.string.Yes))
                                    .setCancelText(getResources().getString(R.string.No))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            Intent i = new Intent(getApplicationContext(), LoginActivityRetailer.class);
                                            startActivity(i);

                                           finish();

                                            sDialog.dismiss();

                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();


                        }


                    } else {
                        com.nispok.snackbar.Snackbar.with(InternationalEnquiry.this) // context
                                .text("Oops Your Connection Seems Off..!") // text to display
                                .show(InternationalEnquiry.this);


                    }



                }


            }
        });
    }

    private void goBack() {
        super.onBackPressed();
    }

    private boolean isValidPassword(String trim) {
        if (password_s != null && password_s.length() > 6) {
            return true;
        }
        return false;
    }



    public class SendPostRequest extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
            progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/retailer/intsupp-enquiry.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("int_id", cm_id);
                postDataParams.put("user_id", Reg_id);
                postDataParams.put("address", Ads);
                postDataParams.put("pro_qty", "250kg");
                postDataParams.put("description",EnqS);

                postDataParams.put("name", NameS);
                postDataParams.put("shopname",ShpnameS);
                postDataParams.put("phone",PhoneS);
                postDataParams.put("email", EmailS);
                postDataParams.put("ddate",selected_date);



                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
            progress_loader.setVisibility(ProgressBar.GONE);

            if (result.contentEquals("\"success\"")){

                new SweetAlertDialog(InternationalEnquiry.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.AWESOME))
                        // .setTitleText(getResources().getString(R.string.AWESOME))
                        .setCustomImage(R.drawable.logo)
                        .setContentText(getResources().getString(R.string.Enquirysuccessfullyplaced))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                com.nispok.snackbar.Snackbar.with(InternationalEnquiry.this) // context
                                        .text(getResources().getString(R.string.Enquirysuccessfullyplaced)) // text to display
                                        .show(InternationalEnquiry.this);

                       /* com.chootdev.csnackbar.Snackbar.with(InternationalEnquiry.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Enquiry Submitted!")
                                .duration(Duration.SHORT)
                                .show();*/
                                Intent inn= new Intent(getApplicationContext(),LoginActivity.class);
                                finish();




                                sweetAlertDialog.dismiss();

                            }
                        })
                        .show();




            }
            else {
                new SweetAlertDialog(InternationalEnquiry.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .show();

            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}