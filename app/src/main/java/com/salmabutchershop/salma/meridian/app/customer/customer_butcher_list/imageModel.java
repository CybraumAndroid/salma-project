package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

/**
 * Created by Rashid on 10/17/2016.
 */
public class imageModel {
    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    int id;
    String image,shop_name,delivery_info,butcher_discount,added_by,cat_id;

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getDelivery_info() {
        return delivery_info;
    }

    public void setDelivery_info(String delivery_info) {
        this.delivery_info = delivery_info;
    }

    public String getButcher_discount() {
        return butcher_discount;
    }

    public void setButcher_discount(String butcher_discount) {
        this.butcher_discount = butcher_discount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
