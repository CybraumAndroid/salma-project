package com.salmabutchershop.salma.meridian.app.customer.pro_select;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;


/**
 * Created by libin on 10/28/2016.
 */

public class ImagePopupFragment extends Activity {
    View view;
    //ImageView floor_img;
    String s;
    String tag = "events";
    TouchImageView floor_img;
    public Activity c;
    public Dialog d;
    public Button yes, no;
    public EditText editText;
    ProgressBar progress;
    String status;
    public static MKLoader progress_loader;

    /*public ImagePopupFragment(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.floor_plan_item_layout);
        //setContentView(R.layout.floor_planitem_layout);
        // editText= (EditText)rootView. findViewById(R.id.alert_email);
        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
      /*  progress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                progress.setVisibility(View.INVISIBLE);
                return false;
            }
        });*/
        ImageButton close = (ImageButton) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        s = getIntent().getStringExtra("pro_id");

        System.out.println("<<<oo>floooooooooooooooooooooooooooor>>>" + s);
        floor_img = (TouchImageView) findViewById(R.id.imageView8);
        try {


            new Picasso.Builder(ImagePopupFragment.this)
                    .downloader(new OkHttpDownloader(ImagePopupFragment.this, Integer.MAX_VALUE))
                    .build()
                    .load(s)
                    .noFade()
                    // .into(personViewHolder.v);
                    .into(floor_img, new Callback() {
                        @Override
                        public void onSuccess() {

                            progress_loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {


                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
            // Picasso.with(getActivity()).load(s).noFade().into(floor_img);


        }

        }





