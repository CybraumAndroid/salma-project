package com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Butcher_mymenu_product;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Product_mymenu_Model;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Product_mymenu_adapter;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.database.OrderNew_Activity;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Model;
import com.salmabutchershop.salma.meridian.app.customer.login.PostRegistration;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/3/2016.
 */

public class Butcher_main_Product  extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview,recyclerview1;

    Product_main_adapter adapter1;
    Product_main_adapter2 adapter2;
    ProgressBar progress;
    String result, galry_image, Dat, Dat1;
    static ArrayList<Product_main_Model> pmm;
    Product_main_Model pm;
    String event_id, winner_name, winner_image, award_name;
    String k, l, m;
    String pro_id, pro_name, pro_image,sub_id;
    TextView title, qty, plus, minus, cart_txt;
    private int uprange = 20;
    private int downrange = 0;
    private int values = 0;
    TextView tv, tv1, tv2, tv3;
    ImageView v;
    Button button2;
    LinearLayout llm;
    static TextView cnt;
    String Reg_id, names, typ;
    String UP;
    Spinner spinner0, spinner1, spinner2;
    static ArrayList<Product_Services_Model> eem;
    Product_Services_Model ee;
    String id,city;
    ArrayList<String> divisonlist;
    String ids,userid,fullname;
    String Wurl,Burl;
    public static MKLoader progress_loader;
    LinearLayout spinner_item_background;
    private SpinAdapter adapter;
    LinearLayout menu_lay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.butcher_main_product_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
menu_lay= (LinearLayout) findViewById(R.id.menu_lay);
        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullnameb", null);

        /*spinner_item_background=(LinearLayout)findViewById(R.id.spinner_item_background);*/
        spinner0 = (Spinner) findViewById(R.id.spinner);
        /*spinner0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerview.setBackgroundColor(getResources().getColor(R.color.trans));
            }
        });*/


        spinner0.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


               /* spinner_item_background.setBackgroundResource(android.R.color.holo_red_dark);*/


              //  dptmt = spinner0.getSelectedItem().toString();

                System.out.println("<<<<<<<<<divisionid>>>>>" + ids);
                recyclerview.setBackgroundColor(getResources().getColor(R.color.trans));
//                ((TextView) arg0.getChildAt(0)).setTextColor(getResources().getColor(R.color.White));
                //  bn.setText(dptmt);
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    String l=getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>"+l);
                    if (l.equalsIgnoreCase("English")) {
                       Burl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/menuby_category.php?sub_id="+ids+"&butcher_id="+userid;
                    }
                    else{
                        Burl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/ar/menuby_category.php?sub_id="+ids+"&butcher_id="+userid;
                    }
                    new DownloadData1().execute();

                } else {
                    com.nispok.snackbar.Snackbar.with(Butcher_main_Product.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Butcher_main_Product.this);
                    /*com.chootdev.csnackbar.Snackbar.with(Butcher_main_Product.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)
            String l=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>"+l);
            if (l.equalsIgnoreCase("English")) {
                Wurl=SERVER+"json/butcher/butchermenu1.php";
            }
            else{
                Wurl=SERVER+"json/butcher/ar/butchermenu1.php";
            }
            new DownloadData().execute();

        } else {
            com.nispok.snackbar.Snackbar.with(Butcher_main_Product.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Butcher_main_Product.this);
           /* com.chootdev.csnackbar.Snackbar.with(Butcher_main_Product.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/
        }
        recyclerview1 = (RecyclerView) findViewById(R.id.recyclerview1);
        recyclerview1.setHasFixedSize(true);
        Context context = getApplicationContext();
       // LinearLayoutManager llm = new LinearLayoutManager(context);
/*        llm.setOrientation(LinearLayoutManager.HORIZONTAL);*/
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview1.setLayoutManager(layoutManager1);
        recyclerview1.setAdapter(adapter2);
        // Change to gridLayout
      //  recyclerview1.setLayoutManager(new GridLayoutManager(context, 1));

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
       // Context context = getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        recyclerview.setAdapter(adapter1);
// recyclerview.setVisibility(View.VISIBLE);
        menu_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerview1.setVisibility(View.VISIBLE);
                recyclerview.setVisibility(View.GONE);
            }
        });
        ImageView back = (ImageView) findViewById(R.id.back_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Butcher_main_Product.this.goBack();
            }
        });
        cnt = (TextView) findViewById(R.id.imageView2);



    }

    private void goBack() {
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }

    public void functionToRun() {
        final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
        int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        cnt.setText("" + s);
    }

    public static void changeindex(int si) {
        cnt.setText("" + si);
    }


    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Burl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
           // String sam = result.trim();
    System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
         //   String value = result;
            if
                    (result.equalsIgnoreCase("")) {
               // Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
             recyclerview.setVisibility(View.GONE);
            } else {
                recyclerview.setVisibility(View.VISIBLE);
                JSONArray mArray;
                pmm = new ArrayList<Product_main_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        pm = new Product_main_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        pro_id = mJsonObject.getString("pro_id");
                        pro_name = mJsonObject.getString("pro_name");
                        sub_id= mJsonObject.getString("sub_id");
                        pro_image = mJsonObject.getString("pro_image");


                        pm.setPro_id(pro_id);
                        pm.setPro_name(pro_name);
                        pm.setSub_id(sub_id);
                        pm.setPro_image(pro_image);

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        //   System.out.println("<<<oo>>>>" + o);
                        //   onComplete();

                        pmm.add(pm);
                        System.out.println("<<<oo>>>>" + pm);


                        adapter1 = new Product_main_adapter(pmm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                            //    String cat_id = pmm.get(position).getPro_id();
                                Intent i = new Intent(getApplicationContext(), Butcher_Product_Submit.class);
                                i.putExtra("cat_id", pmm.get(position).getSub_id());
                                i.putExtra("pro_id",pmm.get(position).getPro_id());
                                i.putExtra("pro_name", pmm.get(position).getPro_name());
                               /* ActivityOptions options = ActivityOptions.makeScaleUpAnimation(view, 0,
                                        0, view.getWidth(), view.getHeight());*/
                                startActivity(i);
                                finish();

                            }
                        })
                );

            }


        }
    }

    private class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
        /*    if
                    (result.equalsIgnoreCase("")) {
                com.nispok.snackbar.Snackbar.with(Butcher_main_Product.this) // context
                        .text("No Items") // text to display
                        .show(Butcher_main_Product.this);
                // Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
            } else */if (result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;

                eem = new ArrayList<Product_Services_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                        ee = new Product_Services_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //Log.d("OutPut", mJsonObject.getString("doctor_publish"));
//

                        id = mJsonObject.getString("menu_id");
                        city = mJsonObject.getString("menu_name");


                        ee.setId(id);
                        ee.setCategory_name(city);


                        eem.add(ee);
                        divisonlist = new ArrayList<String>();
                        adapter2 = new Product_main_adapter2(eem, getApplicationContext());
                        recyclerview1.setAdapter(adapter2);
                        //Dep.add(dm);
                       /* for (Product_Services_Model dm : eem) {
                            divisonlist.add(dm.getCategory_name());
                            //  divisonlist.add(dv.getBranch_id());
                        }
                        System.out.println("" + ee);
                        *//*ArrayAdapter<String> spinnerAdapter0 = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.simplespinner, R.id.txt, divisonlist);
*//*

                        adapter = new SpinAdapter(getApplicationContext(),
                                android.R.layout.select_dialog_item,
                                eem);

                        spinner0.setAdapter(adapter); // Set the custom adapter to the spinner
*/


                      /*  spinner0.setAdapter(spinnerAdapter0);
                        spinnerAdapter0.notifyDataSetChanged();*/
                       /* if(!((Activity) Butcher_main_Product.this).isFinishing()) {
                            spinner0.performClick();
                        }
*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                recyclerview1.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                ids = eem.get(position).getId();
                                recyclerview1.setVisibility(View.GONE);
                                recyclerview.setVisibility(View.VISIBLE);
                                if (DetectConnection
                                        .checkInternetConnection(getApplicationContext())) {
                                    String l=getResources().getConfiguration().locale.getDisplayLanguage();
                                    System.out.println("<<<<<<lang>>>>>"+l);
                                    if (l.equalsIgnoreCase("English")) {
                                        Burl=SERVER+"json/butcher/menuby_category.php?sub_id="+ids+"&butcher_id="+userid;
                                    }
                                    else{
                                        Burl=SERVER+"json/butcher/ar/menuby_category.php?sub_id="+ids+"&butcher_id="+userid;
                                    }
                                    new DownloadData1().execute();

                                } else {
                                    com.nispok.snackbar.Snackbar.with(Butcher_main_Product.this) // context
                                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                            .show(Butcher_main_Product.this);
                    /*com.chootdev.csnackbar.Snackbar.with(Butcher_main_Product.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/
                                }


                            }

                        })
                );


            }
            else {
                System.out.println("nulllllllllllllllllllllllllllllllll");

            }

        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}