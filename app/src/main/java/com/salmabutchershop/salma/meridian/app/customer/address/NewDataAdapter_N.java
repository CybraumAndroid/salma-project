package com.salmabutchershop.salma.meridian.app.customer.address;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.DatabaseHandler;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.database.OrderNew_Activity;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Rashid on 11/29/2016.
 */

public class NewDataAdapter_N extends RecyclerView.Adapter <NewDataAdapter_N.ViewHolder>{
private ArrayList<DataValues_N> DataValues;
    String inst;
    String but_id;
private Context context1;
    DatabaseHandler_N dh=new DatabaseHandler_N();

public NewDataAdapter_N(Context context2, ArrayList<DataValues_N> dataValues, String inst, String but_id){
        this.DataValues=dataValues;
    this.inst=inst;
    this.but_id=but_id;
        context1=context2;

        }
@Override
public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.address_local_db,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(final ViewHolder holder,final int position){
holder.primary_id.setText(""+DataValues.get(position).id);
    holder.name.setText(DataValues.get(position).fname+" "+DataValues.get(position).lname);
    holder.city.setText(DataValues.get(position).city);
    holder.street.setText(DataValues.get(position).addresstype);
    holder.ad_typ.setText(DataValues.get(position).street);
    holder.building.setText("Building"+DataValues.get(position).building);
    holder.floor.setText("Floor:"+DataValues.get(position).floor);
    holder.apart_no.setText("Apartment No:"+DataValues.get(position).apartno);
    holder.mobile.setText("Mob:"+DataValues.get(position).mobile);
    holder.email.setText("Emai:"+DataValues.get(position).phone);
    System.out.println("libintantony>"+but_id);
    System.out.println("<<<<<<<<<<<<<<<<<<>>>>>>>>>>"+DataValues.get(position).addresstype);
    holder.deliver_here.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            System.out.println("<<<<<<<<<<<<<<<<<<>>>>>>>>>>"+DataValues.get(position).addresstype);
            System.out.println("url "+inst);
            Intent i = new Intent(context1, AddressFragment.class);
            i.putExtra("id_nam","antony");
            i.putExtra("fnames",DataValues.get(position).fname);
            i.putExtra("lnames",DataValues.get(position).lname);
            i.putExtra("citys",DataValues.get(position).city);
            i.putExtra("buildings",DataValues.get(position).building);
            i.putExtra("floors",DataValues.get(position).floor);
            i.putExtra("apartrments",DataValues.get(position).apartno);
            i.putExtra("dlvry_area",DataValues.get(position).deliveryarea);
            i.putExtra("mobile",DataValues.get(position).mobile);
            i.putExtra("phoe",DataValues.get(position).phone);
            i.putExtra("loc",DataValues.get(position).addtional_direc);
            i.putExtra("inst",inst);
            i.putExtra("but_id",but_id);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //   i.putExtra("sub_tot",foo);
         System.out.println("detailll6"+but_id);
           context1. startActivity(i);

        }
    });
    holder.clse_lt.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
         //System.out.println("url "+DataValues.get(position).getImage());
            new SweetAlertDialog(view.getRootView().getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setCustomImage(R.drawable.logo)
                    .setTitleText("")
                    .setContentText(context1.getResources().getString(R.string.AreyousureWanttodeletetheproduct))
                    //  .setContentText("Want to delete this cart item ?")

                    .setCancelText(context1.getResources().getString(R.string.No))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .setConfirmText(context1.getResources().getString(R.string.Yes))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {


                            try {



                                dh.DeleteViews(context1,holder);


                                ///////////////// REMOVING ITEM FROM RECYCLE LIST --START/////////////////
                                DataValues.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,DataValues.size());
                                ///////////////// REMOVING ITEM FROM RECYCLE LIST --END/////////////////



                            }
                            catch(Exception e) {
                                e.printStackTrace();
                                com.nispok.snackbar.Snackbar.with(context1) // context
                                        .text("Failed!") // text to display
                                        .show((Activity) context1);
                            }
                            sDialog
                                    .setTitleText(context1.getResources().getString(R.string.Deleted))
                                    .setContentText(context1.getResources().getString(R.string.YourCartItemhasbeen))
                                    .setConfirmText(context1.getResources().getString(R.string.ok))
                                    .setConfirmClickListener(null)
                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        }
                    })
                    .show();






        }
    });

        }

@Override
public int getItemCount(){
        return DataValues.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder {


    public TextView name,city,street,building,floor,apart_no,mobile,phone,primary_id,ad_typ,email;
LinearLayout clse_lt;
    Button deliver_here;
    public ViewHolder(View itemView) {
        super(itemView);

primary_id=(TextView)itemView.findViewById(R.id.pid);

        name=(TextView)itemView.findViewById(R.id.name);
        Typeface myFont1 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
        name.setTypeface(myFont1);

        city=(TextView)itemView.findViewById(R.id.city);
        Typeface myFont2 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
       city.setTypeface(myFont2);

        street=(TextView)itemView.findViewById(R.id.street);
        Typeface myFont3 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
       street.setTypeface(myFont3);

        building=(TextView)itemView.findViewById(R.id.building);
        Typeface myFont4 = Typeface.createFromAsset(context1.getAssets(),"Roboto-Regular.ttf");
        building.setTypeface(myFont4);

        floor=(TextView)itemView.findViewById(R.id.floor);
        Typeface myFont5 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
      floor.setTypeface(myFont5);

        apart_no=(TextView)itemView.findViewById(R.id.apartNo);
        Typeface myFont6 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
        apart_no.setTypeface(myFont6);


        mobile=(TextView)itemView.findViewById(R.id.mobile);
        Typeface myFont7 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
        mobile.setTypeface(myFont7);

        ad_typ=(TextView)itemView.findViewById(R.id.ad_typ);
        Typeface myFont8 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
        ad_typ.setTypeface(myFont8);

        email=(TextView)itemView.findViewById(R.id.eml);
        Typeface myFont10 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
       email.setTypeface(myFont10);

        clse_lt=(LinearLayout) itemView.findViewById(R.id.clse_l);
        deliver_here=(Button) itemView.findViewById(R.id.button4);
        Typeface myFont9 = Typeface.createFromAsset(context1.getAssets(), "Roboto-Regular.ttf");
        deliver_here.setTypeface(myFont9);
    }

}
}