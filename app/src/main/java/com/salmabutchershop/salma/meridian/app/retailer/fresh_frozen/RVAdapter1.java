package com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Libin_Cybraum on 6/27/2016.
 */
public class RVAdapter1 extends RecyclerView.Adapter<ItemViewHolder1> {

    private List<CountryModel1> mCountryModel1;
    private List<CountryModel1> mOriginalCountryModel1;
    Context cx;
    public RVAdapter1(List<CountryModel1> mCountryModel1, Context context) {
        this.mCountryModel1 = mCountryModel1;
        this.mOriginalCountryModel1 = mCountryModel1;
        this.cx=context;
    }

    public RVAdapter1() {
    }

    @Override
    public void onBindViewHolder(ItemViewHolder1 itemViewHolder1, int i) {
        final CountryModel1 model1 = mCountryModel1.get(i);
        itemViewHolder1.bind(model1,cx);
    }

    @Override
    public ItemViewHolder1 onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fresh_froz_item_layout, viewGroup, false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            setViews(i);

            }
        });

        return new ItemViewHolder1(view);
    }

    @Override
    public int getItemCount() {
        return mCountryModel1.size();
    }

    public void setFilter(List<CountryModel1> countryModels){
        mCountryModel1 = new ArrayList<>();
        mCountryModel1.addAll(countryModels);
        notifyDataSetChanged();
    }

    void setViews( int pos){

        String sub_id=mCountryModel1.get(pos).getSubcat_id();
        String ct_id=mCountryModel1.get(pos).getCat_id();
        System.out.println("position"+pos);
        Intent i=new Intent(cx,Retailer_Wholeseller_Fragment.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("sub_id",sub_id);
        i.putExtra("ct_id",ct_id);
        cx.startActivity(i);
    }

}
