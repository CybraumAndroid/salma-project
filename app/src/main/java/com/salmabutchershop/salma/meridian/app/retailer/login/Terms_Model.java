package com.salmabutchershop.salma.meridian.app.retailer.login;

/**
 * Created by libin on 12/20/2016.
 */
public class Terms_Model {
    public String getTerm_id() {
        return term_id;
    }

    public void setTerm_id(String term_id) {
        this.term_id = term_id;
    }

    public String getTerm_desc() {
        return term_desc;
    }

    public void setTerm_desc(String term_desc) {
        this.term_desc = term_desc;
    }

    String term_id,term_desc;
}
