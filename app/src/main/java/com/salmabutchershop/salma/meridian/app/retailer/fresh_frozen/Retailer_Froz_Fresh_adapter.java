package com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Model;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_adapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 12/7/2016.
 */
public class Retailer_Froz_Fresh_adapter extends RecyclerView.Adapter<Retailer_Froz_Fresh_adapter.ViewHolder> {


    List<Retailer_fresh_Model> rfm;
    Context context;
   // private List<CountryModel1> mCountryModel1;


    public Retailer_Froz_Fresh_adapter(ArrayList<Retailer_fresh_Model> rfm, Context context) {
        this.rfm = rfm;
        this.context = context;
       // this.mCountryModel1 = mCountryModel1;
    }

    public void getItem(int position) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;
        public static MKLoader progress_loader;
        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            tv=   (TextView) itemView.findViewById(R.id.title);
           /* tv1=   (TextView) itemView.findViewById(R.id.evnt_frm);
            tv2=   (TextView) itemView.findViewById(R.id.evnt_to);
            tv3=   (TextView) itemView.findViewById(R.id.evnt_vnue);
         ;*/
            v=   (ImageView) itemView.findViewById(R.id.imageView8);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);

        }

        public void bind(Retailer_fresh_Model model1, Context context) {
          String s = model1.getCat_image();
            tv.setText(model1.getSubcat_name());

            //  String img=enm.get(i).getNews_img();
            System.out.println("ooo" + s);
            try {


                new Picasso.Builder(context)
                        .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                        .build()
                        .load(s)
                        .noFade()
                        // .into(personViewHolder.v);
                        .into(v, new Callback() {
                            @Override
                            public void onSuccess() {

                           progress_loader.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });
            }catch (Exception e){
                e.printStackTrace();
            }
           // Picasso.with(context).load(s).noFade().into(v);
        }
    }


    @Override
    public int getItemCount() {
        return rfm.size();
    }

    public void setFilter(List<Retailer_fresh_Model> rfm){
       rfm = new ArrayList<>();
        rfm.addAll(rfm);
        notifyDataSetChanged();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fresh_froz_item_layout, viewGroup, false);
        ViewHolder pvh = new ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder personViewHolder, final int i) {

        final Retailer_fresh_Model model1 =rfm.get(i);
       personViewHolder.bind(model1,context);
        String s = rfm.get(i).getCat_image();
        personViewHolder.tv.setText(rfm.get(i).getSubcat_name());

        //  String img=enm.get(i).getNews_im
        // g();
        System.out.println("ooo" + s);

        try {


            new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .build()
                    .load(s)
                    .noFade()
                    // .into(personViewHolder.v);
                    .into(personViewHolder.v, new Callback() {
                        @Override
                        public void onSuccess() {

                            personViewHolder.progress_loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }

      //  Picasso.with(context).load(s).noFade().into(personViewHolder.v);
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}