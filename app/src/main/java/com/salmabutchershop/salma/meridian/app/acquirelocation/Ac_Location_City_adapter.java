package com.salmabutchershop.salma.meridian.app.acquirelocation;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_City_Model;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_City_adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 1/16/2017.
 */
public class Ac_Location_City_adapter extends RecyclerView.Adapter<Ac_Location_City_adapter.ViewHolder> {


    List<Ac_Location_City_Model> lcm;
    Context context;



    public Ac_Location_City_adapter(ArrayList<Ac_Location_City_Model> lcm, Context context) {
        this.lcm = lcm;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;

        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);


            tv=   (TextView) itemView.findViewById(R.id.evt_typ);

        }
    }


    @Override
    public int getItemCount() {
        return lcm.size();
    }


    @Override
    public Ac_Location_City_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.location_city_item_layout, viewGroup, false);
        Ac_Location_City_adapter.ViewHolder pvh = new Ac_Location_City_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Ac_Location_City_adapter.ViewHolder personViewHolder, final int i) {


        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.tv.setTypeface(myFont2);
        personViewHolder.tv.setText(lcm.get(i).getName());

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}