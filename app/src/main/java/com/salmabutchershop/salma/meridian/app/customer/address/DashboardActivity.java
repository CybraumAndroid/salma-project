package com.salmabutchershop.salma.meridian.app.customer.address;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.salmabutchershop.salma.meridian.app.GPSTracker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Fragment;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class DashboardActivity extends Activity implements OnMapReadyCallback {
    int locationMode;
    Context mContext;
    GPSTracker gps;
    double latitude;
    double longitude;

    private GoogleMap googleMap;
EditText textinfo;
    Button Choose;
    String address,city,state,postalCode,knownName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        textinfo= (EditText) findViewById(R.id.textView46);
        Choose= (Button) findViewById(R.id.button7);

        mContext = this;
    Choose.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.putString("loc_ads",textinfo.getText().toString());
            editor.putString("ads",address);
            editor.putString("city",city);
            editor.putString("state",state);
            editor.putString("postal",postalCode);
            editor.putString("knwn",knownName);
            editor.commit();
            Intent i=new Intent(DashboardActivity.this,Location_Services_Fragment.class);

            i.putExtra("rm",textinfo.getText().toString());
startActivity(i);
           /* setResult(2,i);*/

            finish();//finishing activity
             }
        });
        ImageView back= (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*onBackPressed();*/
                Intent i=new Intent(DashboardActivity.this,Location_Services_Fragment.class);
                startActivity(i);
                finish();
            }
        });
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


        } else {

            Toast.makeText(mContext, "You already granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(mContext, DashboardActivity.this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                // \n is for new line
                System.out.println("----------------------------------------");
                System.out.println("inside cangetlocation");
                System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                System.out.println("----------------------------------------");


                LocationOnMap(latitude, longitude);
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(DashboardActivity.this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    if(!addresses.isEmpty()) {
                        address = addresses.get(0).getAddressLine(0);
                        System.out.println("-----------------------adsggg-----------------" + address);
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        city = addresses.get(0).getLocality();
                        System.out.println("------------------city----------------------" + city);
                        state = addresses.get(0).getAdminArea();
                        System.out.println("---------------state-------------------------" + state);
                        String country = addresses.get(0).getCountryName();
                        System.out.println("-----------------cntry-----------------------" + country);
                        postalCode = addresses.get(0).getPostalCode();
                        System.out.println("----------------postalcode------------------------" + postalCode);
                        knownName = addresses.get(0).getFeatureName();
                        System.out.println("-----------------knwn-----------------------" + knownName);
                        textinfo.setText("" + address);/*"\n" + "" + city + "\n" + "" + state + "\n" + "" + postalCode + "\n" + "" + knownName)*/;
                    }
                    {
                        System.out.println("----------------------------------------");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
/*if (address!=null){


    //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
}else {


}*/
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
              int a=  gps.showSettingsAlert(DashboardActivity.this);

            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(mContext, DashboardActivity.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();

                        // \n is for new line
                        System.out.println("----------------------------------------");
                        System.out.println("inside cangetlocation");
                        System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                        System.out.println("----------------------------------------");


                        LocationOnMap(latitude, longitude);
                        Geocoder geocoder;
                        List<Address> addresses = null;
                        geocoder = new Geocoder(DashboardActivity.this, Locale.getDefault());

                        try {
                         addresses = geocoder.getFromLocation(latitude, longitude, 1);
                            if(!addresses.isEmpty()) {
                                address = addresses.get(0).getAddressLine(0);
                                System.out.println("-----------------------adsggg-----------------" + address);
                                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                city = addresses.get(0).getLocality();
                                System.out.println("------------------city----------------------" + city);
                                state = addresses.get(0).getAdminArea();
                                System.out.println("---------------state-------------------------" + state);
                                String country = addresses.get(0).getCountryName();
                                System.out.println("-----------------cntry-----------------------" + country);
                                postalCode = addresses.get(0).getPostalCode();
                                System.out.println("----------------postalcode------------------------" + postalCode);
                                knownName = addresses.get(0).getFeatureName();
                                System.out.println("-----------------knwn-----------------------" + knownName);
                                textinfo.setText("" + address);
                            }
                            {
                                System.out.println("----------------------------------------");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        /*Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        int a=gps.showSettingsAlert(DashboardActivity.this);


                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    Toast.makeText(mContext,(getResources().getString(R.string.you_need_to_grant_permission)), Toast.LENGTH_SHORT).show();
                }


                return;
            }
        }
    }


    public void LocationOnMap(double lat, double lng) {
        try {

            latitude = lat;
            longitude = lng;
          /*  MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Me");
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            googleMap.addMarker(marker);*/

            initilizeMap();


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initilizeMap() {
        if (googleMap == null) {
            ((MapFragment) getFragmentManager().findFragmentById(R.id.mapID)).getMapAsync(this);
            if (googleMap == null) {
               /* Toast.makeText(getApplicationContext(), "Sorry unable to create maps", Toast.LENGTH_LONG).show();*/
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap map) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        googleMap=map;

        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true); // true to enable

        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);


        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(17).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
      googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                //myMap.addMarker(new MarkerOptions().position(point).title(point.toString()));

                //The code below demonstrate how to convert between LatLng and Location
                googleMap.clear();
                //Convert LatLng to Location
                Location location = new Location("Test");
                location.setLatitude(point.latitude);
                location.setLongitude(point.longitude);
                location.setTime(new Date().getTime()); //Set time as current Date
             //  textinfo.setText(location.toString());
 Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(DashboardActivity.this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(point.latitude,point.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if(!addresses.isEmpty()) {
                String address = addresses.get(0).getAddressLine(0);
                System.out.println("-----------------------ads-----------------" + address);
                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                System.out.println("------------------city----------------------" + city);
                String state = addresses.get(0).getAdminArea();
                System.out.println("---------------state-------------------------" + state);
                String country = addresses.get(0).getCountryName();
                System.out.println("-----------------cntry-----------------------" + country);
                String postalCode = addresses.get(0).getPostalCode();
                System.out.println("----------------postalcode------------------------" + postalCode);
                String knownName = addresses.get(0).getFeatureName();
                System.out.println("-----------------knwn-----------------------" + knownName);
                //Convert Location to LatLng

                LatLng newLatLng = new LatLng(location.getLatitude(), location.getLongitude());

                MarkerOptions markerOptions = new MarkerOptions()
                        .position(newLatLng)
                        .title(newLatLng.toString());
                textinfo.setText("" +address );

                googleMap.addMarker(markerOptions);
            }
            else {
                System.out.println("----------------------------------------");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(DashboardActivity.this,Location_Services_Fragment.class);
        startActivity(i);
        finish();
    }
}