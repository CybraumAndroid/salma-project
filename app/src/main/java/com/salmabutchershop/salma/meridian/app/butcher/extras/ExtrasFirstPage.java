package com.salmabutchershop.salma.meridian.app.butcher.extras;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;

/**
 * Created by libin on 12/20/2016.
 */

public class ExtrasFirstPage extends AppCompatActivity {
LinearLayout Term,Notfy,Cpwd,Tdy_Menu,profile_update,profile_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extras_first_layout);
        Term= (LinearLayout) findViewById(R.id.term);
       Notfy= (LinearLayout) findViewById(R.id.notfy);
        Tdy_Menu=(LinearLayout) findViewById(R.id.menu);
     Cpwd= (LinearLayout) findViewById(R.id.cpwd);
        profile_view= (LinearLayout) findViewById(R.id.profile_view);
        profile_update= (LinearLayout) findViewById(R.id.profile_update);
        profile_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intlog = new Intent(getApplicationContext(),Profile_View_Activity.class);
                startActivity(intlog);
            }
        });
        profile_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intlog = new Intent(getApplicationContext(),Profile_update_butcher.class);
                startActivity(intlog);
            }
        });
        Tdy_Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intlog = new Intent(getApplicationContext(), Butcher_NewMenu.class);
                startActivity(intlog);
            }
        });
        Term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intlog = new Intent(getApplicationContext(), Terms_Conditions.class);
                startActivity(intlog);
            }
        });
        Notfy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intlog = new Intent(getApplicationContext(), NotificationFragment.class);
                startActivity(intlog);
            }
        });
        Cpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intlog = new Intent(getApplicationContext(), Changepassword.class);
                startActivity(intlog);
            }
        });
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExtrasFirstPage.this.goBack();
            }
        });
    }

    private void goBack() {
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}
