package com.salmabutchershop.salma.meridian.app.butcher.wholseller.productdetail;

/**
 * Created by libin on 9/29/2016.
 */
public class Butcher_product_Model {
    String pro_id,pro_name,pro_cat_id,pro_subcat_id,stock_unit,price,currency,price_quantity,discount,discount_unit,pro_image,minimum_order,wholeseller_id,delivery_time,pro_description ;

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getPro_description() {
        return pro_description;
    }

    public void setPro_description(String pro_description) {
        this.pro_description = pro_description;
    }

    public String getPro_id() {
        return pro_id;
    }

    public String getMinimum_order() {
        return minimum_order;
    }

    public void setMinimum_order(String minimum_order) {
        this.minimum_order = minimum_order;
    }

    public String getWholeseller_id() {
        return wholeseller_id;
    }

    public void setWholeseller_id(String wholeseller_id) {
        this.wholeseller_id = wholeseller_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_cat_id() {
        return pro_cat_id;
    }

    public void setPro_cat_id(String pro_cat_id) {
        this.pro_cat_id = pro_cat_id;
    }

    public String getPro_subcat_id() {
        return pro_subcat_id;
    }

    public void setPro_subcat_id(String pro_subcat_id) {
        this.pro_subcat_id = pro_subcat_id;
    }

    public String getStock_unit() {
        return stock_unit;
    }

    public void setStock_unit(String stock_unit) {
        this.stock_unit = stock_unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice_quantity() {
        return price_quantity;
    }

    public void setPrice_quantity(String price_quantity) {
        this.price_quantity = price_quantity;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_unit() {
        return discount_unit;
    }

    public void setDiscount_unit(String discount_unit) {
        this.discount_unit = discount_unit;
    }

    public String getPro_image() {
        return pro_image;
    }

    public void setPro_image(String pro_image) {
        this.pro_image = pro_image;
    }
}
