package com.salmabutchershop.salma.meridian.app.butcher.orderdetail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_my_order;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by libin on 11/24/2016.
 */

public class Butcher_My_orderDetail extends Activity {
    String result;
    ProgressBar progress;
    static ArrayList<Butcher_MyOrder_List_Model> bmo;
    Butcher_MyOrder_List_Model bm;
    String k,l,o;
    String user_id,order_item_id, order_id, pro_name, pro_type_name, pro_qty, pro_price, fname,instruction,
            lname, city, deliveryarea, addresstype, street, building, floor, house_no, mobile, phone,
            aditional_direction, service_fee, container_charges, delivery_charges, subtotal, total, order_date, order_status;
    TextView Name, Ads, Item_Name, Qty, Delivry_date, Location,Phone,Email,Instruction,type;
    Button accepted;
String userid,fullname,email;
    String Wurl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.butcher_detail_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullnameb", null);
        Name = (TextView) findViewById(R.id.nme);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
       Name.setTypeface(myFont3);
        Name.setText(fullname);
        Ads = (TextView) findViewById(R.id.ads);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
       Ads.setTypeface(myFont4);
        Qty = (TextView) findViewById(R.id.qty);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
       Qty.setTypeface(myFont5);
        Delivry_date = (TextView) findViewById(R.id.dlvry_date);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
       Delivry_date.setTypeface(myFont6);
       /* Location = (TextView) findViewById(R.id.lctn);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
       Location.setTypeface(myFont7);*/
        Item_Name = (TextView) findViewById(R.id.item_name);
        Typeface myFont8 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
    Item_Name.setTypeface(myFont8);
        accepted = (Button) findViewById(R.id.accepted);
        Typeface myFont9 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
    accepted.setTypeface(myFont9);

        Phone = (TextView) findViewById(R.id.phone);
        Typeface myFont1= Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
      Phone.setTypeface(myFont1);

        Email = (TextView) findViewById(R.id.email);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
       Email.setTypeface(myFont2);

     Instruction = (TextView) findViewById(R.id.instruction);
        Typeface myFont11 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
      Instruction.setTypeface(myFont11);

       type = (TextView) findViewById(R.id.pro_type_name);
        Typeface myFont12 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        type.setTypeface(myFont12);

        accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    new SendPostRequest3().execute();

                } else {
                    com.nispok.snackbar.Snackbar.with(Butcher_My_orderDetail.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Butcher_My_orderDetail.this);
                /*    com.chootdev.csnackbar.Snackbar.with(Butcher_My_orderDetail.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();

*/
                }


            }
        });
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Butcher_My_orderDetail.this.finish();
            }
        });
        k = getIntent().getStringExtra("add_id");
        l= getIntent().getStringExtra("pr_id");
        o=getIntent().getStringExtra("order_id");
        System.out.println("ggggggggggggg"+k);
        System.out.println("hhhhhhhhhhhhhhhhh"+l);
        System.out.println("jjjjjjjjjjjjjjjjjjjj"+o);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {

            com.nispok.snackbar.Snackbar.with(Butcher_My_orderDetail.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Butcher_My_orderDetail.this);

        }


    }

    private void goBack() {
        super.onBackPressed();
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User
            String l=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>"+l);
            if (l.equalsIgnoreCase("English")) {
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/order_detail.php?id="+l+"&order_item_id="+o;
            }
            else{
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/ar/order_detail.php?id="+l+"&order_item_id="+o;
            }
            try {

                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress.setVisibility(ProgressBar.GONE);
           // String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
        //    System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
            if
                    (result.equalsIgnoreCase("")) {
                com.nispok.snackbar.Snackbar.with(Butcher_My_orderDetail.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Butcher_My_orderDetail.this);
                //Toast.makeText(getApplicationContext(), "No products", Toast.LENGTH_SHORT).show();
            } else {
                JSONArray mArray;
                bmo = new ArrayList<Butcher_MyOrder_List_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        bm = new Butcher_MyOrder_List_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));

                        user_id = mJsonObject.getString("user_id");
                        order_id = mJsonObject.getString("order_id");
                        order_item_id= mJsonObject.getString("order_item_id");
                        pro_name = mJsonObject.getString("pro_name");
                        pro_type_name = mJsonObject.getString("pro_type_name");
                        pro_qty = mJsonObject.getString("pro_qty");
                        pro_price = mJsonObject.getString("pro_price");
                        fname = mJsonObject.getString("fname");
                        lname = mJsonObject.getString("lname");
                        city = mJsonObject.getString("city");
                        deliveryarea = mJsonObject.getString("deliveryarea");
                        addresstype = mJsonObject.getString("addresstype");
                        street = mJsonObject.getString("street");
                        building = mJsonObject.getString("building");
                        floor = mJsonObject.getString("floor");
                        house_no = mJsonObject.getString("house_no");
                        mobile = mJsonObject.getString("mobile");
                        phone = mJsonObject.getString("phone");
                        email= mJsonObject.getString("email");
                        instruction= mJsonObject.getString("instruction");
                        aditional_direction = mJsonObject.getString("aditional_direction");
                        service_fee = mJsonObject.getString("service_fee");
                        container_charges = mJsonObject.getString("container_charges");
                        delivery_charges = mJsonObject.getString("delivery_charges");
                        subtotal = mJsonObject.getString("subtotal");
                        total = mJsonObject.getString("total");
                        order_date = mJsonObject.getString("order_date");
                        order_status = mJsonObject.getString("order_status");

                        bm.setUser_id(user_id);
                        bm.setOrder_item_id(order_item_id);
                        bm.setOrder_id(order_id);
                        bm.setPro_name(pro_name);
                        bm.setPro_type_name(pro_type_name);
                        bm.setPro_qty(pro_qty);
                        bm.setPro_price(pro_price);
                        bm.setFname(fname);
                        bm.setLname(lname);
                        bm.setCity(city);
                        bm.setDeliveryarea(deliveryarea);
                        bm.setAddresstype(addresstype);
                        bm.setStreet(street);
                        bm.setBuilding(building);
                        bm.setFloor(floor);
                        bm.setHouse_no(house_no);
                        bm.setMobile(mobile);
                        bm.setPhone(phone);
                        bm.setEmail(email);
                        bm.setInstruction(instruction);

                        bm.setAditional_direction(aditional_direction);
                        bm.setService_fee(service_fee);
                        bm.setContainer_charges(container_charges);
                        bm.setDelivery_charges(delivery_charges);
                        bm.setSubtotal(subtotal);
                        bm.setTotal(total);
                        bm.setOrder_date(order_date);
                        bm.setOrder_status(order_status);

                        bmo.add(bm);
                        System.out.println("<<<oo>>>>" + bm);

                        // TextView Name,Ads,Item_Name,Qty,Delivry_date,Location;
                        Name.setText(bm.getFname() + "" + bm.getLname());
                        Ads.setText(bm.getHouse_no() + "," + bm.getBuilding()+ "\n" + bm.getFloor()+ "," + bm.getStreet() + "\n" + bm.getCity());
                        Item_Name.setText(bm.getPro_name());
                        Qty.setText(bm.getPro_qty());
                        Delivry_date.setText(bm.getOrder_date());
                      //  Location.setText(bm.getMobile());
                        Phone.setText(bm.getMobile());
                        Email.setText(bm.getEmail());
                        Instruction.setText(bm.getInstruction());
                        type.setText("-"+bm.getPro_type_name());


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


        }
    }


    private class SendPostRequest3 extends AsyncTask<String, Void, String> {

        public void onPreExecute() {
            progress.setVisibility(View.VISIBLE);
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/accept.php?"); // here is your URL path

                JSONObject postDataParams = new JSONObject();

                //postDataParams.put("user_id", user_id );
                postDataParams.put("order_id",order_id);
                postDataParams.put("order_item_id",order_item_id);
              /*  user_id
                        pro_type_name
                product_id*/
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
   /*         Toast.makeText(Butcher_My_orderDetail.this, result,
                    Toast.LENGTH_LONG).show();*/
            progress.setVisibility(View.GONE);

            //  sl = new Salma_Retailer_Login_Model();
            if (result.equals("\"success\"")) {
                progress.setVisibility(View.GONE);
                MaterialStyledDialog dialog = new MaterialStyledDialog(Butcher_My_orderDetail.this)
                        .setTitle(getResources().getString(R.string.success))
                        .setDescription(getResources().getString(R.string.accept_order))
                        .setIcon(R.drawable.ic_done_white_24dp)
                        .withIconAnimation(true)
                        .withDialogAnimation(true)
                        .setHeaderColor(R.color.colorPrimary)
                        .setCancelable(true)
                        .setPositive(getResources().getString(R.string.ok), new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {


                                Intent i=new Intent(getApplicationContext(),Butcher_my_order.class);


                                startActivity(i);
                                finish();
                                dialog.dismiss();


                            }
                        })
                        .build();

                dialog.show();
                //  String named = jsonObj.getString("name");

            } else {

                System.out.println("result" + result);
                MaterialStyledDialog dialog = new MaterialStyledDialog(Butcher_My_orderDetail.this)
                        .setTitle(getResources().getString(R.string.failed))
                        .setDescription(getResources().getString(R.string.accept_failed))
                        .setIcon(R.drawable.ic_error_white_24dp)
                        .withIconAnimation(true)
                        .withDialogAnimation(true)
                        .setHeaderColor(R.color.colorPrimary)
                        .setCancelable(true)
                        .setPositive(getResources().getString(R.string.ok), new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {


                            }
                        })
                        .build();

                dialog.show();


            }
        }


        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while (itr.hasNext()) {

                String key = itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
    }
}