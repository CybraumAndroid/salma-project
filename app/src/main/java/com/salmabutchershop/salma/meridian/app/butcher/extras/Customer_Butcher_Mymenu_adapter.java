package com.salmabutchershop.salma.meridian.app.butcher.extras;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;
import com.salmabutchershop.salma.meridian.app.butcher.login.Exploreappbutcher;
import com.salmabutchershop.salma.meridian.app.butcher.login.Salma_Butcher_Login_Model;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.DataAdapter_2;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_adapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 3/24/2017.
 */

public class Customer_Butcher_Mymenu_adapter extends RecyclerView.Adapter<Customer_Butcher_Mymenu_adapter.ViewHolder> {

    private Context appContext;
       ArrayList<Customer_Butcher_MEnu_Model> cbm;
        Context context;
    ProgressDialog pDlog2;
    String c,b;
    public String stored_user_id;
/*public Customer_Butcher_Mymenu_adapter(ArrayList<Customer_Butcher_MEnu_Model> cbm,String user_id, Context context) {

        }*/

    public Customer_Butcher_Mymenu_adapter(ArrayList<Customer_Butcher_MEnu_Model> cbm, Context applicationContext, String butcher_id) {
        this.cbm = cbm;
        this.context = applicationContext;

        stored_user_id=butcher_id;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        Button remove;
    // CardView cv;
    TextView speakr, topic, div, dept, evntnam;
    String imag;
    ImageView vs;
    TextView tv,tv1,tv2,tv3;
    // TextView personAge;
    public static MKLoader progress_loader;
    ImageView personPhoto;
    ProgressBar gallery_progressbar;
    ViewHolder(View itemView) {
        super(itemView);
        remove= (Button) itemView.findViewById(R.id.textViewName3);

        tv=   (TextView) itemView.findViewById(R.id.textViewName1);
        tv1=   (TextView) itemView.findViewById(R.id.textViewName2);
        tv2=   (TextView) itemView.findViewById(R.id.textViewName4);
        gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
        progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
        vs=   (ImageView) itemView.findViewById(R.id.imageView6);
    }
}


    @Override
    public int getItemCount() {
        return cbm.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.butcher_current_menu_item_layout, viewGroup, false);
        ViewHolder pvh = new ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder personViewHolder, final int i) {


        String s = cbm.get(i).getTyp_image();
        String y=cbm.get(i).getTyp_id();
        personViewHolder.tv.setText(cbm.get(i).getPro_sub_cat_name());
        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont5);
personViewHolder.tv1.setText(cbm.get(i).getPro_cutting_type_name());
        Typeface myFont6 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv1.setTypeface(myFont6);
        personViewHolder.tv2.setText(cbm.get(i).getTyp_currency()+"  "+cbm.get(i).getTyp_prize()+"/"+cbm.get(i).getTyp_qnty());
        Typeface myFont7 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont7);
        c= cbm.get(i).getButcher_id();
        b=cbm.get(i).getTyp_id();
        //  String img=enm.get(i).getNews_img();
        System.out.println("oooyyyyyyyyyyyy>>>>>>" +b);

        personViewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {

                    new SweetAlertDialog(view.getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(context.getResources().getString(R.string.Delete))
                         .setContentText(context.getResources().getString(R.string.AreyousureWanttodeletetheproduct))
                            .setCustomImage(R.drawable.logo)
                            .setConfirmText(context.getResources().getString(R.string.Yes))
                            .setCancelText(context.getResources().getString(R.string.No))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    if (DetectConnection
                                            .checkInternetConnection(context)) {
                                        // cbm.get(i).getTyp_id(),i
                                        final String  SSS = cbm.get(i).getTyp_id();
                                        System.out.println("ayooooooooooooooooooooooooooooooooooooo>>>>>>"+SSS);
                                        new SendPostRequest1(SSS,i).execute();
                                        personViewHolder.progress_loader.setVisibility(View.VISIBLE);
                                    } else {
                                        com.nispok.snackbar.Snackbar.with(context) // context
                                                .text(R.string.oops_your_connection_Seems_off) // text to display
                                                .show((Activity) context);
                                    }
                                  sDialog
                                            /*.setTitleText("Deleted!")
                                            .setContentText("Your Menu Item has been deleted!")*/
                                            //.setConfirmText("OK")
                                            .setConfirmClickListener(null);
                                    sDialog.dismiss();
                                           // .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                }
                            })

                            .show();





                }
                catch(Exception e){
                    e.printStackTrace();

                }




            }
        });
        //  Picasso.with(context)
        try {


        new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                .build()
                .load(s)
                .noFade()
                .into(personViewHolder.vs, new Callback() {
                    @Override
                    public void onSuccess() {
                        personViewHolder.progress_loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });

    }catch (Exception e){
        e.printStackTrace();
    }

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    private class SendPostRequest1 extends AsyncTask<String, Void, String> {
        int i;
        int position;
        String delete_id;
        public SendPostRequest1(String SSS,int pos){
            delete_id=SSS;
            position=pos;
          //  i=pos;
        }
        public void onPreExecute() {
           //progress.setVisibility(View.VISIBLE);
       /*     pDlog2 = new ProgressDialog(context,R.style.MyTheme);

            pDlog2.setCancelable(false);
            pDlog2.show();*/
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/deletemenu.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();
System.out.println("<<<<<<<<<<<<<<<<<<<<<<typeid"+delete_id);

                postDataParams.put("butcher_id",stored_user_id);
                postDataParams.put("type_id",b);
               // postDataParams.put("type","3");
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
            //   Toast.makeText(LoginActivityRetailer.this, result,
            //    Toast.LENGTH_LONG).show();
     //Toast.makeText(context,result,Toast.LENGTH_LONG).show();
          /*  if(pDlog2.isShowing())
                pDlog2.dismiss();*/
            if (result.contentEquals("\"success\"")) {

                 /*   new SweetAlertDialog((Activity) context , SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Deleted!")
                            .setContentText("You clicked the button!")
                            .show();*/
                    //  Toast.makeText(context,"Deleted",Toast.LENGTH_LONG).show();*/
                    cbm.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, cbm.size());
                    if (cbm.size() == 0) {
                   /* ((Activity)context).getFragmentManager().popBackStackImmediate();*/
                    }

            }

            else
            {
                Toast.makeText(context,"Failed.Try again later.",Toast.LENGTH_LONG).show();

            }
        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}
