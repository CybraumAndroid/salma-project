package com.salmabutchershop.salma.meridian.app.customer.address;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;

/**
 * Created by libin on 3/23/2017.
 */

public class Collect_final_Screen extends AppCompatActivity {
    TextView CF,PC,AD,PH,EM;
    String lat,lng,Shop_ads,Shop_email,Shop_phone;
    Button Loc,Hom;
    Double la,ln;
  //  TextView AD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collect_final_screen);
        PC = (TextView) findViewById(R.id.tf);
        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        PC.setTypeface(myFont1);
        CF = (TextView) findViewById(R.id.cf);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        CF.setTypeface(myFont2);
        Intent intent = getIntent();
        lat=intent.getStringExtra("lat");
        lng=intent.getStringExtra("lng");
        Shop_ads=intent.getStringExtra("Shop_ads");
        Shop_email=intent.getStringExtra("Email");
        Shop_phone=intent.getStringExtra("Phone");
        AD= (TextView) findViewById(R.id.ad);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        AD.setTypeface(myFont4);
        PH = (TextView) findViewById(R.id.ph);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        PH.setTypeface(myFont6);

        EM= (TextView) findViewById(R.id.em);
        Typeface myFont5= Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        EM.setTypeface(myFont5);
        AD.setText(Shop_ads);
        PH.setText(Shop_email);
        EM.setText(Shop_phone);
        Hom= (Button) findViewById(R.id.button10);
        Hom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                startActivity(i);
                finish();

            }
        });
        Loc=(Button) findViewById(R.id.button11);
        Loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                la= Double.valueOf(lat);
                ln= Double.valueOf(lng);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr="+la+","+ln));
                startActivity(intent);
            }
        });
    }
}