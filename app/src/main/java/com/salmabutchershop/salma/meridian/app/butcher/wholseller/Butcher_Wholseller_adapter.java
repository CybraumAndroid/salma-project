package com.salmabutchershop.salma.meridian.app.butcher.wholseller;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by libin on 9/29/2016.
 */
public class Butcher_Wholseller_adapter extends RecyclerView.Adapter<Butcher_Wholseller_adapter.ViewHolder> {


    List<Butcher_Wholseller_Model> zwm;
    Context context;



    public Butcher_Wholseller_adapter(ArrayList<Butcher_Wholseller_Model> zwm, Context context) {
        this.zwm = zwm;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3,tv4;
        // TextView personAge;
ProgressBar gallery_progressbar;
        ImageView personPhoto;
        public static MKLoader progress_loader;
        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            ///  tv=   (TextView) itemView.findViewById(R.id.title);
            tv1=   (TextView) itemView.findViewById(R.id.textView8);
            tv2=   (TextView) itemView.findViewById(R.id.textView11);
            tv3=   (TextView) itemView.findViewById(R.id.textView10);
            tv4=   (TextView) itemView.findViewById(R.id.textView14);
            gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
            v=   (ImageView) itemView.findViewById(R.id.imageView8);
        }
    }


    @Override
    public int getItemCount() {
        return zwm.size();
    }


    @Override
    public Butcher_Wholseller_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wholeseller_item_layout, viewGroup, false);
        Butcher_Wholseller_adapter.ViewHolder pvh = new Butcher_Wholseller_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Butcher_Wholseller_adapter.ViewHolder personViewHolder, final int i) {


        String s = zwm.get(i).getWholeseller_logo();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
      personViewHolder.tv1.setTypeface(myFont2);
             personViewHolder.tv1.setText(zwm.get(i).getShop_name());
        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont3);
        personViewHolder.tv2.setText(zwm.get(i).getName());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
      personViewHolder.tv3.setTypeface(myFont4);
        personViewHolder.tv3.setText(zwm.get(i).getAddress());


        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv4.setTypeface(myFont5);
        personViewHolder.tv4.setText(zwm.get(i).getLocation());

        //  String img=enm.get(i).getNews_img();
        System.out.println("ooo" + s);
        try{
        Picasso.with(context).load(s).noFade()  .into(personViewHolder.v, new Callback() {
            @Override
            public void onSuccess() {
             personViewHolder.progress_loader.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
        }catch (Exception e){
            e.printStackTrace();
        }
              //  into(personViewHolder.v);
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}