package com.salmabutchershop.salma.meridian.app.customer.database;

/**
 * Created by libin on 11/18/2016.
 */
public class JsonModel {
    public String getOut_tt() {
        return out_tt;
    }

    public void setOut_tt(String out_tt) {
        this.out_tt = out_tt;
    }

    String productid,product,qty,price,butcher_id,price_quantity,type_id,out_tt;

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getPrice_quantity() {
        return price_quantity;
    }

    public void setPrice_quantity(String price_quantity) {
        this.price_quantity = price_quantity;
    }

    Float total;

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getButcher_id() {
        return butcher_id;
    }

    public void setButcher_id(String butcher_id) {
        this.butcher_id = butcher_id;
    }
}
