package com.salmabutchershop.salma.meridian.app.butcher.wholseller;

/**
 * Created by libin on 9/29/2016.
 */
public class Butcher_Wholseller_Model {
    String id,shop_name,name,address,address2,location,wholeseller_logo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getWholeseller_logo() {
        return wholeseller_logo;
    }

    public void setWholeseller_logo(String wholeseller_logo) {
        this.wholeseller_logo = wholeseller_logo;
    }
}
