package com.salmabutchershop.salma.meridian.app.customer.sidebar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.sidebar.Butcher_FeedBack;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

public class FeedBack extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    Button submit;
    EditText feedback;
    int selectedRadiobuttonId;
    private String USER_ID_KEY="user_id";
    private String MESSAGE_KEY="message";
    //private String SUBJECT_KEY="message";

    private String feedbackMessage,subject;
    public static MKLoader progress_loader;
    ProgressBar progress;
    CheckBox wait_time,customer_service,environment,quality,other;
    boolean isChecked1,isChecked2,isChecked3,isChecked4;
    String one,two,three,four;
    String c1,c2,c3,c4,userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        SharedPreferences preferencesd= getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        userid = preferencesd.getString("user_id", null);
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<Customerloginid1>>>>>>>>>>>>>>>>>>>>>>"+userid);
      //  radioGroup=(RadioGroup)findViewById(R.id.radio_group);
       wait_time=(CheckBox)findViewById(R.id.wait_time);
       customer_service=(CheckBox)findViewById(R.id.customer_service);
      //  environment=(CheckBox)findViewById(R.id.environment);
       quality=(CheckBox)findViewById(R.id.quality);
        other=(CheckBox)findViewById(R.id.other);
        submit=(Button)findViewById(R.id.submit);
        feedback=(EditText)findViewById(R.id.feedback);
        if (userid != null && !userid.isEmpty()){

        }
        else {
            new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText(getResources().getString(R.string.Pleaseregister))
                    .setCustomImage(R.drawable.logo)
                    .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                    .setConfirmText(getResources().getString(R.string.Yes))
                    .setCancelText(getResources().getString(R.string.No))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                            sDialog.dismiss();

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
        }
            progress = (ProgressBar) findViewById(R.id.progress_bar);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.layout);
        rl.setBackgroundColor(Color.parseColor("#ffffff"));
        wait_time.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    one=wait_time.getText().toString();
                    c1="Yes";
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<3"+one);
                    // perform logic
                }
                else {
                    c1="No";
                }

            }
        });
        customer_service.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    two=customer_service.getText().toString();
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<1"+two);
                    c2="Yes";
                    // perform logic
                }
                else {
                    c2="No";
                }

            }
        });
        quality.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    three=quality.getText().toString();
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<1"+three);
                    // perform logic
                    c3="Yes";
                }
                else {
                    c3="No";
                }

            }
        });
        other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    // perform logic
                    four=other.getText().toString();
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<1"+four);
                    // perform logic
                    c4="Yes";
                }
                else {
                    c4="No";
                }

            }
        });
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              FeedBack.this.goBack();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                isChecked1 = ((CheckBox) findViewById(R.id.wait_time)).isChecked();
                    isChecked2 = ((CheckBox) findViewById(R.id.customer_service)).isChecked();
                    isChecked3 = ((CheckBox) findViewById(R.id.quality)).isChecked();
                    isChecked4 = ((CheckBox) findViewById(R.id.quality)).isChecked();
                    if(wait_time.isChecked()){
                        one=wait_time.getText().toString();

                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<1"+one);
                    }
                    else if(customer_service.isChecked()){
two=customer_service.getText().toString();
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<1"+two);
                    }
                    else if(quality.isChecked()){
three=quality.getText().toString();
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<3"+three);
                    }
                    else if(other.isChecked()){
four=other.getText().toString();
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<4"+four);
                    }

                    feedbackMessage=feedback.getText().toString();
                    if(!feedbackMessage.isEmpty()){
                        if (userid != null && !userid.isEmpty()){
                            new PostFeedback().execute();
                        }
                        else {
                            new SweetAlertDialog(FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText(getResources().getString(R.string.Pleaseregister))
                                    .setCustomImage(R.drawable.logo)
                                    .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                                    .setConfirmText(getResources().getString(R.string.Yes))
                                    .setCancelText(getResources().getString(R.string.No))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(i);
                                            finish();
                                            sDialog.dismiss();

                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                        }

                    }
                    else {
                        com.nispok.snackbar.Snackbar.with(FeedBack.this) // context
                                .text("Please type Something!") // text to display
                                .show(FeedBack.this);

                    }


                } else {
                    com.nispok.snackbar.Snackbar.with(FeedBack.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(FeedBack.this);

                }





            }
        });

    }

  /*  private void goBack() {
        super.onBackPressed();
    }*/


    class PostFeedback extends AsyncTask<String,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try{
            URL url = new URL(SERVER_CUSTOMER+"feedback.php"); // here is your URL path

            JSONObject postDataParams = new JSONObject();

              /*  wait_time
                        customer_service
                quality
                        other*/

            //
            postDataParams.put(USER_ID_KEY, userid);
           // postDataParams.put(SUBJECT_KEY,feedbackMessage );
            postDataParams.put(MESSAGE_KEY, feedbackMessage);
         postDataParams.put("wait_time",c1);
          postDataParams.put("customer_service",c2);
                postDataParams.put("quality",c3);
        postDataParams.put("other", c4);
            Log.e("params", postDataParams.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new
                        InputStreamReader(
                        conn.getInputStream()));

                StringBuffer sb = new StringBuffer("");
                String line = "";

                while ((line = in.readLine()) != null) {

                    sb.append(line);
                    break;
                }

                in.close();
                return sb.toString();

            } else {
                return new String("false : " + responseCode);
            }
        } catch (Exception e)

        {
            return new String("Exception: " + e.getMessage());
        }


        }

        @Override
        protected void onPostExecute(String result) {


       //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            super.onPostExecute(result);
            progress_loader.setVisibility(View.GONE);


            if (result.equals("\"failed\"")) {
                new SweetAlertDialog(FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


            } else if(result.equals("\"success\"")){
                new SweetAlertDialog(FeedBack.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getResources().getString(R.string.ThankYou))
                        .setContentText(getResources().getString(R.string.Feedbacksubmitted))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                                startActivity(i);
                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();






            }
            else {
                new SweetAlertDialog(FeedBack.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setContentText(getResources().getString(R.string.Somethingwentwrong))
                        .setCustomImage(R.drawable.logo)
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }


        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
    private void goBack() {
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),Customer_Services_Fragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}
