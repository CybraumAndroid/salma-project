package com.salmabutchershop.salma.meridian.app.butcher.extras;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Model;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_adapter;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 3/23/2017.
 */

public class Butcher_NewMenu extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

    Customer_Butcher_Mymenu_adapter adapter;
    ProgressBar progress;
    String result, galry_image,Dat;
    static ArrayList<Customer_Butcher_MEnu_Model> cbm;
    Customer_Butcher_MEnu_Model cb;
    String added_by,cat_id,subcat_name,sub_image,sub_id;
  String k,l="1";
    Button Beef,Mutton,Camel,Other,Othe,Fish;
    LinearLayout other;
  //  private BottomBar mBottomBar;
    String Wurl;
    String shp_nam,del_info;
    String userid,fullname;
    String butcher_id,typ_id,pro_cutting_type_id,pro_cutting_type_name,pro_cat_id,
            pro_cat_name,pro_sub_cat_id,pro_sub_cat_name,typ_old_price,typ_prize,
            typ_qnty,typ_unit,typ_currency,typ_discount,typ_discount_unit,typ_desc,typ_delivery,typ_image;
    TextView pro_nm,butcher_name;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buther_current_menu_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        pro_nm= (TextView) findViewById(R.id.pro_nm);
        butcher_name= (TextView) findViewById(R.id.butcher_name);
       /* Intent intent = getIntent();
        k = intent.getStringExtra("add_id");
        l= intent.getStringExtra("cat_id");*/
       /* shp_nam=intent.getStringExtra("shp_nam");
        del_info=intent.getStringExtra("del_info");*/
        TextView tv = (TextView) findViewById(R.id.textView1);
        String ct = DateFormat.getDateInstance().format(new Date());
        tv.setText(ct);
        //  i.putExtra("shp_nam",d);
        // i.putExtra("del_info",k);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +shp_nam);
        System.out.println(">>>>>>cattttttttttttttttttttttttttttttttttttttttttt>>>>>>>>>" +del_info);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullnameb", null);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaHHHHHHHHHHHHHHHHHHaaaaaaaaaaaaaaaaaaaa>>>>>>>>>"+userid);
        System.out.println(">>>>>>fullnameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee>>>>>>>>>"+fullname);
        butcher_name.setText(fullname);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        other= (LinearLayout) findViewById(R.id.other);
        other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l = "6";
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                    new DownloadData1().execute();
                } else {
                    com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Butcher_NewMenu.this);
                 /*   com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_Sublist_Fragment.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/


                }

            }
        });
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Butcher_NewMenu.this.goBack();
            }
        });
        final String[] colors = {"#d62041", "#d62041", "#d62041"};

       /* final SquareFragment fragment = new SquareFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("color", Color.parseColor(colors[0]));
        fragment.setArguments(bundle);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, fragment, "square")
                .commit();*/

        AHBottomNavigation bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        // AHBottomNavigationItem item1 = new AHBottomNavigationItem(getString(R.string.Beef), R.drawable.beef_img, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item2 = new AHBottomNavigationItem(getString(R.string.Mutton), R.drawable.mutton, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item3 = new AHBottomNavigationItem(getString(R.string.Camel), R.drawable.camel, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item4 = new AHBottomNavigationItem(getString(R.string.Chicken), R.drawable.other, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item5 = new AHBottomNavigationItem(getString(R.string.Fish), R.drawable.fish, Color.parseColor(colors[0]));
        //  AHBottomNavigationItem item6 = new AHBottomNavigationItem(getString(R.string.tab_3), R.drawable.fish, Color.parseColor(colors[0]));
// Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.BeefMeat, R.drawable.beef_img, R.color.colorAccent);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.MuttonMeat, R.drawable.mutton, R.color.colorAccent);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.CamelMeat, R.drawable.camel, R.color.colorAccent);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.ChickenMeat, R.drawable.other, R.color.colorAccent);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.Fishes, R.drawable.fish, R.color.colorAccent);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);
        //  bottomNavigation.addItem(item6);
        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

// Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

        // Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

// Display color under navigation bar (API 21+)
// Don't forget these lines in your style-v21
// <item name="android:windowTranslucentNavigation">true</item>
// <item name="android:fitsSystemWindows">true</item>
        bottomNavigation.setTranslucentNavigationEnabled(true);

// Manage titles
        //  bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        //  bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

// Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);
l="1";
        pro_nm.setText(getResources().getString(R.string.BeefMeat));
// Set current item programmatically
        bottomNavigation.setCurrentItem(Integer.parseInt(l));


// Enable the translation of the FloatingActionButton
        //  bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);

// Change colors
        //  bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        //bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

// Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#d62041"));

        bottomNavigation.setAccentColor(Color.parseColor("#d62041"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        //  Enables Reveal effect
        bottomNavigation.setColored(true);

        bottomNavigation.setCurrentItem(0);
        // bottomNavigation.

        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if(position==0){
                    l = "1";
                    pro_nm.setText(getResources().getString(R.string.BeefMeat));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {

                        com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off))// text to display
                                .show(Butcher_NewMenu.this);

                    }
                }
                else if (position==1){
                    l="2";
                    pro_nm.setText(getResources().getString(R.string.MuttonMeat));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {

                        com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off))  // text to display
                                .show(Butcher_NewMenu.this);

                    }
                }
                else if (position==2){
                    l="3";
                    pro_nm.setText(getResources().getString(R.string.CamelMeat));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off))// text to display
                                .show(Butcher_NewMenu.this);

                    }
                }
                else if (position==3){
                    l="4";
                    pro_nm.setText(getResources().getString(R.string.ChickenMeat));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {
                        com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(Butcher_NewMenu.this);
                    }
                }
                else if (position==4){
                    l="5";
                    pro_nm.setText(getResources().getString(R.string.Fishes));
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)

                        new DownloadData1().execute();
                    } else {

                        com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off))// text to display
                                .show(Butcher_NewMenu.this);
                    }
                }
                return true;
            }
        });
        //End of BottomBar menu
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {

            com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Butcher_NewMenu.this);

        }
        // new DownloadData1().execute();

    }

    private void goBack() {
        super.onBackPressed();
    }




    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);
            String lk=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>"+Wurl);
            if (lk.equalsIgnoreCase("English")) {
                Wurl=SERVER+"json/butcher/butcher_added_menu.php?butcher_id="+userid+"&cat_id="+l;
                System.out.println("<<<<<<lang>>>>>"+Wurl);
            }
            else{
                Wurl=SERVER+"json/butcher/ar/butcher_added_menu.php?butcher_id="+userid+"&cat_id="+l;
            }


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();

                System.out.println("<<<<<<<<<<<<<<<<,subinkuriakose>>>>>>>>>>>>>"+k+l);
                HttpPost httppost = new HttpPost(Wurl);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
            //  String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //   System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
        /*    if
                    (result.equalsIgnoreCase("")) {

             *//*   com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Butcher_NewMenu.this);*//*
                *//*Snackbar.with(Customer_Butcher_Sublist_Fragment.this,null)
                        .type(Type.SUCCESS)
                        .message("No Products!")
                        .duration(Duration.LONG)
                        .show();*//*
           Toast.makeText(getApplicationContext(), "No Products", Toast.LENGTH_SHORT).show();
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                recyclerview.setVisibility(View.VISIBLE);
                JSONArray mArray;
                cbm = new ArrayList<Customer_Butcher_MEnu_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        cb = new Customer_Butcher_MEnu_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                      /*  String butcher_id,typ_id,pro_cutting_type_id,pro_cutting_type_name,pro_cat_id,
                                pro_cat_name,pro_sub_cat_id,pro_sub_cat_name,typ_old_price,typ_prize,
                                typ_qnty,typ_unit,typ_currency,typ_discount,typ_discount_unit,typ_desc,typ_delivery,typ_image;*/

                        butcher_id = mJsonObject.getString("butcher_id");
                        typ_id = mJsonObject.getString("typ_id");
                        pro_cutting_type_id = mJsonObject.getString("pro_cutting_type_id");
                        pro_cutting_type_name = mJsonObject.getString("pro_cutting_type_name");
                        pro_cat_id = mJsonObject.getString("pro_cat_id");
                        pro_cat_name = mJsonObject.getString("pro_cat_name");
                        pro_sub_cat_id = mJsonObject.getString("pro_sub_cat_id");
                        pro_sub_cat_name = mJsonObject.getString("pro_sub_cat_name");
                        typ_old_price = mJsonObject.getString("typ_old_price");
                        typ_prize = mJsonObject.getString("typ_prize");
                        typ_qnty = mJsonObject.getString("typ_qnty");
                        typ_unit = mJsonObject.getString("typ_unit");
                        typ_currency = mJsonObject.getString("typ_currency");
                        typ_discount = mJsonObject.getString("typ_discount");
                        typ_discount_unit = mJsonObject.getString("typ_discount_unit");
                        typ_desc= mJsonObject.getString("typ_desc");
                        typ_delivery= mJsonObject.getString("typ_delivery");
                        typ_image= mJsonObject.getString("typ_image");

                        cb.setButcher_id(butcher_id);
                        cb.setTyp_id(typ_id);
                        cb.setPro_cutting_type_id(pro_cutting_type_id);
                        cb.setPro_cutting_type_name(pro_cutting_type_name);
                        cb.setPro_cat_id(pro_cat_id);
                        cb.setPro_cat_name(pro_cat_name);
                        cb.setPro_sub_cat_id(pro_sub_cat_id);
                        cb.setPro_sub_cat_name(pro_sub_cat_name);
                        cb.setTyp_old_price(typ_old_price);
                        cb.setTyp_prize(typ_prize);
                        cb.setTyp_qnty(typ_qnty);
                        cb.setTyp_unit(typ_unit);
                        cb.setTyp_currency(typ_currency);
                        cb.setTyp_discount(typ_discount);
                        cb.setTyp_discount_unit(typ_discount_unit);
                        cb.setTyp_desc(typ_desc);
                        cb.setTyp_delivery(typ_delivery);
                        cb.setTyp_image(typ_image);

                        /*sm.setAdded_by(added_by);
                        sm.setCat_id(cat_id);
                        sm.setSub_id(sub_id);
                        sm.setSubcat_name(subcat_name);
                        sm.setSub_image(sub_image);*/


                        cbm.add(cb);
                        System.out.println("<<<oo>>>>" + cb);

                        adapter = new Customer_Butcher_Mymenu_adapter(cbm, getApplicationContext(),butcher_id);
                        recyclerview.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    System.out.println("nulllllllllllllllllllllllllllllllll");
                    recyclerview.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.NoProducts), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            else{
            /*    com.nispok.snackbar.Snackbar.with(Butcher_NewMenu.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Butcher_NewMenu.this);*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
                recyclerview.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.NoProducts), Toast.LENGTH_SHORT).show();
            }

           /* recyclerview.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                           *//* String added_id=esm.get(position).getAdded_by();
                            String sub_id=esm.get(position).getSub_id();
                            String title=esm.get(position).getSubcat_name();
                            Intent i=new Intent(getApplicationContext(),BeefDetail.class);
                            i.putExtra("add_id",added_id);
                            i.putExtra("sub_id",sub_id);
                            i.putExtra("title",title);
                              *//**//*  i.putExtra("shp_nam",shp_nam);
                                i.putExtra("del_info",del_info);*//**//*
                            //  shp_nam=intent.getStringExtra("shp_nam");
                            //    del_info=intent.getStringExtra("del_info");
                            //i.putExtra("value",status);
                            startActivity(i);
                            // overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                            //  finish();*//*
                        }
                    })
            );*/
        }


    }
}

