package com.salmabutchershop.salma.meridian.app.customer.login;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;

import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Product_mymenu_Model;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Vis_FilePath;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.address.HttpHandler;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Model;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.location.ClearableAutoCompleteTextView;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Model;
import com.salmabutchershop.salma.meridian.app.customer.newintro.GPSTracker1;
import com.salmabutchershop.salma.meridian.app.retailer.login.LoginActivityRetailer;
import com.salmabutchershop.salma.meridian.app.retailer.login.PostRegistrationRetailer;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tuyenmonkey.mkloader.MKLoader;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.ContentValues.TAG;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;


/**
 * Created by libin on 11/18/2016.
 */

public class PostRegistration  extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView AlReg;
    EditText edtemail, edtphon, edtfullnam, edtusername,  edtpass, edtloctn,edtshopname,edtads1,edtads2,edtcntry;
    //  String email, phon, loc, pass, statusd, firstname, lastname, username, confirmpass;
    Button butsignup;
    boolean edittexterror = false;
    String REGISTER_URL = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/register.php";
    ImageView profile_img_upload, profile_img;
    String filePath;
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    //    ProgressDialog pd;
    ProgressBar progress;

    Spinner spinner;
    static String filename = "null", filenamepath = "null";
    private static final int REQUEST_CODE = 1;
    String type,fullname,phone,email,location,username,password_s,edtcntrys;
    String item;
    String result;
    String token;
    String refreshedToken,lng,lat;
    public static MKLoader progress_loader;
    CountryCodePicker ccp;
    String Cntry,Cty,cvc;
    String address,state,postalCode,knownName,Area,city,Country;
    GPSTracker1 gps;
    Double lt;
    Double lg;
    ImageView logo_img;
    private static final int PICK_FILE_REQUEST = 1;
    TextView Browse;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    String selectedFilePath;
    ClearableAutoCompleteTextView edtsub_locality;
   // String SubLocality;
    String SubLocality;
   // String vicinity_array[];
    String vicinity_array1[];
    AutoCompleteTextView autoCompleteTextView;
    AutoCompleteTextView autoCompleteTextView1;
    String autoCompleteTextViewss;
    double latitude;
    double longitude;
    String vicinity_array[],city_array_id[],city_array_city[],city_array_country_id[],city_array_lat[],city_array_lng[];
    String lat1,lng1;
    static ArrayList<Loc_Model> loc_city;
    Loc_Model loc_model;
    PeopleAdapter adapter;
    ArrayList<String> divisonlist;
    String   starttext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        lng = myPrefs.getString("longitude", null);
        lat = myPrefs.getString("latitude", null);
        Cntry = myPrefs.getString("cntry", null);
        Cty = myPrefs.getString("citys", null);
        cvc = myPrefs.getString("cvc", null);
        SubLocality= myPrefs.getString("SubLocality", null);
        // final String token = SharedPrefManager.getInstance(getApplicationContext()).getDeviceToken();
        //  token = SharedPrefManager.getInstance(this).getDeviceToken();
        System.out.println("<<<<<<<<<<<<<<<ref>>>>>>>>" + refreshedToken);
        logo_img = (ImageView) findViewById(R.id.logo_img);
        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            new getVicinity().execute();
            new getCountryList().execute();
            if (ContextCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistration.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistration.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                 latitude = gps.getLatitude();
              longitude = gps.getLongitude();
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                    // \n is for new line
                    // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }


        } else {
            com.nispok.snackbar.Snackbar.with(PostRegistration.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(PostRegistration.this);
        /*    com.chootdev.csnackbar.Snackbar.with(ButcherRegistration.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/

        }
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            if (ContextCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistration.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistration.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                 latitude = gps.getLatitude();
                  longitude = gps.getLongitude();

                    // \n is for new line
                    //    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(PostRegistration.this, Locale.getDefault());

                  /*  try {
                        lt = Double.valueOf(String.valueOf(latitude));
                        lg = Double.valueOf(String.valueOf(longitude));
                        addresses = geocoder.getFromLocation(lt, lg, 1);
                        progress_loader.setVisibility(View.VISIBLE);
                        if (!addresses.isEmpty()) {
                            Cntry = addresses.get(0).getCountryName();
                            System.out.println("-----------------------Country-----------------" + Country);
                            // Cntry.setText(Country);
                            // address = addresses.get(0).getAddressLine(0);
                            //  System.out.println("-----------------------adsggg-----------------" + address);
                            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            Cty = addresses.get(0).getLocality();
                            System.out.println("------------------city----------------------" + city);
                            //  Cty.setText(city);
                            Area = addresses.get(0).getSubLocality();
                            state = addresses.get(0).getLocality();
                            System.out.println("---------------Area-------------------------" + Area);
                            address = addresses.get(0).getAddressLine(0);
                               *//* if(autoCompView.getText().toString().equalsIgnoreCase("")){
                                    autoCompView.setText(address);
                                }*//*

                            System.out.println("-----------------------adsggg-----------------" + address);

                           *//* if(!Area.equalsIgnoreCase("")&&!Area.isEmpty()&&Area.equals(null)) {
                                autoCompView.setText(Area + "," + state);
                            }
                            else {
                                autoCompView.setHint("Type Your Lacality");
                            }*//*
                            /*//*//**//*    String country = addresses.get(0).getCountryName();
                            // System.out.println("-----------------cntry-----------------------" + country);*//**//*
                            postalCode = addresses.get(0).getPostalCode();
                            System.out.println("----------------postalcode------------------------" + postalCode);
                            knownName = addresses.get(0).getFeatureName();
                            System.out.println("-----------------knwn-----------------------" + knownName);
                            cvc = addresses.get(0).getCountryCode();
                            // textinfo.setText("" + address + "\n" + "" + city + "\n" + "" + state + "\n" + "" + postalCode + "\n" + "" + knownName);
                            SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();

                            editor.putString("city_id", cvc);
                            editor.putString("citys", city);
                            editor.putString("cntry", Country);
                            editor.putString("cvc", cvc);
                            // editor.putString("latitude", String.valueOf(latitude));
                            editor.commit();
                            progress_loader.setVisibility(View.GONE);
                        }
                        {
                            System.out.println("----------------------------------------");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }
            ccp = (CountryCodePicker) findViewById(R.id.ccp);
            ccp.setCountryForNameCode(cvc);
            ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected() {
                    //  Toast.makeText(PostRegistration.this, "Updated " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
                }
            });
            edtfullnam = (EditText) findViewById(R.id.edt_fullname);
            Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtfullnam.setTypeface(myFont1);
            edtphon = (EditText) findViewById(R.id.edt_phone);
            Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtphon.setTypeface(myFont2);
            edtemail = (EditText) findViewById(R.id.edt_email);
            try {

                Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
                edtemail.setTypeface(myFont3);
                autoCompleteTextView1 = (AutoCompleteTextView) findViewById(R.id.edt_location);
                autoCompleteTextView1.setThreshold(1);
                autoCompleteTextView1.setText(Cty);
            }catch (NullPointerException  e){

            }

           /* Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtloctn.setTypeface(myFont4);*/

            edtcntry = (EditText) findViewById(R.id.edt_country);
            Typeface myFont9 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtcntry.setTypeface(myFont9);
            edtcntry.setText(Cntry);
            edtcntry.setEnabled(false);

            edtusername = (EditText) findViewById(R.id.edt_username);
            Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtusername.setTypeface(myFont5);
            edtpass = (EditText) findViewById(R.id.edt_password);
            Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtpass.setTypeface(myFont6);
            edtshopname = (EditText) findViewById(R.id.edt_shopname);
            Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtshopname.setTypeface(myFont7);

            edtads1 = (EditText) findViewById(R.id.edt_ads1);
            Typeface myFont10 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtads1.setTypeface(myFont10);

            edtads2 = (EditText) findViewById(R.id.edt_ads2);
            Typeface myFont11 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            edtads2.setTypeface(myFont11);
            try {


            autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
            autoCompleteTextView.setThreshold(1);
            autoCompleteTextView.setText(SubLocality);
            }catch (NullPointerException e){

            }
            butsignup = (Button) findViewById(R.id.butsignup);
            Typeface myFont8 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            butsignup.setTypeface(myFont8);
            progress = (ProgressBar) findViewById(R.id.progress_bar);
            // profile_img= (ImageView) findViewById(R.id.profile_image);


            progress_loader.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    progress_loader.setVisibility(View.INVISIBLE);
                    return false;
                }
            });
            autoCompleteTextView1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    String l = getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>" + l);

                    if (l.equalsIgnoreCase("English")) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (autoCompleteTextView1.getRight() - autoCompleteTextView1.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                autoCompleteTextView1.setText("");
                                return true;
                            }
                        }
                    } else {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (autoCompleteTextView1.getLeft() - autoCompleteTextView1.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                                // your action here
                                autoCompleteTextView1.setText("");
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });

            autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    String l = getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>" + l);

                    if (l.equalsIgnoreCase("English")) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (autoCompleteTextView.getRight() - autoCompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                autoCompleteTextView.setText("");
                                return true;
                            }
                        }
                    } else {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (autoCompleteTextView.getLeft() - autoCompleteTextView.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                                // your action here
                                autoCompleteTextView.setText("");
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });
           /* edtloctn.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    String l = getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>" + l);

                    if (l.equalsIgnoreCase("English")) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtloctn.getRight() - edtloctn.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                edtloctn.setText("");
                                return true;
                            }
                        }
                    } else {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtloctn.getLeft() - edtloctn.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                                // your action here
                                edtloctn.setText("");
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });*/
           /* edtcntry.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    String l = getResources().getConfiguration().locale.getDisplayLanguage();
                    System.out.println("<<<<<<lang>>>>>" + l);

                    if (l.equalsIgnoreCase("English")) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtcntry.getRight() - edtcntry.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                edtcntry.setText("");
                                return true;
                            }
                        }
                    } else {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (edtcntry.getLeft() - edtcntry.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                                // your action here
                                edtcntry.setText("");
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });*/
            Browse = (TextView) findViewById(R.id.brwse);
            Browse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED) {

                        requestPermissions();
                    }
                    showFileChooser();
                }
            });
            butsignup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                /*final String email = editTextEmail.getText().toString();

                if (token == null) {

                    Toast.makeText(getApplicationContext(), "Token not generated", Toast.LENGTH_LONG).show();
                    return;
                }*/
                    email = edtemail.getText().toString();
                    password_s = edtpass.getText().toString();
                    // type = edtcnfrmpass.getText().toString();
                    phone = edtphon.getText().toString();
                    fullname = edtfullnam.getText().toString();
                    location = autoCompleteTextView1.getText().toString();
                    username = edtusername.getText().toString();
                    edtcntrys = edtcntry.getText().toString();
                    autoCompleteTextViewss= autoCompleteTextView.getText().toString();
                    edittexterror = false;
                    if (edtemail.getText().toString().isEmpty() || edtpass.getText().toString().isEmpty() || edtfullnam.getText().toString().trim().isEmpty() || edtphon.getText().toString().isEmpty() || autoCompleteTextView1.getText().toString().isEmpty() || edtusername.getText().toString().isEmpty()||autoCompleteTextView.getText().toString().isEmpty()) {
                        com.nispok.snackbar.Snackbar.with(PostRegistration.this) // context
                                .text(getResources().getString(R.string.empty_fields)) // text to display
                                .show(PostRegistration.this);
                    /*com.chootdev.csnackbar.Snackbar.with(PostRegistration.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Field Empty!")
                            .duration(Duration.SHORT)
                            .show();*/
                        edittexterror = true;
                        //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (edtemail.getText().toString().isEmpty()) {
                        edtemail.setError(getResources().getString(R.string.Enter_Email_Id));
                        edittexterror = true;
                    } else if (!isValidPassword(edtpass.getText().toString().trim())) {
                        edtpass.setError(getResources().getString(R.string.Password_should_be_minimum_6_characters));
                        edittexterror = true;
                    } else if (edtphon.getText().toString().isEmpty()) {
                        edtphon.setError(getResources().getString(R.string.Enter_Phone));
                        edittexterror = true;
                    } else if (edtfullnam.getText().toString().isEmpty()) {
                        edtfullnam.setError(getResources().getString(R.string.Enter_Full_name));
                        edittexterror = true;
                    } else if (autoCompleteTextView1.getText().toString().isEmpty()) {
                        autoCompleteTextView1.setError(getResources().getString(R.string.Enter_location));
                        edittexterror = true;
                    } else if (edtusername.getText().toString().isEmpty()) {
                        edtusername.setError(getResources().getString(R.string.Enter_user_name));
                        edittexterror = true;
                    }
                    else if (autoCompleteTextView.getText().toString().isEmpty()) {
                        autoCompleteTextView.setError(getResources().getString(R.string.Enter_location));
                        edittexterror = true;
                    }
                    else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
                        edtemail.setError(getResources().getString(R.string.Invalid_Email));
                        edittexterror = true;
                    } else if (!android.util.Patterns.PHONE.matcher(edtphon.getText().toString().trim()).matches()) {
                        edtphon.setError(getResources().getString(R.string.Invalid_Phone));
                        edittexterror = true;
                    }

                   /* else if (!isValidPassword(edtphon.getText().toString().trim())) {
                        edtphon.setError("Invalid Phone");
                        edittexterror = true;
                    }*/
                    else if (edtfullnam.getText().toString().isEmpty()) {
                        edtfullnam.setError(getResources().getString(R.string.Enter_First_Name));
                        edittexterror = true;
                    } else if (autoCompleteTextView.getText().toString().isEmpty()) {
                        autoCompleteTextView.setError(getResources().getString(R.string.Enter_location));
                        edittexterror = true;
                    } else if (edtusername.getText().toString().isEmpty()) {
                        edtusername.setError(getResources().getString(R.string.Enter_user_name));
                        edittexterror = true;
                    } else if (filenamepath != null) {
                        filename = filenamepath;


                    } else {
                        filename = "null";

                    }

                    if (edittexterror == false) {
                        if (DetectConnection
                                .checkInternetConnection(PostRegistration.this)) {
                            //Toast.makeText(getActivity(),
                            //	"You have Internet Connection", Toast.LENGTH_LONG)

                            if (selectedFilePath != null && !selectedFilePath.isEmpty())

                            {
                                uploadVideo1();


                                new SendPostRequest().execute();
                                // tt="";
                            } else {
                                new SendPostRequest().execute();
                                /*new SweetAlertDialog(PostRegistration.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(getResources().getString(R.string.MyMenu))
                                        .setContentText(getResources().getString(R.string.Areyousurewanttosetimageasdefault))
                                        .setConfirmText(getResources().getString(R.string.Yes))
                                        .setCancelText(getResources().getString(R.string.No))
                                        .setCustomImage(R.drawable.logo)
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {



                                                sDialog.dismiss();
                                            }
                                        })
                                        .show();*/

                            }

                        } else {
                            com.nispok.snackbar.Snackbar.with(PostRegistration.this) // context
                                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                    .show(PostRegistration.this);
                      /*  com.chootdev.csnackbar.Snackbar.with(PostRegistration.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("No internet Connection!")
                                .duration(Duration.SHORT)
                                .show();*/


                        }


                    }


                }
            });
        }
    }

    private void uploadVideo1() {
        class UploadVideo extends AsyncTask<Void, Void, String> {

            ProgressDialog uploading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress_loader.setVisibility(View.VISIBLE);
                // uploading = ProgressDialog.show(ButcherRegistration.this, "Salma", "Please wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progress_loader.setVisibility(View.GONE);
                // uploading.dismiss();

            }

            @Override
            protected String doInBackground(Void... params) {
                Vis_Upload3 u = new Vis_Upload3();
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<md>>>>>>>>>>>>>>>>>>>>>>>>>" + selectedFilePath);
                String msg = u.uploadVideo(selectedFilePath);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<mdeeeeeeeeeeeeeeeeeee>>>>>>>>>>>>>>>>>>>>>>>>>" + msg);

                return msg;
            }
        }
        UploadVideo uv = new UploadVideo();
        uv.execute();
    }


    private void requestPermissions() {
            ActivityCompat.requestPermissions(PostRegistration.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE);
            // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }


    private void showFileChooser() {
        try {
            Intent intent = new Intent();
            //sets the select file to all types of files
            intent.setType("image/*");
            //allows to select data and return it
            intent.setAction(Intent.ACTION_GET_CONTENT);
            //starts new activity to select file and return data
            startActivityForResult(Intent.createChooser(intent,"Choose File to Vis_Upload.."),PICK_FILE_REQUEST);
        } catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            // Toast.makeText(Butcher_Product_Submit.this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                if(data == null){
                    //no data present
                    return;
                }

                try {
                    Uri selectedFileUri = data.getData();
                    selectedFilePath = Vis_FilePath.getPath(getApplicationContext(),selectedFileUri);
                    Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                    Log.i(TAG,"Selected File Path:" + selectedFilePath);

                    if(selectedFilePath != null && !selectedFilePath.equals("")){
                        // textViewResponse.setText(selectedFilePath);

                        filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);



                        CropImage.activity(selectedFileUri)
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .start(this);

                    }else{
                        com.nispok.snackbar.Snackbar.with(PostRegistration.this) // context
                                .text(getResources().getString(R.string.cannot_upload_file_to_server)) // text to display
                                .show(PostRegistration.this);
                   /* Snackbar.with(Butcher_Product_Submit.this,null)
                            .type(Type.SUCCESS)
                            .message("Cannot upload file to server!")
                            .duration(Duration.LONG)
                            .show();*/
                        //  Toast.makeText(getApplicationContext(),"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                    }
                    logo_img.setImageBitmap(bmp);
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
           /* else if(requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
                if(resultCode==RESULT_OK){
                    previewCapturedImage();
                }*/

            //  }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            try {


                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    System.out.println("selectedFileUri : "+resultUri);
            selectedFilePath = Vis_FilePath.getPath(getApplicationContext(), resultUri);
                    Bitmap bmp = BitmapFactory.decodeFile(selectedFilePath);
                    if(selectedFilePath != null && !selectedFilePath.equals("")){
                        // textViewResponse.setText(selectedFilePath);
                        filenamepath=selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                    }else{
                        com.nispok.snackbar.Snackbar.with(PostRegistration.this) // context
                                .text("Cannot upload file to server!") // text to display
                                .show(PostRegistration.this);

                    }
                    logo_img.setImageBitmap(bmp);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }catch (NullPointerException e){
                e.printStackTrace();
            }
        }

    }
    private boolean isValidPassword(String trim) {
        if (password_s != null && password_s.length() >= 6) {
            return true;
        }
        return false;
    }
    @Override
    protected void onStart() {
        super.onStart();

        checkAndRequestPermissions();


    }
    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();


        if(item.equalsIgnoreCase("customer")){
            type="1";
            edtads1.setVisibility(View.GONE);
            edtads2.setVisibility(View.GONE);
            edtshopname.setVisibility(View.GONE);
            edtloctn.setVisibility(View.VISIBLE);
        }else if(item.equalsIgnoreCase("retailer")){
            type="2";
            edtads1.setVisibility(View.VISIBLE);
            edtads2.setVisibility(View.VISIBLE);
            edtshopname.setVisibility(View.VISIBLE);
            edtloctn.setVisibility(View.GONE);
        }
        else if(item.equalsIgnoreCase("butchers")){
            type="3";
            edtads1.setVisibility(View.VISIBLE);
            edtads2.setVisibility(View.VISIBLE);
            edtshopname.setVisibility(View.VISIBLE);
            edtloctn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public class SendPostRequest extends AsyncTask<String, Void, String> {

        public void onPreExecute(){
            progress_loader.setVisibility(ProgressBar.VISIBLE);
        }

        public String doInBackground(String... arg0) {
            String js=filename.replaceAll(" ","");
            try {

                URL url = new URL(SERVER+"json/customer/register.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();



                postDataParams.put("fullname",fullname);
                postDataParams.put("phone",phone);
                postDataParams.put("email",email);
                postDataParams.put("location",location);
                postDataParams.put("username",username);
                postDataParams.put("password",password_s);
               postDataParams.put("token",refreshedToken);
                postDataParams.put("country",edtcntrys);
                postDataParams.put("city", location);
                postDataParams.put("longitude",lng);
                postDataParams.put("latitude",lat);
                postDataParams.put("type","1");
                postDataParams.put("profilepic",js);
                postDataParams.put("location",autoCompleteTextViewss);
                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
            progress_loader.setVisibility(ProgressBar.GONE);
           /* Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();*/
            if (result.contentEquals("\"success\"")){
                new SweetAlertDialog(PostRegistration.this,SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getResources().getString(R.string.AWESOME))
                        .setContentText(getResources().getString(R.string.PleaseLogin))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent inn= new Intent(getApplicationContext(),LoginActivity.class);

                                startActivity(inn);
                                finish();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

            }
            else if(result.contentEquals("\"already registerd\"")){

                new SweetAlertDialog(PostRegistration.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.AlreadyRegistred))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
    private class getVicinity extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(View.VISIBLE);
            if (ContextCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistration.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistration.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String result;

            try {
                HttpHandler h = new HttpHandler();
                System.out.println("location : " + latitude + "," + longitude);
                String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lat+","+lng+"&radius=500000&type=all&keyword=&key=AIzaSyAo27AsUvSaSFEQ5OZWGc4l2SczpZhwnL4"; // here is your URL path
                System.out.println("url : " + url);

                result = h.makeServiceCall(url);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute (String result){

            System.out.println("result : " + result);


            try {
                if (result != null) {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray gmap_result = jsonObject.getJSONArray("results");
                    vicinity_array = new String[gmap_result.length()];
                    for (int i = 0; i < gmap_result.length(); i++) {
                        JSONObject results_object = gmap_result.getJSONObject(i);
                        String vicinity = results_object.getString("vicinity");
                        vicinity_array[i] = vicinity;
                    }
                    System.out.println("----------------- VICINITY ------------------------");
                    for (int i = 0; i < vicinity_array.length; i++) {
                        System.out.println("vicinity : " + vicinity_array[i]);
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (PostRegistration.this, R.layout.customautocompleteview, R.id.autoCompleteItem, vicinity_array);
                    autoCompleteTextView.setAdapter(adapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            progress_loader.setVisibility(View.GONE);
        }
    }

    private class getVicinity1 extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(View.VISIBLE);
            if (ContextCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistration.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistration.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String result;

            try {
                HttpHandler h = new HttpHandler();
                System.out.println("location : " + latitude + "," + longitude);
                String url = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/city.php?cid="+cvc; // here is your URL path
                System.out.println("url : " + url);

                result = h.makeServiceCall(url);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute (String result){

            System.out.println("result : " + result);

            JSONArray mArray;
            try {
                mArray = new JSONArray(result);
                for (int i = 0; i < mArray.length(); i++) {

                   // ee = new Location_Services_Model();

                    JSONObject mJsonObject = mArray.getJSONObject(i);
                    //Log.d("OutPut", mJsonObject.getString("doctor_publish"));
//
                    for (int i1 = 0; i < mArray.length(); i++) {
                        JSONObject results_object = mArray.getJSONObject(i);
                        String city = results_object.getString("city");

                        vicinity_array1[i1] = city;
                    }

                   /* id = mJsonObject.getString("id");*/
                   // city = mJsonObject.getString("city");
                  //  country_id = mJsonObject.getString("country_id");


                    /*ee.setId(id);
                    ee.setCity(city);
                    ee.setCountry_id(country_id);


                    eem.add(ee);*/
               /* if (result != null) {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray gmap_result = jsonObject.getJSONArray("results");
                    vicinity_array1 = new String[gmap_result.length()];
                    for (int i = 0; i < gmap_result.length(); i++) {
                        JSONObject results_object = gmap_result.getJSONObject(i);
                        String vicinity = results_object.getString("vicinity");
                        vicinity_array1[i] = vicinity;
                    }
                    System.out.println("----------------- VICINITY ------------------------");
                    for (int i = 0; i < vicinity_array.length; i++) {
                        System.out.println("vicinity : " + vicinity_array[i]);
                    }
*/
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (PostRegistration.this, R.layout.customautocompleteview, R.id.autoCompleteItem, vicinity_array);
                    autoCompleteTextView1.setAdapter(adapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            progress_loader.setVisibility(View.GONE);
        }
    }

    private class getCountryList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(View.VISIBLE);
            if (ContextCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PostRegistration.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PostRegistration.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                gps = new GPSTracker1(PostRegistration.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(PostRegistration.this, Locale.getDefault());

                    try {
                              /*  lt= Double.valueOf(C_lat);
                                lg= Double.valueOf(C_long);*/
                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                        // progress.setVisibility(View.VISIBLE);
                        if (!addresses.isEmpty()) {
                            Country = addresses.get(0).getCountryName();

                            cvc = addresses.get(0).getCountryCode();

                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String result;

            try {
                HttpHandler h = new HttpHandler();

                String url =SERVER+"json/butcher/city.php?cid="+cvc; // here is your URL path
                System.out.println("url : " + url);

                result = h.makeServiceCall(url);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute (String result){

            System.out.println("result : " + result);
            loc_city = new ArrayList<Loc_Model>();
String cityh,id;
            final List<Loc_Model> list = new ArrayList<Loc_Model>();
            try {
                if (result != null) {

                    JSONArray jsonArray=new JSONArray(result);
                    city_array_id=new String[jsonArray.length()];
                    city_array_city=new String[jsonArray.length()];
                    city_array_country_id=new String[jsonArray.length()];
                    city_array_lat=new String[jsonArray.length()];
                    city_array_lng=new String[jsonArray.length()];
                    for(int i=0;i<jsonArray.length();i++){
                        loc_model = new Loc_Model();
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        city_array_id[i]=jsonObject.getString("id");
                        city_array_city[i]=jsonObject.getString("city");
                        city_array_country_id[i]=jsonObject.getString("country_id");
                        city_array_lat[i]=jsonObject.getString("latitude");
                        city_array_lng[i]=jsonObject.getString("longitude");

                    //  lat1= jsonObject.getString("latitude");
                    //  lng1= jsonObject.getString("longitude");
                        cityh= jsonObject.getString("city");
                        id= jsonObject.getString("id");

loc_model.setCity(cityh);
                        loc_model.setId(id);
                       // loc_model.setLat1(lat1);
                      //  loc_model.setLng1(lng1);
                        loc_city.add(loc_model);
                       /* divisonlist = new ArrayList<String>();
                        //Dep.add(dm);
                        for (Loc_Model loc_model : loc_city) {
                            divisonlist.add(loc_model.getLat1());
                            divisonlist.add(loc_model.getLng1());
                            //  divisonlist.add(dv.getBranch_id());
                        }*/
                        list.add(loc_model);

                    }
                    System.out.println("----------------- CITY ------------------------");
                    for (int i = 0; i < city_array_city.length; i++) {
                        System.out.println("city : " + city_array_city[i]);
                    }

                  ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (PostRegistration.this, R.layout.customautocompleteview, R.id.autoCompleteItem, city_array_city);

                   // adapter = new PeopleAdapter(PostRegistration.this, R.layout.customautocompleteview, R.id.autoCompleteItem, list);
                    autoCompleteTextView1.setAdapter(adapter);

                  autoCompleteTextView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            //TextView lbl_name= (TextView) view.findViewById(R.id.lbl_name);
                      starttext = String.valueOf(adapterView.getItemAtPosition(i));
                            System.out.println("name"+starttext);
                            new DownloadData3().execute();

                            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<lats>>>>>>>>>>>>"+lat1);
                            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<lats>>>>>>>>>>>>"+starttext);
                            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<lats>>>>>>>>>>>>"+lng1);


                        }
                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            progress_loader.setVisibility(View.GONE);
        }
    }

    private class DownloadData3 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;

        StringBuilder stringBuilder;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User


            String strnew = starttext.replaceAll(" ", "%20");
            String uri = "http://maps.google.com/maps/api/geocode/json?address="+strnew+"&sensor=false";
            HttpGet httpGet = new HttpGet(uri);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // JSONObject jsonObject = new JSONObject();
            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(View.GONE);
           // System.out.println(">>>>>>>>>>>>>>>" + result1);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());

                double lngd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                double latd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.d("latitude", "" + latd);
                Log.d("longitude", "" + lngd);
                lng = String.valueOf(lngd);
                lat = String.valueOf(latd);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlng>>>>>>>>>>>>>>>" + lng);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlat>>>>>>>>>>>>>>>" + lat);
                new getVicinity().execute();
               // new Customer_Butcher_List_Fragment.DownloadData1().execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}
