package com.salmabutchershop.salma.meridian.app.retailer;

/**
 * Created by libin on 12/6/2016.
 */
public class Retailer_main_Model {
    String id,retail_cat,retail_img;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRetail_cat() {
        return retail_cat;
    }

    public void setRetail_cat(String retail_cat) {
        this.retail_cat = retail_cat;
    }

    public String getRetail_img() {
        return retail_img;
    }

    public void setRetail_img(String retail_img) {
        this.retail_img = retail_img;
    }
}
