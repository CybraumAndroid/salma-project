package com.salmabutchershop.salma.meridian.app.retailer.login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.firebase.messaging.FirebaseMessaging;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;
import com.salmabutchershop.salma.meridian.app.butcher.login.Salma_Butcher_Login_Model;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.MainActivity;
import com.salmabutchershop.salma.meridian.app.customer.NetworkCheckingClass;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.CustomAlertDialog;

import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.login.PostRegistration;
import com.salmabutchershop.salma.meridian.app.firebase.app.Config;
import com.salmabutchershop.salma.meridian.app.retailer.RetailerMainPage;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;
import com.salmabutchershop.salma.meridian.app.retailer.login.news.NewsUpdates;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 11/3/2016.
 */
public class LoginActivityRetailer extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Button login;

    TextView Reg, txtnew, gst;
    EditText edtusrnam, edtpass;
    String usernam, pass, locationR;
    ProgressDialog pd;
    TextView twtr, gpls, lnkdlin, frgt;
    ImageView fb;
    WebView wv;
    // ArrayList<UserDetailsModel> arr_usrs = new ArrayList<>();
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String eml, status;


    //String REGISTER_URL = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/login.php";

    String Reg_id;
    static ArrayList<Salma_Butcher_Login_Model> slm;
    Salma_Butcher_Login_Model sl;
    ProgressBar progress;
    public static MKLoader progress_loader,progress_loader1;
    Button bsignup;
    String id, user_name, usertype, location, phone, email, log_status, name,address,address2,shop_name;
    Spinner spinner;
    String item,names,typ;
    String k,l;
    TextView Type;
    LinearLayout password_eye_show,password_eye_hide;
    boolean edittexterror = false;
    ProgressBar progress1;
    EditText editText;String countryR;
   String ret_typ,logo,city,fullname,user_names;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_retailer);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        // Spinner element
        spinner = (Spinner) findViewById(R.id.spinner);
        Type= (TextView) findViewById(R.id.type);
        Intent intent = getIntent();
        k = intent.getStringExtra("country");
        l = intent.getStringExtra("name");
        Type.setText(l);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add(getResources().getString(R.string.English));
        categories.add(getResources().getString(R.string.Arabic));

        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_idL", null);
        names= myPrefs.getString("fullname", null);
        typ= myPrefs.getString("usertype", null);
        ret_typ= myPrefs.getString("Ret_typ", null);

        System.out.println("<<<<<<<<<sharedname>>>>" + ret_typ);
        if (Reg_id != null && !Reg_id.isEmpty()){
            //Toast.makeText(getApplicationContext(), Reg_id, Toast.LENGTH_SHORT).show();


            Intent synin = new Intent(getApplicationContext(), Fresh_Frozen_Retailer.class);
            // synin.putExtra("brn_id",u);
            startActivity(synin);
            finish();
        }


        ArrayAdapter<String> dataAdapter=new ArrayAdapter<String>(this, R.layout.spinner_item_layout, R.id.textView17, categories);

        spinner.setAdapter(dataAdapter);


        edtusrnam = (EditText) findViewById(R.id.us_nam);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtusrnam.setTypeface(myFont7);


        edtpass = (EditText) findViewById(R.id.pw_d);
      final   Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtpass.setTypeface(myFont1);
        fb = (ImageView) findViewById(R.id.fbt);


        frgt = (TextView) findViewById(R.id.fgt);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        frgt.setTypeface(myFont2);



        TextView skip = (TextView) findViewById(R.id.txt_skip);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), Fresh_Frozen_Retailer.class);
                startActivity(i);
                // finish();
            }
        });


        wv = new WebView(this);

        frgt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  CustomAlertDialog cc = new CustomAlertDialog(LoginActivityRetailer.this);
                cc.show();*/
                final Dialog dialog = new Dialog(LoginActivityRetailer.this);
                //   String REGISTER_URL="http://app.zayedevents.com.php56-9.dfw3-2.websitetestlink.com/services/forgot.php?";
                //  dialog.setContentView(R.layout.loc_popup);
                final Window window = dialog.getWindow();
                window.requestFeature(Window.FEATURE_NO_TITLE);
                window.setContentView(R.layout.popup_forgot_layout);
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                editText= (EditText)dialog. findViewById(R.id.alert_email);
                progress1 = (ProgressBar)dialog.findViewById(R.id.progress_bars1);
                progress_loader1=(MKLoader)findViewById(R.id.progress_loader);
                Button btnDismiss = (Button) dialog.findViewById(R.id.gb);
                btnDismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String eml = editText.getText().toString();
                        edittexterror = false;
                        if (eml.matches("")) {
                            Toast.makeText(LoginActivityRetailer.this, "Empty Field", Toast.LENGTH_SHORT).show();
                        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString().trim()).matches()) {
                            editText.setError("Invalid Email");
                            edittexterror = true;
                        } else if (editText.getText().toString().isEmpty()) {
                            editText.setError("Enter Email Id");
                            edittexterror = true;

                        } else {
                            if (edittexterror == false) {
                                NetworkCheckingClass networkCheckingClass = new NetworkCheckingClass(LoginActivityRetailer.this);
                                boolean i = networkCheckingClass.ckeckinternet();
                                progress1.setVisibility(View.VISIBLE);


                                if (i == true) {
                                    String REGISTER_URL = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/forgotpassword.php";
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    System.out.println("statusforgot" + response + "xxxxx");

                                                    JSONObject jsonObj = null;
                                                    JSONArray jsonArray = null;
//                                        try {
//                                            jsonArray = new JSONArray(response);
//                                            for(int i=0;i<jsonArray.length();i++) {
//
//
//                                                jsonObj=jsonArray.getJSONObject(i);
//
//                                                status = jsonObj.getString("result");
//                                            }
                                                    if (response.contentEquals("\"success\"")) {
                                                        com.nispok.snackbar.Snackbar.with(LoginActivityRetailer.this) // context
                                                                .text("Please Check Your mail") // text to display
                                                                .show(LoginActivityRetailer.this);
                                                   /* Snackbar.with(LoginActivity.this,null)
                                                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                                                            .message("Please Check Your mail")
                                                            .duration(Duration.SHORT)
                                                            .show();*/
                                                        // Toast.makeText(LoginActivity.this, "Please Check Your mail", Toast.LENGTH_SHORT).show();
                                                        progress_loader1.setVisibility(View.GONE);
                                                        dialog.dismiss();

                                                    } else {
                                                        com.nispok.snackbar.Snackbar.with(LoginActivityRetailer.this) // context
                                                                .text("Invalid Credentials!") // text to display
                                                                .show(LoginActivityRetailer.this);
                                                  /*  Snackbar.with(LoginActivity.this,null)
                                                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                                                            .message("Invalid Credentials!")
                                                            .duration(Duration.SHORT)
                                                            .show();*/
                                                        // Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();
                                                        progress_loader1.setVisibility(View.GONE);
                                                    }


//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {

                                                }
                                            }) {
                                        @Override
                                        protected Map<String, String> getParams() {
                                            Map<String, String> params = new HashMap<String, String>();
                                            params.put("email",eml);
                                            return params;
                                        }

                                    };

                                    RequestQueue requestQueue = Volley.newRequestQueue(LoginActivityRetailer.this);
                                    requestQueue.add(stringRequest);
                                }
                            } else {
                                com.nispok.snackbar.Snackbar.with(LoginActivityRetailer.this) // context
                                        .text("No internet connection!") // text to display
                                        .show(LoginActivityRetailer.this);
                              /*  Snackbar.with(LoginActivity.this,null)
                                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                                        .message("No internet connection!")
                                        .duration(Duration.LONG)
                                        .show();*/
                            }



                        }
                    }
                });
                Button rb = (Button) dialog.findViewById(R.id.rb);
                rb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                    }
                });

                dialog.show();
                            }
                        });

//password eye--start
        password_eye_show= (LinearLayout) findViewById(R.id.password_eye_show);
        password_eye_hide=(LinearLayout) findViewById(R.id.password_eye_hide);

        password_eye_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_show.setVisibility(View.GONE);
                password_eye_hide.setVisibility(View.VISIBLE);
                edtpass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                edtpass.setTypeface(myFont1);
                edtpass.setSelection(edtpass.length());
            }
        });

        password_eye_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_hide.setVisibility(View.GONE);
                password_eye_show.setVisibility(View.VISIBLE);

                edtpass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                edtpass.setTypeface(myFont1);
                edtpass.setSelection(edtpass.length());

            }
        });
//password eye--end
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);

                startActivity(i);

            }
        });



        login = (Button) findViewById(R.id.sig_in);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        login.setTypeface(myFont3);

        bsignup = (Button) findViewById(R.id.sig_up);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        bsignup.setTypeface(myFont4);
        bsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {

                    Intent i = new Intent(getApplicationContext(), PostRegistrationRetailer.class);
                    i.putExtra("Retail_cat",ret_typ);
                    startActivity(i);
                    finish();

                } else {
                    com.nispok.snackbar.Snackbar.with(LoginActivityRetailer.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(LoginActivityRetailer.this);
                   /* com.chootdev.csnackbar.Snackbar.with(LoginActivityRetailer.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();
*/
                }

            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    user_name = edtusrnam.getText().toString();
                    pass = edtpass.getText().toString();
                    if (user_name.matches("") || pass.matches("")) {
                        com.nispok.snackbar.Snackbar.with(LoginActivityRetailer.this) // context
                                .text("Empty Fields!") // text to display
                                .show(LoginActivityRetailer.this);
                       /* Snackbar.with(LoginActivityRetailer.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Empty Fields!")
                                .duration(Duration.LONG)
                                .show();*/




                    } else {

                        new SendPostRequest().execute();

                    }

                } else {
                    com.nispok.snackbar.Snackbar.with(LoginActivityRetailer.this) // context
                            .text("Empty Fields!") // text to display
                            .show(LoginActivityRetailer.this);
                   /* com.chootdev.csnackbar.Snackbar.with(LoginActivityRetailer.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/

                }


            }
        });


    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        item = adapterView.getItemAtPosition(i).toString();


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public class SendPostRequest extends AsyncTask<String, Void, String> {

        public void onPreExecute() {
            progress_loader.setVisibility(View.VISIBLE);
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL(SERVER+"json/retailer/login.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("username",user_name);
                postDataParams.put("password",pass);
                postDataParams.put("type","2");
                postDataParams.put("retailer_type",ret_typ.replaceAll(" ",""));
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
            //   Toast.makeText(LoginActivityRetailer.this, result,
            //    Toast.LENGTH_LONG).show();
            progress_loader.setVisibility(View.GONE);

            sl = new Salma_Butcher_Login_Model();
            if (result.equals("\"failed\"")) {
                progress.setVisibility(View.GONE);
                com.nispok.snackbar.Snackbar.with(LoginActivityRetailer.this) // context
                        .text("Invalid Credentials!") // text to display
                        .show(LoginActivityRetailer.this);
               /* Snackbar.with(LoginActivityRetailer.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("Invalid Credentials!")
                        .duration(Duration.LONG)
                        .show();*/

            } else /*if(result.equals("\"success\""))*/{
                try {
                    JSONObject jsonObj = null;
                    slm = new ArrayList<Salma_Butcher_Login_Model>();
                    jsonObj = new JSONObject(result);
                  //  System.out.println("<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>"+result);
                    id = jsonObj.getString("id");

                    user_names = jsonObj.getString("user_name");
                    usertype = jsonObj.getString("usertype");
                    shop_name = jsonObj.getString("shop_name");
                    name = jsonObj.getString("name");
                    address = jsonObj.getString("address");
                    address2 = jsonObj.getString("address2");
                    phone= jsonObj.getString("phone");
                    email= jsonObj.getString("email");
                    log_status= jsonObj.getString("log_status");
                    // address2= jsonObj.getString("token");
                    logo = jsonObj.getString("logo");
                    city = jsonObj.getString("city");
                  locationR = jsonObj.getString("location");
                   countryR = jsonObj.getString("country");
                    //fullname = jsonObj.getString("fullname");

                    sl.setId(id);
                    sl.setUser_name(user_name);
                    sl.setUsertype(usertype);
                    sl.setShop_name(shop_name);
                    sl.setAddress(address);
                    sl.setAddress2(address2);
                    sl.setLocation(locationR);
                    sl.setPhone(phone);
                    sl.setEmail(email);
                    sl.setLog_status(log_status);
                    sl.setName(name);



                    slm.add(sl);
                    System.out.println("countryyyyyyyyyy" + countryR);

                                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();

                                    editor.putString("user_idL", id);
                                    editor.putString("fullname",shop_name);
                    editor.putString("fullnameR",user_names);
                    editor.putString("shp_nm",shop_name);
                    editor.putString("nam",name);
                    editor.putString("ads",address);
                    editor.putString("phne",phone);
                    editor.putString("eml",email);
                    editor.putString("logoR",logo);
                    editor.putString("cityR",city);
                    editor.putString("locationR",locationR);
                    editor.putString("countryR",countryR);
                                    // editor.putString("usertype",usertype );

                                    editor.commit();

                                    Intent inn= new Intent(getApplicationContext(),Exploreappretailer.class);
                                    startActivity(inn);
                                finish();
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivityRetailer.this);
                                    boolean Islogin= Boolean.parseBoolean("true");
                                    prefs.edit().putBoolean("Islogin", Islogin).commit();
                                    //   pd.dismiss();
                                    finish();



                              //  }
                       /*     })
                            .build();

                    dialog.show();*/





                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
           /* else {

            }*/
        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {

        Intent inn= new Intent(getApplicationContext(),SelectionPage.class);
        startActivity(inn);
        finish();
    }
}
