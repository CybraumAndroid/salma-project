package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.salmabutchershop.salma.meridian.app.GPSTracker;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Contact_Our_Team;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class DashboardActivity1 extends Activity implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener {
    int locationMode;
    Context mContext;
    GPSTracker gps;
    double latitude;
    double longitude;
    public static MKLoader progress_loader, progress_loader1;
    private GoogleMap googleMap;
    EditText textinfo;
    Button Choose;
    String address, city, state, postalCode, knownName;
    //String result;
    String id, shop_nam1, address1;
    static ArrayList<MySho_Near_Model> msn;
    MySho_Near_Model mn;
    ArrayList<LatLng> locations = new ArrayList();
    Double latitude1, longitude1;
    BitmapDescriptor icon;
    String citys;
    RecyclerView recyclerview;
    //   static ArrayList<UpcomingModel> upm;
    Customer_Butcher_List_adapter_For_Map adapter1;
    ProgressBar progress;
    String result, galry_image, Dat, Dat1;
    static ArrayList<Customer_Butcher_List_Model> zwm;
    Customer_Butcher_List_Model wm;

    String added_by, cat_id, butcher_logo, shop_name, delivery_info, butcher_discount;
    String dptmt, dvn_id, country_id, loc_name, shop_description;
    String Wurl;
    String shop_addrs;
    static LinearLayout Lct;
    // public static MKLoader progress_loader;
    String discount_desc;
    String js,jk;
    String dist;
    Button List;
    String lat,lng,strnew,k;
    String newlat,newlng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard1);

        textinfo = (EditText) findViewById(R.id.textView46);
//        Choose = (Button) findViewById(R.id.button7);
        progress_loader = (MKLoader) findViewById(R.id.progress_loader);
        List= (Button) findViewById(R.id.map_icon_image);


        icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_map_white_24dp);
        mContext = this;
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        Intent intent = getIntent();
       js = intent.getStringExtra("lat");
       jk=intent.getStringExtra("lng");
        k=intent.getStringExtra("cat_id");
      //  citys=intent.getStringExtra("strnew");
     latitude= Double.parseDouble(js);
        longitude= Double.parseDouble(jk);
        LocationOnMap(latitude, longitude);
        // Vertical
        //  OverScrollDecoratorHelper.setUpOverScroll(recyclerview, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        //   new HorizontalOverScrollBounceEffectDecorator(new RecyclerViewOverScrollDecorAdapter(recyclerview));
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerview.setLayoutManager(layoutManager1);

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(DashboardActivity1.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(DashboardActivity1.this);
           /* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


            //	Toast.makeText(getActivity(),
            //	getResources().getString(R.string.Sorry) +
            //	getResources().getString(R.string.cic),
            //	Toast.LENGTH_SHORT).show();
        }
        /*Choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("loc_ads", textinfo.getText().toString());
                editor.putString("ads", address);
                editor.putString("city", city);
                editor.putString("state", state);
                editor.putString("postal", postalCode);
                editor.putString("knwn", knownName);
                editor.commit();
                Intent i = new Intent(DashboardActivity1.this, Customer_Butcher_List_Fragment.class);

                i.putExtra("rm", textinfo.getText().toString());
                startActivity(i);
           *//* setResult(2,i);*//*

                finish();//finishing activity
            }
        });*/

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DashboardActivity1.this, Customer_Butcher_List_Fragment.class);
                i.putExtra("cat_id", "1");
                i.putExtra("value","1");
              //  i.putExtra("rm",textinfo.getText().toString());
                startActivity(i);
                finish();
            }
        });
        List.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DashboardActivity1.this, Customer_Butcher_List_Fragment.class);
                i.putExtra("cat_id", "1");
                i.putExtra("value","1");
                //  i.putExtra("rm",textinfo.getText().toString());
                startActivity(i);
                finish();
            }
        });
      /*  if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DashboardActivity1.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


        } else {

            Toast.makeText(mContext, "You already granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(mContext, DashboardActivity1.this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                // \n is for new line
                System.out.println("----------------------------------------");
                System.out.println("inside cangetlocation");
                System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                System.out.println("----------------------------------------");


                LocationOnMap(latitude, longitude);
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(DashboardActivity1.this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    if (!addresses.isEmpty()) {
                        address = addresses.get(0).getAddressLine(0);
                        System.out.println("-----------------------adsggg-----------------" + address);
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        city = addresses.get(0).getLocality();
                        System.out.println("------------------city----------------------" + city);
                        state = addresses.get(0).getAdminArea();
                        System.out.println("---------------state-------------------------" + state);
                        String country = addresses.get(0).getCountryName();
                        System.out.println("-----------------cntry-----------------------" + country);
                        postalCode = addresses.get(0).getPostalCode();
                        System.out.println("----------------postalcode------------------------" + postalCode);
                        knownName = addresses.get(0).getFeatureName();
                        System.out.println("-----------------knwn-----------------------" + knownName);
                        textinfo.setText("" + address);*//*"\n" + "" + city + "\n" + "" + state + "\n" + "" + postalCode + "\n" + "" + knownName)*//*
                        ;
                    }
                    {
                        System.out.println("----------------------------------------");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
*//*if (address!=null){


    //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
}else {


}*//*
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                int a = gps.showSettingsAlert(DashboardActivity1.this);

            }
        }*/


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                  /*  gps = new GPSTracker(mContext, DashboardActivity1.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();

                        // \n is for new line
                        System.out.println("----------------------------------------");
                        System.out.println("inside cangetlocation");
                        System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);
                        System.out.println("----------------------------------------");


                        LocationOnMap(latitude, longitude);
                        Geocoder geocoder;
                        List<Address> addresses = null;
                        geocoder = new Geocoder(DashboardActivity1.this, Locale.getDefault());

                        try {
                            addresses = geocoder.getFromLocation(latitude, longitude, 1);
                            if (!addresses.isEmpty()) {
                                address = addresses.get(0).getAddressLine(0);
                                System.out.println("-----------------------adsggg-----------------" + address);
                                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                city = addresses.get(0).getLocality();
                                System.out.println("------------------city----------------------" + city);
                                state = addresses.get(0).getAdminArea();
                                System.out.println("---------------state-------------------------" + state);
                                String country = addresses.get(0).getCountryName();
                                System.out.println("-----------------cntry-----------------------" + country);
                                postalCode = addresses.get(0).getPostalCode();
                                System.out.println("----------------postalcode------------------------" + postalCode);
                                knownName = addresses.get(0).getFeatureName();
                                System.out.println("-----------------knwn-----------------------" + knownName);
                                textinfo.setText("" + address);
                            }
                            {
                                System.out.println("----------------------------------------");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        *//*Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*//*
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        int a = gps.showSettingsAlert(DashboardActivity1.this);


                    }*/

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                  //  Toast.makeText(mContext, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }


                return;
            }
        }
    }


    public void LocationOnMap(double lat, double lng) {
        try {

            latitude = lat;
            longitude = lng;
          /*  MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Me");
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            googleMap.addMarker(marker);*/

            initilizeMap();


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initilizeMap() {
        if (googleMap == null) {
            ((MapFragment) getFragmentManager().findFragmentById(R.id.mapID)).getMapAsync(this);
            if (googleMap == null) {
               /* Toast.makeText(getApplicationContext(), "Sorry unable to create maps", Toast.LENGTH_LONG).show();*/
            }
            else {
               /* googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        LatLng position = marker.getPosition();
                        js= String.valueOf(position.latitude);
                        jk= String.valueOf(position.longitude);
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)
                        if (DetectConnection
                                .checkInternetConnection(getApplicationContext())) {
                            //  Lct.performClick();
                                           *//* Toast.makeText(getApplicationContext().getApplicationContext(), "Test1", Toast.LENGTH_LONG).show();*//*
                            new DownloadData1().execute();

                        } else {
                            com.nispok.snackbar.Snackbar.with(DashboardActivity1.this) // context
                                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                    .show(DashboardActivity1.this);

                        }

                        return true;
                    }
                });*/
               /* googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                    @Override
                    public void onMapClick(LatLng point) {
                        Log.d("Map","Map clicked");

                        js= String.valueOf(point.latitude);
                        jk= String.valueOf(point.longitude);
                        //Toast.makeText(getActivity(),
                        //	"You have Internet Connection", Toast.LENGTH_LONG)
                        new SweetAlertDialog(DashboardActivity1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                                .setCustomImage(R.drawable.logo)
                                .setTitleText("Are you sure")
                                .setContentText("Want to Select This Shop?")

                                .setCancelText(getResources().getString(R.string.Yes))
                                .setConfirmText(getResources().getString(R.string.No))
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    Intent i = new Intent(getApplicationContext(), Contact_Our_Team.class);
                                    // i.putExtra("cat_id",cat_id);
                                    //i.putExtra("value",status);

                                    startActivity(i);
                                    finish();
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        if (DetectConnection
                                                .checkInternetConnection(getApplicationContext())) {
                                            //  Lct.performClick();
                                            Toast.makeText(getApplicationContext().getApplicationContext(), "Test1", Toast.LENGTH_LONG).show();
                                            new DownloadData1().execute();

                                        } else {
                                            com.nispok.snackbar.Snackbar.with(DashboardActivity1.this) // context
                                                    .text("Oops Your Connection Seems Off..!") // text to display
                                                    .show(DashboardActivity1.this);
                   *//*     com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();
*//*
                                        }
                                        sDialog.dismiss();
                            .setTitleText("We are rapidly expanding!")
                            .setContentText("We will let you \n know when our shop is available in your Area")
                            .setConfirmText("OK")
                            .setConfirmClickListener(null)
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                    }
                                })
                                .show();

                    }
                });*/
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap map) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        googleMap = map;

        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true); // true to enable

        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);


        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(10).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        List<Address> addresses = null;
        // BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_shopping_basket_black_24dp);
        // ArrayList<LatLng> locations = new ArrayList();
        //   locations.add(new LatLng(mn.getLatitude1(), mn.getLongitude1()));
      /*  locations.add(new LatLng(11.3711608 ,75.7749367));
        locations.add(new LatLng(11.2588 ,75.7804));
        locations.add(new LatLng(11.3049303, 75.8770752));
        locations.add(new LatLng(11.3190171 ,75.8843493));*/
        for (LatLng location1 : locations) {
            googleMap.addMarker(new MarkerOptions()
                    .position(location1)
                    .title("Butcher Shop")
                    .icon(icon));
        }

        googleMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(30000)
                .strokeWidth(0f));
               // .fillColor(Color.parseColor("#500084d3")));
       /* googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)


                    double lat = latLng.latitude;
                    double lng = latLng.longitude;
                newlat= String.valueOf(lat);
                    newlng= String.valueOf(lng);
                    new DownloadData().execute();
                } else {
                    com.nispok.snackbar.Snackbar.with(DashboardActivity1.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(DashboardActivity1.this);
           *//* com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*//*


                    //	Toast.makeText(getActivity(),
                    //	getResources().getString(R.string.Sorry) +
                    //	getResources().getString(R.string.cic),
                    //	Toast.LENGTH_SHORT).show();
                }
            }
        });*/
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                LatLng position = marker.getPosition();
                js= String.valueOf(position.latitude);
                jk= String.valueOf(position.longitude);
                //Toast.makeText(getActivity(),
                //	"You have Internet Connection", Toast.LENGTH_LONG)
                String measure="km";
                try {


                    double distance;

                    Location locationA = new Location("");
                    locationA.setLatitude(latitude);
                    locationA.setLongitude(longitude);

                    Location locationB = new Location("");
                    locationB.setLatitude(position.latitude);
                    locationB.setLongitude(position.longitude);

// distance = locationA.distanceTo(locationB);   // in meters
                    distance = locationA.distanceTo(locationB)/1000;   // in km

                    String begin_char=String.valueOf(distance).substring(0,2);
                    System.out.println("begin_char : "+begin_char);
                    if(begin_char.equals("0.")){
                        distance=distance*1000;
                        measure=" m";
                    }
                    else {
                        measure=" km";
                    }


                    System.out.println("distance : "+locationA.distanceTo(locationB));
                    distance=Math.round( distance * 100.0 ) / 100.0;
                    dist= String.valueOf(distance);
                    dist=dist+measure;
                }catch (NumberFormatException e){

                }
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //  Lct.performClick();
                                    /*Toast.makeText(getApplicationContext().getApplicationContext(), "Test1", Toast.LENGTH_LONG).show();*/
                    new DownloadData1().execute();

                } else {
                    com.nispok.snackbar.Snackbar.with(DashboardActivity1.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(DashboardActivity1.this);
                       /* com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();
*/
                }
    /*            Location loc1 = new Location("");
                loc1.setLatitude(latitude);
                loc1.setLongitude(longitude);

                Location loc2 = new Location("");
                loc2.setLatitude(position.latitude);
                loc2.setLongitude(position.longitude);

                float distanceInMeters = loc1.distanceTo(loc2);*/

              /*  new SweetAlertDialog(DashboardActivity1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                        .setCustomImage(R.drawable.logo)
                        .setTitleText("Are you sure Want to Select This Shop?")
                     .setContentText(dist+" "+"Km  From Your your location")

                        .setConfirmText(getResources().getString(R.string.Yes))
                        .setCancelText(getResources().getString(R.string.No))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                  *//*  Intent i = new Intent(getApplicationContext(), Contact_Our_Team.class);
                                    // i.putExtra("cat_id",cat_id);
                                    //i.putExtra("value",status);

                                    startActivity(i);
                                    finish();*//*
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                sDialog.dismiss();
                           *//* .setTitleText("We are rapidly expanding!")
                            .setContentText("We will let you \n know when our shop is available in your Area")
                            .setConfirmText("OK")
                            .setConfirmClickListener(null)
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*//*
                            }
                        })
                        .show();*/
                return true;
            }
        });
      /*  googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                //myMap.addMarker(new MarkerOptions().position(point).title(point.toString()));
                Toast.makeText(getApplicationContext().getApplicationContext(), "Test", Toast.LENGTH_LONG).show();
                //The code below demonstrate how to convert between LatLng and Location
                googleMap.clear();
                //Convert LatLng to Location
                Location location = new Location("Test");
                location.setLatitude(point.latitude);
                location.setLongitude(point.longitude);
                location.setTime(new Date().getTime());
                if (ActivityCompat.checkSelfPermission(DashboardActivity1.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DashboardActivity1.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)
                    Toast.makeText(getApplicationContext().getApplicationContext(), "Test1", Toast.LENGTH_LONG).show();
                    new DownloadData1().execute();

                    js= String.valueOf(point.latitude);
                    jk= String.valueOf(point.longitude);
                } else {
                    com.nispok.snackbar.Snackbar.with(DashboardActivity1.this) // context
                            .text("Oops Your Connection Seems Off..!") // text to display
                            .show(DashboardActivity1.this);
                       *//* com.chootdev.csnackbar.Snackbar.with(Customer_Butcher_List_Fragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();
*//*
                }
                //locations.add(new LatLng(mn.getLatitude1(), mn.getLongitude1()));
                //    ArrayList<LatLng> locations = new ArrayList();
                *//*locations.add(new LatLng(mn.getLatitude1(), 75.8265209));
                locations.add(new LatLng(11.3711608 ,75.7749367));
                locations.add(new LatLng(11.2588 ,75.7804));
                locations.add(new LatLng(11.3049303, 75.8770752));
                locations.add(new LatLng(11.3190171 ,75.8843493));*//*
                googleMap.addCircle(new CircleOptions()
                        .center(new LatLng(latitude, longitude))
                        .radius(50000)
                        .strokeWidth(0f)
                        .fillColor(0x550000FF));
                for (LatLng location1 : locations) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(location1)
                            .title("Butcher Shop")
                            .icon(icon));
                }
                //Set time as current Date
                //  textinfo.setText(location.toString());
                Geocoder geocoder;
                List<Address> addresses = null;
              *//*  ArrayList<LatLng> locations = new ArrayList();
                locations.add(new LatLng(11.3370376, 75.8265209));
                locations.add(new LatLng(11.3711608 ,75.7749367));

                locations.add(new LatLng(11.3049303, 75.8770752));
                locations.add(new LatLng(11.3190171 ,75.8843493));
                for(LatLng location1 : locations){
                  googleMap.addMarker(new MarkerOptions()
                            .position(location1)
                            .title("Butcher Shop"));
                }*//*
                geocoder = new Geocoder(DashboardActivity1.this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (!addresses.isEmpty()) {
                        String address = addresses.get(0).getAddressLine(0);
                        System.out.println("-----------------------ads-----------------" + address);
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        city = addresses.get(0).getLocality();
                        System.out.println("------------------city----------------------" + city);
                        String state = addresses.get(0).getAdminArea();
                        System.out.println("---------------state-------------------------" + state);
                        String country = addresses.get(0).getCountryName();
                        System.out.println("-----------------cntry-----------------------" + country);
                        String postalCode = addresses.get(0).getPostalCode();
                        System.out.println("----------------postalcode------------------------" + postalCode);
                        String knownName = addresses.get(0).getFeatureName();
                        System.out.println("-----------------knwn-----------------------" + knownName);
                        //Convert Location to LatLng

                        LatLng newLatLng = new LatLng(location.getLatitude(), location.getLongitude());

                        MarkerOptions markerOptions = new MarkerOptions()
                                .position(newLatLng)
                                .title(newLatLng.toString());
                        textinfo.setText("" + address);

                        googleMap.addMarker(markerOptions);

                    } else {
                        System.out.println("----------------------------------------");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
*/
    }

    private float GetDistanceFromCurrentPosition(Double latitude1, double longitude, double latitude, double longitude1) {
        double earthRadius = 3958.75;

        double dLat = Math.toRadians(latitude - latitude1);

        double dLng = Math.toRadians(longitude1 - longitude);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(latitude1))
                * Math.cos(Math.toRadians(latitude)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = earthRadius * c;

        int meterConversion = 1609;
//dist=Float(dist * meterConversion).floatValue();
        return new Float(dist * meterConversion).floatValue();

    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        Toast.makeText(this,marker.getTitle(),Toast.LENGTH_LONG).show();
        return true;
    }

    private class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                SharedPreferences myPrefs = DashboardActivity1.this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                //SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                String citys;
             citys = myPrefs.getString("cvc", null);
                if (citys.isEmpty()&&citys==null){
                    citys = myPrefs.getString("ccode", null);
                }else {
                   citys = myPrefs.getString("cvc", null);
                }
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<City>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+citys);
                HttpClient httpclient = new DefaultHttpClient();

                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<Liby>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/nearshoplocation.php?city="+citys);
                HttpPost httppost = new HttpPost("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/nearshoplocation.php?city="+citys);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            //   pd.dismiss();
            // String sam = result.trim();            // pdt.setEnabled(false);

            progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
            }  else*/
            if (result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                //  mCountryModel1 = new ArrayList<>();
                msn = new ArrayList<MySho_Near_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                        mn = new MySho_Near_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //Log.d("OutPut", mJsonObject.getString("doctor_publish"));
//String user_id,subtotal,order_time,email,instruction;
                        id = mJsonObject.getString("id");
                        latitude1 = mJsonObject.getDouble("latitude");
                        longitude1 = mJsonObject.getDouble("longitude");
                        shop_nam1 = mJsonObject.getString("shop_name");
                        address1 = mJsonObject.getString("address");
                        mn.setId(id);
                        mn.setLatitude1(latitude1);
                        mn.setLongitude1(longitude1);
                        mn.setShop_nam1(shop_nam1);
                        mn.setAddress1(address1);

                        msn.add(mn);
                        for (MySho_Near_Model mn : msn) {
                            locations.add(new LatLng(mn.getLatitude1(), mn.getLongitude1()));
                            //  divisonlist.add(dv.getBranch_id());
                        }
                        System.out.println("" + mn);





                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for (LatLng location1 : locations) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(location1)
                            .title("Butcher Shop")
                            .icon(icon));
                }
            } else {

                if (!((Activity) DashboardActivity1.this).isFinishing()) {
                    new SweetAlertDialog(DashboardActivity1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                            .setCustomImage(R.drawable.logo)
                            .setTitleText(getResources().getString(R.string.Sorry))
                            .setContentText(getResources().getString(R.string.Wearerapidlyexpandingwewillletyouknowwhenourshopinavailableinyourarea))
                            //.setConfirmText(getResources().getString(R.string.Change) + "     " + "\n" + getResources().getString(R.string.location))
                            .setConfirmText(getResources().getString(R.string.contactus))

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    Intent i = new Intent(getApplicationContext(), Contact_Our_Team.class);
                                    // i.putExtra("cat_id",cat_id);
                                    //i.putExtra("value",status);

                                    startActivity(i);
                                    finish();
                                    sweetAlertDialog.dismiss();
                                }
                            })

                            .show();

                    System.out.println("nulllllllllllllllllllllllllllllllll");
                }
            }
        }
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;
//

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);

            String l = getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<lang>>>>>" + l);
            System.out.println("<<<<<<lattitude>>>>>" + lng);
            System.out.println("<<<<<<longitude>>>>>" + lat);
            System.out.println("<<<<<<longitude>>>>>" + lat);
            System.out.println("<<<<<<libnitnatony>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat_location.php?cat_id="+"1"+"&longitude="+jk+"&latitude="+js);

            if (l.equalsIgnoreCase("English")) {
                Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat_location.php?cat_id="+"1"+"&longitude="+jk+"&latitude="+js;
               // Wurl = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat.php?cat_id="+"1"+"&longitude="+jk+"&latitude="+js;
                // Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat.php?cat_id="+k;
            } else {
                Wurl = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/shopbycat_location.php?cat_id="+"1"+"&longitude="+jk+"&latitude="+js;
                //  Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/shopbycat.php?cat_id="+k;
                System.out.println("<<<<<<longitude>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/shopbycat.php?cat_id=" + "k" + "&longitude=" + lng + "&latitude=" + lat);
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(Wurl//+"&longitude="+lng+"&latitude="+lat
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            progress_loader.setVisibility(ProgressBar.GONE);
            // String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
            // System.out.println(">>>>>>>>>>>>>>>" + sam);
            //   String value = result;

            if (result != null && !result.isEmpty() && !result.equals("null") && !result.equalsIgnoreCase("")) {
                JSONArray mArray;
                zwm = new ArrayList<Customer_Butcher_List_Model>();
                try {
                    recyclerview.setVisibility(View.VISIBLE);
                    System.out.println("<<<<<<Rejeesh>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/shopbycat.php?cat_id=" + "k" + "&longitude=" + lng + "&latitude=" + lat);
                    System.out.println("<<<<<<Rejeesh>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat.php?cat_id=" + "k" + "&longitude=" + lng + "&latitude=" + lat);
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        wm = new Customer_Butcher_List_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        id = mJsonObject.getString("id");
                        added_by = mJsonObject.getString("added_by");
                        cat_id = mJsonObject.getString("cat_id");
                        butcher_logo = mJsonObject.getString("butcher_logo");
                        shop_name = mJsonObject.getString("shop_name");
                        delivery_info = mJsonObject.getString("delivery_info");
                        butcher_discount = mJsonObject.getString("butcher_discount");
                        shop_addrs = mJsonObject.getString("shop_addrs");
                        shop_description = mJsonObject.getString("shop_description");
                        discount_desc = mJsonObject.getString("discount_desc");
                        // butcher_discount
                        //  String o = mJsonObject.getString("specialty_other");
                        wm.setId(id);
                        wm.setAdded_by(added_by);
                        wm.setCat_id(cat_id);
                        wm.setButcher_logo(butcher_logo);
                        wm.setShop_name(shop_name);
                        wm.setDelivery_info(delivery_info);
                        wm.setShop_description(shop_description);
                        wm.setShop_addrs(shop_addrs);
                        wm.setButcher_discount(butcher_discount);
                        wm.setDiscount_desc(discount_desc);
                        wm.setDistance(dist);
                        //  wm.s

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        //   System.out.println("<<<oo>>>>" + o);
                        //   onComplete();

                        zwm.add(wm);
                        System.out.println("<<<oo>>>>" + wm);

                        adapter1 = new Customer_Butcher_List_adapter_For_Map(zwm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }


            } else {
                if (!((Activity) DashboardActivity1.this).isFinishing()) {
                    System.out.println("<<<<<<Rejeeshttt>>>>>" + "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/shopbycat.php?cat_id=" + "k" + "&longitude=" + lng + "&latitude=" + lat);
                    recyclerview.setVisibility(View.GONE);
                    new SweetAlertDialog(DashboardActivity1.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)

                            .setCustomImage(R.drawable.logo)
                            .setTitleText(getResources().getString(R.string.Sorry))
                            .setContentText(getResources().getString(R.string.Wearerapidlyexpandingwewillletyouknowwhenourshopinavailableinyourarea))
                            //.setConfirmText(getResources().getString(R.string.Change) + "     " + "\n" + getResources().getString(R.string.location))
                            .setConfirmText(getResources().getString(R.string.contactus))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    Intent i = new Intent(getApplicationContext(), Contact_Our_Team.class);
                                    // i.putExtra("cat_id",cat_id);
                                    //i.putExtra("value",status);

                                    startActivity(i);
                                    finish();
                                    sweetAlertDialog.dismiss();
                                }
                            })

                            .show();
                }
            }

        }
    }
        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event)  {
            if (Integer.parseInt(Build.VERSION.SDK) > 5
                    && keyCode == KeyEvent.KEYCODE_BACK
                    && event.getRepeatCount() == 0) {
                Log.d("CDA", "onKeyDown Called");
                onBackPressed1();
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }

        private void onBackPressed1() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Intent inn= new Intent(getApplicationContext(),SelectionPage.class);
                startActivity(inn);
                finish();
            }else {

                Intent inn= new Intent(getApplicationContext(),Customer_Butcher_List_Fragment.class);
                inn.putExtra("cat_id", "1");
                inn.putExtra("value","1");
                startActivity(inn);
                finish();
            }
            // dialog.dismiss();
            //finish();
        }
    }
