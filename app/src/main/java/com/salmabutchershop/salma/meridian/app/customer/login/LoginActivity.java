package com.salmabutchershop.salma.meridian.app.customer.login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.NetworkCheckingClass;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Exploreapp;
import com.salmabutchershop.salma.meridian.app.customer.newintro.New_WelcomeActivity;
import com.salmabutchershop.salma.meridian.app.firebase.app.Config;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

/**
 * Created by libin on 11/3/2016./////////////////////////////////////////////////////////////////////////////////////////
 */
public class LoginActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,GoogleApiClient.OnConnectionFailedListener {

    //password eye--start

    LinearLayout password_eye_show,password_eye_hide;
    //password eye--end



    Button login;
    EditText edtusrnam, edtpass;
    String  pass;
    TextView  frgt;
    ImageView fb;
    WebView wv;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String eml, status;
    String Reg_id,names,typ;
    static ArrayList<Salma_Login_Model> slm;
    Salma_Login_Model sl;
    ProgressBar progress;
    Button bsignup;
    String id, user_name, usertype, location, phone, email, log_status, name,ccode;
    Spinner spinner;
    String item;
    String k,l;
    TextView Type,txtRegId;
    private static final String TAG =New_WelcomeActivity.class.getSimpleName();
    boolean edittexterror = false;
    String c_names,c_email,c_phone,c_location;
    ImageView ggle,fbg;
    String lat,lng,fl_id;
    ProgressBar progress1;
    EditText editText;
    LoginButton loginButton;
    ProfileTracker profileTracker;
    CallbackManager callbackManager;
    String ss,dg;
    private SignInButton signInButton;

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    public GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;

    //TextViews
    public TextView textViewName;
    public TextView textViewEmail;
    public NetworkImageView profilePhoto;
    private ProgressDialog mProgressDialog;
    private ImageLoader imageLoader;

    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_salma_layout);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile email");

        progress_loader=(MKLoader)findViewById(R.id.progress_loader);

        progress = (ProgressBar) findViewById(R.id.progress_bar);
        System.out.println("testtttttttttttttttttttttttttttttttttttt");
        System.out.println("testtttttttttttttttttttttttttttttttttttt");
      //  txtRegId = (TextView) findViewById(R.id.txt_reg_id);
        ggle= (ImageView) findViewById(R.id.gt);
        fbg= (ImageView) findViewById(R.id.fbt);
        // Spinner element
        spinner = (Spinner) findViewById(R.id.spinner);
        Type= (TextView) findViewById(R.id.type);
        Intent intent = getIntent();
        k = intent.getStringExtra("country");
        l = intent.getStringExtra("name");
//        Type.setText(l);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add(getResources().getString(R.string.English));
        categories.add(getResources().getString(R.string.Arabic));


        ArrayAdapter<String> dataAdapter=new ArrayAdapter<String>(this, R.layout.spinner_item_layout, R.id.textView17, categories);

        spinner.setAdapter(dataAdapter);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);
        fl_id=myPrefs.getString("user_id",null);
        names= myPrefs.getString("fullname", null);
        typ= myPrefs.getString("usertype", null);
        lng=myPrefs.getString("longitude",null);
        lat=myPrefs.getString("latitude",null);
        System.out.println("<<<<<<<<<sharedname>>>>" + Reg_id);
        if (Reg_id != null && !Reg_id.isEmpty()){
            //Toast.makeText(getApplicationContext(), Reg_id, Toast.LENGTH_SHORT).show();
            c_names= myPrefs.getString("c_name", null);

            c_email= myPrefs.getString("c_email", null);
            c_phone= myPrefs.getString("c_phone", null);
            c_location= myPrefs.getString("c_location", null);
            System.out.println("bbbbbbbbbbbbbbbbbbbb"+c_names);
            System.out.println("cccccccccccccccccccc"+c_email);
            System.out.println("bbbbbbbbbbbbbbbbbbbb"+c_phone);
            System.out.println("cccccccccccccccccccc"+c_location);
            System.out.println("bbbbbbbbbbbbbbbbbbbb"+c_phone);
            System.out.println("cccccccccccccccccccc"+c_location);
            Intent synin = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
           // synin.putExtra("brn_id",u);
            startActivity(synin);
            finish();
        }

ggle.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        /*Intent synin = new Intent(getApplicationContext(), Googlelogin.class);
        // synin.putExtra("brn_id",u);
        startActivity(synin);
        finish();*/
        signIn();
    }
});
        fbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent synin = new Intent(getApplicationContext(), FbActivity.class);
                // synin.putExtra("brn_id",u);
                startActivity(synin);
               // finish();*/
                loginButton.performClick();
            }
        });
        edtusrnam = (EditText) findViewById(R.id.us_nam);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtusrnam.setTypeface(myFont7);


        edtpass = (EditText) findViewById(R.id.pw_d);
        final Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtpass.setTypeface(myFont1);

        //password eye--start
        password_eye_show= (LinearLayout) findViewById(R.id.password_eye_show);
        password_eye_hide=(LinearLayout) findViewById(R.id.password_eye_hide);

        password_eye_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_show.setVisibility(View.GONE);
                password_eye_hide.setVisibility(View.VISIBLE);
                edtpass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                edtpass.setTypeface(myFont1);
                edtpass.setSelection(edtpass.length());
            }
        });

        password_eye_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password_eye_hide.setVisibility(View.GONE);
                password_eye_show.setVisibility(View.VISIBLE);

                edtpass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                edtpass.setTypeface(myFont1);
                edtpass.setSelection(edtpass.length());

            }
        });
        //password eye--end


        fb = (ImageView) findViewById(R.id.fbt);


        frgt = (TextView) findViewById(R.id.fgt);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        frgt.setTypeface(myFont2);



        TextView skip = (TextView) findViewById(R.id.txt_skip);
        skip.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent i = new Intent(getApplicationContext(), Customer_Services_Fragment.class);

                                        SharedPreferences preferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("fromskip","true");

                                        startActivity(i);

                                        overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                                        finish();

            }
        });


        wv = new WebView(this);

        frgt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(LoginActivity.this);
             //   String REGISTER_URL="http://app.zayedevents.com.php56-9.dfw3-2.websitetestlink.com/services/forgot.php?";
                //  dialog.setContentView(R.layout.loc_popup);
                final Window window = dialog.getWindow();
                window.requestFeature(Window.FEATURE_NO_TITLE);
                window.setContentView(R.layout.popup_forgot_layout);
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            editText= (EditText)dialog. findViewById(R.id.alert_email);
           progress1 = (ProgressBar)dialog.findViewById(R.id.progress_bars1);
                Button btnDismiss = (Button) dialog.findViewById(R.id.gb);
                btnDismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String eml = editText.getText().toString();
                        edittexterror = false;
                        if (eml.matches("")) {
                            Toast.makeText(LoginActivity.this, "Empty Field", Toast.LENGTH_SHORT).show();
                        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString().trim()).matches()) {
                            editText.setError("Invalid Email");
                            edittexterror = true;
                        } else if (editText.getText().toString().isEmpty()) {
                            editText.setError("Enter Email Id");
                            edittexterror = true;

                        } else {
                            if (edittexterror == false) {
                            NetworkCheckingClass networkCheckingClass = new NetworkCheckingClass(LoginActivity.this);
                            boolean i = networkCheckingClass.ckeckinternet();
                            progress1.setVisibility(View.VISIBLE);


                            if (i == true) {
                                String REGISTER_URL = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/forgotpassword.php";
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                System.out.println("statusforgot" + response + "xxxxx");

                                                JSONObject jsonObj = null;
                                                JSONArray jsonArray = null;
//                                        try {
//                                            jsonArray = new JSONArray(response);
//                                            for(int i=0;i<jsonArray.length();i++) {
//
//
//                                                jsonObj=jsonArray.getJSONObject(i);
//
//                                                status = jsonObj.getString("result");
//                                            }
                                                if (response.contentEquals("\"success\"")) {
                                                    com.nispok.snackbar.Snackbar.with(LoginActivity.this) // context
                                                            .text("Please Check Your mail") // text to display
                                                            .show(LoginActivity.this);
                                                   /* Snackbar.with(LoginActivity.this,null)
                                                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                                                            .message("Please Check Your mail")
                                                            .duration(Duration.SHORT)
                                                            .show();*/
                                                   // Toast.makeText(LoginActivity.this, "Please Check Your mail", Toast.LENGTH_SHORT).show();
                                                    progress1.setVisibility(View.GONE);
                                                    dialog.dismiss();

                                                } else {
                                                    com.nispok.snackbar.Snackbar.with(LoginActivity.this) // context
                                                            .text(getResources().getString(R.string.invalid_credentials)) // text to display
                                                            .show(LoginActivity.this);
                                                  /*  Snackbar.with(LoginActivity.this,null)
                                                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                                                            .message("Invalid Credentials!")
                                                            .duration(Duration.SHORT)
                                                            .show();*/
                                                   // Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();
                                                    progress1.setVisibility(View.GONE);
                                                }


//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("email", eml);
                                        return params;
                                    }

                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                                requestQueue.add(stringRequest);
                            }
                            } else {
                                com.nispok.snackbar.Snackbar.with(LoginActivity.this) // context
                                        .text("No internet connection!") // text to display
                                        .show(LoginActivity.this);
                              /*  Snackbar.with(LoginActivity.this,null)
                                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                                        .message("No internet connection!")
                                        .duration(Duration.LONG)
                                        .show();*/
                            }



                        }
                    }
                });
                Button rb = (Button) dialog.findViewById(R.id.rb);
                rb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                    }
                });

                dialog.show();
                /*CustomAlertDialog cc = new CustomAlertDialog(LoginActivity.this);
                cc.show();
*/

            }
        });

/*
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);

                startActivity(i);

            }
        });*/



        login = (Button) findViewById(R.id.sig_in);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        login.setTypeface(myFont3);

        bsignup = (Button) findViewById(R.id.sig_up);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        bsignup.setTypeface(myFont4);
        bsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                    Intent i = new Intent(getApplicationContext(), PostRegistration.class);
                    startActivity(i);
                    // finish();
                } else {
                    com.nispok.snackbar.Snackbar.with(LoginActivity.this) // context
                            .text(getResources().getString(R.string.Updated_Successfully)) // text to display
                            .show(LoginActivity.this);
                   /* Snackbar.with(LoginActivity.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Profile updated successfully!")
                            .duration(Duration.SHORT)
                            .show();*/

                }

            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    user_name = edtusrnam.getText().toString();
                    pass = edtpass.getText().toString();
                    if (user_name.matches("") || pass.matches("")) {

                        com.nispok.snackbar.Snackbar.with(LoginActivity.this) // context
                                .text(getResources().getString(R.string.empty_fields)) // text to display
                                .show(LoginActivity.this);
                      /*  Snackbar.with(LoginActivity.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Empty Fields!")
                                .duration(Duration.SHORT)
                                .show();
*/

                    /*    final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(LoginActivity.this).create();
                      //  alertDialog.setTitle("Alert");

                        alertDialog.setMessage("Empty Fields");


                        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();


                            }
                        });
                        alertDialog.show();
                        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL);
                        nbutton.setTextColor(getResources().getColor(R.color.dot_dark_screen1));
                        nbutton.setGravity(Gravity.CENTER);
                        nbutton.setBackgroundColor(getResources().getColor(R.color.dot_light_screen1));*/


                    } else {

                            new SendPostRequest().execute();


                    }

                } else {
                    com.nispok.snackbar.Snackbar.with(LoginActivity.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(LoginActivity.this);
                  /*  Snackbar.with(LoginActivity.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/


                }


            }
        });

        FacebookSdk.sdkInitialize(LoginActivity.this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        loginButton.setVisibility(View.INVISIBLE);
                   /*     Toast.makeText(getActivity(), ""+loginResult.toString(),
                                Toast.LENGTH_LONG).show();*/
                        System.out.println("json responseee"+loginResult.toString());

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {



                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("Main", response.toString());
                                        //displayMessage(profile);
                                        setProfileToView(object);

                                        try {
                                            ss=object.getString("id");
                                            dg=object.getString("name");
                                            email=object.getString("email");
                                            // String pic=object.getString()
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {


                                            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<facebookid>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ss);
                                            SharedPreferences preferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                                            SharedPreferences.Editor editor = preferences.edit();

                                            editor.putString("social_user_id", ss);
                                            editor.putString("c_name",dg);
                                            editor.putString("c_email",email);
                                            editor.commit();


                                            SharedPreferences preferences2 = getSharedPreferences("SocialPref", MODE_PRIVATE);
                                            SharedPreferences.Editor editor2 = preferences2.edit();
                                            editor2.putString("sociallogin","true");
                                            editor2.putString("google", "false");
                                            editor2.putString("facebook","true");

                                            editor2.commit();



                                            Intent inn= new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                                            startActivity(inn);
                                        }catch (NullPointerException e){

                                        }

                                    }

                                    private void setProfileToView(JSONObject jsonObject) {
                                        try {
                                            String  email=jsonObject.getString("email");
                                            String name=jsonObject.getString("name");
                                            String id=jsonObject.getString("id");
                                            String gender=jsonObject.getString("gender");



                                            System.out.println("social_user_id : "+id);
                                            System.out.println("social_email : "+email);
                                            System.out.println("social_gender : "+gender);
                                            System.out.println("social_name : "+name);


                                          /*  String birthday=jsonObject.getString("user_birthday");*/
                                            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<facebookid>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+id);
                                            //   String photo= jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");

                                            //  textView.setText(jsonObject.getString("email"));
                                            System.out.println("Email"+jsonObject.getString("email"));
                                            SharedPreferences preferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                                            SharedPreferences.Editor editor = preferences.edit();

                                            editor.putString("social_user_id", id);
                                            editor.putString("social_name",name);
                                            editor.putString("social_email",email);
                                            editor.putString("social_gender",gender);

                                            try {

                                                URL imageURL = new URL("https://graph.facebook.com/"+id+"/picture?type=large");
                                                editor.putString("social_profile_image",imageURL.toString());
                                            }catch (Exception e){e.printStackTrace();}

                                           /* editor.putString("social_birthday",birthday);*/


                                            editor.commit();
                                          /*  SharedPreferences.Editor editor1 = getActivity().getSharedPreferences("myfb", getActivity().MODE_PRIVATE).edit();
                                            editor.putString("FBemails", email);
                                            editor.putString("Fbnames", name);
                                            editor.putString("user_id", name);
                                            editor1.commit();*/


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields","id,name,email,gender");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                // App code



            }
        };
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);
        profilePhoto = (NetworkImageView) findViewById(R.id.profileImage);

        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("778644506647-fdq786qefvscbvjggjd3kbdtk12t4j7c.apps.googleusercontent.com")
                .build();

        //Initializing signinbutton
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId))
            txtRegId .setText("Firebase Reg Id: " + regId);

        else
            txtRegId .setText("Firebase Reg Id is not received yet!");
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        item = adapterView.getItemAtPosition(i).toString();
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>"+item);
        if(item.equalsIgnoreCase("English")){

            Locale locale2 = new Locale("en");
            Locale.setDefault(locale2);
            Configuration config2 = new Configuration();
            config2.locale = locale2;
            getBaseContext().getResources().updateConfiguration(config2, null);
          /*  Intent intent = getIntent();
            finish();
            startActivity(intent);*/
            //getBaseContext().getResources().getDisplayMetrics());
        }else {
            Locale locale2 = new Locale("ar");
            Locale.setDefault(locale2);
            Configuration config2 = new Configuration();
            config2.locale = locale2;
            getBaseContext().getResources().updateConfiguration(config2,null);
            Intent intent = getIntent();
            finish();
            startActivity(intent);//getBaseContext().getResources().getDisplayMetrics());
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public class SendPostRequest extends AsyncTask<String, Void, String> {

        public void onPreExecute() {
            progress_loader.setVisibility(View.VISIBLE);
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL(SERVER_CUSTOMER+"login.php"); // here is your URL path

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("username", user_name);
                postDataParams.put("password", pass);

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
               /*Toast.makeText(LoginActivityRetailer.this, result,
                Toast.LENGTH_LONG).show();*/
            progress_loader.setVisibility(View.GONE);

            sl = new Salma_Login_Model();
            if (result.equals("\"failed\"")) {
                progress_loader.setVisibility(View.GONE);
                com.nispok.snackbar.Snackbar.with(LoginActivity.this) // context
                        .text(getResources().getString(R.string.invalid_credentials)) // text to display
                        .show(LoginActivity.this);
              /*  Snackbar.with(LoginActivity.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("Invalid Credentials!")
                        .duration(Duration.LONG)
                        .show();*/


              /*  MaterialStyledDialog dialog = new MaterialStyledDialog(LoginActivity.this)
                        .setTitle("Failed!")
                        .setDescription("Invalid Credentials")
                        .setIcon(R.drawable.ic_error_white_24dp)
                        .withIconAnimation(true)
                        .withDialogAnimation(true)
                        .setHeaderColor(R.color.colorPrimary)
                        .setCancelable(true)
                        .setPositive(getResources().getString(R.string.ok), new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {


                               dialog.dismiss();
                                //finish();

                            }
                        })
                        .build();

                dialog.show();*/
                //  String named = jsonObj.getString("name");
               // success
            } else /*if(result.equals("\"success\""))*/{
                try {
                    JSONObject jsonObj = null;
                    slm = new ArrayList<Salma_Login_Model>();
                    System.out.println("result : "+result);
                    jsonObj = new JSONObject(result);
                    id = jsonObj.getString("id");

                    user_name = jsonObj.getString("user_name");
                    usertype = jsonObj.getString("usertype");
                    location = jsonObj.getString("location");
                    phone = jsonObj.getString("phone");
                    email = jsonObj.getString("email");
                    log_status = jsonObj.getString("log_status");
                    name = jsonObj.getString("name");

                    ccode= jsonObj.getString("ccode");

                    //patient_type=jsonObj.getString("patient_type");
                    //   log_status=jsonObj.getString("log_status");

                    sl.setId(id);
                    sl.setUser_name(user_name);
                    sl.setUsertype(usertype);
                    sl.setLocation(location);
                    sl.setPhone(phone);
                    sl.setEmail(email);
                    sl.setLog_status(log_status);
                    sl.setName(name);

                    slm.add(sl);
                    System.out.println("result" + result);
               /*     MaterialStyledDialog dialog = new MaterialStyledDialog(LoginActivity.this)
                            .setTitle("SUCCESS!")
                            .setDescription("Login Successfull")
                            .setIcon(R.drawable.ic_done_white_24dp)
                            .withIconAnimation(true)
                            .withDialogAnimation(true)
                            .setHeaderColor(R.color.colorPrimary)
                            .setCancelable(true)
                            .setPositive(getResources().getString(R.string.ok), new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {*/

                                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();

                                    editor.putString("user_id", id);
                                    editor.putString("fullname",name);
                                    editor.putString("usertype",usertype);
                                    editor.putString("c_name",name);
                                    editor.putString("c_phone",phone);
                                    editor.putString("c_email",email);
                                    editor.putString("c_location",location);
                    editor.putString("ccode",ccode);
                                    editor.commit();



                    SharedPreferences preferences3 = getSharedPreferences("SocialPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor3 = preferences3.edit();
                    editor3.putString("sociallogin","false");
                    editor3.commit();


                                    Intent inn= new Intent(getApplicationContext(),Exploreapp.class);
                                    startActivity(inn);
                    finish();
                                    overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                                   //finish();
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                                    boolean Islogin= Boolean.parseBoolean("true");
                                    prefs.edit().putBoolean("Islogin", Islogin).commit();
                                    //   pd.dismiss();
                                    finish();

                     /*           }
                            })
                            .build();

                    dialog.show();

*/


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            /*else {

            }*/
        }
    }


        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while (itr.hasNext()) {

                String key = itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Intent inn= new Intent(getApplicationContext(),SelectionPage.class);
            startActivity(inn);
            finish();
        }else {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
       /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);*/
            // finish();
            //  finish();


/*
        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/

     /*   Intent intent = new Intent(this, Splash_Activity.class);
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/
            Intent inn= new Intent(getApplicationContext(),SelectionPage.class);
            startActivity(inn);
            finish();
        }
        // dialog.dismiss();
        //finish();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            try {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                //Calling a new function to handle signin
                GoogleSignInAccount acct = result.getSignInAccount();
                System.out.println("<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>" + acct.getDisplayName());
                //Displaying name and email
                textViewName.setText(acct.getDisplayName());
                textViewEmail.setText(acct.getEmail());
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("social_user_id", acct.getId());
                editor.putString("c_name", acct.getDisplayName());
                editor.putString("c_email", acct.getEmail());
                editor.putString("social_name", acct.getGivenName());
                editor.putString("social_email", acct.getEmail());
                editor.putString("social_gender", "");
                editor.putString("social_profile_image", acct.getPhotoUrl().toString());
                editor.commit();
               /* Person person  = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                if (person != null) {
                        Log.i(TAG, "--------------------------------");
                        Log.i(TAG, "Display Name: " + person.getDisplayName());
                        Log.i(TAG, "Gender: " + person.getGender());
                        Log.i(TAG, "About Me: " + person.getAboutMe());
                        Log.i(TAG, "Birthday: " + person.getBirthday());
                        Log.i(TAG, "Current Location: " + person.getCurrentLocation());
                        Log.i(TAG, "Language: " + person.getLanguage());
                } else {
                        Log.e(TAG, "Error!");
                }*/

                handleSignInResult(result);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        profileTracker.stopTracking();
    }
    private void signIn() {
        //Creating an intent


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            try {
                //Getting google account
                Log.d("TAG", "handleSignInResult:" + result.isSuccess());

                GoogleSignInAccount acct = result.getSignInAccount();

                //Displaying name and email
                textViewName.setText(acct.getDisplayName());
                textViewEmail.setText(acct.getEmail());
                System.out.println("<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>" + result.isSuccess());
                //Initializing image loader
                imageLoader = CustomVolleyRequest.getInstance(this.getApplicationContext())
                        .getImageLoader();
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("social_user_id", acct.getId());
                editor.putString("c_name", acct.getGivenName());
                editor.putString("c_email", acct.getEmail());
                editor.putString("social_name", acct.getGivenName());
                editor.putString("social_email", acct.getEmail());
                editor.putString("social_gender", "");
                editor.commit();


                SharedPreferences preferences2 = getSharedPreferences("SocialPref", MODE_PRIVATE);
                SharedPreferences.Editor editor2 = preferences2.edit();
                editor2.putString("sociallogin", "true");
                editor2.putString("google", "true");
                editor2.putString("facebook", "false");
                editor2.commit();

                // imageLoader.get(acct.getPhotoUrl().toString(),
                // ImageLoader.getImageListener(profilePhoto,
                // R.mipmap.ic_launcher,
                //   R.mipmap.ic_launcher));

                //Loading image
                //   profilePhoto.setImageUrl(acct.getPhotoUrl().toString(), imageLoader);
                Intent inn = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
                startActivity(inn);
            }catch (Exception e){
                e.printStackTrace();
            }

        } else {
            //If login fails
            //  Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            // Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("loading");
            mProgressDialog.setIndeterminate(true);
        }

        /*mProgressDialog.show();*/
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    }
