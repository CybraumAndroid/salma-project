package com.salmabutchershop.salma.meridian.app.retailer.login;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 11/3/2016.
 */

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView AlReg;
    EditText edtemail, edtphon, edtfullnam, edtusername,  edtpass, edtloctn,edtshopname,edtads1,edtads2;
  //  String email, phon, loc, pass, statusd, firstname, lastname, username, confirmpass;
    Button butsignup;
    boolean edittexterror = false;
    String REGISTER_URL = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/register.php";
    ImageView profile_img_upload, profile_img;
    String filePath;
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    //    ProgressDialog pd;
    ProgressBar progress;

    Spinner spinner;
    static String filename = "null", filenamepath = "null";
    private static final int REQUEST_CODE = 1;
String type,fullname,phone,email,location,username,password_s;
    String item;
    String result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.registration_layout);


        // Spinner element
       spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("customer");
        categories.add("retailer");
        categories.add("butchers");
       // categories.add("wholesellers");
       // categories.add("Personal");
       // categories.add("Travel");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        edtfullnam = (EditText) findViewById(R.id.edt_fullname);
        Typeface myFont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtfullnam.setTypeface(myFont1);
        edtphon = (EditText) findViewById(R.id.edt_phone);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtphon.setTypeface(myFont2);
        edtemail = (EditText) findViewById(R.id.edt_email);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtemail.setTypeface(myFont3);
        edtloctn = (EditText) findViewById(R.id.edt_location);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtloctn.setTypeface(myFont4);
        edtusername= (EditText) findViewById(R.id.edt_username);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtusername.setTypeface(myFont5);
        edtpass = (EditText) findViewById(R.id.edt_password);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtpass.setTypeface(myFont6);
      edtshopname = (EditText) findViewById(R.id.edt_shopname);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtshopname.setTypeface(myFont7);

        edtads1 = (EditText) findViewById(R.id.edt_ads1);
        Typeface myFont10 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtads1.setTypeface(myFont10);

        edtads2 = (EditText) findViewById(R.id.edt_ads2);
        Typeface myFont11 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        edtads2.setTypeface(myFont11);

        butsignup = (Button) findViewById(R.id.butsignup);
        Typeface myFont8 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        butsignup.setTypeface(myFont8);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        // profile_img= (ImageView) findViewById(R.id.profile_image);


        progress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                progress.setVisibility(View.INVISIBLE);
                return false;
            }
        });

        butsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reg();


            }
        });
    }



            // other 'case' lines to check for other
            // permissions this app might request




    private void reg() {


        System.out.println("selected image-file" + filename);
        email = edtemail.getText().toString();
        password_s = edtpass.getText().toString();
        // type = edtcnfrmpass.getText().toString();
        phone = edtphon.getText().toString();
        fullname = edtfullnam.getText().toString();
        location = edtloctn.getText().toString();
        username = edtusername.getText().toString();

        edittexterror = false;
        // boolean s = checkPassWordAndConfirmPassword(pass, confirmpass);

        if (edtemail.getText().toString().isEmpty() || edtpass.getText().toString().isEmpty() || edtfullnam.getText().toString().trim().isEmpty() || edtphon.getText().toString().isEmpty() || edtloctn.getText().toString().isEmpty() || edtusername.getText().toString().isEmpty()) {
            com.nispok.snackbar.Snackbar.with(RegisterActivity.this) // context
                    .text("Empty Fields!") // text to display
                    .show(RegisterActivity.this);

         /*   com.chootdev.csnackbar.Snackbar.with(RegisterActivity.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Empty Fields!")
                    .duration(Duration.SHORT)
                    .show();*/

            edittexterror = true;
            //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
            edtemail.setError("Invalid Email");
            edittexterror = true;
        } else if (edtemail.getText().toString().isEmpty()) {
            edtemail.setError("Enter Email Id");
            edittexterror = true;
        } else if (!isValidPassword(edtpass.getText().toString().trim())) {
            edtpass.setError("Password should be minimum 6 characters");
            edittexterror = true;
        } else if (edtphon.getText().toString().isEmpty()) {
            edtphon.setError("Enter Phone");
            edittexterror = true;
        } else if (edtfullnam.getText().toString().isEmpty()) {
            edtfullnam.setError("Enter Full Name");
            edittexterror = true;
        } else if (edtloctn.getText().toString().isEmpty()) {
            edtloctn.setError("Enter Location");
            edittexterror = true;
        } else if (edtusername.getText().toString().isEmpty()) {
            edtusername.setError("Enter Username");
            edittexterror = true;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtemail.getText().toString().trim()).matches()) {
            edtemail.setError("Invalid Email");
            edittexterror = true;
        } else if (!android.util.Patterns.PHONE.matcher(edtphon.getText().toString().trim()).matches()) {
            edtphon.setError("Invalid Phone");
            edittexterror = true;
        } else if (edtfullnam.getText().toString().isEmpty()) {
            edtfullnam.setError("Enter First Name");
            edittexterror = true;
        } else if (edtloctn.getText().toString().isEmpty()) {
            edtloctn.setError("Enter Last Name");
            edittexterror = true;
        } else if (edtusername.getText().toString().isEmpty()) {
            edtusername.setError("Enter User name");
            edittexterror = true;
        }/* else if (!edtpass.getText().toString().contentEquals(edtcnfrmpass.getText().toString())) {
            edtcnfrmpass.setError("Enter valid Password");
            edittexterror = true;
        }*/ /*else if (!edtpass.getText().toString().equalsIgnoreCase(edtcnfrmpass.getText().toString())) {
            edtcnfrmpass.setError("Do not Match");
            edittexterror = true;
        }*/ else if (filenamepath != null) {
            filename = filenamepath;


        } else {
            filename = "null";

        }

        if (edittexterror == false) {
            new DownloadData().execute();
        }

         else
        {
            com.nispok.snackbar.Snackbar.with(RegisterActivity.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(RegisterActivity.this);
          /*  com.chootdev.csnackbar.Snackbar.with(RegisterActivity.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }

        }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {


            super.onBackPressed();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();


if(item.equalsIgnoreCase("customer")){
    type="1";
    edtads1.setVisibility(View.GONE);
    edtads2.setVisibility(View.GONE);
    edtshopname.setVisibility(View.GONE);
    edtloctn.setVisibility(View.VISIBLE);
}else if(item.equalsIgnoreCase("retailer")){
    type="2";
edtads1.setVisibility(View.VISIBLE);
    edtads2.setVisibility(View.VISIBLE);
   edtshopname.setVisibility(View.VISIBLE);
    edtloctn.setVisibility(View.GONE);
}
else if(item.equalsIgnoreCase("butchers")){
    type="3";
    edtads1.setVisibility(View.VISIBLE);
    edtads2.setVisibility(View.VISIBLE);
    edtshopname.setVisibility(View.VISIBLE);
    edtloctn.setVisibility(View.GONE);
}

        // Showing selected spinner item
       // Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class DownloadData extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/register1.php?type="+type+"&fullname="+fullname+"&phone="+phone+"&email="+email+"&location="+location+"&username="+username+"&password="+password_s
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            // String sam = result.trim();
            progress.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result);
            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
            if
                    (result.equalsIgnoreCase("[]")) {
                Toast.makeText(getApplicationContext(), "No Events", Toast.LENGTH_SHORT).show();
            } else {
                JSONArray mArray;
                //  mCountryModel1 = new ArrayList<>();
               // eem = new ArrayList<Order_history_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {

                       // ee = new Order_history_Model();

                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        //Log.d("OutPut", mJsonObject.getString("doctor_publish"));
//

                   String     id = mJsonObject.getString("result");





                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}


