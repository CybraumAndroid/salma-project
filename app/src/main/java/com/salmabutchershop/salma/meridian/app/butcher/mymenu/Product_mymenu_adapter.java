package com.salmabutchershop.salma.meridian.app.butcher.mymenu;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.pro_select.BeefDetail;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 12/3/2016.
 */
public class Product_mymenu_adapter extends RecyclerView.Adapter<Product_mymenu_adapter.ViewHolder> {


    List<Product_mymenu_Model> pmm;
    Context context;
    TextView qty;
    private int uprange = 20;
    private int downrange = 1;
    private int values = 1;

    public Product_mymenu_adapter(ArrayList<Product_mymenu_Model> pmm, Context context) {
        this.pmm = pmm;
        this.context = context;

    }

    public Product_mymenu_adapter(BeefDetail beefDetail) {
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv, tv1, tv2, tv3;
        // TextView personAge;
        TextView title, qty, plus, minus, cart_txt;

        ImageView personPhoto;
        public static MKLoader progress_loader;
        LinearLayout ltx;

        ViewHolder(View itemView) {
            super(itemView);

            tv = (TextView) itemView.findViewById(R.id.pro_name);

            v = (ImageView) itemView.findViewById(R.id.beef_img);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
        }
    }


    @Override
    public int getItemCount() {
        return pmm.size();
    }


    @Override
    public Product_mymenu_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.butcher_mymenu_item_layout, viewGroup, false);
        Product_mymenu_adapter.ViewHolder pvh = new Product_mymenu_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Product_mymenu_adapter.ViewHolder personViewHolder, final int i) {


        String s = pmm.get(i).getType_image();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont2);
        personViewHolder.tv.setText(pmm.get(i).getType_name());


        System.out.println("ooo" + s);
       // Picasso.with(context).load(s).resize(150, 150).noFade().into(personViewHolder.v);

        try {


            new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .build()
                    .load(s)
                    .noFade()
                    // .into(personViewHolder.v);
                    .into(personViewHolder.v, new Callback() {
                        @Override
                        public void onSuccess() {

                            personViewHolder.progress_loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }


    }



    private void showpopup(View view) {

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public long getCount() {
        final DatabaseHelper dataHelper = new DatabaseHelper(context);
        int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        return  s;
    }
}