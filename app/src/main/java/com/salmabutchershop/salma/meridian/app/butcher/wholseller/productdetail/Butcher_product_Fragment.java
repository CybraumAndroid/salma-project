package com.salmabutchershop.salma.meridian.app.butcher.wholseller.productdetail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.enquiry.EnquiryFragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 9/29/2016.
 */

public class Butcher_product_Fragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

 Butcher_product_adapter adapter1;
    ProgressBar progress;
    String result, galry_image,Dat,Dat1;
    static ArrayList<Butcher_product_Model> zwm;
    Butcher_product_Model wm;
    String ids,ids1;
    String pro_id,pro_name,pro_cat_id,pro_subcat_id,stock_unit,price,currency,price_quantity,discount,discount_unit,pro_image,minimum_order,wholeseller_id,delivery_time,pro_description;
    String Wurl,shopname;
    public static MKLoader progress_loader;
    TextView header_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.butcher_product_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
       // Dat= getArguments().getString("desc");
        ids=getIntent().getStringExtra("id");
ids1=getIntent().getStringExtra("add");

        header_text=(TextView)findViewById(R.id.header_text);
        shopname=getIntent().getStringExtra("shopname");
        header_text.setText(shopname);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Butcher_product_Fragment.this.goBack();
            }
        });
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Butcher_product_Fragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(Butcher_product_Fragment.this);
          /*  com.chootdev.csnackbar.Snackbar.with(Butcher_product_Fragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/



        }


    }

    private void goBack() {
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {



                HttpClient httpclient = new DefaultHttpClient();
                String l=getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>"+l);
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+"http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/ar/wholeseller-product_deatails.php?cat_id="+ids+"&added_by="+ids1);
                if (l.equalsIgnoreCase("English")) {
                    Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/wholeseller-product_deatails.php?cat_id="+ids+"&added_by="+ids1;
                }
                else{
                    Wurl="http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/ar/wholeseller-product_deatails.php?cat_id="+ids+"&added_by="+ids1;
                }



                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
          //  String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
        //    System.out.println(">>>>>>>>>>>>>>>" + sam);
           // String value = result;
        /*    if
                    (result.equalsIgnoreCase("")) {
                new SweetAlertDialog(Butcher_product_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                sDialog.dismiss();
                            }
                        })
                        .show();
             *//*   com.nispok.snackbar.Snackbar.with(Butcher_product_Fragment.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(Butcher_product_Fragment.this);*//*
;
            } else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                zwm = new ArrayList<Butcher_product_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        wm = new Butcher_product_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        pro_id = mJsonObject.getString("pro_id");
                        pro_name = mJsonObject.getString("pro_name");
                        pro_cat_id = mJsonObject.getString("pro_cat_id");
                        pro_subcat_id = mJsonObject.getString("pro_subcat_id");
                        stock_unit = mJsonObject.getString("stock_unit");

                        price = mJsonObject.getString("price");
                        currency = mJsonObject.getString("currency");
                        price_quantity = mJsonObject.getString("price_quantity");
                        discount = mJsonObject.getString("discount");
                        discount_unit = mJsonObject.getString("discount_unit");
                        pro_image = mJsonObject.getString("pro_image");
                        minimum_order=mJsonObject.getString("minimum_order");
                        wholeseller_id=mJsonObject.getString("wholeseller_id");
                        delivery_time=mJsonObject.getString("delivery_time");
                        pro_description=mJsonObject.getString("pro_description");
                        //  String o = mJsonObject.getString("specialty_other");
                        wm.setPro_id(pro_id);
                        wm.setPro_cat_id(pro_cat_id);
                        wm.setPro_name(pro_name);
                        wm.setPro_subcat_id(pro_subcat_id);
                        wm.setStock_unit(stock_unit);
                        wm.setPrice(price);
                        wm.setCurrency(currency);
                        wm.setPrice_quantity(price_quantity);
                        wm.setDiscount(discount);
                        wm.setDiscount_unit(discount_unit);
                        wm.setPro_image(pro_image);
                    wm.setMinimum_order(minimum_order);
                        wm.setWholeseller_id(wholeseller_id);
                        wm.setDelivery_time(delivery_time);
                        wm.setPro_description(pro_description);

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        zwm.add(wm);
                        System.out.println("<<<oo>>>>" + wm);

                        adapter1 = new Butcher_product_adapter(zwm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                new SweetAlertDialog(Butcher_product_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })
                        .show();
         /*       com.nispok.snackbar.Snackbar.with(Butcher_product_Fragment.this) // context
                        .text("No Products!") // text to display
                        .show(Butcher_product_Fragment.this);*/

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i = new Intent(getApplicationContext(),EnquiryFragment.class);
                                i.putExtra("id",zwm.get(position).getPro_id());
                                i.putExtra("img",zwm.get(position).getPro_image());
                                i.putExtra("nam",zwm.get(position).getPro_name());
                                i.putExtra("wh_id",zwm.get(position).getWholeseller_id());
                                i.putExtra("description",zwm.get(position).getPro_description());

                                startActivity(i);
                                finish();

                            }
                        })
                );
            }


        }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    }
