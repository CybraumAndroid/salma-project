package com.salmabutchershop.salma.meridian.app.customer.notify;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by libin on 1/7/2017.
 */
public class PushNotifiction_adapter extends RecyclerView.Adapter<PushNotifiction_adapter.ViewHolder> {


    List<PushNotifiction_Model> pem;
    Context context;



    public PushNotifiction_adapter(ArrayList<PushNotifiction_Model> pem, Context context) {
        this.pem = pem;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3,tv4;
        // TextView personAge;

        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            tv=   (TextView) itemView.findViewById(R.id.not_title);
            tv1=   (TextView) itemView.findViewById(R.id.not_desc);
            tv2=   (TextView) itemView.findViewById(R.id.not_date);
          tv3=   (TextView) itemView.findViewById(R.id.adsxc);
         /*   tv4=   (TextView) itemView.findViewById(R.id.status);*/
            ;
            // v=   (ImageView) itemView.findViewById(R.id.imageView13);
        }
    }


    @Override
    public int getItemCount() {
        return pem.size();
    }

//25.257621,55.298126
    @Override
    public PushNotifiction_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.push_item_layout, viewGroup, false);
        PushNotifiction_adapter.ViewHolder pvh = new PushNotifiction_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PushNotifiction_adapter.ViewHolder personViewHolder, final int i) {
       // String input = "name:score";
        final String[] splitStringArray = pem.get(i).getNot_date().split(" ");
        String a = splitStringArray[0];
        String b = splitStringArray[1];

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<timestamp>>>>>>>>>>>>>>>>>>>>>"+currentDateTimeString);
        Typeface myFont1 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont1);
        personViewHolder.tv.setText(pem.get(i).getNot_title());

        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv1.setTypeface(myFont2);
        personViewHolder.tv1.setText(pem.get(i).getNot_desc());

        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv2.setTypeface(myFont3);
        personViewHolder.tv2.setText(pem.get(i).getNot_date());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv3.setTypeface(myFont4);
        personViewHolder.tv3.setText(pem.get(i).getTime_diff()+"  "+"ago");

     /*

        Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.tv4.setTypeface(myFont5);
        personViewHolder.tv4.setText(eem.get(i).getOrder_status());*/


    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}