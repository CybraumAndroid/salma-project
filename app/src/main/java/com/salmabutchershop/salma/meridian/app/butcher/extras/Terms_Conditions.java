package com.salmabutchershop.salma.meridian.app.butcher.extras;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Model;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_adapter;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/20/2016.
 */

public class Terms_Conditions extends Activity {
    ProgressBar progress;
    String result,term_id,term_desc;
    static ArrayList<Terms_Model> tsm;
    Terms_Model tm;
    TextView Tv;
    WebView webDetail;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.termsconditions);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
       // Tv= (TextView) findViewById(R.id.textView45);
        webDetail=(WebView) findViewById(R.id.webview);

        WebSettings websetting = webDetail.getSettings();

        websetting.setDefaultTextEncodingName("utf-8");


        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Terms_Conditions.this.goBack();
            }
        });

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Terms_Conditions.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off))  // text to display
                    .show(Terms_Conditions.this);
           /* com.chootdev.csnackbar.Snackbar.with(Terms_Conditions.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();
*/


        }
    }

  /*  private void goBack() {
        super.onBackPressed();
    }*/

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {



                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER+"json/customer/terms_conditions.php"
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
          //  String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
            if
                    (result.equalsIgnoreCase("")) {
                com.nispok.snackbar.Snackbar.with(Terms_Conditions.this) // context
                        .text("No items!") // text to display
                        .show(Terms_Conditions.this);
               // Toast.makeText(getApplicationContext(), "No items", Toast.LENGTH_SHORT).show();
            }  else if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
               tsm = new ArrayList<Terms_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        tm = new Terms_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);



                        term_id= mJsonObject.getString("term_id");
                        term_desc= mJsonObject.getString("term_desc");

                      tm.setTerm_id(term_id);
                        tm.setTerm_desc(term_desc);


                        tsm.add(tm);
                        System.out.println("<<<oo>>>>" + tm);
                        webDetail.loadData(term_desc, "text/html; charset=utf-8", null);
//Tv.setText(term_desc);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }

            else{

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

        }
    }
    private void goBack() {
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
}