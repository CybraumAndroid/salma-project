package com.salmabutchershop.salma.meridian.app.retailer.international;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Model;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_adapter;
import com.salmabutchershop.salma.meridian.app.retailer.enquiry.RetailerEnquiryFragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/7/2016.
 */

public class InternationalFragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;
    //   static ArrayList<UpcomingModel> upm;
    Retailer_International_List_adapter adapter1;
    ProgressBar progress;
    String result, galry_image, Dat, Dat1;
    static ArrayList<Retailer_International_List_Model> rim;
    Retailer_International_List_Model im;
    String int_id,com_name,logo,country,city,con_person,designation,phon_no,email,address;
    String k;
    public static MKLoader progress_loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.international_list_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        // Dat= getArguments().getString("desc");
        // Dat= getArguments().getString("des_id");
        Intent intent = getIntent();
        k = intent.getStringExtra("cat_id");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" + k);

        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InternationalFragment.this.goBack();
            }
        });


        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);

        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(InternationalFragment.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(InternationalFragment.this);
            /*com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }


    }

    private void goBack() {
        super.onBackPressed();
    }




    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {



                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER+"json/retailer/intsuppliers.php"//+k//+Dat
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
         //   String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
         //   String value = result;
           /* if
                    (result.equalsIgnoreCase("[]")) {
                com.nispok.snackbar.Snackbar.with(InternationalFragment.this) // context
                        .text("No International Shops Available!") // text to display
                        .show(InternationalFragment.this);
               *//* com.chootdev.csnackbar.Snackbar.with(InternationalFragment.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("Currently No International Shops Available!")
                        .duration(Duration.SHORT)
                        .show();*//*
                //Toast.makeText(getApplicationContext(), "No Events", Toast.LENGTH_SHORT).show();
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                rim = new ArrayList<Retailer_International_List_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        im = new Retailer_International_List_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);

                        int_id = mJsonObject.getString("int_id");
                        com_name = mJsonObject.getString("com_name");
                        logo = mJsonObject.getString("logo");
                        country = mJsonObject.getString("country");
                        city = mJsonObject.getString("city");
                        con_person = mJsonObject.getString("con_person");
                        designation = mJsonObject.getString("designation");
                        phon_no = mJsonObject.getString("phon_no");
                        email = mJsonObject.getString("email");
                        address = mJsonObject.getString("address");
                        //  event_id = mJsonObject.getString("event_id");

                        //  String o = mJsonObject.getString("specialty_other");
                        im.setInt_id(int_id);
                        im.setCom_name(com_name);
                        im.setLogo(logo);
                        im.setCountry(country);
                        im.setCity(city);
                        im.setCon_person(con_person);
                        im.setDesignation(designation);
                        im.setPhon_no(phon_no);
                        im.setEmail(email);
                        im.setAddress(address);
                        System.out.println("<<<galry_img>>>>" + galry_image);

                        //   System.out.println("<<<oo>>>>" + o);
                        //   onComplete();

                        rim.add(im);
                        System.out.println("<<<oo>>>>" + im);

                        adapter1 = new Retailer_International_List_adapter(rim, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                com.nispok.snackbar.Snackbar.with(InternationalFragment.this) // context
                        .text("No International Shops Available!") // text to display
                        .show(InternationalFragment.this);
        /*        com.chootdev.csnackbar.Snackbar.with(InternationalFragment.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("Currently No International Shops Available!")
                        .duration(Duration.SHORT)
                        .show();*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

               /* ArrayAdapter<PastModel> adapter1 = new MyList();
                ListV.setAdapter(adapter1);
*/
                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                // Lm=pns.get(position).getEvent_year();
                                //  System.out.println("<<<oo>lllllllllllllllllllll>>>" + Lm);
                                // ListV.setVisibility(View.GONE);
                                String in_id=rim.get(position).getInt_id();
                                String cm_nm=rim.get(position).getCom_name();
                                String ads=rim.get(position).getAddress();
                                String ph=rim.get(position).getPhon_no();
                                String em=rim.get(position).getEmail();
                                String lg=rim.get(position).getLogo();
                                Intent i=new Intent(getApplicationContext(),InternationalEnquiry.class);
                                i.putExtra("in_id",in_id);
                                i.putExtra("cm_nm",cm_nm);
                                i.putExtra("ads",ads);
                                i.putExtra("ph",ph);
                                i.putExtra("em",em);
                                i.putExtra("lg",lg);
                                startActivity(i);
                            }
                        })
                );
            }


        }
    }
