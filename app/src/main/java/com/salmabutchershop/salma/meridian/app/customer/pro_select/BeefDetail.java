package com.salmabutchershop.salma.meridian.app.customer.pro_select;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_sublist.Customer_Butcher_Sublist_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.database.OrderNew_Activity;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.customer.login.PostRegistration;
import com.salmabutchershop.salma.meridian.app.customer.notify.PushNotifiction;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;
import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER_AR;

/**
 * Created by libin on 11/14/2016.
 */
//AIzaSyCXT1WQw7SN9zetLUvYU-5YTzo2sGH5HYk
public class BeefDetail extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

    Product_Select_adapter adapter1;
    ProgressBar progress;
    String result, galry_image,Dat,Dat1;
    static ArrayList<Product_Select_Model> zwm;
    Product_Select_Model wm;
    String event_id,winner_name,winner_image,award_name;
    String k,l,m;
    String pro_id,pro_name,sub_id,description,type_old_price,
            brand,stock_in,stock_balance,stock_unit,price,main_pro_name,old_price,
            currency,price_quantity,discount,discount_unit,percent,pro_image,added_by,user_type,delivery_info,butcher_discount,shop_name,phone,email,latitude,longitude;
    TextView title,qty,plus,minus,cart_txt;
    private int uprange = 20;
    private int downrange = 0;
    private int values = 0;
    TextView tv,tv1,tv2,tv3;
    ImageView v;
    Button button2;
   static LinearLayout llm1;
   static TextView cnt;
    String Reg_id,names,typ,butcher_id,shp_nam,del_info,shop_addrs;
    String Wurl,min_order_amount,delivery_charge,cat_id,typ_id;
    public static MKLoader progress_loader;
//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=25.2048151,55.2708&radius=500&key=AIzaSyAo27AsUvSaSFEQ5OZWGc4l2SczpZhwnL4
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_select_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        title= (TextView) findViewById(R.id.header_text);
        Intent intent = getIntent();
        k = intent.getStringExtra("add_id");
        l= intent.getStringExtra("sub_id");
        m= intent.getStringExtra("title");
       /* Intent intent = getIntent();
        k = intent.getStringExtra("add_id");
        l= intent.getStringExtra("cat_id");*/
       // shp_nam=intent.getStringExtra("shp_nam");
       // del_info=intent.getStringExtra("del_info");
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa>>>>>>>>>" +k);
        System.out.println(">>>>>>cattttttttttttttttttttttttttttttttttttttttttt>>>>>>>>>" +l);
        System.out.println(">>>>>>mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmttt>>>>>>>>>" +m);
        title.setText(m);


        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        recyclerview.setHasFixedSize(true);
        Context context=getApplicationContext();

        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerview.setLayoutManager(llm);
        // Change to gridLayout
        recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BeefDetail.this.goBack();
            }
        });
        cnt= (TextView) findViewById(R.id.imageView2);
       /* SharedPreferences myPrefs = this.getSharedPreferences("MyPref", MODE_WORLD_READABLE);
        Reg_id = myPrefs.getString("user_id", null);*/

        System.out.println("<<<<<<<<<sharedname>>>>" + Reg_id);
        final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
        final int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        cnt.setText(""+s);


        llm1= (LinearLayout) findViewById(R.id.bottom_menu);
        llm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SharedPreferences myPrefs = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                Reg_id = myPrefs.getString("user_id", null);
                names= myPrefs.getString("fullname", null);
                typ= myPrefs.getString("usertype", null);

                System.out.println("<<<<<<<<<sharedname>>>>" + Reg_id);
                if (Reg_id != null && !Reg_id.isEmpty()){
                    //Toast.makeText(getApplicationContext(), Reg_id, Toast.LENGTH_SHORT).show();

                  if (cnt.getText().toString().equalsIgnoreCase("0")){
                 Toast.makeText(getApplicationContext(),"No Items In Cart", Toast.LENGTH_SHORT).show();
                }else {
                      System.out.println("-----------------------but_id-----------------"+butcher_id);
                  Intent i1 = new Intent(getApplicationContext(), OrderNew_Activity.class);
                      i1.putExtra("but_id",butcher_id);
                      i1.putExtra("min_order_amount",min_order_amount);
                    startActivity(i1);
                    finish();
                                }                }
                   else {

                    new SweetAlertDialog(BeefDetail.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(getResources().getString(R.string.Pleaseregister))
                            .setCustomImage(R.drawable.logo)
                            .setContentText(getResources().getString(R.string.AreyouSurewanttoregisternow))
                            .setConfirmText(getResources().getString(R.string.Yes))
                            .setCancelText(getResources().getString(R.string.No))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                    sDialog.dismiss();
                                   /* .setTitleText("Deleted!")
                                    .setContentText("Your imaginary file has been deleted!")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(null)
                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*/
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();






                    System.out.println("loginnn");


                }

            }
        });



        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(BeefDetail.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(BeefDetail.this);
       /*     com.chootdev.csnackbar.Snackbar.with(BeefDetail.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }

    }

    private void goBack() {
    finish();
    }

    public void functionToRun() {
        final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
        int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        cnt.setText(""+s);
    }

    public static void changeindex(int si) {
        cnt.setText(""+si);
      /*  float x = cnt.getScaleX();
        float y = cnt.getScaleY();
  cnt.setScaleX((float) (x + 1));
       cnt.setScaleY((float) (y + 1));*/

       // llm1.setBackgroundResource(R.color.colorgreen);
        llm1.setBackgroundResource(R.color.colorgreen);
    }

    public static void changeindex1(int si) {
        try {
            cnt.setText(""+si);
            llm1.setBackgroundResource(R.color.colorAccent);

        }
        catch (NullPointerException e)
        {

        }


    }




    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {
                String lk=getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>"+lk);
                if (lk.equalsIgnoreCase("English")) {
                    Wurl=SERVER_CUSTOMER+"butcher_products.php?&sub_id="+l+"&butcher_id="+k;
                }
                else{
                    Wurl=SERVER_CUSTOMER_AR+"butcher_products.php?&sub_id="+l+"&butcher_id="+k;
                }


                HttpClient httpclient = new DefaultHttpClient();

             //   http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/customer/ar/butcher_products.php?sub_id=1&butcher_id=213
                HttpPost httppost = new HttpPost(Wurl);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            progress_loader.setVisibility(ProgressBar.GONE);
            //String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
         //   String value = result.toString();
         //   System.out.println(">>>>>>>>>>>>>>>" +value);
       /*     if
                    (result.equalsIgnoreCase("")) {
                new SweetAlertDialog(BeefDetail.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                sDialog.dismiss();
                            }
                        })
                        .show();


            *//*    com.nispok.snackbar.Snackbar.with(BeefDetail.this) // context
                        .text(getResources().getString(R.string.NoProducts)) // text to display
                        .show(BeefDetail.this);*//*
                *//*Snackbar.with(BeefDetail.this,null)
                        .type(Type.SUCCESS)
                        .message("No Products available!")
                        .duration(Duration.LONG)
                        .show();*//*
              //  Toast.makeText(getApplicationContext(), "No Products available", Toast.LENGTH_SHORT).show();
            } else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                zwm = new ArrayList<Product_Select_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        wm = new Product_Select_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        pro_id = mJsonObject.getString("pro_id");
                        pro_name = mJsonObject.getString("pro_name");
                        //sub_id = mJsonObject.getString("sub_id");
                        description = mJsonObject.getString("description");
                      //  brand = mJsonObject.getString("brand");
                    //    stock_in = mJsonObject.getString("stock_in");
                       // stock_balance = mJsonObject.getString("stock_balance");
                        stock_unit = mJsonObject.getString("stock_unit");
                        type_old_price= mJsonObject.getString("type_old_price");
                        price = mJsonObject.getString("price");
                        currency = mJsonObject.getString("currency");
                        price_quantity = mJsonObject.getString("price_quantity");
                        discount = mJsonObject.getString("discount");
                        discount_unit = mJsonObject.getString("discount_unit");
                        //    percent= mJsonObject.getString("percent");
                        pro_image = mJsonObject.getString("pro_image");
                        main_pro_name= mJsonObject.getString("main_pro_name");
                       // added_by = mJsonObject.getString("added_by");
                      //  user_type = mJsonObject.getString("user_type");
                       delivery_info = mJsonObject.getString("delivery_info");
                      //  butcher_discount = mJsonObject.getString("butcher_discount");
                        //  event_id = mJsonObject.getString("event_id");
                        shop_addrs= mJsonObject.getString("shop_addrs");
                        butcher_id = mJsonObject.getString("butcher_id");
                        shop_name= mJsonObject.getString("shop_name");
                                phone= mJsonObject.getString("phone");
                        email= mJsonObject.getString("email");
//                        min_order_amount= mJsonObject.getString("min_order_amount");
                        latitude= mJsonObject.getString("latitude");
                        longitude= mJsonObject.getString("longitude");
                        delivery_charge= mJsonObject.getString("delivery_charge");
                        cat_id= mJsonObject.getString("cat_id");
                        typ_id= mJsonObject.getString("typ_id");
                        wm.setPro_id(pro_id);
                        wm.setPro_name(pro_name);
                       // wm.setSub_id(sub_id);
                        wm.setDescription(description);
          wm.setButcher_id(butcher_id);
                     //   wm.setStock_in(stock_in);
                       // wm.setStock_balance(stock_balance);
                        wm.setStock_unit(stock_unit);
                        wm.setPrice(price);
                        wm.setType_old_price(type_old_price);
                        wm.setCurrency(currency);
                        wm.setPrice_quantity(price_quantity);
                        wm.setDiscount(discount);
                        wm.setDiscount_unit(discount_unit);
                        wm.setPercent(percent);
                        wm.setPro_image(pro_image);
                        wm.setMain_pro_name(main_pro_name);
                        wm.setShop_addrs(shop_addrs);
                        wm.setShop_name(shop_name);
                        wm.setPhone(phone);
                        wm.setEmail(email);
                      //  wm.setMin_order_amount(min_order_amount);
                        wm.setLatitude(latitude);
                        wm.setLongitude(longitude);
                        wm.setCat_id(cat_id);
                       // wm.setAdded_by(added_by);
                     //   wm.setUser_type(user_type);
                      wm.setDelivery_info(delivery_info);
                        wm.setDelivery_charge(delivery_charge);
                        wm.setTyp_id(typ_id);

                       // wm.setButcher_discount(butcher_discount);
                        //  wm.s

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        //   System.out.println("<<<oo>>>>" + o);
                        //   onComplete();

                        zwm.add(wm);
                        System.out.println("<<<oo>>>>" + wm);
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<CURRENCY>>>>>>>>>>>>>>>>>>>>>"+zwm.get(i).getCurrency());


                         adapter1 = new Product_Select_adapter(zwm, getApplicationContext());
                           recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("currency",currency);
                editor.commit();

            }

            else{
                if(!((Activity) BeefDetail.this).isFinishing())
                {
                new SweetAlertDialog(BeefDetail.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })

                        .show();

                    //show dialog
                }
              //  System.out.println("nulllllllllllllllllllllllllllllllll");
            }


        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        final DatabaseHelper dataHelper = new DatabaseHelper(getApplicationContext());
        final int s = (int) dataHelper.getProfilesCount();
        System.out.println("counttttttttttttttttttttttttt" + s);
        cnt.setText(""+s);
        if(s==0){
            llm1.setBackgroundResource(R.color.colorAccent);

        }
        else {
            llm1.setBackgroundResource(R.color.colorgreen);
        }
    }
}