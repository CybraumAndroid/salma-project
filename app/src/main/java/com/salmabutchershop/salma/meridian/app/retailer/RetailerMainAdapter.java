package com.salmabutchershop.salma.meridian.app.retailer;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.productdetail.Butcher_product_Model;
import com.salmabutchershop.salma.meridian.app.retailer.productdet.RetailerProductAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 12/6/2016.
 */
public class RetailerMainAdapter extends RecyclerView.Adapter<RetailerMainAdapter.ViewHolder> {


    List<Retailer_main_Model> rmm;
    Context context;



    public RetailerMainAdapter(ArrayList<Retailer_main_Model> rmm, Context context) {
        this.rmm = rmm;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;
        public static MKLoader progress_loader;
        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            ///  tv=   (TextView) itemView.findViewById(R.id.title);
            tv1=   (TextView) itemView.findViewById(R.id.textView35);
           // progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);

            v=   (ImageView) itemView.findViewById(R.id.imageView13);
        }
    }


    @Override
    public int getItemCount() {
        return rmm.size();
    }


    @Override
    public RetailerMainAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.retail_main_item_layout, viewGroup, false);
        RetailerMainAdapter.ViewHolder pvh = new RetailerMainAdapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final RetailerMainAdapter.ViewHolder personViewHolder, final int i) {


        String s = rmm.get(i).getRetail_img();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv1.setTypeface(myFont2);
        personViewHolder.tv1.setText(rmm.get(i).getRetail_cat());
        /*Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.tv2.setTypeface(myFont3);
        personViewHolder.tv2.setText(zwm.get(i).getPrice());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.tv3.setTypeface(myFont4);
        personViewHolder.tv3.setText(zwm.get(i).getPrice_quantity());
*/
        //  String img=enm.get(i).getNews_img();
        System.out.println("ooo" + s);
     //   Picasso.with(context).load(s).noFade().into(personViewHolder.v);
        try {


            new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .build()
                    .load(s)
                    .noFade()
                    // .into(personViewHolder.v);
                    .into(personViewHolder.v, new Callback() {
                        @Override
                        public void onSuccess() {

                          //  personViewHolder.progress_loader.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}