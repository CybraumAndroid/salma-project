package com.salmabutchershop.salma.meridian.app.customer.custom_er;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by libin on 9/29/2016.
 */
public class Customer_Services_adapter extends RecyclerView.Adapter<Customer_Services_adapter.ViewHolder> {


    List<Customer_Services_Model>csm;
    Context context;



    public Customer_Services_adapter(ArrayList<Customer_Services_Model> csm, Context context) {
        this.csm = csm;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;
ProgressBar gallery_progressbar;
        ImageView personPhoto;
        public static MKLoader progress_loader;
        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

         tv=   (TextView) itemView.findViewById(R.id.title);
           /* tv1=   (TextView) itemView.findViewById(R.id.evnt_frm);
            tv2=   (TextView) itemView.findViewById(R.id.evnt_to);
            tv3=   (TextView) itemView.findViewById(R.id.evnt_vnue);
         ;*/
            v=   (ImageView) itemView.findViewById(R.id.imageView8);
            gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
            progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
        }
    }


    @Override
    public int getItemCount() {
        return csm.size();
    }


    @Override
    public Customer_Services_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.customers_item_layout, viewGroup, false);
        Customer_Services_adapter.ViewHolder pvh = new Customer_Services_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Customer_Services_adapter.ViewHolder personViewHolder, final int i) {


        String s = csm.get(i).getSub_image();
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv.setTypeface(myFont2);
      personViewHolder.tv.setText(csm.get(i).getSubcat_name());

        //  String img=enm.get(i).getNews_img();
        System.out.println("ooo" + s);
      //  Picasso.with(context).load(s).noFade().into(personViewHolder.v);
      /*  Glide
                .with(context)
                .load(s)
                .into(personViewHolder.v);*/
        try {
        new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                .build()
                .load(s)
                .noFade()

                .into(personViewHolder.v, new Callback() {
                    @Override
                    public void onSuccess() {
                        personViewHolder.progress_loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                       // personViewHolder.progress_loader.setVisibility(View.GONE);
                    }
                });
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}