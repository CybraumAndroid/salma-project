package com.salmabutchershop.salma.meridian.app.customer.myaccount;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.address.AddressFragment;
import com.salmabutchershop.salma.meridian.app.customer.address.DashboardActivity;
import com.salmabutchershop.salma.meridian.app.customer.address.DatabaseHandler_N;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Favorites;
import com.salmabutchershop.salma.meridian.app.customer.database.CartModel;
import com.salmabutchershop.salma.meridian.app.customer.database.JsonModel;
import com.salmabutchershop.salma.meridian.app.customer.login.RegisterActivity;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by libin on 12/22/2016.
 */
public class AddressNewFragment extends AppCompatActivity {
    EditText fnames, lnames, citys, dlvry_area, street, building, floor, apartment_no, mob_no, email, loctn;
    Button submt;
    String name;
    Float price;
    String fnames_s, lnames_s, citys_s, dlvry_area_s, street_s, building_s, floor_s, apartment_no_s, mob_no_s, email_s, loctn_s, delivery_ads_s;
    RadioGroup radio_group;
    RadioButton radio_home, radio_work, radio_newh;
    String radio_home_s;
    ArrayList<CartModel> arrayListcart = new ArrayList<>();
    ArrayList<JsonModel> array_lstcart;
    ArrayList<String> array_item = new ArrayList<>();
    ArrayList<String> array_qty = new ArrayList<>();
    ArrayList<String> array_product = new ArrayList<>();
    ArrayList<String> array_price = new ArrayList<>();
    ArrayList<String> some_jsn = new ArrayList<>();
    String sme_jsn;
    JsonModel s;
    SQLiteDatabase db;
    ProgressBar progress;
    JSONArray jsonArray1 = new JSONArray();
    String csv, csd, csf, csg;
    boolean edittexterror = false;
    static String filename = "null", filenamepath = "null";
    private DatabaseHandler_N dh;
    private Button viewFav;
    private String[] values;
    String fn, ln, ci, buil, flo, apt, mob, em, loc, dla;
    LinearLayout Lx, Ly;
    String Reg_id, inst;
    String c_names, c_email, c_phone, c_location,loc_ads;
    String ads,city,state,postalCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_myacnt_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        fnames = (EditText) findViewById(R.id.us_nam);
        lnames = (EditText) findViewById(R.id.edt_lastname);

        citys = (EditText) findViewById(R.id.edt_city);
        dlvry_area = (EditText) findViewById(R.id.edt_dvry_area);
        street = (EditText) findViewById(R.id.edt_strt);
        building = (EditText) findViewById(R.id.edt_bldng);
        floor = (EditText) findViewById(R.id.edt_flr);
        apartment_no = (EditText) findViewById(R.id.edt_aptno);
        mob_no = (EditText) findViewById(R.id.edt_mble);
        email = (EditText) findViewById(R.id.edt_phone);
        loctn= (EditText) findViewById(R.id.edt_lctn);
        radio_group= (RadioGroup) findViewById(R.id.radio_group);

        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Reg_id = myPrefs.getString("user_id", null);
        loc_ads=myPrefs.getString("loc_ads",null);


if(loc_ads != null && !loc_ads.isEmpty() && !loc_ads.equals("null")) {
    loc_ads= myPrefs.getString("loc_ads", null);

    ads=myPrefs.getString("ads",null);
    city=myPrefs.getString("city",null);
    state=myPrefs.getString("state",null);
    postalCode=myPrefs.getString("postal",null);
    loctn.setText(""+ads+"\n"+""+city+","+state+""+","+postalCode);
}else {

}
        c_names= myPrefs.getString("c_name", null);

        c_email= myPrefs.getString("c_email", null);
        c_phone= myPrefs.getString("c_phone", null);
        c_location= myPrefs.getString("c_location", null);
        if (Reg_id != null && !Reg_id.isEmpty()){
            fnames.setText(c_names);
            // lnames.setText(string2);
            mob_no.setText(c_phone);
            email.setText(c_email);
            // loctn.setText(c_location);
        }
        System.out.println("bbbbbbbbbbbbbbbbbbbb"+c_names);
        System.out.println("cccccccccccccccccccc"+c_email);
        System.out.println("bbbbbbbbbbbbbbbbbbbb"+c_phone);
        System.out.println("cccccccccccccccccccc"+c_location);
        radio_home= (RadioButton) findViewById(R.id.home);
        radio_work= (RadioButton) findViewById(R.id.work);
        radio_newh= (RadioButton) findViewById(R.id.newh);

        Lx= (LinearLayout) findViewById(R.id.lx);
        Ly= (LinearLayout) findViewById(R.id.ly);

        ImageView back= (ImageView) findViewById(R.id.back_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddressNewFragment.this.goBack();
            }
        });
        loctn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),DashboardActivity.class);
                startActivity(i);
            }
        });
        submt = (Button) findViewById(R.id.button);
        submt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(radio_home.isChecked())
                {
                    radio_home_s=radio_home.getText().toString();
                }
                else if(radio_work.isChecked())
                {
                    radio_home_s=radio_work.getText().toString();
                }

                else if(radio_newh.isChecked())
                {
                    radio_home_s=radio_newh.getText().toString();
                }

                fnames_s= fnames.getText().toString();
                lnames_s = lnames.getText().toString();
                citys_s=citys.getText().toString();
                dlvry_area_s= dlvry_area.getText().toString();
                street_s = street.getText().toString();
                building_s = building.getText().toString();
                floor_s= floor.getText().toString();
                apartment_no_s= apartment_no.getText().toString();
                mob_no_s = mob_no.getText().toString();
                email_s= email.getText().toString();
                loctn_s = loctn.getText().toString();
                edittexterror = false;


                if (fnames.getText().toString().isEmpty() ||lnames.getText().toString().isEmpty()
                        || citys.getText().toString().isEmpty() ||dlvry_area.getText().toString().isEmpty()
                        || street.getText().toString().isEmpty()
                        || building.getText().toString().isEmpty()|| floor.getText().toString().isEmpty()
                        || mob_no.getText().toString().isEmpty()
                        || email.getText().toString().isEmpty()) {
                    com.nispok.snackbar.Snackbar.with(AddressNewFragment.this) // context
                            .text("Field Empty!") // text to display
                            .show(AddressNewFragment.this);
                  /*  com.chootdev.csnackbar.Snackbar.with(AddressNewFragment.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Field Empty!")
                            .duration(Duration.SHORT)
                            .show();*/
                    edittexterror = true;
                    //  nbutton.setTextColor(getResources().getColor(R.color.Orange));

                }  else if (email.getText().toString().isEmpty()) {
                    email.setError("Enter Phone");
                    edittexterror = true;
                } else if (fnames.getText().toString().isEmpty()) {
                    fnames.setError("Enter Full Name");
                    edittexterror = true;
                } else if (lnames.getText().toString().isEmpty()) {
                    lnames.setError("Enter Last Name");
                    edittexterror = true;
                } else if (citys.getText().toString().isEmpty()) {
                    citys.setError("Enter City");
                    edittexterror = true;
                }else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()) {
                    email.setError("Invalid Email");
                    edittexterror = true;
                } else if (street.getText().toString().isEmpty()) {
                    street.setError("Enter Street");
                    edittexterror = true;
                } else if (dlvry_area.getText().toString().isEmpty()) {
                    dlvry_area.setError("Enter Delivery Area");
                    edittexterror = true;
                } else if (building.getText().toString().isEmpty()) {
                    building.setError("Enter Building");
                    edittexterror = true;
                } else if (filenamepath != null) {
                    filename = filenamepath;


                } else {
                    filename = "null";

                }

                if (edittexterror == false) {
                    if (DetectConnection
                            .checkInternetConnection(getApplicationContext())) {
                        dh=new DatabaseHandler_N();
                        dh.CreateDatas(getApplicationContext());
                        values=new String[12];
                        values[0]=fnames_s;
                        values[1]=lnames_s;
                        values[2]=citys_s;
                        values[3]=dlvry_area_s;
                        values[4]=street_s;
                        values[5]=radio_home_s;
                        values[6]=building_s;
                        values[7]=floor_s;
                        values[8]=apartment_no_s;

                        values[9]=mob_no_s;
                        values[10]=email_s;
                        values[11]="dubai";
                        dh.InsertDatas(getApplicationContext(),values);
                   /*     com.chootdev.csnackbar.Snackbar.with(AddressNewFragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Successfulyy address added!")
                                .duration(Duration.SHORT)
                                .show();
                        finish();*/
                        final Snackbar snackbar = Snackbar
                                .make(view, "Address Added", Snackbar.LENGTH_LONG)
                                .setAction("View", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent i=new Intent(AddressNewFragment.this,AddressMyaccount.class);
                                        // i.putExtra("add_id",added_id);
                                        //  i.putExtra("cat_id",cat_id);
                                       // i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        //i.putExtra("value",status);
                                      startActivity(i);


                                      /*  Snackbar snackbar1 = Snackbar.make(view, "Image Deleted!", Snackbar.LENGTH_SHORT);
                                        notifyDataSetChanged();
                                        View sbView = snackbar1.getView();
                                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                                        textView.setTextColor(Color.YELLOW);
                                        snackbar1.show();*/

                                    }
                                });
                        snackbar.setActionTextColor(Color.RED);
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                        Intent i=new Intent(AddressNewFragment.this,AddressMyaccount.class);
                        // i.putExtra("add_id",added_id);
                        //  i.putExtra("cat_id",cat_id);
                        // i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        //i.putExtra("value",status);
                        startActivity(i);
                        finish();

                    } else {
                        com.nispok.snackbar.Snackbar.with(AddressNewFragment.this) // context
                                .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                                .show(AddressNewFragment.this);
                      /*  com.chootdev.csnackbar.Snackbar.with(AddressNewFragment.this,null)
                                .type(com.chootdev.csnackbar.Type.SUCCESS)
                                .message("Oops Your Connection Seems Off..!!")
                                .duration(Duration.SHORT)
                                .show();*/

                    }


                }
            }
        });
    }

    private void goBack() {
        super.onBackPressed();
    }
}