package com.salmabutchershop.salma.meridian.app.customer.myaccount.history;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER_CUSTOMER;

/**
 * Created by libin on 1/23/2017.
 */

public class My_Coustomer_history_details extends  Activity {
    String result;
    ProgressBar progress;
    static ArrayList<Coustomer_MyOrder_List_Model> cmo;
    Coustomer_MyOrder_List_Model cm;
    String k,l,o;
    String user_id,order_item_id, order_id, pro_name, pro_type_name, pro_qty, pro_price,instruction,
           subtotal, total, order_date, order_status;
    TextView Name, Ads, Item_Name, Qty, Delivry_date, Location,Phone,Email,Instruction,type,Order_no;
    Button accepted;
    String userid,fullname,email;
    RecyclerView recyclerview;
    Coustomer_My_order_Details_adapter adapter1;
    String stat,currency,purchase_quantity;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_historydetail_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        userid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullnameb", null);


        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);

        Name = (TextView) findViewById(R.id.textView2);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Name.setTypeface(myFont3);

        Ads = (TextView) findViewById(R.id.dlvry_ads);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Ads.setTypeface(myFont4);
        /*Qty = (TextView) findViewById(R.id.qty);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Qty.setTypeface(myFont5);*/
        Delivry_date = (TextView) findViewById(R.id.dlvry_date);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Delivry_date.setTypeface(myFont6);
       /* Location = (TextView) findViewById(R.id.lctn);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
       Location.setTypeface(myFont7);*/
       /* Item_Name = (TextView) findViewById(R.id.item_name);
        Typeface myFont8 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Item_Name.setTypeface(myFont8);
*/

        Phone = (TextView) findViewById(R.id.textView34);
        Typeface myFont1= Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Phone.setTypeface(myFont1);

        Email = (TextView) findViewById(R.id.dlvry_datellq);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Email.setTypeface(myFont2);

        Instruction = (TextView) findViewById(R.id.instruction);
        Typeface myFont11 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Instruction.setTypeface(myFont11);

        /*type = (TextView) findViewById(R.id.pro_type_name);
        Typeface myFont12 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        type.setTypeface(myFont12);*/
        accepted = (Button) findViewById(R.id.accepted);
        Typeface myFont9 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        accepted.setTypeface(myFont9);
        Order_no= (TextView) findViewById(R.id.order_no);
        Typeface myFontf = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Order_no.setTypeface(myFontf);
     /*   accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    new Butcher_MyOrder_New_Detail.SendPostRequest3().execute();

                } else {

                    final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(Butcher_MyOrder_New_Detail.this).create();
                    alertDialog.setTitle("Alert");

                    alertDialog.setMessage("No Internet");


                    alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();


                        }
                    });
                    alertDialog.show();



                }


            }
        });*/
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              My_Coustomer_history_details.this.finish();
            }
        });
        k = getIntent().getStringExtra("order_id");
       /* l= getIntent().getStringExtra("pr_id");
        o=getIntent().getStringExtra("order_id");
        stat=getIntent().getStringExtra("stat");*/
      /*  if (stat.equalsIgnoreCase("1")){
            accepted.setVisibility(View.VISIBLE);
        }
        else if(stat.equalsIgnoreCase("2")){
            accepted.setVisibility(View.GONE);
        }
        else {

        }*/

        System.out.println("ggggggggggggg"+k);
        System.out.println("hhhhhhhhhhhhhhhhh"+l);
        System.out.println("jjjjjjjjjjjjjjjjjjjj"+o);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(My_Coustomer_history_details.this) // context
                    .text("Oops Your Connection Seems Off..!") // text to display
                    .show(My_Coustomer_history_details.this);
         /*   com.chootdev.csnackbar.Snackbar.with(this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }


    }

    private void goBack() {
        super.onBackPressed();
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost(SERVER_CUSTOMER+"history_details.php?order_id="+k
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
            String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
            System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
         /*   if
                    (result.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "No products", Toast.LENGTH_SHORT).show();
            } else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
             cmo = new ArrayList<Coustomer_MyOrder_List_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        cm = new Coustomer_MyOrder_List_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));

                        user_id = mJsonObject.getString("user_id");
                        order_id = mJsonObject.getString("order_id");
                        order_item_id= mJsonObject.getString("order_item_id");
                        pro_name = mJsonObject.getString("pro_name");
                        pro_type_name = mJsonObject.getString("pro_type_name");
                        pro_qty = mJsonObject.getString("pro_qty");
                        pro_price = mJsonObject.getString("pro_price");
                        instruction= mJsonObject.getString("instruction");
                        subtotal= mJsonObject.getString("total");
                        currency= mJsonObject.getString("currency");
                       // subtotal = mJsonObject.getString("subtotal");
                       // total = mJsonObject.getString("total");
                      //  order_date = mJsonObject.getString("order_date");

                        purchase_quantity= mJsonObject.getString("purchase_quantity");
                        order_status = mJsonObject.getString("order_status");

                       cm.setUser_id(user_id);
                        cm.setOrder_item_id(order_item_id);
                       cm.setOrder_id(order_id);
                       cm.setPro_name(pro_name);
                       cm.setPro_type_name(pro_type_name);
                       cm.setPro_qty(pro_qty);
                        cm.setPro_price(pro_price);
                        cm.setInstruction(instruction);
                        cm.setCurrency(currency);
                        cm.setPurchase_quantity(purchase_quantity);
                      /*  bm.setFname(fname);
                        bm.setLname(lname);
                        bm.setCity(city);
                        bm.setDeliveryarea(deliveryarea);
                        bm.setAddresstype(addresstype);
                        bm.setStreet(street);
                        bm.setBuilding(building);
                        bm.setFloor(floor);
                        bm.setHouse_no(house_no);
                        bm.setMobile(mobile);
                        bm.setPhone(phone);*/
                      //  bm.setEmail(email);
                      //  bm.setInstruction(instruction);

                        /*bm.setAditional_direction(aditional_direction);
                        bm.setService_fee(service_fee);
                        bm.setContainer_charges(container_charges);
                        bm.setDelivery_charges(delivery_charges);*/
                        cm.setSubtotal(subtotal);
                        cm.setTotal(total);
                        cm.setOrder_date(order_date);
                       cm.setOrder_status(order_status);

                       cmo.add(cm);
                        System.out.println("<<<oo>>>>" + cm);

                        // TextView Name,Ads,Item_Name,Qty,Delivry_date,Location;
                      Name.setText(cm.getCurrency()+" "+cm.getSubtotal());
                    //    Ads.setText(cm.getHouse_no() + "," + bm.getBuilding()+ "\n" + bm.getFloor()+ "," + bm.getStreet() + "," + bm.getCity());
                        // Item_Name.setText(bm.getPro_name());
                        //  Qty.setText(bm.getPro_qty());
                       // Delivry_date.setText(bm.getOrder_date());
                        //  Location.setText(bm.getMobile());
                     //   Phone.setText("mob:"+bm.getMobile());
                   //     Email.setText("email:"+bm.getEmail());
                        Instruction.setText(cm.getInstruction());
                        Order_no.setText("Order No: "+cm.getOrder_id());
                        //type.setText("-"+bm.getPro_type_name());
                        adapter1 = new Coustomer_My_order_Details_adapter(cmo, getApplicationContext());
                        recyclerview.setAdapter(adapter1);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else{

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

        }
    }




}