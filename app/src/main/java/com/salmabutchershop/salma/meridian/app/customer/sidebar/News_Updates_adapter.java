package com.salmabutchershop.salma.meridian.app.customer.sidebar;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Notfy_Model;
import com.salmabutchershop.salma.meridian.app.butcher.extras.Notfy_adapter;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 12/23/2016.
 */
public class News_Updates_adapter extends RecyclerView.Adapter<News_Updates_adapter.ViewHolder> {


    List<NewsUpdates_Model> num;
    Context context;



    public News_Updates_adapter(ArrayList<NewsUpdates_Model> num, Context context) {
        this.num = num;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;
        // CardView cv;
        TextView speakr, topic, div, dept, evntnam;
        String imag;
        ImageView v1;
        TextView tv,tv1,tv2,tv3;
        // TextView personAge;

        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            //   cv1 = ( LinearLayout) itemView.findViewById(R.id.cv_crc);

            tv=   (TextView) itemView.findViewById(R.id.news_title);

            tv1=   (TextView) itemView.findViewById(R.id.news_desc);
            v1=   (ImageView) itemView.findViewById(R.id.imageView8);
        }
    }


    @Override
    public int getItemCount() {
        return num.size();
    }


    @Override
    public News_Updates_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_updates_item_layout, viewGroup, false);
        News_Updates_adapter.ViewHolder pvh = new News_Updates_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final News_Updates_adapter.ViewHolder personViewHolder, final int i) {


      String s = num.get(i).getNews_image();
        personViewHolder.tv.setText(num.get(i).getNews_title());
        Typeface myFont1 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
    personViewHolder.tv.setTypeface(myFont1);
        personViewHolder.tv1.setText(num.get(i).getNews_desc());
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.tv1.setTypeface(myFont2);


     Picasso.with(context).load(s).noFade().into(personViewHolder.v1);


       personViewHolder.v1.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               String s = num.get(i).getNews_image();
               Uri bmpUri = getLocalBitmapUri(personViewHolder.v1);
               if (bmpUri != null) {
                   // Construct a ShareIntent with link to image
                   Intent shareIntent = new Intent();
                   shareIntent.setAction(Intent.ACTION_SEND);
                   shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                   shareIntent.setType("image/*");
                shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                  view.getContext().startActivity(Intent.createChooser(shareIntent, "Share Image"));
               } else {


               }
           }
       });
    }

    public Uri getLocalBitmapUri(ImageView v1) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = v1.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) v1.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }




    

   

    /*// Can be triggered by a view event such as a button press
    public void onShareItem(View v) {
        // Get access to bitmap image from view
       // ImageView ivImage = (ImageView)i findViewById(R.id.ivResult);
        // Get access to the URI for the bitmap
        Uri bmpUri = getLocalBitmapUri((ImageView) v);
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image*//*");
            // Launch sharing dialog for image
            context.startActivity(Intent.createChooser(shareIntent, "Share Image"));
        } else {
            // ...sharing failed, handle error
        }
    }

    // Returns the URI path to the Bitmap displayed in specified ImageView
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private File getExternalFilesDir(String directoryPictures) {
    }*/

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}