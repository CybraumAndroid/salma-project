package com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

/**
 * Created by Rashid on 12/6/2016.
 */

public class Retailer_Wholeseller_DataAdapter extends RecyclerView.Adapter<Retailer_Wholeseller_DataAdapter.ViewHolder> {

    public ArrayList<WholeSellerModel>datas;
    public Context context1;


    public Retailer_Wholeseller_DataAdapter(Context context, ArrayList<WholeSellerModel> datas){
        this.context1=context;
        this.datas=datas;
        System.out.println("********************** inside adapter class constructor *************************");
        for(int i=0;i<datas.size();i++){
            System.out.println("///////////////////////  name : "+datas.get(i).name);
            System.out.println("/////////////////////// image url : "+datas.get(i).logo);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.retailer_wholeseller_row_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final  ViewHolder holder, int position) {
        holder.whole_seller.setText(datas.get(position).getShope_name());
        holder.seller_name.setText(datas.get(position).getName());
        holder.address.setText(datas.get(position).getAddress());
      /*  holder.cardView1.setBackground(datas.get(position).getLogo());*/





try {


        Picasso.with(context1)
                .load(datas.get(position).getLogo())
                .fit()
                .noFade()
               // .into(holder.imageId);
 .into(holder.imageId, new Callback() {
            @Override
            public void onSuccess() {
              holder.progress_loader.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
}catch (Exception e){
    e.printStackTrace();
}
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

    private LinearLayout image_layout;
        CardView cardView1;
    TextView whole_seller,seller_name,address;
        public static MKLoader progress_loader;
        ImageView imageId;
ProgressBar gallery_progressbar;
    public ViewHolder(View itemView) {
        super(itemView);
        cardView1=(CardView) itemView.findViewById(R.id.card_view1);
     /*   image_layout=(RelativeLayout)itemView.findViewById(R.id.image_layout);*/
        whole_seller=(TextView)itemView.findViewById(R.id.whole_seller);
        seller_name=(TextView)itemView.findViewById(R.id.seller_name);
        address=(TextView)itemView.findViewById(R.id.whole_seller_address);
        imageId=(ImageView)itemView.findViewById(R.id.imageId);
        gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
        progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
    }

}

}
