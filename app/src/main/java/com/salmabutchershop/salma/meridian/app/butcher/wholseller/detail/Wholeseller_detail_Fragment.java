package com.salmabutchershop.salma.meridian.app.butcher.wholseller.detail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_my_order;
import com.salmabutchershop.salma.meridian.app.butcher.orderdetail.Butcher_My_orderDetail;
import com.salmabutchershop.salma.meridian.app.butcher.sidebar.NewsUpdates;
import com.salmabutchershop.salma.meridian.app.butcher.wholseller.productdetail.Butcher_product_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.RecyclerItemClickListener;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 9/29/2016.
 */

public class Wholeseller_detail_Fragment extends Activity {
    FrameLayout container;
    FragmentManager fragmentManager;
    String tag = "events";
    Fragment fragment;
    View view;
    RecyclerView recyclerview;

 Wholeseller_detail_adapter adapter1;
    ProgressBar progress;
    String result, galry_image,Dat,Dat1;
    static ArrayList<Wholeseller_detail_Model> zwm;
    Wholeseller_detail_Model wm;
    String cat_id,main_cat_id,category_name,category_desc,cat_image,added_by;
    String ids;
    String Wurl,type_id,shopname;
    public static MKLoader progress_loader;
    Button C_Order,Order_H;
    TextView header_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wholeseller_fragment_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);
        ids=getIntent().getStringExtra("id");
        shopname=getIntent().getStringExtra("shopname");
        header_text=(TextView)findViewById(R.id.header_text);

        header_text.setText(shopname);
        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Wholeseller_detail_Fragment.this.goBack();
            }
        });
type_id="1";
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData1().execute();
        } else {
            com.nispok.snackbar.Snackbar.with(Wholeseller_detail_Fragment.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Wholeseller_detail_Fragment.this);
            /*com.chootdev.csnackbar.Snackbar.with(Wholeseller_detail_Fragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


        }
        C_Order= (Button) findViewById(R.id.button8);
        Order_H= (Button) findViewById(R.id.button9);

        C_Order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                C_Order.setTextColor(Color.parseColor("#d62041"));
                Order_H.setTextColor(Color.parseColor("#A9A9A9"));
                C_Order.setBackgroundResource(R.drawable.divider_tab);
                Order_H.setBackgroundResource(R.color.md_divider_white);
                type_id="1";
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                    new DownloadData1().execute();
                } else {
                    com.nispok.snackbar.Snackbar.with(Wholeseller_detail_Fragment.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Wholeseller_detail_Fragment.this);
            /*com.chootdev.csnackbar.Snackbar.with(Wholeseller_detail_Fragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


                }
            }
        });

        Order_H.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                C_Order.setTextColor(Color.parseColor("#A9A9A9"));
                Order_H.setTextColor(Color.parseColor("#d62041"));
                Order_H.setBackgroundResource(R.drawable.divider_tab);
                C_Order.setBackgroundResource(R.color.md_divider_white);
              type_id="2";
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    //Toast.makeText(getActivity(),
                    //	"You have Internet Connection", Toast.LENGTH_LONG)

                    new DownloadData1().execute();
                } else {
                    com.nispok.snackbar.Snackbar.with(Wholeseller_detail_Fragment.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Wholeseller_detail_Fragment.this);
            /*com.chootdev.csnackbar.Snackbar.with(Wholeseller_detail_Fragment.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/


                }

            }
        });



    }

    private void goBack() {
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {



                HttpClient httpclient = new DefaultHttpClient();
                String l=getResources().getConfiguration().locale.getDisplayLanguage();
                System.out.println("<<<<<<lang>>>>>"+l);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>"+SERVER+"json/butcher/wholeseller-product_cat.php?id="+ids+"&type_id="+type_id);
                if (l.equalsIgnoreCase("English")) {
                    Wurl=SERVER+"json/butcher/wholeseller-product_cat.php?id="+ids+"&type_id="+type_id;
                }
                else{
                    Wurl=SERVER+"json/butcher/ar/wholeseller-product_cat.php?id="+ids+"&type_id="+type_id;
                }


                HttpPost httppost = new HttpPost(Wurl
                );
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            progress_loader.setVisibility(ProgressBar.GONE);
         //   String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
         //   System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
    /*        if
                    (result.equalsIgnoreCase("")) {
                new SweetAlertDialog(Wholeseller_detail_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*//*;
                                sDialog.dismiss();
                            }
                        })
                        .show();
             *//*   com.nispok.snackbar.Snackbar.with(Wholeseller_detail_Fragment.this) // context
                        .text("No Products") // text to display
                        .show(Wholeseller_detail_Fragment.this);*//*
              *//*  com.chootdev.csnackbar.Snackbar.with(Wholeseller_detail_Fragment.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("No Products!")
                        .duration(Duration.SHORT)
                        .show();
*//*
               // Toast.makeText(getApplicationContext(), "No Events", Toast.LENGTH_SHORT).show();
            } else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                zwm = new ArrayList<Wholeseller_detail_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        wm = new Wholeseller_detail_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        cat_id = mJsonObject.getString("cat_id");
                        main_cat_id = mJsonObject.getString("main_cat_id");
                        category_name = mJsonObject.getString("category_name");
                        added_by = mJsonObject.getString("added_by");
                        category_desc = mJsonObject.getString("category_desc");
                        cat_image = mJsonObject.getString("cat_image");
                        //  event_id = mJsonObject.getString("event_id");

                        //  String o = mJsonObject.getString("specialty_other");
                        wm.setCat_id(cat_id);
                        wm.setMain_cat_id(main_cat_id);
                        wm.setCategory_name(category_name);
                        wm.setCategory_desc(category_desc);
                        wm.setCat_image(cat_image);
                        wm.setAdded_by(added_by);
                        //  wm.s

                        System.out.println("<<<galry_img>>>>" + galry_image);

                        //   System.out.println("<<<oo>>>>" + o);
                        //   onComplete();

                        zwm.add(wm);
                        System.out.println("<<<oo>>>>" + wm);

                        adapter1 = new Wholeseller_detail_adapter(zwm, getApplicationContext());
                        recyclerview.setAdapter(adapter1);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                new SweetAlertDialog(Wholeseller_detail_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(getResources().getString(R.string.Sorry))
                        .setContentText(getResources().getString(R.string.NoProducts))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setCustomImage(R.drawable.logo)

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                               /* Intent i=new Intent(getApplicationContext(),MainFragment.class);


                                startActivity(i);
                                finish()*/;
                                sDialog.dismiss();
                            }
                        })
                        .show();
               /* com.nispok.snackbar.Snackbar.with(Wholeseller_detail_Fragment.this) // context
                        .text("No Products") // text to display
                        .show(Wholeseller_detail_Fragment.this);*/
       /*         com.chootdev.csnackbar.Snackbar.with(Wholeseller_detail_Fragment.this,null)
                        .type(com.chootdev.csnackbar.Type.SUCCESS)
                        .message("No Products!")
                        .duration(Duration.SHORT)
                        .show();*/
                System.out.println("nulllllllllllllllllllllllllllllllll");
            }
                recyclerview.addOnItemTouchListener(
                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i = new Intent(getApplicationContext(),Butcher_product_Fragment.class);
                                i.putExtra("id",zwm.get(position).getCat_id());
                                i.putExtra("add",zwm.get(position).getAdded_by());
                                i.putExtra("shopname",shopname);
                                startActivity(i);
                                finish();

                            }
                        })
                );
            }


        }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed1();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onBackPressed1() {
      /*  Intent i = new Intent(getApplicationContext(), MainFragment.class);
        // i.putExtra("cat_id", cat_id);
        //i.putExtra("value",status);
        startActivity(i);*/
        Intent i=new Intent(getApplicationContext(),MainFragment.class);


        startActivity(i);
        // finish();
        finish();
    }
    }
