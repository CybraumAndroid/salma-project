package com.salmabutchershop.salma.meridian.app.butcher.mymenu.mainproduct;

/**
 * Created by libin on 12/5/2016.
 */
public class Product_Services_Model {
  String  id,category_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
