package com.salmabutchershop.salma.meridian.app.customer.login.fb;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the

 * to handle interaction events.
 * Use the {@link FaceBukFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FaceBukFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    String user_id;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
LoginButton loginButton;
    ProfileTracker profileTracker;
    CallbackManager callbackManager;
    String ss,dg,email;

    public FaceBukFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FaceBukFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FaceBukFragment newInstance(String param1, String param2) {
        FaceBukFragment fragment = new FaceBukFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        loginButton.setVisibility(View.INVISIBLE);
                   /*     Toast.makeText(getActivity(), ""+loginResult.toString(),
                                Toast.LENGTH_LONG).show();*/
                        System.out.println("json responseee"+loginResult.toString());

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {



                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("Main", response.toString());
                                        //displayMessage(profile);
                                        setProfileToView(object);

                                        try {
                                          ss=object.getString("id");
                                            dg=object.getString("name");
                                            email=object.getString("email");
                                           // String pic=object.getString()
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {


                                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<facebookid>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ss);
                                        SharedPreferences preferences = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();

                                        editor.putString("user_id", ss);
                                        editor.putString("c_name",dg);
                                        editor.putString("c_email",email);
                                        editor.commit();


                                            SharedPreferences preferences2 = getActivity().getSharedPreferences("SocialPref", MODE_PRIVATE);
                                            SharedPreferences.Editor editor2 = preferences2.edit();
                                            editor2.putString("sociallogin","true");
                                            editor2.putString("google", "false");
                                            editor2.putString("facebook","true");

                                            editor2.commit();



                                        Intent inn= new Intent(getApplicationContext(),Customer_Services_Fragment.class);
                                        startActivity(inn);
                                        }catch (NullPointerException e){

                                        }

                                    }

                                    private void setProfileToView(JSONObject jsonObject) {
                                        try {
                                            String  email=jsonObject.getString("email");
                                            String name=jsonObject.getString("name");
                                            String id=jsonObject.getString("id");
                                            String gender=jsonObject.getString("gender");



                                            System.out.println("social_user_id : "+id);
                                            System.out.println("social_email : "+email);
                                            System.out.println("social_gender : "+gender);
                                            System.out.println("social_name : "+name);


                                          /*  String birthday=jsonObject.getString("user_birthday");*/
System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<facebookid>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+id);
                                            //   String photo= jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");

                                            //  textView.setText(jsonObject.getString("email"));
                                            System.out.println("Email"+jsonObject.getString("email"));
                                            SharedPreferences preferences = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
                                            SharedPreferences.Editor editor = preferences.edit();

                                            editor.putString("user_id", id);
                                            editor.putString("social_name",name);
                                            editor.putString("social_email",email);
                                            editor.putString("social_gender",gender);

                                            try {

                                                 URL imageURL = new URL("https://graph.facebook.com/"+id+"/picture?type=large");
                                                editor.putString("social_profile_image",imageURL.toString());
                                            }catch (Exception e){e.printStackTrace();}

                                           /* editor.putString("social_birthday",birthday);*/


                                            editor.commit();
                                          /*  SharedPreferences.Editor editor1 = getActivity().getSharedPreferences("myfb", getActivity().MODE_PRIVATE).edit();
                                            editor.putString("FBemails", email);
                                            editor.putString("Fbnames", name);
                                            editor.putString("user_id", name);
                                            editor1.commit();*/


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields","id,name,email,gender");
                        request.setParameters(parameters);
                        request.executeAsync();


                }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                // App code



            }
        };



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view=inflater.inflate(R.layout.fac_layout, container, false);
        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        // If using in a fragment
        loginButton.setFragment(this);

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event





    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        profileTracker.stopTracking();
    }

}
