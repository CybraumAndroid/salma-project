package com.salmabutchershop.salma.meridian.app.customer;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.messaging.FirebaseMessaging;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.SelectionPage;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.newintro.GPSTracker1;
import com.salmabutchershop.salma.meridian.app.customer.newintro.New_WelcomeActivity;
import com.salmabutchershop.salma.meridian.app.customer.newintro.PrefManager;
import com.salmabutchershop.salma.meridian.app.firebase.app.Config;
import com.salmabutchershop.salma.meridian.app.firebase.util.NotificationUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;




/**
 * Created by libin on 10/18/2016.
 */

public class Splash_Activity extends Activity implements LocationListener {
    int status=0;

    public Splash_Activity() {
    }
    TextView tv;
    private static final String TAG =Splash_Activity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private PrefManager prefManager;
    LocationManager locationManager;
    Handler handler;
    Location location;

    double latitude;
    double longitude;
    GPSTracker1 gps;
    TextView lat;
    TextView lon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.splsh_layout);
        System.out.println("<<<<<<<<<<<<<<<< first time>>>>>>>>>>>>>>>>>>>>>>>>");
//        prefManager.setFirstTimeLaunch(false);
        tv=(TextView) findViewById(R.id.textView22);
        handler = new Handler();
        gps = new GPSTracker1(Splash_Activity.this);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            if (ContextCompat.checkSelfPermission(Splash_Activity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Splash_Activity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Splash_Activity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                    // \n is for new line
                    //   Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
            else {
                gps = new GPSTracker1(Splash_Activity.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("longitude", String.valueOf(longitude));
                    editor.putString("latitude", String.valueOf(latitude));
                    editor.commit();
                    // \n is for new line
                    //   Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }

        }
        mRegistrationBroadcastReceiver =
                new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    tv.setText(message);
                }
            }
        };
        displayFirebaseRegId();


     /*   try {
            PackageInfo info1 = getPackageManager().getPackageInfo(
                    "com.salmabutchershop.salma.meridian.app",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info1.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("YourKeyHash :", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("YourKeyHash: "+ Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/
      //  String info="Salma "+getVersionInfo();
      //  System.out.println("+++++++++++++++++++++++++++++++"+info);


     //   tv.setText(info);
        Thread timer=new Thread(){
            public void run(){

                try{
                    sleep(3000);
                }
                catch(InterruptedException ex)
                {
                    ex.printStackTrace();
                }
                finally{

                    Intent intent = new Intent(Splash_Activity.this, New_WelcomeActivity.class);
                    startActivity(intent);
                    finish();





                }
            }

        };
        timer.start();

    }


    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId))
            tv.setText("Firebase Reg Id: " + regId);
        else
            tv.setText("Firebase Reg Id is not received yet!");
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
