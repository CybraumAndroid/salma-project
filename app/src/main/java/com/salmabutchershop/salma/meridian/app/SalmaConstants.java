package com.salmabutchershop.salma.meridian.app;

/**
 * Created by libin on 5/16/2017.
 */

public class SalmaConstants {
    public SalmaConstants() {
        // TODO Auto-generated constructor stub
    }
    public static String SERVER = "http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/";
    public static String SERVER_CUSTOMER = SERVER +"json/customer/";
    public static String SERVER_CUSTOMER_AR = SERVER_CUSTOMER+"ar/";

    public static String SERVER_BUTCHER = SERVER +"json/butcher/";
    public static String SERVER_BUTCHER_AR = SERVER_BUTCHER+"ar/";


    public static String SERVER_RETAILER = SERVER +"json/retailer/";
    public static String SERVER_RETAILER_AR = SERVER_RETAILER+"ar/";
}
