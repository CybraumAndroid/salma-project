package com.salmabutchershop.salma.meridian.app.customer.newintro;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.salmabutchershop.salma.meridian.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;


import java.util.ArrayList;

/**
 * Created by Rashid on 10/18/2016.
 */
public class New_ViewPagerAdapter extends PagerAdapter {
     Context context;
     ArrayList<ZayedModel> imageURL;

    public New_ViewPagerAdapter(Context ReceivedContext, ArrayList<ZayedModel> ReceivedImageURL)
    {
        this.context=ReceivedContext;
        this.imageURL=ReceivedImageURL;
    }

    @Override
    public int getCount() {
        return imageURL.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==((RelativeLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ImageView imgView;
        final ProgressBar gallery_progressbar;
       final  MKLoader progress_loader;
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView=inflater.inflate(R.layout.new_viewpager_item,container,false);

        imgView=(ImageView)itemView.findViewById(R.id.imageView3);
        gallery_progressbar=(ProgressBar)itemView.findViewById(R.id.gallery_progressbar);
        progress_loader=(MKLoader)itemView.findViewById(R.id.progress_loader);
        System.out.println("#################################################################################");
        System.out.println("URI in PAGE ADAPTER : "+ Uri.parse(imageURL.get(position).getUrl()));
        System.out.println("#################################################################################");

     /*   Picasso.with(context)
                .load(imageURL.get(position).getUrl())
                .fit()
                .into(imgView);*/

       /* new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                .build()
                .load(imageURL.get(position).getUrl())
                .noFade()
                .fit()
                .into(imgView);*/
       try {


        new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                .build()
                .load(imageURL.get(position).getUrl())
                .noFade()
                .into(imgView, new Callback() {
                    @Override
                    public void onSuccess() {
                       progress_loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
       }catch (Exception e){
           e.printStackTrace();
       }
        ((ViewPager)container).addView(itemView);
        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager)container).removeView((RelativeLayout)object);
    }

}
