package com.salmabutchershop.salma.meridian.app.butcher.orderdetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_MyOrder_Model;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_My_order_List_adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by libin on 12/29/2016.
 */
public class Butcher_My_order_Details_adapter extends RecyclerView.Adapter<Butcher_My_order_Details_adapter.ViewHolder> {


    List<Butcher_MyOrder_List_Model> bmo;
    Context context;



    public Butcher_My_order_Details_adapter(ArrayList<Butcher_MyOrder_List_Model> bmo, Context context) {
        this.bmo = bmo;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cv1;

        ImageView v;
        TextView tv,Name,Place,Estmtd_dlvry,Date,Name2;
        Button Nw;

        ImageView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);

            Name=   (TextView) itemView.findViewById(R.id.textView48);
            Name2=   (TextView) itemView.findViewById(R.id.textView49);
            Place=   (TextView) itemView.findViewById(R.id.textView50);
            Estmtd_dlvry=   (TextView) itemView.findViewById(R.id.textView51);
           /* Date=   (TextView) itemView.findViewById(R.id.date);
            Nw= (Button) itemView.findViewById(R.id.imageView16);*/




        }
    }


    @Override
    public int getItemCount() {
        return bmo.size();
    }


    @Override
    public Butcher_My_order_Details_adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.my_order_details_item_layout, viewGroup, false);
        Butcher_My_order_Details_adapter.ViewHolder pvh = new Butcher_My_order_Details_adapter.ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final Butcher_My_order_Details_adapter.ViewHolder personViewHolder, final int i) {

        Typeface myFont7 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Name2.setTypeface(myFont7);
        personViewHolder.Name2.setText(bmo.get(i).getPro_type_name());
        Typeface myFont2 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Name.setTypeface(myFont2);
        personViewHolder.Name.setText(bmo.get(i).getPro_name());
        Typeface myFont3 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Place.setTypeface(myFont3);
        personViewHolder.Place.setText(bmo.get(i).getPro_qty());
        Typeface myFont4 = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        personViewHolder.Estmtd_dlvry.setTypeface(myFont4);
        personViewHolder.Estmtd_dlvry.setText(bmo.get(i).getPro_price()+"/"+bmo.get(i).getPrice_quantity());
       /* Typeface myFont5 = Typeface.createFromAsset(context.getAssets(), "OpenSansLight.ttf");
        personViewHolder.Date.setTypeface(myFont5);
        personViewHolder.Date.setText(bmo.get(i).getOrder_date());
        if(bmo.get(i).getOrder_status().equalsIgnoreCase("New")){
            personViewHolder.Nw.setBackgroundResource(R.drawable.neworder);
            personViewHolder.Nw.setText("New Order");

        }
        else {
            personViewHolder.Nw.setBackgroundResource(R.drawable.accepted);
            personViewHolder.Nw.setText("Accepted");
        }*/
        //  String img=enm.get(i).getNews_img();


    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}