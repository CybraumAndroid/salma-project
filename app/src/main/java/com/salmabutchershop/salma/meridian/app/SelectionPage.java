package com.salmabutchershop.salma.meridian.app;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherLogin;
import com.salmabutchershop.salma.meridian.app.butcher.login.ButcherRegistration;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.customer.address.AddressFragment;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Contact_Our_Team;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Model;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_adapter;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.customer.location.Location_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.login.LoginActivity;
import com.salmabutchershop.salma.meridian.app.retailer.RetailerMainPage;
import com.salmabutchershop.salma.meridian.app.retailer.fresh_frozen.Fresh_Frozen_Retailer;
import com.salmabutchershop.salma.meridian.app.retailer.login.LoginActivityRetailer;
import com.salmabutchershop.salma.meridian.app.retailer.login.PostRegistrationRetailer;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by libin on 11/24/2016.
 */

public class SelectionPage extends Activity implements View.OnClickListener {
    Button Customer, Retailer, Butcher;
    private PopupWindow mPopupWindow, mPopupWindowCall;
    LinearLayout update_yes, update_no, call_yes, call_no;
    View customView, customViewCall;
    String insatalled_app_version = "", new_app_version = "", result, latest_version;
    RelativeLayout top_layout;
    ProgressBar progress;
    String strVersion = "";
    String fl_b,fl_c,fl_r,enable_popup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectionpage_layout_layout);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        Customer = (Button) findViewById(R.id.customer);
        Retailer = (Button) findViewById(R.id.retailer);
        Butcher = (Button) findViewById(R.id.butcher);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        fl_b = myPrefs.getString("user_idb", null);
        fl_c=myPrefs.getString("user_id",null);
        fl_r=myPrefs.getString("user_idL",null);

        if (fl_r != null && !fl_r.isEmpty()){
            //Toast.makeText(getApplicationContext(), Reg_id, Toast.LENGTH_SHORT).show();


            Intent synin = new Intent(getApplicationContext(), Fresh_Frozen_Retailer.class);
            // synin.putExtra("brn_id",u);
            startActivity(synin);
            finish();
        }
       else if (fl_c != null && !fl_c.isEmpty()){


            Intent synin = new Intent(getApplicationContext(), Customer_Services_Fragment.class);
            // synin.putExtra("brn_id",u);
            startActivity(synin);
            finish();
        }
        else if (fl_b != null && !fl_b.isEmpty()){


            Intent synin = new Intent(getApplicationContext(), MainFragment.class);
            // synin.putExtra("brn_id",u);
            startActivity(synin);
            finish();
        }
        top_layout = (RelativeLayout) findViewById(R.id.top_layout);
        getCurrentVersionInfo();
        new DownloadData1().execute();
        Customer.setOnClickListener(this);
        Retailer.setOnClickListener(this);
        Butcher.setOnClickListener(this);
    }
       /* LayoutInflater inflater2 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        customView = inflater2.inflate(R.layout.popup_update, null);

        update_yes = (LinearLayout) customView.findViewById(R.id.update_yes);
        update_no = (LinearLayout) customView.findViewById(R.id.update_no);
        update_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.zulekhahospitals.zulekhaapp"));
                startActivity(i);
                mPopupWindow.dismiss();
            }
        });


        update_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopupWindow.dismiss();
            }
        });
    }*/

    private String getCurrentVersionInfo() {


        PackageInfo packageInfo;
        try {
            packageInfo = getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(
                            getApplicationContext().getPackageName(),
                            0
                    );
            strVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        return strVersion;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.customer:
              /*  Customer.setBackgroundResource(R.drawable.selection);
               Retailer.setBackgroundResource(R.drawable.common);
                Butcher.setBackgroundResource(R.drawable.common);*/
                // Retailer.setBackgroundResource(R.drawable.common);
                try {
                  /*  SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("flag_id", flag);
                    editor.commit();


                    Locale locale2 = new Locale("en");
                    Locale.setDefault(locale2);
                    Configuration config2 = new Configuration();
                    config2.locale = locale2;
                    getBaseContext().getResources().updateConfiguration(config2, null);
                    Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
                    startActivity(i1);
                    finish();*/
                    new SweetAlertDialog(SelectionPage.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Choose Your Language")
                            // .setContentText("Won't be able to recover this file!")

                            .setCustomImage(R.drawable.logo)
                            .setCancelText("Arabic")
                            .setConfirmText("English")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                            /*        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();*/
                                    Locale locale2 = new Locale("ar");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);
                                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                    i.putExtra("name", "Customer");
                                    startActivity(i);
                                    finish();
                                    sDialog.dismiss();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                          /*          SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();*/


                                    Locale locale2 = new Locale("en");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);
                                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                    i.putExtra("name", "Customer");
                                    startActivity(i);
                                    finish();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();


                }
                catch (Exception e){
                    e.printStackTrace();
                }


                break;
            case R.id.retailer:
                try {
                  /*  SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("flag_id", flag);
                    editor.commit();


                    Locale locale2 = new Locale("en");
                    Locale.setDefault(locale2);
                    Configuration config2 = new Configuration();
                    config2.locale = locale2;
                    getBaseContext().getResources().updateConfiguration(config2, null);
                    Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
                    startActivity(i1);
                    finish();*/
                    new SweetAlertDialog(SelectionPage.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Choose Your Language")
                            // .setContentText("Won't be able to recover this file!")

                            .setCustomImage(R.drawable.logo)
                            .setCancelText("Arabic")
                            .setConfirmText("English")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                              /*      SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();*/
                                    Locale locale2 = new Locale("ar");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);
                                    Intent i1 = new Intent(getApplicationContext(), RetailerMainPage.class);
                                    i1.putExtra("name", "Retailer");
                                    startActivity(i1);
                                    finish();
                                    sDialog.dismiss();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                   /* SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();
*/

                                    Locale locale2 = new Locale("en");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);
                                    Intent i1 = new Intent(getApplicationContext(), RetailerMainPage.class);
                                    i1.putExtra("name", "Retailer");
                                    startActivity(i1);
                                    finish();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();


                }
                catch (Exception e){
                    e.printStackTrace();
                }


            /*    Customer.setBackgroundResource(R.drawable.common);
                Retailer.setBackgroundResource(R.drawable.selection);
                Butcher.setBackgroundResource(R.drawable.common);*/

                break;
            case R.id.butcher:

                try {
                  /*  SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("flag_id", flag);
                    editor.commit();


                    Locale locale2 = new Locale("en");
                    Locale.setDefault(locale2);
                    Configuration config2 = new Configuration();
                    config2.locale = locale2;
                    getBaseContext().getResources().updateConfiguration(config2, null);
                    Intent i1 = new Intent(getApplicationContext(), SelectionPage.class);
                    startActivity(i1);
                    finish();*/
                    new SweetAlertDialog(SelectionPage.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Choose Your Language")
                            // .setContentText("Won't be able to recover this file!")

                            .setCustomImage(R.drawable.logo)
                            .setCancelText("Arabic")
                            .setConfirmText("English")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                                   /* SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();*/
                                    Locale locale2 = new Locale("ar");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);
                                    Intent i2 = new Intent(getApplicationContext(), ButcherRegistration.class);
                                    i2.putExtra("name", "Butcher");
                                    startActivity(i2);
                                    finish();
                                    sDialog.dismiss();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                             /*       SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("flag_id", flag);
                                    editor.commit();*/


                                    Locale locale2 = new Locale("en");
                                    Locale.setDefault(locale2);
                                    Configuration config2 = new Configuration();
                                    config2.locale = locale2;
                                    getBaseContext().getResources().updateConfiguration(config2, null);
                                    Intent i2 = new Intent(getApplicationContext(), ButcherRegistration.class);
                                    i2.putExtra("name", "Butcher");
                                    startActivity(i2);
                                    finish();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();


                }
                catch (Exception e){
                    e.printStackTrace();
                }
              /*  Customer.setBackgroundResource(R.drawable.common);
                Retailer.setBackgroundResource(R.drawable.common);
                Butcher.setBackgroundResource(R.drawable.selection);*/

                break;
        }

    }


    // }
    public void displayPopup() {
        if(!((Activity) SelectionPage.this).isFinishing())
        {
        new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("New Update is Available")
                .setContentText("Update Now ?")
                .setConfirmText("Yes")
                .setCancelText("No")
                .setCustomImage(R.drawable.logo)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salmabutchershop.salma.meridian.app"));
                        startActivity(i);


                        sDialog.dismiss();

                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
      /*  final Dialog dialog = new Dialog(SelectionPage.this);

        //  dialog.setContentView(R.layout.loc_popup);
        final Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setContentView(R.layout.popup_update);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        LinearLayout btnDismiss = (LinearLayout) dialog.findViewById(R.id.update_yes);
        LinearLayout btnDismisse = (LinearLayout) dialog.findViewById(R.id.update_no);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.zulekhahospitals.zulekhaapp"));
                startActivity(i);

                dialog.dismiss();
            }
        });
        btnDismisse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
                    *//*Button rb = (Button) dialog.findViewById(R.id.rb);
                    rb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });hr@sherji.com
*/


            //show dialog
        }
       //               dialog.show();

    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;
//

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {


                HttpClient httpclient = new DefaultHttpClient();


                HttpPost httppost = new HttpPost("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/common/latestversion.php");
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {
            // btnSignIn.setEnabled(false);
            // edt.setEnabled(false);
            // pdt.setEnabled(false);
            //   pd.dismiss();
            progress.setVisibility(ProgressBar.GONE);
            // String sam = result.trim();
            System.out.println(">>>>>>>>>>>>>>>" + result);
            // System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
            if (result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                //  zwm = new ArrayList<Customer_Butcher_List_Model>();
                try {

                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        //  wm = new Customer_Butcher_List_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));


                        latest_version = mJsonObject.getString("latest_version");
                        enable_popup= mJsonObject.getString("enable_popup");
                     if (latest_version.equalsIgnoreCase(strVersion)){

                        }
                        else{
                         if (enable_popup.equalsIgnoreCase("no")){

                         }else {
                             displayPopup();
                         }

                     }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {


            }
        }

    }
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

            intent.putExtra("EXIT", true);
            startActivity(intent);
// dialog.dismiss();
            finishAffinity(); ;
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        com.nispok.snackbar.Snackbar.with(SelectionPage.this) // context
                .text(R.string.Press_again_to_exit_from_salma) // text to display
                .show(SelectionPage.this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }

}