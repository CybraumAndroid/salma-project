package com.salmabutchershop.salma.meridian.app.butcher.extras;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;

import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Profile_View_Activity extends AppCompatActivity {
    String Reg_id,result,logo,country,city,address,fullname,email,phone,shop_name;
    ImageView imag_logo;
    TextView txt_name,txt_city,txt_email,txt_address,txt_shopname,txt_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile__view_);
        txt_name= (TextView) findViewById(R.id.txt_view_butcher_name);
        txt_phone= (TextView) findViewById(R.id.txt_view_butcher_phone);
        txt_email= (TextView) findViewById(R.id.txt_view_butcher_email);
        txt_address= (TextView) findViewById(R.id.txt_view_butcher_address);
        txt_shopname= (TextView) findViewById(R.id.txt_view_butcher_shopname);
        txt_city= (TextView) findViewById(R.id.txt_view_butcher_city);
        imag_logo= (ImageView) findViewById(R.id.imag_butcher_logo);

        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        fullname = myPrefs.getString("namb", null);
        shop_name= myPrefs.getString("shp_nmb", null);
        address = myPrefs.getString("adsb", null);
        phone= myPrefs.getString("phneb", null);
        email= myPrefs.getString("emlb", null);
        city= myPrefs.getString("cityb", null);
        logo= myPrefs.getString("logob", null);
        Reg_id= myPrefs.getString("user_idb", null);
        txt_name.setText(fullname);
        txt_shopname.setText(shop_name);
        txt_address.setText(address);
        txt_phone.setText(phone);
        txt_email.setText(email);
        txt_city.setText(city);
        Picasso.with(getApplicationContext()).load(logo).into(imag_logo);


        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Profile_View_Activity.this.goBack();
            }
        });


        //new DownloadData().execute();
    }
//    public class DownloadData extends AsyncTask<Void, Void, Void> {
//
//        ProgressDialog pd = null;
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            //    progress_loader.setVisibility(ProgressBar.VISIBLE);
//
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
////http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User
//
//            try {
//
//
//                HttpClient httpclient = new DefaultHttpClient();
//
//
//                HttpPost httppost = new HttpPost("http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/view_profile.php?user_id="+Reg_id
//                );
//                HttpResponse response = httpclient.execute(httppost);
//                HttpEntity entity = response.getEntity();
//                InputStream is = entity.getContent();
//                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
//                StringBuilder sb = new StringBuilder();
//                String line = null;
//                while ((line = reader.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                is.close();
//                result = sb.toString();
//
//
//                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
//            } catch (Exception e) {
//                Log.e("Loading connection  :", e.toString());
//            }
//
//
//            return null;
//        }
//
//        protected void onPostExecute(Void args) {
//
//
//            System.out.println(">>>>>>>>>>>>>>>" + result);
//            //  System.out.println(">>>>>>>>>>>>>>>" + sam);
//            String value = result;
//         /*   if
//                    (result.equalsIgnoreCase("")) {
//                Toast.makeText(getApplicationContext(), "No Items", Toast.LENGTH_SHORT).show();
//            }  else*/
//            if (result != null && !result.isEmpty() && !result.equals("null")) {
//                JSONArray mArray;
//                //  mCountryModel1 = new ArrayList<>();
//            //    eem = new ArrayList<MyAccount_Profile_Model>();
//                String  logos = null;
//                try {
//                    mArray = new JSONArray(result);
//                    for (int i = 0; i < mArray.length(); i++) {
//
//
//
//                        JSONObject mJsonObject = mArray.getJSONObject(i);
//
//                 logo = mJsonObject.getString("logo");
//                        System.out.println("logoooooooo"+logo);
//                        Picasso.with(getApplicationContext()).load(logo).into(Pfile);
//
//
//                        SharedPreferences myPrefs = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = myPrefs.edit();
//                        editor.putString("logob",logo);
//                        editor.commit();
//
//
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    new Picasso.Builder(Profile_View_Retailer.this)
//                            .downloader(new OkHttpDownloader(Profile_View_Retailer.this, Integer.MAX_VALUE))
//                            .build()
//                            .load(logo)
//                            .noFade()
//                            .into(Pfile, new Callback() {
//                                @Override
//                                public void onSuccess() {
//                                   // progress_loader1.setVisibility(View.GONE);
//                                }
//
//                                @Override
//                                public void onError() {
//                                    // personViewHolder.progress_loader.setVisibility(View.GONE);
//                                }
//                            });
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//            } else {
//            /*    new SweetAlertDialog(My_Account_Fragment.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
//                        .setTitleText(getResources().getString(R.string.Sorry))
//                        .setContentText(getResources().getString(R.string.NoContent))
//                        .setConfirmText(getResources().getString(R.string.ok))
//                        .setCustomImage(R.drawable.logo)
//
//                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(SweetAlertDialog sDialog) {
//                               *//* Intent i=new Intent(getApplicationContext(),MainFragment.class);
//
//
//                                startActivity(i);
//                                finish()*//*;
//                                finish();
//                                sDialog.dismiss();
//                            }
//                        })
//                        .show();*/
//                System.out.println("nulllllllllllllllllllllllllllllllll");
//            }
//        }
//    }
private void goBack() {
    Intent i = new Intent(getApplicationContext(),MainFragment.class);

    startActivity(i);
    // finish();
    finish();
}

}
