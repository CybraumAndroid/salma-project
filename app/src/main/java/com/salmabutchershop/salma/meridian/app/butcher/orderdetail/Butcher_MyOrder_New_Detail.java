package com.salmabutchershop.salma.meridian.app.butcher.orderdetail;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.salmabutchershop.salma.meridian.app.R;
import com.salmabutchershop.salma.meridian.app.butcher.mainpage.MainFragment;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.Butcher_mymenu_product;
import com.salmabutchershop.salma.meridian.app.butcher.mymenu.submit.Butcher_Product_Submit;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_My_order_List_adapter;
import com.salmabutchershop.salma.meridian.app.butcher.order.Butcher_my_order;
import com.salmabutchershop.salma.meridian.app.customer.DetectConnection;
import com.salmabutchershop.salma.meridian.app.customer.address.AddressFragment;
import com.salmabutchershop.salma.meridian.app.customer.custom_er.Customer_Services_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list.Customer_Butcher_List_Fragment;
import com.salmabutchershop.salma.meridian.app.customer.database.DatabaseHelper;
import com.salmabutchershop.salma.meridian.app.retailer.retailer_wholeseller.Retailer_Wholeseller_Fragment;
import com.tuyenmonkey.mkloader.MKLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.salmabutchershop.salma.meridian.app.SalmaConstants.SERVER;

/**
 * Created by libin on 12/29/2016.
 */

public class Butcher_MyOrder_New_Detail extends Activity {
    String result;
    ProgressBar progress;
    static ArrayList<Butcher_MyOrder_List_Model> bmo;
    Butcher_MyOrder_List_Model bm;
    String k,l,o;
    String user_id,order_item_id, order_id, pro_name, pro_type_name, pro_qty, pro_price, fname,instruction,
            lname, city, deliveryarea, addresstype, street, building, floor, house_no, mobile, phone,
            aditional_direction, service_fee, container_charges, delivery_charges, subtotal, total, order_date, order_status,onsite_delivery,price_quantity;
    TextView Name, Ads, Item_Name, Qty, Delivry_date, Location,Phone,Email,Instruction,type,Order_no;
    Button accepted;
    String Buserid,fullname,email;
    RecyclerView recyclerview;
    Butcher_My_order_Details_adapter adapter1;
    String stat;
   ImageView Lcc;
    String result1,lng,lat;
    String Wurl,currency;
    public static MKLoader progress_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_order_details);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress_loader=(MKLoader)findViewById(R.id.progress_loader);
        Lcc= (ImageView) findViewById(R.id.loc_map);
        SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Reg_id = myPrefs.getString("username", null);
        // photo=myPrefs.getString("photo", null);
        Buserid=myPrefs.getString("user_idb",null);
        fullname = myPrefs.getString("fullnameb", null);
        System.out.println(">>>>>>daaaaaaaaaaaaaaaaaaaaHHHHHHHHHHHHHHHHHHaaaaaaaaaaaaaaaaaaaa>>>>>>>>>"+Buserid);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager1);
        recyclerview.setAdapter(adapter1);
Order_no= (TextView) findViewById(R.id.order_no);
        Typeface myFontf = Typeface.createFromAsset(getApplicationContext().getAssets(), "open-sans.bold.ttf");
       Order_no.setTypeface(myFontf);

       Name = (TextView) findViewById(R.id.textView2);
        Typeface myFont3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Name.setTypeface(myFont3);

        Ads = (TextView) findViewById(R.id.dlvry_ads);
        Typeface myFont4 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Ads.setTypeface(myFont4);
        /*Qty = (TextView) findViewById(R.id.qty);
        Typeface myFont5 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Qty.setTypeface(myFont5);*/
        Delivry_date = (TextView) findViewById(R.id.dlvry_date);
        Typeface myFont6 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Delivry_date.setTypeface(myFont6);
       /* Location = (TextView) findViewById(R.id.lctn);
        Typeface myFont7 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
       Location.setTypeface(myFont7);*/
       /* Item_Name = (TextView) findViewById(R.id.item_name);
        Typeface myFont8 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        Item_Name.setTypeface(myFont8);
*/

        Phone = (TextView) findViewById(R.id.textView34);
        Typeface myFont1= Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Phone.setTypeface(myFont1);

        Email = (TextView) findViewById(R.id.dlvry_datellq);
        Typeface myFont2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Email.setTypeface(myFont2);

        Instruction = (TextView) findViewById(R.id.instruction);
        Typeface myFont11 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        Instruction.setTypeface(myFont11);

        /*type = (TextView) findViewById(R.id.pro_type_name);
        Typeface myFont12 = Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSansLight.ttf");
        type.setTypeface(myFont12);*/
        accepted = (Button) findViewById(R.id.accepted);
        Typeface myFont9 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
        accepted.setTypeface(myFont9);
        accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DetectConnection
                        .checkInternetConnection(getApplicationContext())) {
                    new SweetAlertDialog(Butcher_MyOrder_New_Detail.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText(getResources().getString(R.string.Areyousure))
                            .setContentText(getResources().getString(R.string.AcceptthisOrder))
                            .setConfirmText(getResources().getString(R.string.Yes))
                            .setCustomImage(R.drawable.logo)
                            .setCancelText(getResources().getString(R.string.No))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    new SendPostRequest3().execute();
                                    sDialog.dismiss();
                                }
                            })
                            .show();


                } else {
                    com.nispok.snackbar.Snackbar.with(Butcher_MyOrder_New_Detail.this) // context
                            .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                            .show(Butcher_MyOrder_New_Detail.this);
                 /*   com.chootdev.csnackbar.Snackbar.with(Butcher_MyOrder_New_Detail.this,null)
                            .type(com.chootdev.csnackbar.Type.SUCCESS)
                            .message("Oops Your Connection Seems Off..!!")
                            .duration(Duration.SHORT)
                            .show();*/

                }


            }
        });
      //  SharedPreferences myPrefs = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);


        ImageView back = (ImageView) findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(getApplicationContext(),Butcher_my_order.class);


                startActivity(i);
                finish();
            }
        });
        k = getIntent().getStringExtra("add_id");
        l= getIntent().getStringExtra("pr_id");
        o=getIntent().getStringExtra("order_id");
        stat=getIntent().getStringExtra("stat");
        if (stat.equalsIgnoreCase("1")){
            accepted.setVisibility(View.VISIBLE);
        }
        else if(stat.equalsIgnoreCase("2")){
           accepted.setVisibility(View.GONE);
        }
        else {

        }
        Lcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   new DownloadData3().execute();
                Double la,ln;
                la= Double.valueOf(lat);
                ln= Double.valueOf(lng);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr="+la+","+ln));
                startActivity(intent);
            }
        });

        System.out.println("ggggggggggggg"+k);
        System.out.println("hhhhhhhhhhhhhhhhh"+l);
        System.out.println("jjjjjjjjjjjjjjjjjjjj"+o);
        if (DetectConnection
                .checkInternetConnection(getApplicationContext())) {
            new DownloadData1().execute();


        } else {
            com.nispok.snackbar.Snackbar.with(Butcher_MyOrder_New_Detail.this) // context
                    .text(getResources().getString(R.string.oops_your_connection_Seems_off)) // text to display
                    .show(Butcher_MyOrder_New_Detail.this);
            /*com.chootdev.csnackbar.Snackbar.with(Butcher_MyOrder_New_Detail.this,null)
                    .type(com.chootdev.csnackbar.Type.SUCCESS)
                    .message("Oops Your Connection Seems Off..!!")
                    .duration(Duration.SHORT)
                    .show();*/

        }


    }

    private void goBack() {
        super.onBackPressed();
    }

    private class DownloadData1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);
            String lk=getResources().getConfiguration().locale.getDisplayLanguage();
            System.out.println("<<<<<<langlibin>>>>>"+lk);
            if (lk.equalsIgnoreCase("English")) {
                System.out.println("<<<<<<libin>>>>>"+l);
                System.out.println("<<<<<<libin>>>>>"+k);
                Wurl=SERVER+"json/butcher/order_detail.php?id="+l+"&butcher_id="+Buserid;
            }
            else{
                System.out.println("<<<<<<libin2>>>>>"+l);
                System.out.println("<<<<<<libin2>>>>>"+k);
                Wurl=SERVER+"json/butcher/ar/order_detail.php?id="+l+"&butcher_id="+Buserid;
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

            try {

                HttpClient httpclient = new DefaultHttpClient();

                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"+"http://app.salmacorp.com.php56-11.dfw3-2.websitetestlink.com/json/butcher/order_detail.php?id="+l+"&butcher_id="+Buserid);
                HttpPost httppost = new HttpPost(Wurl);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();


                // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e("Loading connection  :", e.toString());
            }


            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(ProgressBar.GONE);
//            String sam = result.trim();
          //  System.out.println(">>>>>>>>>>>>>>>" + result);
          //  System.out.println(">>>>>>>>>>>>>>>" + sam);
            String value = result;
           /* if
                    (result.equalsIgnoreCase("")) {
                com.nispok.snackbar.Snackbar.with(Butcher_MyOrder_New_Detail.this) // context
                        .text("No products") // text to display
                        .show(Butcher_MyOrder_New_Detail.this);
              //  Toast.makeText(getApplicationContext(), "No products", Toast.LENGTH_SHORT).show();
            }  else*/ if(result != null && !result.isEmpty() && !result.equals("null")) {
                JSONArray mArray;
                bmo = new ArrayList<Butcher_MyOrder_List_Model>();
                try {
                    mArray = new JSONArray(result);
                    for (int i = 0; i < mArray.length(); i++) {
                        bm = new Butcher_MyOrder_List_Model();
                        JSONObject mJsonObject = mArray.getJSONObject(i);
                        // Log.d("OutPut", mJsonObject.getString("image"));

                        user_id = mJsonObject.getString("user_id");
                        order_id = mJsonObject.getString("order_id");
                        order_item_id= mJsonObject.getString("order_item_id");
                        pro_name = mJsonObject.getString("pro_name");
                        pro_type_name = mJsonObject.getString("pro_type_name");
                        pro_qty = mJsonObject.getString("pro_qty");
                        pro_price = mJsonObject.getString("pro_price");
                        instruction= mJsonObject.getString("instruction");
                        fname = mJsonObject.getString("fname");
                        lname = mJsonObject.getString("lname");
                        city = mJsonObject.getString("city");
                        deliveryarea = mJsonObject.getString("deliveryarea");
                        addresstype = mJsonObject.getString("addresstype");
                        street = mJsonObject.getString("street");
                        building = mJsonObject.getString("building");
                        floor = mJsonObject.getString("floor");
                        house_no = mJsonObject.getString("house_no");
                        mobile = mJsonObject.getString("mobile");
                        phone = mJsonObject.getString("phone");
                        email= mJsonObject.getString("email");
                        currency= mJsonObject.getString("currency");
                        aditional_direction = mJsonObject.getString("aditional_direction");
                        service_fee = mJsonObject.getString("service_fee");
                        container_charges = mJsonObject.getString("container_charges");
                        delivery_charges = mJsonObject.getString("delivery_charges");
                        subtotal = mJsonObject.getString("subtotal");
                        total = mJsonObject.getString("total");
                        order_date = mJsonObject.getString("order_date");
                        order_status = mJsonObject.getString("order_status");
                   lng=mJsonObject.getString("latitude");
                           lat=mJsonObject.getString("longitude");
                        onsite_delivery=mJsonObject.getString("onsite_delivery");
                        price_quantity=mJsonObject.getString("price_quantity");
                        bm.setUser_id(user_id);
                        bm.setOrder_item_id(order_item_id);
                        bm.setOrder_id(order_id);
                        bm.setPro_name(pro_name);
                        bm.setPro_type_name(pro_type_name);
                        bm.setPro_qty(pro_qty);
                        bm.setPro_price(pro_price);
                        bm.setFname(fname);
                        bm.setLname(lname);
                        bm.setCity(city);
                        bm.setDeliveryarea(deliveryarea);
                        bm.setAddresstype(addresstype);
                        bm.setStreet(street);
                        bm.setBuilding(building);
                        bm.setFloor(floor);
                        bm.setHouse_no(house_no);
                        bm.setMobile(mobile);
                        bm.setPhone(phone);
                        bm.setEmail(email);
                        bm.setInstruction(instruction);
bm.setPrice_quantity(price_quantity);
                        bm.setAditional_direction(aditional_direction);
                        bm.setService_fee(service_fee);
                        bm.setContainer_charges(container_charges);
                        bm.setDelivery_charges(delivery_charges);
                        bm.setSubtotal(subtotal);
                        bm.setTotal(total);
                        bm.setOrder_date(order_date);
                        bm.setOrder_status(order_status);
                        bm.setCurrency(currency);
bm.setLat(lat);
                        bm.setLat(lng);
                        bm.setOnsite_delivery(onsite_delivery);
                        bmo.add(bm);
                       // bm.
                        System.out.println("<<<oo>>>>" + bm);

                        // TextView Name,Ads,Item_Name,Qty,Delivry_date,Location;
                    Name.setText(bm.getCurrency()+"  "+bm.getSubtotal());
                     if(onsite_delivery.equalsIgnoreCase("Yes")){
                         Ads.setText(bm.getFname()+" "+bm.getLname()+"\n" +"House no:"+" "+bm.getHouse_no() + "," +"Building no:"+" "+ bm.getBuilding()+ "\n" +"Floor no:"+" "+ bm.getFloor()+ "," + bm.getStreet() + "," + bm.getCity());

                     }else {

                         Ads.setTextColor(Color.RED);
                         Ads.setText("No Delivery");
//Ads.setVisibility(View.GONE);
                     }

                        if(order_status.contentEquals("New"))
                        {
                            accepted.setText(R.string.Acceptede);

                        }else if(order_status.contentEquals("Order Received"))
                        {
                            accepted.setText(R.string.Accepted);
                        }


                        // Item_Name.setText(bm.getPro_name());
                      //  Qty.setText(bm.getPro_qty());
                        Delivry_date.setText(bm.getOrder_date());
                        //  Location.setText(bm.getMobile());

                        Phone.setText("mob:"+bm.getMobile());
                        Email.setText("email:"+bm.getEmail());
                        Instruction.setText(bm.getInstruction());
                        Order_no.setText("Order No: "+bm.getOrder_id());
                        //type.setText("-"+bm.getPro_type_name());
                        adapter1 = new Butcher_My_order_Details_adapter(bmo, getApplicationContext());
                        recyclerview.setAdapter(adapter1);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else{

                System.out.println("nulllllllllllllllllllllllllllllllll");
            }

        }
    }


    private class SendPostRequest3 extends AsyncTask<String, Void, String> {

        public void onPreExecute() {
            progress_loader.setVisibility(View.VISIBLE);
        }

        public String doInBackground(String... arg0) {

            try {

                URL url = new URL(SERVER+"json/butcher/accept.php?"); // here is your URL path

                JSONObject postDataParams = new JSONObject();

                //postDataParams.put("user_id", user_id );
                postDataParams.put("order_id",order_id);
           postDataParams.put("butcher_id",Buserid);
              /*  user_id
                        pro_type_name
                product_id*/
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        public void onPostExecute(String result) {
        /*    Toast.makeText(Butcher_MyOrder_New_Detail.this, result,
                    Toast.LENGTH_LONG).show();*/
            progress_loader.setVisibility(View.GONE);
//            "success"
            //  sl = new Salma_Retailer_Login_Model();
            if (result.equals("\"success\"")) {
                progress.setVisibility(View.GONE);
                new SweetAlertDialog(Butcher_MyOrder_New_Detail.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getResources().getString(R.string.AWESOME))
                        .setContentText(getResources().getString(R.string.order_accepted_successfully))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                Intent i=new Intent(getApplicationContext(),Butcher_my_order.class);


                                startActivity(i);
                                finish();
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .show();
              /*  final Dialog dialog = new Dialog(Butcher_MyOrder_New_Detail.this);

                //  dialog.setContentView(R.layout.loc_popup);
                final Window window = dialog.getWindow();
                window.requestFeature(Window.FEATURE_NO_TITLE);
                window.setContentView(R.layout.popup_order_accept_layout);
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                Button btnDismiss = (Button) dialog.findViewById(R.id.gb);
                btnDismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                    }
                });
                 *//*   Button rb = (Button) dialog.findViewById(R.id.rb);
                    rb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });*//*

                dialog.show();*/

             /*   MaterialStyledDialog dialog = new MaterialStyledDialog(Butcher_MyOrder_New_Detail.this)
                        .setTitle("Success!")
                        .setDescription("Accept Order")
                        .setIcon(R.drawable.ic_done_white_24dp)
                        .withIconAnimation(true)
                        .withDialogAnimation(true)
                        .setHeaderColor(R.color.colorPrimary)
                        .setCancelable(true)
                        .setPositive(getResources().getString(R.string.ok), new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {


                                dialog.dismiss();


                            }
                        })
                        .build();

                dialog.show();*/
                //  String named = jsonObj.getString("name");

            } else {

                System.out.println("result" + result);
                new SweetAlertDialog(Butcher_MyOrder_New_Detail.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getResources().getString(R.string.oops))
                        .setContentText(getResources().getString(R.string.something_went_wrong))
                        .show();
              /*  MaterialStyledDialog dialog = new MaterialStyledDialog(Butcher_MyOrder_New_Detail.this)
                        .setTitle("Failed!")
                        .setDescription("Accecpt Failed")
                        .setIcon(R.drawable.ic_error_white_24dp)
                        .withIconAnimation(true)
                        .withDialogAnimation(true)
                        .setHeaderColor(R.color.colorPrimary)
                        .setCancelable(true)
                        .setPositive(getResources().getString(R.string.ok), new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {


                            }
                        })
                        .build();

                dialog.show();
*/

            }
        }


        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while (itr.hasNext()) {

                String key = itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
    }

    private class DownloadData3 extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = null;

        StringBuilder stringBuilder;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_loader.setVisibility(ProgressBar.VISIBLE);


        }

        @Override
        protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User


            String strnew = bm.getCity()+bm.getStreet().replaceAll(" ", "%20");
            String uri = "http://maps.google.com/maps/api/geocode/json?address="+strnew+"&sensor=false";
            HttpGet httpGet = new HttpGet(uri);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // JSONObject jsonObject = new JSONObject();
            return null;
        }

        protected void onPostExecute(Void args) {

            progress_loader.setVisibility(View.GONE);
            System.out.println(">>>>>>>>>>>>>>>" + result1);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());

                double lngd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                double latd = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.d("latitude", "" + latd);
                Log.d("longitude", "" + lngd);
                lng = String.valueOf(lngd);
                lat = String.valueOf(latd);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlng>>>>>>>>>>>>>>>" + lng);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<libinlat>>>>>>>>>>>>>>>" + lat);
               // new Customer_Butcher_List_Fragment.DownloadData1().execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) < 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
        }

        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {

        Intent i=new Intent(getApplicationContext(),Butcher_my_order.class);


        startActivity(i);
        finish();
      /*  Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);

        return;*/
    }

}