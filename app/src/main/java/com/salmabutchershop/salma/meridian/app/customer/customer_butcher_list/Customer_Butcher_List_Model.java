package com.salmabutchershop.salma.meridian.app.customer.customer_butcher_list;

/**
 * Created by libin on 9/29/2016.
 */
public class Customer_Butcher_List_Model {


    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public String getDiscount_desc() {
        return discount_desc;
    }

    public void setDiscount_desc(String discount_desc) {
        this.discount_desc = discount_desc;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getButcher_logo() {
        return butcher_logo;
    }

    public void setButcher_logo(String butcher_logo) {
        this.butcher_logo = butcher_logo;
    }

    public String getShop_description() {
        return shop_description;
    }

    public void setShop_description(String shop_description) {
        this.shop_description = shop_description;
    }

    public String getShop_name() {
        return shop_name;
    }

    public String getShop_addrs() {
        return shop_addrs;
    }

    public void setShop_addrs(String shop_addrs) {
        this.shop_addrs = shop_addrs;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getDelivery_info() {
        return delivery_info;
    }

    public void setDelivery_info(String delivery_info) {
        this.delivery_info = delivery_info;
    }

    public String getButcher_discount() {
        return butcher_discount;
    }

    public void setButcher_discount(String butcher_discount) {
        this.butcher_discount = butcher_discount;
    }

    String id,added_by,cat_id,butcher_logo,shop_name,delivery_info,butcher_discount,shop_description,shop_addrs,discount_desc,distance;
}
