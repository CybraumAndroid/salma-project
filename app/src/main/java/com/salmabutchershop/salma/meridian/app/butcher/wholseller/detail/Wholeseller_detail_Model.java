package com.salmabutchershop.salma.meridian.app.butcher.wholseller.detail;

/**
 * Created by libin on 9/29/2016.
 */
public class Wholeseller_detail_Model {
    String cat_id,main_cat_id,category_name,category_desc,cat_image,added_by;

    public String getCat_id() {
        return cat_id;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getMain_cat_id() {
        return main_cat_id;
    }

    public void setMain_cat_id(String main_cat_id) {
        this.main_cat_id = main_cat_id;
    }

    public String getCategory_desc() {
        return category_desc;
    }

    public void setCategory_desc(String category_desc) {
        this.category_desc = category_desc;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCat_image() {
        return cat_image;
    }

    public void setCat_image(String cat_image) {
        this.cat_image = cat_image;
    }
}
